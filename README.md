# BZNews Admin

## 简介
暴走大事件Admin

## 环境

* express
* react
* react-router

## 开发

Install dependencies.

```shell
$ npm install
```

start webpack to build

```shell
$ npm run build
```

start webpack in develop environment

```shell
$ npm run dev
```

start webpack in product environment to build

```shell
$ npm run deploy
```

View in browser at `http://localhost:8080/`.

## 自动化

```shell
$ npm install --global gulp-cli

test
```
