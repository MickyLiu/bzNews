# 认证模块API

这个文档描述Auth认证模块。

## 引用

```javascript
import Auth from '../_utils/auth';
```

## 实例化

| 属性 | 类型 | 描述 |
| --- | --- | --- |
| page | string | 当前页别名，在['/', 'login', 'index']中选择。 |
| expect | bool | 当前页面是否需要认证，也就是认证的期望。 |
| target | string | 若认证与期望不符，则跳转到此页面。 |

```javascript
// 当前为login页面，此页面不需要认证通过，若已认证，则跳转到index
var auth = new Auth({
  page: 'login',
  target: 'index',
  expect: false,
});
```

## 函数成员

### verify(callback)

这个函数帮助确认当前是否通过认证。若认证与期望不符，则调用回调函数。

| 参数 | 类型 | 描述 |
| --- | --- | --- |
| callback | function | 当认证与期望相符时调用的回调函数 |

```javascript
auth.verify(function() {
  // render page
});
```

### login(email, password, onfail)

这个函数用于登录.

| 参数 | 类型 | 描述 |
| --- | --- | --- |
| email | string | 用户邮箱 |
| password | string | 密码 |
| onfail | function | 登录失败时的回调函数 |

```javascript
auth.login(email, pwd, function() {
  // login fail
});
```

### logout()

这个函数用于登出。

```javascript
auth.logout();
```

### token()

这个函数获得当前token。

```javascript
var token = auth.token;
```

### ajax(obj)

这个函数是jQuery.ajax的包装，用于需要认证的请求，使用方式和jQuery.ajax一模一样。

```javascript
auth.ajax('/api/v1/administrators/1')
.done(function(data) {
  console.log(data);
})
.fail(function() {
  console.log('failed');
});
```

## 如何在模块中使用Auth

在`start.jsx`中已经有了实例化的Auth，将Auth传入模块中，在模块中使用`this.props.Auth`获得实例。

`login/start.jsx`中的例子:
```javascript
ReactDOM.render(
  <LoginDialog Auth={auth}/>,
  document.getElementById('content')
);
```

在模块中可以使用：
```javascript
var auth = this.props.Auth;
auth.ajax(...)
```
