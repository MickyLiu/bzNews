# Index页设计文档

这个文档方便我们项目确定设计。

## 颜色

本节将包含主题色。  
**注意:**
* 主题色只能用于特定元素，避免将反相色用于大范围内容。
* 实际操作部分使用白（#fff）底及灰(#666)字。

### 主题色

* 浅色背景：
<table>
<tbody>
<tr>
  <td style="background:#09C; color:#fff">#09C</td>
  <td style="background:#008fbf; color:#fff">#008fbf</td>
<tr/>
</tbody>
</table>

* 深色背景
<table>
<tbody>
<tr>
  <td style="background:#37424f; color:#fff">#37424f</td>
  <td style="background:#293038; color:#fff">#293038</td>
  <td style="background:#22282e; color:#fff">#22282e</td>
  <td style="background:#aeb9c2; color:#000">#aeb9c2</td>
<tr/>
</tbody>
</table>

## 布局

### 全局

全局确定为One Page Application，整体布局及功能如下：
* 顶部bar将拥有主题logo，当前路径深度以及用户信息相关入口。
* 左侧菜单将有各个panel的跳转链接，为最常用的部分。
* 右侧为实际操作区域。

### 顶部bar

| 属性 | 值 |
| --- | --- |
| 高度 | 50px |
| 宽度 | 100% |
| fix | 是 |

### 左侧Sidebar

| 属性 | 值 |
| --- | --- |
| 高度 | 100vh - 50px; |
| 宽度(展开) | 180px |
| 宽度(收缩) | 50px |
| fix | 是 |

### 左侧Sidebar菜单项

| 属性 | 值 |
| --- | --- |
| 高度 | 40px; |
| 宽度(展开) | 180px |
| 宽度(收缩) | 50px |
| fix | 否 |