### 目录结构
+ bzNewsAdmin
    + docs:文档说明
    + node_modules:node模块
    + root:项目根目录
		+ _test_: 自动化测试
        + assets: 静态资源
        + build: 开发环境打包文件
        + dist: 生产环境打包文件
        + routes: 路由文件
            + routes.jsx 入口文件
        + src: 业务逻辑文件
            + index : 首页入口
            + login : 登陆入口
            + _utils : 工具类
            + components : 页面公用模块
            + lib : 三方库
            + adminManagePanel : 管理员页面
            + appManagePanel : app自定义管理页面
            + carouselPicsPanel : 轮播图管理页面
            + dataStatisticsPanel : 数据统计页面
            + groupManagePanel : 群组管理页面
            + userInfoPanel : 当前登陆用户模块
            + seriesManagePanel : 系列页面
            + videoManagePanel : 视频页面
                + store : 数据处理与存贮
        + templates: react注入模板
	+ webpack: webpack打包配置文件	            
					                            	
								                    
								                    
### 基础框架
    react: <http://reactjs.cn/react/docs/getting-started.html>
    react-router: <https://github.com/reactjs/react-router>
    ant-design: <http://ant.design/>
    webpack: <https://github.com/webpack/webpack>