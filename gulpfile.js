var gulp = require('gulp');
var spritesmith = require('gulp.spritesmith');

gulp.task('spriteImage', function() {
  var spriteData = gulp.src('root/assets/image/ad/*.png').pipe(spritesmith({
    imgName: 'sprite.png',
    cssName: 'sprite.css',
  }));
  return spriteData.pipe(gulp.dest('root/src/components/adArea/'));
});
