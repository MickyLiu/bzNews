'use strict';

jest.unmock('../src/_utils/eventSubscriber');

describe('测试eventSubscriber基础功能', () => {
  let textFactor = 1;
  const eventSubscriber = require('../src/_utils/eventSubscriber');

  it('通过subscribe方法可以绑定订阅事件', () => {
    eventSubscriber.subscribe("oneTopic",() => {
      textFactor+= 1;
    });
    var topics = eventSubscriber.getTopics();
    expect(topics["oneTopic"].queue).toBeDefined();
  });

  it('通过publish方法可以触发事件', () => {
    eventSubscriber.publish("oneTopic");
    expect(textFactor).toEqual(2);
  });

  // it('通过unsubscribe方法解绑订阅事件', () => {
  //   eventSubscriber.unsubscribe("oneTopic");
  //   var topics = eventSubscriber.getTopics();
  //   expect(topics["oneTopic"]).toBeUndefined();
  // });
});
