import { message } from 'antd';
let $ = require('jquery');
window.jQuery = window.$ = $;
require('./../../src/lib/jquery.form');
var errorMsg = function(er){
      message.error('出错啦！');
}

var successMsg = function(){
      message.success('成功！');
}

var  loadingMsg = function(){
      return message.loading('正在执行中...', 0);
}

var xhrHandler = function(p){
     var xhr = new window.XMLHttpRequest();
     if(p)
     {
         xhr.addEventListener("progress", function(ev){
             if (ev.lengthComputable) {
      　　　　　　 var percentComplete = Math.floor(ev.loaded / ev.total * 100);
                 p(percentComplete);
      　　　　}
         });
     }
     return xhr;
}

var ajaxHandler = function(op,suc,err,pro){
          var op = !op.type?(op.type="GET",op):op;
          var inputOption = getInputOption(op,suc,err,pro);
          if(op.form) inputOption.data = op.form;
          return $.ajax(inputOption);
}

var getInputOption = function(op,suc,err,pro){
        var over = op.enabledLoadingMsg?loadingMsg():'';
        var headers = op.headers?op.headers:{};
        if(!headers['Authorization']) headers["Authorization"]= 'Token ' + sessionStorage.getItem('user');
        
        var inputOption = {
                url: op.url,
                type: op.type,
                cache:false,
                headers: headers,
                error(e){
                    op.enabledErrorMsg?errorMsg(e):'';
                    err? err(e):'';
                },
                success(data){
                    op.enabledSuccessMsg?successMsg():'';
                    suc?suc(data):'';
                },
                complete(){
                      op.enabledLoadingMsg?over():'';
                },
                xhr(){
                      return xhrHandler(pro);
                }
          };
          return inputOption;
}

var ajaxEx = {
    /** ajax extension
        param @op url,enabledErrorMsg..->enable show global error msg ect
        param @suc,err,pro ->callback function ect
    **/
    get: function(op,suc,err,pro){
          var option = !op.url? {url:op}:op;
          option.type = "GET";
          return ajaxHandler(option,suc,err,pro);
    },
    put: function(op,suc,err,pro){
          var option = !op.url? {url:op}:op;
          option.type = "PUT";
          return ajaxHandler(option,suc,err,pro);
    },
    delete: function(op,suc,err,pro){
          var option = !op.url? {url:op}:op;
          option.type = "DELETE";
          return ajaxHandler(option,suc,err,pro);
    },
    post: function(op,suc,err,pro){
          var option = !op.url? {url:op}:op;
          option.type = "POST";
          return ajaxHandler(option,suc,err,pro);
    },
    submit: function(op,suc,err,pro){
          var option = !op.url? {url:op}:op;
          option.type = $(option.formEle).attr('method');
          $(op.formEle).ajaxSubmit(getInputOption(option,suc,err,pro));
    }
}

module.exports = ajaxEx;
