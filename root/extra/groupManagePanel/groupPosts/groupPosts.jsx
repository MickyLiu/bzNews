import { Table, Icon, Spin, Popconfirm } from 'antd';
import {Link} from 'react-router';

let React = require('react');

let events = require('./../../../src/_utils/eventSubscriber');
let ajaxEx = require('./../../_utils/ajaxConfiger');
let Const = require('./../../../src/_utils/const');

let postManageStore = require('./../../groupManagePanel/store/postManageStore');

const columns = [{
    title: '帖子ID',
    dataIndex: 'id',
    key: 'id'
  },{
    title: '用户',
    dataIndex: 'user_id',
    key: 'user_id',
  },{
    title: '帖子',
    dataIndex: 'title',
    key: 'title',
  }, {
    title: '点赞数',
    dataIndex: 'likings_count',
    key: 'likings_count',
  }, {
    title: '回复数',
    dataIndex: 'replies_count',
    key: 'replies_count',
  },{
    title: '发布时间',
    dataIndex: 'created_at',
    key: 'created_at',
  },{
    title: '操作',
    key: 'operation',
    render(text, record) {
      var groupId = postManageStore.getRelations().group.data.id;
      var creator = record.user_id,current_user = sessionStorage.getItem('user_id');
      var editLink = "groups/" + groupId +"/posts/"+ record.id + "/edit";
      var showLink = "groups/" + groupId + "/" + record.id;
      return (
        <span>
          <Link to={showLink}>查看</Link>
          <span className="ant-divider"></span>
          <Link to={current_user==creator?editLink:'#/'} disabled={current_user==creator?'':'disabled'}>编辑</Link>
          <span className="ant-divider"></span>
          <Popconfirm title="确定要删除这个帖子么？" postId={record.id} onConfirm={delPostConfirm}>
            <a href="#" disabled={current_user==creator?'':'disabled'}>删除</a>
          </Popconfirm>
        </span>
      );
    }
  }];

var delPostConfirm =  function(){
      //VideoManageStore.delItemByProperty('id',this.props.videoId);
      var _this =this;
      var groupId = postManageStore.getRelations().group.data.id;
      ajaxEx.delete({url:Const.API.posts + '/' + this.props.postId,enabledLoadingMsg:true,enabledErrorMsg:true},function(d){
                   events.publish('groupPostsDataChange');
      });
}

const TableView = React.createClass({
  getInitialState() {
     return{
        dataSource :[],
        pageSize:10,
        total:1,
        loading: true,
        currentPage:1
    };
  },
  componentDidMount: function () {
       events.subscribe('groupPostsDataChange',(function(filter){
             this.getDataSource(this,filter);
       }).bind(this));

       this.setState({loading:true});
       this.getDataSource(this);
  },
  getDataSource(_this,filter){
      var page = 1;
      if(filter)
      {
         page = filter.page?filter.page:1;
      }
      ajaxEx.get({url:Const.API.groups + '/' +  _this.props.groupId +"/posts?page="+ page +"&per="+_this.state.pageSize},function(d){
           postManageStore.clearData().pushData(postManageStore.mapping(d.data));
           _this.setState({
              dataSource : postManageStore.getDataSource(),
              total: d.meta.total_pages*_this.state.pageSize,
              currentPage:page,
              loading:false
           });
       }).complete(function(){
             _this.setState({loading:false});
       });
  },
  pageChange:function(current){
        var _this = this;

        _this.setState({
            loading: true
        });
       this.getDataSource(this,{"page":current});
  },
  componentWillUnmount: function(){
       postManageStore.clearData();
       events.unsubscribe('groupPostsDataChange');
  },
  render() {
    return (
        <Table columns={columns} dataSource={this.state.dataSource} loading={this.state.loading} pagination={{ onChange:this.pageChange, current:this.state.currentPage, pageSize: this.state.pageSize, total: this.state.total, showQuickJumper: true }} />
    );
  }
});

module.exports = TableView;
