import {Link} from 'react-router';
import { Table, Icon, Spin, Popconfirm } from 'antd';

let React = require('react');
let events = require('./../../../src/_utils/eventSubscriber');
let ajaxEx = require('./../../_utils/ajaxConfiger');
let Const = require('./../../../src/_utils/const');
let groupManageStore = require('../store/groupManageStore');
let PanelTopBar = require('../panelTopBar/panelTopBar');

const columns = [{
    title: 'ID',
    dataIndex: 'id',
    key: 'id'
  },{
    title: '群名',
    dataIndex: 'name',
    key: 'name',
  },{
    title: '群类型',
    dataIndex: 'kind',
    key: 'kind',
    render(text) {
       return (<span>{text=="star"?'明星':'非明星'}</span>);
    }
  },{
    title: '群头像',
    dataIndex: 'image',
    key: 'image',
    render(text) {
      return (<img className="thumbnail" src={text} />);
    }
  },{
    title: '成员数',
    dataIndex: 'users_count',
    key: 'users_count',
  },{
    title: '帖子数',
    dataIndex: 'posts_count',
    key: 'posts_count',
  },{
    title: '操作',
    key: 'operation',
    render(text, record) {
      var showLink = "groups/" + record.id;
      return (
        <span>
          <Link to={showLink}>编辑</Link>        
        </span>
      );
    }
  }];

const TableView = React.createClass({
  getInitialState() {
     return{
        dataSource :[],
        pageSize:8,
        total:1,
        loading: true,
        currentPage:1
    };
  },
  componentDidMount: function () {
       events.subscribe('groupDataChange',(function(filter){
            this.getDataSource(this,filter);
       }).bind(this));

       this.setState({loading:true});
       this.getDataSource(this);
  },
  getDataSource(_this,filter){
      var page = 1;
      if(filter)
      {
         page = filter.page?filter.page:1;
      }
      ajaxEx.get({url:Const.API.projectGroups+"?page="+ page +"&per="+this.state.pageSize},function(d){
           groupManageStore.clearData().pushData(groupManageStore.mapping(d.data));
           _this.setState({
              dataSource : groupManageStore.getDataSource(),
              total: d.meta.total_pages*_this.state.pageSize,
              currentPage:page,
              loading:false
           });
       }).complete(function(){
           _this.setState({loading:false});
       });
  },
  pageChange:function(current){
        this.setState({
            loading: true
        });
        this.getDataSource(this,{"page":current});
  },
  componentWillUnmount: function(){
       events.unsubscribe('groupDataChange');
  },
  render() {
    return (
      <div>
        <PanelTopBar panelTitle= "群组列表"/>
        <Table columns={columns} dataSource={this.state.dataSource} loading={this.state.loading} pagination={{ onChange:this.pageChange, current:this.state.currentPage, pageSize: this.state.pageSize, total: this.state.total, showQuickJumper: true }} />
      </div>
    );
  }
});

module.exports = TableView;
