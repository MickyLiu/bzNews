import { Button } from 'antd';
import {Link} from 'react-router';

let React = require('react');
require('./panelTopBar.scss');

const PanelTopBar = React.createClass({
  render() {
    return (
      <header className="panel-top-bar group-top">
        <h1 className="panel-title">{this.props.panelTitle}</h1>
      </header>
    );
  }
});

module.exports = PanelTopBar;
