import {Link} from 'react-router';
import { Icon,Spin,Form,Input,Button,DatePicker,Upload,notification,message } from 'antd';

let React = require('react');
let events = require('./../../../src/_utils/eventSubscriber');
let ajaxEx = require('./../../_utils/ajaxConfiger');
let Const = require('./../../../src/_utils/const');
let groupManageStore = require('../store/groupManageStore');
let $ = require('jquery') ;
require('./../postEdit/postEdit.scss');

const FormItem = Form.Item;
let postCreateView = React.createClass({
  getInitialState() {
        return {
          data: {},
          fileList: [],
          pics: [],
          uploadToken: '',
          requesting:false,
          content:''
        };
  },
  componentDidMount: function () {
      var _this = this;
      this.fetchGroup();
      ajaxEx.get({url:Const.API.urlPrefix +'upload_token',form:{}},function(d){
                      _this.setState({"uploadToken":d.upload_token});
      });
  },
  fetchGroup(gid){
       var _this = this;
       var groupId = gid? gid:this.props.params.id;
       if(!groupManageStore.getDataSource().length)
       {
             ajaxEx.get({url:Const.API.groups+'/'+ groupId},function(d){
                   var  mappingData= groupManageStore.mapping([d.data]);
                  _this.setState({title:mappingData[0].name,data:mappingData[0]});
             });
       }
       else
       {
           var itemData = groupManageStore.getItemByProperty('id',groupId);
           _this.setState({title:itemData[0].name,data:itemData[0]});
       }
  },
  componentWillUnmount: function(){
     //events.unsubscribe('progressChange');
  },
  contentChange(e) {
    this.setState({content: e.target.value.trim()});
  },
  handleSubmit(e) {
       e.preventDefault();
       var _this= this;
       this.props.form.validateFields((errors, values) => {
                if (!!errors) {
                  console.log('Errors in form!!!');
                  e.preventDefault();
                  return;
                }
             var postData = {
              "post": {
                "title": values.title,
                "content": _this.state.content,
                "pictures": _this.state.pics.toString().replace(/,/ig,'，')
              },
              "group_id": _this.props.params.id
            };

           ajaxEx.post({url:Const.API.groups +'/'+ _this.props.params.id + '/posts',form:postData,enabledLoadingMsg:true,enabledErrorMsg:true},function(d){
                   $('#postForm').resetForm();
                   window.history.back();
           }).complete(function(){
               _this.setState({requesting:false});
           });
           return false;
      });
  },
  render() {
    var _this = this;
    const { getFieldProps,getFieldError } = this.props.form;

    const titleProps = getFieldProps('title', {
        validate: [{
          rules: [
            { required: true, message: '标题不能为空'}
          ],
          trigger: 'onBlur',
        }],
        initialValue: _this.state.data.title
    });

    const props = {
        action:'https://up.qbox.me',
        data:{
         'token':this.state.uploadToken
        },
        listType: 'picture-card',
        accept: 'image/jpeg,image/png,image/jpg,image/bmp',
        onChange(info) {
              var files = _this.state.fileList,
                pics = _this.state.pics;
              if (info.file.status == 'uploading') {

              } else if (info.file.status === 'error') {
                  notification['error']({
                    message: '错误',
                    description: info.file.error
                  });
              } else if (info.file.status === 'removed') {
                 //
                  pics.withOut(function(i,o){return o==info.file.url});

                 _this.setState({
                      pics: pics
                  });
              } else if (info.file.status === 'done') {
                  files.push({
                      uid: info.file.uid,
                      name: info.file.name,
                      status: 'done',
                      url: 'http://'+ Const.API.qiniuUpload+ '/' + info.file.response.hash,
                      thumbUrl: 'http://' + Const.API.qiniuUpload + '/' + info.file.response.hash
                  });
                  pics.push('http://' + Const.API.qiniuUpload + '/' + info.file.response.hash);
                  _this.setState({
                      fileList: files,
                      pics: pics
                  });
              } else if (info.file.status === 'error') {
                _this.setState({
                    fileList: files,
                    pics: pics
                });
              } else if (!info.file.status) {
                //_this.setState({ loading: true });
              }
        },
        beforeUpload(file) {
            var flag = _this.state.fileList.length<9;
            if (!flag) {
                message.error('图片最多添加9张！');
            }
            return flag;
        }
    };

    return (
    <div>
       <div className="ant-breadcrumb custom">
          <span>
            <span className="ant-breadcrumb-link"><Link to='groups'>群组列表</Link></span>
            <span className="ant-breadcrumb-separator">/</span>
          </span>
          <span>
            <span className="ant-breadcrumb-link"><Link to={'groups/'+this.state.data.id}>{this.state.title}</Link></span>
            <span className="ant-breadcrumb-separator">/</span>
          </span>
          <span>
            <span className="ant-breadcrumb-link">新建帖子</span>
          </span>
       </div>
       <div className="post-con">
          <Form horizontal id={'postForm'} form={this.props.form} method="post">
              <FormItem
                label="标题：" labelCol={{ span: 3 }} wrapperCol={{ span: 14 }}>
                <Input placeholder="" {...titleProps} name={'post[title]'} />
              </FormItem>
              <FormItem
                label="正文：" labelCol={{ span: 3 }} wrapperCol={{ span: 14 }}>
                 <Input type="textarea" id="control-textarea"  {...getFieldProps('content')} name={'post[content]'} value={this.state.content} onChange={this.contentChange} rows="3" />
              </FormItem>
              <FormItem
                    label="图片（最多添加9张）：" labelCol={{ span: 3 }} wrapperCol={{ span: 14 }}>
                      <Upload ref="uploader" {...props} fileList={this.state.fileList}>
                          <Icon type="plus" />
                          <div className="ant-upload-text">上传照片</div>
                      </Upload>
                  </FormItem>
                   <FormItem wrapperCol={{ offset: 3 }}>
                        <Button type="primary" onClick={this.handleSubmit.bind(this)} loading={this.state.requesting}>保存</Button>
                  </FormItem>
          </Form>

       </div>
     </div>
    );
  }
});

postCreateView = Form.create()(postCreateView);

module.exports = postCreateView;
