import {Link} from 'react-router';
import { Table, Icon, Spin,Form,Input,InputNumber,Button,Upload,Popconfirm } from 'antd';
let _ = require('lodash');
let React = require('react');
let events = require('./../../../src/_utils/eventSubscriber');
let ajaxEx = require('./../../_utils/ajaxConfiger');
let postManageStore = require('../store/postManageStore');
let Const = require('./../../../src/_utils/const');
require('./postDetail.scss');


const FormItem = Form.Item;

let CommentItem = React.createClass({
  getInitialState() {
      return {
         loading:false,
         pageSize:8,
         total:0,
         currentPage:1,
         comments:[]
      };
  },
  componentDidMount: function () {
      if(this.props.level==0)
      {       
         this.getCommentsData(1);
      }
  },
  getCommentsData(page){
       var _this=this,per=this.state.pageSize;
       _this.setState({loading:true});
       var commentsData = this.state.comments || [];
       ajaxEx.get({url:Const.API.comments +'/'+ this.props.data.id +'/children?comment_id='+ this.props.data.id +'&page='+page + '&per=' + per},function(d){
                _this.setState({comments:commentsData.concat(d.data),totalComments:d.meta.total_counts,currentPage:!d.data.length?(page-1):page});
       }).complete(function(){
           _this.setState({loading:false});
       });
  },
  loadMore(){
     var page = ++this.state.currentPage;
     this.getCommentsData(page);
  },
  componentWillUnmount: function(){
  },
  deleteItem(){
       var _this =this;
       var commentId= this.props.data.id;
       ajaxEx.delete({url:Const.API.comments+'/'+ commentId,enabledLoadingMsg:true,enabledErrorMsg:true},function(d){
               events.publish('commentsChange');
       });
  },
  render() {
    var _this = this;
    var replyStr = ()=>{
       if(this.props.data.attributes.parent_user_name)
       {
         return <span><span className="reply-str">回复:</span><a>{this.props.data.attributes.parent_user_name}</a></span>      
       }
       else
       {
          return null;
       }
    }
    return (
          <div className="comment_con">
            {this.props.level<=1?   
                <div>
                   <div className="user_avatar">
                      <p><img src={this.props.data.attributes.user_avatar_url} /></p>
                  </div>
                  <div className="comment_content">
                     <p><a>{this.props.data.attributes.user_name}</a>{replyStr()}</p>
                     <p>{new Date(this.props.data.attributes.created_at).toLocaleString()}</p>
                     <p>{this.props.data.attributes.content}</p>
                     <p>
                       <Icon type="like" /><span className="vote-count">{this.props.data.attributes.vote_count}</span>
                       <Popconfirm title="确定要删除这条评论么？" onClick={(e)=>{ e.stopPropagation()}} onConfirm={this.deleteItem}>
                          <a href="javascript:void()">删除</a>
                       </Popconfirm>
                      </p>
                  </div>
                   <ul> 
                       <Spin spining={this.state.loading}>
                           {this.state.comments.length>0?
                              <li>
                                   <h3><span>共{this.state.totalComments}条</span></h3>
                                   {_.map(this.state.comments,(o,index)=>
                                        <CommentItem  data={o} level={1}/>
                                   )}
                              </li>
                           :null}
                          {this.props.level==0?<a href="javascript:void(0)" onClick={this.loadMore}>更多</a>:null}
                       </Spin>
                   </ul>  
                </div> :null}
          </div>
    );
  }
});

const postDetailView = React.createClass({
  getInitialState() {
     if(postManageStore.getDataSource().length)
     {
        var itemData = postManageStore.getItemByProperty('id',this.props.routeParams.pid);
        return {
             data:itemData[0],
             loading:true,
             pageSize:8,
             total:0,
             currentPage:1,
             comments:[],
             user:{}
        };
     }
     else
     {
        return  {
           data:{"pictures":""},
           title:'',
           loading:true,
           pageSize:8,
           total:0,
           currentPage:1,
           comments:[],
           user:{}
         }
        
     }
  },
  fetchPost(pid){
       var _this=this;
       var postId = pid? pid:this.props.routeParams.pid,per=this.state.pageSize,page=this.state.currentPage;

       if(!postManageStore.getDataSource().length)
       {
             ajaxEx.get({url:Const.API.posts + '/' + postId},function(d){
                    var  mappingData= postManageStore.mapping([d.data]);
                    _this.setState({data:mappingData[0],title:mappingData[0].name});

                    ajaxEx.get({url:Const.API.users + '/' + mappingData[0].user_id},function(d){
                        _this.setState({user:{name:d.data.attributes.name,avatar:d.data.attributes.avatar}});
                    });
             });
       }
       else
       {
           var itemData = postManageStore.getItemByProperty('id',postId);
           _this.setState({data:itemData[0],title:itemData[0].name});
            ajaxEx.get({url:Const.API.users + '/' + itemData[0].user_id},function(d){
                _this.setState({user:{name:d.data.attributes.name,avatar:d.data.attributes.avatar}});
            });
       }
       this.getCommentsData(1);
  },
  getCommentsData(page){
       var _this=this,postId = this.props.routeParams.pid,per=this.state.pageSize;
       _this.setState({loading:true});
       var commentsData = this.state.comments || [];
       ajaxEx.get({url:Const.API.posts +'/'+ postId + '/comments' +'?page='+page + '&per=' + per},function(d){
                    _this.setState({comments:commentsData.concat(d.data),totalComments:d.meta.total_counts,currentPage:!d.data.length?(page-1):page});
       }).complete(function(){
           _this.setState({loading:false});
       });
  },
  loadMore(){
     var page = ++this.state.currentPage;
     this.getCommentsData(page);
  },
  componentDidMount() {
        this.fetchPost();
  },
  componentWillReceiveProps(nextProps){
        if(this.props.params.pid==nextProps.params.pid) return;
        this.fetchPost(nextProps.params.pid);
  },
  componentWillUnmount: function(){
     //events.unsubscribe('progressChange');
  },
  render() {
    return (
    <div>
     <div className="ant-breadcrumb custom">
          <span>
            <span className="ant-breadcrumb-link"><Link to='groups'>群组列表</Link></span>
            <span className="ant-breadcrumb-separator">/</span>
          </span>
         <span>
            <span className="ant-breadcrumb-link"><Link to={'groups/'+this.props.params.id}>返回群组</Link></span>
            <span className="ant-breadcrumb-separator">/</span>
          </span>
          <span>
            <span className="ant-breadcrumb-link">帖子详情</span>
          </span>
       </div>
       <div className="post-detail-con">
               <div>
                  <div className="post-user">
                        <span className="user_info">
                          <img src={this.state.user.avatar} />
                          <span>{this.state.user.name}</span>
                          <span>{this.state.data.created_at}</span>
                        </span>
                        
                   </div>
                   <div className="post-info">
                         <span><Icon type="like" /><span>{this.state.data.likings_count}</span></span>
                         <span><Icon type="message" /><span>{this.state.data.replies_count}</span></span>
                   </div>
                   <p className="post-title">{this.state.data.title}</p>
                   <p className="post-content">{this.state.data.content}</p>
                    {_.map(this.state.data.pictures,(o,index)=>
                       <p className="post-img"><img src={o} /></p>
                    )}
               </div>
               <ul> 
                   <Spin spining={this.state.loading}>
                       {this.state.comments.length>0?
                          <li>
                               <h3><span>共{this.state.totalComments}条</span></h3>
                               {_.map(this.state.comments,(o,index)=>
                                    <CommentItem  data={o} level={0}/>
                               )}
                          </li>
                       :null}
                      {this.state.currentPage*this.state.pageSize<this.state.totalComments?<a href="javascript:void(0)" onClick={this.loadMore}>更多</a>:null}
                   </Spin>
               </ul>  
       </div>
     </div>
    );
  }
});

module.exports = postDetailView;
