import {Link} from 'react-router';
import { Table, Icon, Spin,Form,Input,InputNumber,Button,DatePicker,Upload,message } from 'antd';

let React = require('react');
let events = require('./../../../src/_utils/eventSubscriber');
let ajaxEx = require('./../../_utils/ajaxConfiger');
let Const = require('./../../../src/_utils/const');
let $ = require('jquery') ;
let postManageStore = require('../store/postManageStore');
require('./postEdit.scss');


const FormItem = Form.Item;
let postEditView = React.createClass({
  getInitialState() {
      if(postManageStore.getDataSource().length)
      {
          var itemData = postManageStore.getItemByProperty('id',this.props.routeParams.pid);
          return {
              data:itemData[0],
              title:itemData[0].name,
              fileList:this.mappingPicList(itemData[0].pictures),
              requesting:false,
              pics:itemData[0].pictures,
              uploadToken:''
          };
      }
      else
      {
          return { data:{},fileList:this.mappingPicList([]),requesting:false,pics:[],uploadToken:''};
      }
  },
  mappingPicList(data){
    var result=[];
    if(data.length)
    {
       data.forEach(function(o,i){
             result.push({
                    uid: i,
                    status: 'done',
                    url: o,
                    thumbUrl: o
              });
       });
     }
     return result;
  },
  fetchPost(pid){
        var _this=this;
        var postId = pid? pid:this.props.routeParams.pid;
        if (!postManageStore.getDataSource().length) {
          ajaxEx.get({
              url: Const.API.posts + '/' + postId
          }, function(d) {
              var mappingData = postManageStore.mapping([d.data]);
              _this.setState({
                data: mappingData[0],
                title: mappingData[0].name,
                fileList: _this.mappingPicList(mappingData[0].pictures),
                pics: mappingData[0].pictures
              });
          });
        } else {
          var itemData = postManageStore.getItemByProperty('id', postId);
          _this.setState({
              data: itemData[0],
              title: itemData[0].name,
              fileList: _this.mappingPicList(itemData[0].pictures),
              pics: itemData[0].pictures
          });
        }
  },
  componentDidMount: function () {
       var _this =this;
       this.fetchPost();
       ajaxEx.get({url:Const.API.urlPrefix +'upload_token',form:{}},function(d){
                  _this.setState({"uploadToken":d.upload_token});
       });
  },
  componentWillReceiveProps(nextProps){
        if(this.props.params.pid==nextProps.params.pid) return;
        this.fetchPost(nextProps.params.pid);
  },
  componentWillUnmount: function(){
     //events.unsubscribe('progressChange');
  },
  handleSubmit(e) {
       e.preventDefault();
       var _this= this;
       this.props.form.validateFields((errors, values) => {
                if (!!errors) {
                  console.log('Errors in form!!!');
                  e.preventDefault();
                  return;
                }

           var postData={
                "post": {
                  "title": values.title,
                  "content": _this.state.data.content,
                  "pictures": _this.state.pics.toString().replace(/,/ig,'，')
                },
                "group_id" : _this.props.params.id
           }
          _this.setState({requesting:true});
          ajaxEx.put({url:Const.API.posts + '/'+ _this.props.params.pid,form:postData,enabledLoadingMsg:true,enabledErrorMsg:true},function(d){
                  //_this.setState({requesting:false});
                  $('#postForm').resetForm();
                  window.history.back();
          }).complete(function(){
               _this.setState({requesting:false});
          });
          return false;
      });
  },
  handleInputChange(e) {
      var data = this.state.data;
      data[e.target.attributes['id'].value]= e.target.value;
      this.setState({
          data: data
      });
  },
  handleFileChange(info){
      var _this = this;
      var files = _this.state.fileList,
        pics = _this.state.pics;
      if (info.file.status == 'uploading') {

      } else if (info.file.status === 'error') {

      } else if (info.file.status === 'removed') {
        //
         pics.withOut(function(i,o){return o==info.file.url});
         /*this.delQinNiuResource(info.file.url.substring(info.file.url.lastIndexOf('/')+1,info.file.url.length)).call(this);*/
         _this.setState({
              pics: pics
          });
      } else if (info.file.status === 'done') {
        files.push({
            uid: info.file.uid,
            name: info.file.name,
            status: 'done',
            url: 'http://' + Const.API.qiniuUpload + '/' + info.file.response.hash,
            thumbUrl: 'http://' + Const.API.qiniuUpload + '/' + info.file.response.hash
        });
        pics.push('http://' + Const.API.qiniuUpload + '/' + info.file.response.hash);
          _this.setState({
            fileList: files,
            pics: pics
          });
      } else if (info.file.status === 'error') {
          _this.setState({
            fileList: files,
            pics: pics
          });
      } else if (!info.file.status) {

      }
  },
  delQinNiuResource(key){
       ajaxEx.post({url:'http://rs.qiniu.com/delete/'+key,headers:{'Authorization':"QBox "+this.state.uploadToken}},function(d){
               console.log(d);
       });
  },
  render() {
    var _this = this;
    const { getFieldProps,getFieldError } = this.props.form;

    const titleProps = getFieldProps('title', {
        validate: [{
          rules: [
            { required: true, message: '标题不能为空'}
          ],
          trigger: 'onBlur',
        }],
        initialValue: _this.state.data.title
    });

    const props = {
        action:'https://up.qbox.me',
        data:{
         'token': this.state.uploadToken
        },
        listType: 'picture-card',
        accept: 'image/jpeg,image/png,image/jpg,image/bmp',
        onChange: _this.handleFileChange,
        beforeUpload(file) {
            var flag = _this.state.fileList.length<9;
            if (!flag) {
                message.error('图片最多添加9张！');
            }
            return flag;
        }
    };

    return (
    <div>
       <div className="ant-breadcrumb custom">
          <span>
            <span className="ant-breadcrumb-link"><Link to='groups'>群组列表</Link></span>
            <span className="ant-breadcrumb-separator">/</span>
          </span>
         <span>
            <span className="ant-breadcrumb-link"><Link to={'groups/'+this.props.params.id}>返回群组</Link></span>
            <span className="ant-breadcrumb-separator">/</span>
          </span>
          <span>
            <span className="ant-breadcrumb-link">编辑帖子</span>
          </span>
       </div>
       <div className="post-con">
          <Form horizontal id={'postForm'} form={this.props.form} method="post">
              <FormItem
                label="标题：" labelCol={{ span: 3 }} wrapperCol={{ span: 14 }}>
                <Input placeholder="" {...titleProps} name={'post[title]'} value={this.state.data.title} onChange={this.handleInputChange}/>
              </FormItem>
              <FormItem
                label="正文：" labelCol={{ span: 3 }} wrapperCol={{ span: 14 }}>
                 <Input type="textarea" id="control-textarea"  {...getFieldProps('content')} name={'post[content]'} value={this.state.data.content} onChange={this.handleInputChange} rows="3" />
              </FormItem>
              <FormItem
                    label="图片（最多添加9张）：" labelCol={{ span: 3 }} wrapperCol={{ span: 14 }}>
                      <Upload ref="uploader" {...props} fileList={this.state.fileList}>
                          <Icon type="plus" />
                          <div className="ant-upload-text">上传照片</div>
                      </Upload>
              </FormItem>
              <FormItem wrapperCol={{ offset: 3 }}>
                <Button type="primary" onClick={this.handleSubmit.bind(this)} loading={this.state.requesting}>保存</Button>
              </FormItem>
          </Form>

       </div>
     </div>
    );
  }
});


postEditView = Form.create()(postEditView);

module.exports = postEditView;
