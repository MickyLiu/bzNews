let $ = require('jquery');

var GroupManageStore = {

   clearData:function(){
      this.dataSource=[];
      return this;
   },
	 pushData:function(data) {
      if(!this.dataSource) this.dataSource=[];
	 	  this.dataSource = this.dataSource.concat(data);
      return this;
	 },
   mapping:function(data){
        var result=[];
        data.forEach(function(o,i){
               $.isNumeric(o.id)?o.attributes.id = o.id.toString():'';
               $.isNumeric(o.id)?o.attributes.key =  o.id.toString():'';
               $.isNumeric(o.attributes.users_count)?o.attributes.users_count =  o.attributes.users_count.toString():'';
               $.isNumeric(o.attributes.posts_count)?o.attributes.posts_count =  o.attributes.posts_count.toString():'';
               result.push(o.attributes);
        });
        return result;
   },
	 getDataSource:function() {
	 	  return !this.dataSource?[]:this.dataSource;
	 },
   getItemByProperty:function(key,value){
        return this.dataSource.filter(function(item){
               return item[key]==value;
        });
   },
   getMappingKeyData:function(){
         return {
         	   "id":"群组ID",
         	   "name":"群名",
         	   "kind":"群类型",
             "image":"群头像",
             "users_count":"成员数",
             "posts_count":"帖子数"
         }
   },
   getItemOnMappingKey:function(item){
   	  var result = {};
   	  var mapping = this.getMappingKeyData();
   	  for(var key in mapping)
   	  {
   	  	  result[mapping[key]] = item[key];
   	  }
        return result;
   }
};

module.exports = GroupManageStore;
