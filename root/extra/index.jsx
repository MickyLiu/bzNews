import {Link} from 'react-router';
import { Menu, Icon, Switch, Table, Row, Col, Spin, Dropdown } from 'antd';

let React = require('react');
let events = require('./../src/_utils/eventSubscriber');

require('./../src/index/index.scss');
require('./../src/index/index_custom.scss');
require('./../src/index/components_common.scss');

let ajaxEx = require('./_utils/ajaxConfiger');
let Const = require('./../src/_utils/const');

const SubMenu = Menu.SubMenu;

var Index = React.createClass({
    getInitialState() {
        return {
          theme: 'dark',
          show: false,
          full: true,
      };
    },
    componentDidMount: function () {
         
    },
    traversalChildren: function(callback) {
        for (var key in this.refs) {
            if (key.startsWith('opt') && this.refs.hasOwnProperty(key)) {
                var ref = this.refs[key];
                callback(ref);
            }
        }
    },
    componentWillUnmount: function(){
          sessionStorage.clear();
    },
    render: function () {
        return (
            <div className="container">
               <div className="opt-area">
                <div>
                    {React.Children.map(this.props.children, (element, idx) => {
                        return React.cloneElement(element, { ref: 'opt' + idx, Auth: window.auth });
                    })}
                </div>
               </div>
            </div>
          )
      }
});

module.exports = Index;
