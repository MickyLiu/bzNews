module.exports = {
  path: ':id/posts/create',
  getComponent(location, cb) {
    require.ensure([], (require) => {
      cb(null, require('./../../groupManagePanel/postCreate/postCreate'))
    })
  }
}
