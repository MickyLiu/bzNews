module.exports = {
  path: ':id/posts/:pid/edit',
  getComponent(location, cb) {
    require.ensure([], (require) => {
      cb(null, require('./../../groupManagePanel/postEdit/postEdit'))
    })
  }
}
