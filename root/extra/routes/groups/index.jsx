module.exports = {
  path: 'groups',

  getChildRoutes(location, cb) {
    require.ensure([], (require) => {
      cb(null, [
        require('./detail'),
        require('./groupPostDetail'),
        require('./groupPostCreate'),
        require('./groupPostEdit')
      ])
    })
  },

  getIndexRoute(location, callback) {
    require.ensure([], function (require) {
      callback(null, {
        component: require('./../../groupManagePanel/groupTable/groupTable'),
      })
    })
  },

  getComponent(location, cb) {
    require.ensure([], (require) => {
      cb(null, require('./../../groupManagePanel/groupManagePanel'))
    })
  }
}
