
const routes = {
  path: '/',
  getChildRoutes(location, callback) {
    require.ensure([], function (require) {
      callback(null, [
        require('./groups')
      ])
    })
  },

  getIndexRoute(location, callback) {
    require.ensure([], function (require) {
      callback(null, {
        component: require('./../groupManagePanel/groupTable/groupTable')
      })
    })
  },

  getComponents(location, callback) {
    require.ensure([], function (require) {
      callback(null, require('./../index'))
    })
  }
}

module.exports = routes;
