import { Router, hashHistory } from 'react-router';
import {render} from 'react-dom';

require('./../src/index/index.scss');
let React = require('react');
let routes = require('./routes/routes');

// require('./../src/index/index.scss');
let ajaxEx = require('./_utils/ajaxConfiger');
let Const = require('./../src/_utils/const');


if(!sessionStorage.getItem('user') || sessionStorage.getItem('user')=="undefined")
{
	 sessionStorage.setItem('user',window.location.search.split('&')[0].split('=')[1]);
	 var uid = window.location.search.split('&')[1].split('=')[1];
	 if(uid)
	 {
	     ajaxEx.get({url:Const.API.users + '/' + uid},function(d){
	         sessionStorage.setItem('user_id',uid);
	         sessionStorage.setItem('user_name',d.data.attributes.name);
	         sessionStorage.setItem('user_image',d.data.attributes.avatar);
	         
	         window.location.href="/extra#/groups";
	     },function(){
	     	  window.location.href="/userlogin";
	     }).complete(function(){
	           
	     });
	 }
}
else
{
	  render(<Router history={hashHistory}>{routes}</Router>, document.getElementById("container")); 
}


  

