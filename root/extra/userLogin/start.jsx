let React = require('react');
let ReactDOM = require('react-dom');
import { Icon,Form,Input,Button, } from 'antd';
require('./../../src/index/index.scss');
require('./start.scss');
require('./../../src/_utils/const');
const FormItem = Form.Item;

let LoginPanel = React.createClass({
  getInitialState() {
        return {data:{}};
  },
  handleOnChange(e) {
        var data = this.state.data;
        data[e.target.attributes['id'].value]= e.target.value;
        this.setState({
            data: data
        });
        globalVars.set('_projectId_',e.target.value);
        return false;
  },
  handleOnEnterProject(){
         if(this.state.data.projectId)
         {
            window.location.href = "https://"+ window.location.host +"/auth/weibo?project_id="+ this.state.data.projectId +"&login_url=https://"+ window.location.host +"/extra";
         }
  },
  render() {
    const { getFieldProps, getFieldError } = this.props.form;
    const projectIdProps = getFieldProps('projectId', {
        validate: [{
          rules: [
            { required: true, message: '项目ID不能为空'}
          ],
          trigger: 'onBlur',
        }]
    });

    return (
      <div className="third-login-con">
          <Form horizontal form={this.props.form}>
              <FormItem
                label="项目ID：">
                <Input {...projectIdProps} onChange={this.handleOnChange} value={this.state.data.projectId}/>
              </FormItem>

               <a onClick={this.handleOnEnterProject}>微博登录</a>
               <a>qq登录</a>
               <a>微信登录</a>
          </Form>
       </div>
    );
  }
});
LoginPanel = Form.create()(LoginPanel);
ReactDOM.render(<LoginPanel />, document.getElementById('block'));
