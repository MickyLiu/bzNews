module.exports = {
  path: 'admin',

  getChildRoutes(location, cb) {
    require.ensure([], (require) => {
      cb(null, [
        require('./show'),
        require('./create')
      ])
    })
  },

  getIndexRoute(location, callback) {
    require.ensure([], function (require) {
      callback(null, {
        component: require('./../../../src/adminManagePanel/adminShow/adminShow'),
      })
    })
  },

  getComponent(location, cb) {
    require.ensure([], (require) => {
      cb(null, require('./../../../src/adminManagePanel/adminManagePanel'))
    })
  }
}
