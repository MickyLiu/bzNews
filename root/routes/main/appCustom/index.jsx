module.exports = {
  path: 'appcustom',
  getComponent(location, cb) {
    require.ensure([], (require) => {
      cb(null, require('./../../../src/appCustomPanel/appCustomPanel'));
    })
  }
}
