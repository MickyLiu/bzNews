module.exports = {
  path: 'appVersion',
  getComponent(location, cb) {
    require.ensure([], (require) => {
      cb(null, require('./../../../src/appVersion/appVersion'));
    })
  }
}
