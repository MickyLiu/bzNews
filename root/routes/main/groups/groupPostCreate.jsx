module.exports = {
  path: ':id/posts/create',
  getComponent(location, cb) {
    require.ensure([], (require) => {
      cb(null, require('./../../../src/groupManagePanel/postCreate/postCreate'))
    })
  }
}
