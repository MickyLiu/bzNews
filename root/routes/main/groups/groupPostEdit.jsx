module.exports = {
  path: ':id/posts/:postId/edit',
  getComponent(location, cb) {
    require.ensure([], (require) => {
      cb(null, require('./../../../src/groupManagePanel/postEdit/postEdit'))
    })
  }
}
