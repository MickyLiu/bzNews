module.exports = {
  path: '/projects/:pid',
  getChildRoutes(location, callback) {
    require.ensure([], function (require) {
      callback(null, [
        require('./series'),
        require('./videos'),
        require('./user'),
        require('./admins'),
        require('./statistic'),
        require('./carouselPics'),
        require('./groups'),
        require('./appCustom'),
        require('./appPack'),
        require('./navy'),
        require('./appVersion'),
        require('./projectSetting'),
        require('./projectMember')
      ])
    })
  },

  getIndexRoute(location, callback) {
    require.ensure([], function (require) {
      callback(null, {
        component: require('./../../src/seriesManagePanel/seriesTable/seriesTable')
      })
    })
  },

  getComponent(location, callback) {
    require.ensure([], function (require) {
      callback(null, require('./../../src/index/index'))
    })
  }
}
