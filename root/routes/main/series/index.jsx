module.exports = {
  path: 'series',

  getChildRoutes(location, cb) {
    require.ensure([], (require) => {
      cb(null, [
        require('./detail'),
        require('./create'),
        require('./edit')
      ])
    })
  },

  getIndexRoute(location, callback) {
    require.ensure([], function (require) {
      callback(null, {
        component: require('./../../../src/seriesManagePanel/seriesTable/seriesTable'),
      })
    })
  },

  getComponent(location, cb) {
    require.ensure([], (require) => {
      cb(null, require('./../../../src/seriesManagePanel/seriesManagePanel'))
    })
  }
}
