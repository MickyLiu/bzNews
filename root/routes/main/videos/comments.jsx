module.exports = {
  path: ':id/comments',
  getComponent(location, cb) {
    require.ensure([], (require) => {
      cb(null, require('./../../../src/videoManagePanel/videoComments/videoComments'))
    })
  }
}
