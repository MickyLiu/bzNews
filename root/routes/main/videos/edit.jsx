module.exports = {
  path: ':id/edit',
  getComponent(location, cb) {
    require.ensure([], (require) => {
      cb(null, require('./../../../src/videoManagePanel/videoEdit/videoEdit'))
    })
  }
}
