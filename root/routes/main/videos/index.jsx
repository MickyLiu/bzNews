module.exports = {
  path: 'videos',

  getChildRoutes(location, cb) {
    require.ensure([], (require) => {
      cb(null, [
        require('./show'),
        require('./edit'),
        require('./comments')
      ])
    })
  },

  getIndexRoute(location, callback) {
    require.ensure([], function (require) {
      callback(null, {
        component: require('./../../../src/videoManagePanel/videoTable/videoTable'),
      })
    })
  },

  getComponent(location, cb) {
    require.ensure([], (require) => {
      cb(null, require('./../../../src/videoManagePanel/videoManagePanel'))
    })
  }
}
