module.exports = {
  path: 'members',

  getChildRoutes(location, cb) {
    require.ensure([], (require) => {
      cb(null, [
        require('./edit')
      ])
    })
  },

  getIndexRoute(location, callback) {
    require.ensure([], function (require) {
      callback(null, {
        component: require('./../../src/members/membersPanel'),
      })
    })
  },

  getComponent(location, cb) {
    require.ensure([], (require) => {
      cb(null, require('./../../src/members/main'));
    })
  }
}
