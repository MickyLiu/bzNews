module.exports = {
  path: 'create/show',
  getComponent(location, cb) {
    require.ensure([], (require) => {
      cb(null, require('./../../src/projectsManagePanel/projectCreate/projectCreate'))
    })
  }
}
