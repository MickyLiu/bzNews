module.exports = {
  path: 'projects',

  getChildRoutes(location, cb) {
    require.ensure([], (require) => {
      cb(null, [
        require('./create')
      ])
    })
  },

  getIndexRoute(location, callback) {
    require.ensure([], function (require) {
      callback(null, {
        component: require('./../../src/projectsManagePanel/projectsIndex'),
      })
    })
  },

  getComponent(location, cb) {
    require.ensure([], (require) => {
      cb(null, require('./../../src/projectsManagePanel/projectsManagePanel'))
    })
  }
}