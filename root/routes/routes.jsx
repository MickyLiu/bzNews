
const routes = {
  path: '/',
  getChildRoutes(location, callback) {
    require.ensure([], function (require) {
      callback(null, [
        require('./main'),
        require('./projects'),
        require('./home'),
        require('./user'),
        require('./members'),
        require('./searchAuthor'),
        require('./joinApprove'),
        require('./joinApproveDetail'),
        require('./joinApproveAll'),
        require('./team'),
        require('./messages'),
      ])
    })
  },

  getIndexRoute(location, callback) {
    require.ensure([], function (require) {
      callback(null, {
        component: require('./../src/projectsManagePanel/projectsIndex')
      })
    })
  },

  getComponent(location, callback) {
    require.ensure([], function (require) {
      callback(null, require('./../src/index/main'))
    })
  }
}



// import { Router, Route, Link, IndexRoute,hashHistory} from 'react-router';
// let React = require('react');
// let Index = require('./../src/index/index');
// let VideoManagePanel = require('../src/videoManagePanel/videoManagePanel');
// let VideoTable = require('../src/videoManagePanel/videoTable/videoTable');
// let VideoEdit = require('../src/videoManagePanel/videoEdit/videoEdit');
// let VideoComments = require('../src/videoManagePanel/videoComments/videoComments');
// let videoDetail = require('../src/videoManagePanel/videoDetail/videoDetail');
// let SeriesManagePanel = require('../src/seriesManagePanel/seriesManagePanel');
// let SeriesTable = require('../src/seriesManagePanel/seriesTable/seriesTable');
// let SeriesCreate = require('../src/seriesManagePanel/seriesCreate/seriesCreate');
// let SeriesEdit = require('../src/seriesManagePanel/seriesEdit/seriesEdit');
// let SeriesDetail = require('../src/seriesManagePanel/seriesDetail/seriesDetail');
// let SeriesVideoAdd = require('../src/seriesManagePanel/seriesVideoAdd/seriesVideoAdd');
// let userInfoPanel = require('../src/userInfoPanel/userInfoPanel');
// let AdminManagePanel = require('../src/adminManagePanel/adminManagePanel');
// let AdminShow = require('../src/adminManagePanel/adminShow/adminShow');
// let AdminCreate = require('../src/adminManagePanel/adminCreate/adminCreate');
// let DataStatisticsPanel = require('../src/dataStatisticsPanel/dataStatisticsPanel');
// let AppCustomPanel = require('../src/appCustomPanel/appCustomPanel');
// let carouselPicsPanel = require('../src/carouselPicsPanel/carouselPicsPanel');
// let events = require('../src/_utils/eventSubscriber');



// var routes = (
//   <Route name="app" path="/" component={Index} >
//     <IndexRoute component={SeriesTable} />
//     <Route name="videos" path="/videos" component={VideoManagePanel}>
//           <IndexRoute component={VideoTable} />
//           <Route name="show" path=":id" component={videoDetail}></Route>
//           <Route name="edit" path=":id/edit" component={VideoEdit}></Route>
//           <Route name="edit" path=":id/comments" component={VideoComments}></Route>
//     </Route>
//     <Route name="series" path="/series" component={SeriesManagePanel}>
//          <IndexRoute component={SeriesTable} />
//          <Route name="create" path="create" component={SeriesCreate}></Route>
//          <Route name="show" path=":id" component={SeriesDetail}></Route>
//          <Route name="edit" path=":id/edit" component={SeriesEdit}></Route>
//          <Route name="edit" path=":id/videoAdd" component={SeriesVideoAdd}></Route>
//     </Route>
//     <Route name="carouselPics" path="/carouselPics" component={carouselPicsPanel} />
//     <Route name="user" path="/user" component={userInfoPanel} />
//     <Route name="admin" path="/admin" component={AdminManagePanel}>
//       <IndexRoute component={AdminShow} />
//       <Route name="show" path="show" component={AdminShow}></Route>
//       <Route name="create" path="create" component={AdminCreate}></Route>
//     </Route>
//     <Route name="statistics" path="/statistics" component={DataStatisticsPanel}/>
//     <Route name="appcustom" path="/appcustom" component={AppCustomPanel}/>
//   </Route>
// );

module.exports = routes;
