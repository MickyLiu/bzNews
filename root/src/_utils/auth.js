let React = require('react');
let $ = require('jquery');
let locationHelper = require('./locationHelper');
let Const = require('./const');

var Auth = function(args) {
  for (var key in args) {
    if (args.hasOwnProperty(key)) {
      this[key] = args[key];
    }
  }
  // get `from` argument
  var afrom = decodeURIComponent(locationHelper.getSearchVar('from'));
  this.from.href = afrom;
  afrom = afrom.split('#');
  this.from.hash = afrom[1] ? '#' + afrom[1] : '';
  afrom = afrom[0].split('?');
  this.from.search = afrom[1] ? '?' + afrom[1]: '';
  this.from.pathname = afrom[0];
}

Auth.prototype = {
  routes: {
    '/': '/index',
    index: '/index',
    teams:'/teams',
    home: '/home',
    projects:'/projects',
    searchAuthor: '/searchAuthor',
  },
  default: 'home',
  // args
  page: undefined,
  target: 'teams',
  expect: false,
  from: {
    pathname: undefined,
    search: undefined,
    hash: undefined,
    href: undefined
  },
  // methods
  verify: function(callback) {
    let self = this;
    if(JSON.parse(localStorage.getItem('rememberPsd')) === true) {
      if(JSON.parse(localStorage.getItem('isLogin')) === false) {
        self._verify(false);
        return;
      }
    }
    let token = this._getToken();
    if(token) {
      $.ajax({
        url: Const.API.customersAuthenticate,
        method: 'POST',
        headers: {
            'Authorization': 'Token token=' + token
        }
      }).done(function(data) {
        if(localStorage.getItem('expired') !== null) {
          localStorage.removeItem('expired');
        }
        self._verify(true, data, callback);
      }).fail(function(jqXHR) {
        if(jqXHR.status === 401) {
          // token expired
          localStorage.removeItem('diao');
          localStorage.setItem('isLogin', false);
          localStorage.setItem('expired', true);
        }
        self._verify(false, jqXHR, callback);
      });
    } else {
      self._verify(false);
    }
  },
  verifyLoginInvite: function(callback) {
    let self = this;
    let token = this._getToken();
    if(token) {
      $.ajax({
        url: Const.API.customersAuthenticate,
        method: 'POST',
        headers: {
            'Authorization': 'Token token=' + token
        }
      }).done(function(jqXHR) {
        if(localStorage.getItem('expired') !== null) {
          localStorage.removeItem('expired');
        }
        callback(true);
      }).fail(function(jqXHR) {
        if(jqXHR.status === 401) {
          // token expired
          localStorage.removeItem('diao');
          localStorage.setItem('isLogin', false);
          localStorage.setItem('expired', true);
        }
        callback(false);
      });
    } else {
      callback(false);
    }
  },
  verifyIf: function(condition,route,callback){
       if(condition()){
          this.goto(route);
       }else{
          callback();
       }
  },
  _verify: function(resp, data, callback) {
    if (resp !== this.expect) {
      this.goto(this.target);
    } else {
      if (callback) {
        callback(data);
      }
    }
  },
  goto: function(key) {
    if (!this.routes[key]) {
      key = this.default;
    }
    window.location.href=locationHelper.buildURI(
      this.routes[key],
      {
       /* from: locationHelper.encodeURIComponent()*/
      }
    );
  },
  login: function(formData, onFail) {
    // authenticate
    var self = this;
    $.ajax({
      url: Const.API.customersAuthorize,
      data: {
          email: formData.name,
          password: formData.password
      },
      method: 'POST'
    }).done(function(data) {
      localStorage.setItem('diao', data.jwt_token);
      localStorage.setItem("rememberPsd", formData.rememberPsd);
      localStorage.setItem("username", formData.name);
      localStorage.setItem('ability', JSON.stringify(data.ability));
      if(formData.rememberPsd) {
        localStorage.setItem('isLogin',true);
      }
      if (self.from.href) {
        window.location.href = self.from.href;
      } else {
        window.location.href = self.routes[self.target];
      }
    }).fail(function(data) {
      onFail(data);
    });
  },
  rememberPsdLogin: function(onfail) {
    let token = this._getToken();
    let self = this;
    $.ajax({
      url: Const.API.customersAuthenticate,
      method: 'POST',
      headers: {
          'Authorization': 'Token token=' + token
      }
    }).done(function(data) {
      localStorage.removeItem('expired');
      localStorage.setItem('isLogin', true);
      if (self.from.href) {
        window.location.href = self.from.href;
      } else {
        window.location.href = self.routes[self.target];
      }
    }).fail(function(jqXHR) {
      localStorage.removeItem('diao');
      localStorage.setItem('isLogin', false);
      localStorage.setItem('expired', true);
      onfail();
    });
  },
  inviteLogin: function(formData, onSuccess, onFail) {
    var self = this;
    $.ajax({
      url: Const.API.customersAuthorize,
      data: {
          email: formData.name,
          password: formData.password
      },
      method: 'POST'
    }).done(function(data) {
      localStorage.setItem('diao', data.jwt_token);
      localStorage.setItem("rememberPsd", formData.rememberPsd);
      localStorage.setItem("username", formData.name);
      if(formData.rememberPsd) {
        localStorage.setItem('isLogin',true);
      }
      onSuccess();
    }).fail(function(data) {
      onFail(data);
    });
  },
  register: function(formData, onFail) {
    var self = this;
    $.ajax({
      url: Const.API.customers,
      data: formData,
      method: 'POST'
    }).done(function(data) {
      localStorage.setItem('diao', data.jwt_token);
      localStorage.setItem("username", formData.customer.email);
      localStorage.setItem('ability', JSON.stringify(data.ability));
      if(localStorage.getItem('rememberPsd')) {
        localStorage.setItem('rememberPsd', false);
      }
      if (self.from.href) {
        window.location.href = self.from.href;
      } else {
        window.location.href = self.routes[self.target];
      }
    }).fail(function(d) {
      let data = JSON.parse(d.responseText);
      onFail(data);
    });
  },
  inviteRegister: function(formData, onFail) {
    var self = this;
    $.ajax({
      url: Const.API.customers + '/create_via_invitation',
      data: formData,
      method: 'POST'
    }).done(function(data) {
      localStorage.setItem('diao', data.jwt_token);
      localStorage.setItem("username", formData.name);
      if(localStorage.getItem('rememberPsd')) {
        localStorage.setItem('rememberPsd', false)
      }
      if (self.from.href) {
        window.location.href = self.from.href;
      } else {
        window.location.href = self.routes[self.target];
      }
    }).fail(function(d) {
      let data = JSON.parse(d.responseText);
      onFail(data);
    });
  },
  logout: function (callback) {
    if(JSON.parse(localStorage.getItem('rememberPsd'))) {
      localStorage.setItem('isLogin', false);
    } else {
      localStorage.removeItem('diao');
    }
    if (callback) {
        callback();
    }
    window.location.href = this.routes['home'];
  },
  ajax: function(obj) {
    var token = this._getToken();
    if (token) {
      if (typeof obj == 'string') {
        obj = {url: obj}
      }
      if (obj['headers']) {
        obj['headers']['Authorization'] = 'Token ' + token;
      } else {
        obj['headers'] = {
          'Authorization': 'Token ' + token
        }
      }
      obj['cache'] = false;
    }
    return $.ajax(obj);
  },
  token: function () {
    let token = this._getToken();
    return token;
  },
  _getToken: function() {
    let token = localStorage.getItem('diao');
    return token;
  }
}

module.exports = Auth;
