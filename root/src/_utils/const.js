require('./global');

let urlPrefix = "";
if(__PRODUCTION__) {
  urlPrefix  = 'https://api.platform.bao.tv/api/web/v1/';
}
if(__STAGE__) {
  urlPrefix  = 'https://dsjstage.bao.tv/api/web/v1/';
}
if (__DEV__) {
  urlPrefix = '/api/web/v1/';
}

const devapiPrefix = '/_dev_api_/';

const APIFn = ()=>{
    return {
         urlPrefix: urlPrefix,
         projectPrefix: urlPrefix + `projects/${globalVars.get('_projectId_')}`,
         projectVideos: urlPrefix + `projects/${globalVars.get('_projectId_')}/videos`,
         videos: urlPrefix + 'videos',
         videoSearch: urlPrefix + `projects/${globalVars.get('_projectId_')}/videos/search`,
         videosBatch: urlPrefix + 'videos/batch',
         comments: urlPrefix + 'comments',
         projectComments: urlPrefix + `projects/${globalVars.get('_projectId_')}/comments`,
         youkuInfoAdd: urlPrefix + 'videos/youku_info',
         series: urlPrefix + 'series',
         projectSeries: urlPrefix + `projects/${globalVars.get('_projectId_')}/series`,
         projectGroups: urlPrefix + `projects/${globalVars.get('_projectId_')}/groups`,
         groups: urlPrefix + 'groups',
         projectPosts: urlPrefix + `projects/${globalVars.get('_projectId_')}/posts`,
         posts: urlPrefix + 'posts',
         admins: urlPrefix + 'administrators',
         projectBanners: urlPrefix + `projects/${globalVars.get('_projectId_')}/banners`,
         banners: urlPrefix + 'banners',
         authenticate: urlPrefix + 'administrators/authenticate.json',
         customersAuthenticate: urlPrefix + 'customers/authenticate',
         customersAuthorize: urlPrefix + 'customers/authorize',
         authorize: urlPrefix + 'administrators/authorize.json',
         getSettings: urlPrefix + `projects/${globalVars.get('_projectId_')}/settings`,
         putSettings: urlPrefix + 'settings',
         icons: urlPrefix + 'icons',
         projectIcons: urlPrefix + `projects/${globalVars.get('_projectId_')}/icons`,
         projectUsers: urlPrefix + `projects/${globalVars.get('_projectId_')}/users`,
         users: urlPrefix + 'users',
         customers: urlPrefix + 'customers',
         customersRoles: urlPrefix + 'customers/sync_roles',
         projectApp: urlPrefix + `projects/${globalVars.get('_projectId_')}/apps`,
         playcounts: urlPrefix + 'playcounts',
         teams: urlPrefix + 'teams',
         teamsSearch: urlPrefix + 'teams/search',
         teamProjects: `${urlPrefix}teams/${globalVars.get('_teamId_')}/projects`,
         teamDeny: urlPrefix + `teams/${globalVars.get('_teamId_')}/team_applications/deny`,
         teamApprove: urlPrefix + `teams/${globalVars.get('_teamId_')}/team_applications/approval`,
         team_applications: urlPrefix + 'team_applications',
         water_armies: urlPrefix + `projects/${globalVars.get('_projectId_')}/water_armies/users`,
         team: urlPrefix + `teams/${globalVars.get('_teamId_')}`,
         uploadToken: urlPrefix + 'upload_token',
         appVersion: urlPrefix + `projects/${globalVars.get('_projectId_')}/android_versions`,
         updateAppVersion: urlPrefix + 'android_versions/',
         memberSettings: urlPrefix + `customers/${localStorage.getItem('currentUserId')}/settings`
    }
}

let ApiOb = {"APIFn":APIFn};

Object.defineProperty(ApiOb, "API", {
    get: function () {
        let apiD = this.APIFn();
        if(__PRODUCTION__) {
          apiD.inviteUrl  = 'http://pfadmin.bao.tv/invite';
        }
        if(__STAGE__) {
          apiD.inviteUrl  = 'http://dsjadmin.baomihua.tv/invite';
        }
        if (__DEV__) {
          apiD.inviteUrl = 'localhost:8080/invite';
        }
        return apiD;
    },
    enumerable: true,
    configurable: true
});

module.exports = ApiOb;
