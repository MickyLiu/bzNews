var eventSubscriber = (function(){
  var topics = {};

  return {
    subscribe: function(topic, listener, key) {
       if(!topics[topic]) topics[topic] = { queue: [] };

       if(!listener) return false;

       var items = topics[topic].queue;

        for(var i in items)
        {
              var item=items[i];
              if(item.key && item.length && item.key==key)
              {
                  topics[topic].queue[i]={"listener":listener,"key":key};
                  return true;
              }
        }

       topics[topic].queue.push({"listener":listener,"key":key}) -1;
       return true;
    },
    getTopics:function(){
        return topics;
    },
    unsubscribe: function(topic, listener, key){
          if(!topics[topic] || !topics[topic].queue.length) return;

          var items = topics[topic].queue;
          var listenerStr = (!listener)? '':listener.toString().replace(/\s/ig,'');
          if(!key)
          {
              items.withOut(function(index,item){
                  return typeof listener=='function'?item.listener.toString().replace(/\s/ig,'')===listenerStr:true;
              });
          }
          else
          {
             items.withOut(function(index,item){
                  return key==items.key;
             });
          }
    },
    publish: function(topic, info, key) {
      if(!topics[topic] || !topics[topic].queue.length) return;

      var items = topics[topic].queue;
      items.forEach(function(item) {
          key===item.key? item.listener(info):'';
      });
    }
  };
})();

Array.prototype.withOut = function(condition){
    for(var i=this.length-1;i>=0;i--)
    {
      if(condition(i,this[i]))
      {
         this.splice(i,1);
      }
    }
}
module.exports = eventSubscriber;