require('./startsWith');
var global = {
             set:function(key,val) {
                if(key.startsWith('_'))
                {
                    localStorage.setItem(key,val);
                }
                else
                {
                    this[key] = val;
                }
             },
             get:function(key){
                if(key.startsWith('_'))
                {
                    return localStorage.getItem(key);
                }
                else
                {
                    return this[key];
                }
             }
     };

window.globalVars = global;

module.exports = global;
