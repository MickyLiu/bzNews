var locationHelper = {
  encodeURI: function () {
    var loc = window.location;
    return encodeURI(loc.pathname + loc.search + loc.hash);
  },
  encodeURIComponent: function () {
    var loc = window.location;
    return encodeURIComponent(loc.pathname + loc.search + loc.hash);
  },
  getSearchVar: function (varname) {
    return decodeURI(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURI(varname).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
  },
  buildURI: function (path, search, hash) {
    search = search || '';
    hash = hash || '';
    if (search !== '') {
      if (typeof search !== 'object') {
        search = {
          '_': search.toString()
        }
      }
      var searchstr = [];
      for (var key in search) {
        if (search.hasOwnProperty(key)) {
            searchstr.push(key + '=' + search[key].toString());
        }
      }
      search = "?" + searchstr.join('&');
    }
    if (hash !== '') {
      hash = '#' + hash;
    }
    return encodeURI(path + search + hash);
  },
  getUrlParamByKey:function(key){
     var hash = window.location.hash;
     if (hash && hash.length > 0) {　　　　　　　　
          var c_start = hash.indexOf(key + "=");　　　　　　　　
          if (c_start != -1) {　　　　　　　　
            c_start = c_start + key.length + 1;　　　　　　　　
            var c_end = hash.indexOf("&", c_start);　　　　　　　　　
            if (c_end == -1) c_end = hash.length;　　　　　　　　
            return hash.substring(c_start, c_end);　　　　　　
          }　
          return '';　　　
      }　　　　
      return "";
  }
}

module.exports = locationHelper;
