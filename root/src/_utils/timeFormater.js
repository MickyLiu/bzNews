
let formater = (time)=> {
       let _b = {
             fromNow:()=>{ 
                 let ts = new Date(time).getTime();
                 let ns = new Date().getTime();
                 let dur = (ns - ts)/1000,m = 60,h=60*m,d = 24*h,w=7*d,M=30*d,Y= 365*d;
                 if(dur<m)
                 {
                    return '刚刚';
                 }
                 else if(dur>=m && dur<=h)
                 {
                     return Math.round(dur/m) + '分钟前';
                 }
                 else if(dur>h && dur<=d)
                 {
                      return Math.round(dur/h) + '小时前';
                 }
                 else if(dur>d && dur<=w)
                 {
                     return Math.round(dur/d) + '天前';
                 }
                 else if(dur>w && dur<=M)
                 {
                     return Math.round(dur/w) + '周前';
                 }
                 else if(dur>M && dur<=Y)
                 {
                     return Math.round(dur/M) + '月前';
                 }
                 else
                 {
                     return Math.round(dur/Y) + '年前';
                 }
             }
       }
      
      return _b;
};

module.exports = formater;
