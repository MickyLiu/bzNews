let events = require('./eventSubscriber');

var progressChange = function(){
      this.setState({
            current: arguments[0],
            show:true
      });

      if(arguments[0]>=100){
          setTimeout(function(){
              this.setState({
                 show: false
              });
          }.bind(this),10);
      }
};

var routeOnNav = function(route){
         switch(route){
               case "":
                  this.setState({current: "g1"});
                  break;
               case 'series':
                  this.setState({current: "g1"});
                  break;
               case 'videos':
                  this.setState({current: "g2"});
                  break;
               case 'groups':
                  this.setState({current: "g3"});
                  break;
               case 'carouselPics':
                 this.setState({current: "g4"});
                 break;
               case 'navy':
                   this.setState({current: "g5"});
                   break
               case 'appVersion':
                  this.setState({current: "g6"});
                  break
               case 'statistics':
                  this.setState({current: "a1"});
                  break;
               case 'appcustom':
                  this.setState({current: "a2"});
                  break;
              case 'apppack':
                  this.setState({current: "a3"});
                  break
               default:
                  break;
         }
}

var routeOnTop = function(route){
         this.setState({currentRoute: route});
}

var UiInfoChange = function(info){
      this.setState(info);
}

var UIChangeMixIn = {
    routeOnNavMixIn:{
     componentDidMount: function () {
          events.subscribe('routeOnNav',routeOnNav.bind(this));
     },
     componentWillUnmount: function(){
          events.unsubscribe('routeOnNav');
     }
   },
   routeOnTopMixIn:{
     componentDidMount: function () {
          events.subscribe('routeOnTop',routeOnTop.bind(this));
     },
     componentWillUnmount: function(){
          events.unsubscribe('routeOnTop');
     }
   },
   UiInfoChangeMixIn:{
     componentDidMount: function () {
          events.subscribe('UiInfoChange',UiInfoChange.bind(this));
     },
     componentWillUnmount: function(){
          events.unsubscribe('UiInfoChange');
     }
   },
   progressMixIn:{
      componentDidMount: function () {
          events.subscribe('progressChange',progressChange.bind(this));
      }
   }
}

module.exports = UIChangeMixIn;
