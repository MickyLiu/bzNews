let React = require('react');
let AdminForm = require('./../../components/adminForm/adminForm').AdminForm;
require('./adminCreate.scss');

const AdminCreate = React.createClass({
  render() {
    return (
      <div>
        <h2 className="admin-panel-title">新建管理员</h2>
        <AdminForm id=""/>
      </div>
    );
  }
});

module.exports = AdminCreate;
