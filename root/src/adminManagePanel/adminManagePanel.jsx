let React = require('react');

const AdminManagePanel = React.createClass({
  render() {
    return (
      <div>
        {this.props.children}
      </div>
    );
  }
});

module.exports = AdminManagePanel;
