import { Table, Button, Message} from 'antd';
let Link = require('./../../components/link/link');
let React = require('react');
let ajaxEx = require('./../../_utils/ajaxConfiger');
let Const = require('./../../_utils/const');
require('./adminShow.scss');

const columns = [{
    title: '邮箱',
    dataIndex: 'email',
    key: 'email'
  },{
    title: '名字',
    dataIndex: 'name',
    key: 'name',
  }];

let testData = [];
for(let i=0; i<17; i++) {
  testData.push({
    "email": "123@678.com",
    "name": "大表哥"
  });
}

const AdminShow = React.createClass({
  getInitialState() {
   return{
      dataSource :[],
      pageSize:25,
      total:1,
      loading: true
    };
  },
  componentDidMount: function () {
     var _this = this;
     ajaxEx.get({url:Const.API.admins},function(d){
      console.log(d);
      let data = [];
       for(let i=0; i< d.data.length; i++) {
         data.push(d.data[i].attributes)
       }
       _this.setState({
          dataSource : data,
          total: data.length,
          loading:false
       });
     });
  },
  render() {
    return (
      <div className="admin-show-table">
        <Link to='admin/create'>
          <Button type="primary" size="large" className="create-admin">新建管理员</Button>
        </Link>
        <Table columns={columns} dataSource={this.state.dataSource} loading={this.state.loading} pagination={{ pageSize: this.state.pageSize, total: this.state.total}} />
      </div>
    );
  }
  });

module.exports = AdminShow;
