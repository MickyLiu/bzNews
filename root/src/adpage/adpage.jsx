let React = require('react');
let Footer = require('./../components/footer/footer');
let AdArea = require('./../components/adArea/adArea');

const AdPage = React.createClass({
  render: function() {
    return (
      <div>
        <AdArea login={true} />
        <Footer />
      </div>
    );
  }
});

module.exports = AdPage;
