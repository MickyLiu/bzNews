import { Form, Input, Row, Col, Icon, Modal, message } from 'antd';
const React = require('react');
const ReactDOM = require('react-dom');
const classnames = require('classnames');
const BottomItemIcon = require('./bottomItemIcon');
require('./addBottomItemModal.scss');
const FormItem = Form.Item;

const AddBottomItemModal = React.createClass({
    upload_promiss: null,
    getInitialState() {
        return {
            visible: false,
            name: '',
            target: '',
            normal_icon: null,
            active_icon: null,
        }
    },
    openBottomItemAddModal() {
      this.setState({visible: true, name: "", target: "", normal_icon: null, active_icon: null});
      if (this.refs.normalIcon) {
        this.refs.normalIcon.iconSelectHandle(null);
      }
      if (this.refs.activeIcon) {
        this.refs.activeIcon.iconSelectHandle(null);
      }
    },
    closeBottomItemAddModal() {
      this.setState({visible: false});
    },
    addBottomItem() {
      if (!!this.state.name.trim() && !!this.state.target.trim() && !!this.state.normal_icon && !!this.state.active_icon) {
        if (this.props.onOK) {
          this.props.onOK({
            icons: {
              normal: this.state.normal_icon,
              active: this.state.active_icon,
            },
            name: this.state.name,
            order: 0,
            target: this.state.target,
            type: "secondary",
          })
        }
        this.setState({visible: false});
      } else {
        message.error('请填写完整信息');
      }
    },
    render() {
        if (this.props.data.length < 5) {
          const formItemLayout = {
              labelCol: { span: 4 },
              wrapperCol: { span: 20 },
          };
          return (
              <div className="app-custom-bottomItems-addone" onClick={this.openBottomItemAddModal}>
                  <Icon type="plus-circle-o" />添加版块
                  <Modal className="app-custom-bottomItems-addone-modal" title="添加版块" visible={this.state.visible} onOk={this.addBottomItem} onCancel={this.closeBottomItemAddModal}>
                      <Form horizontal>
                          <Row>
                              <Col span="24">
                                  <FormItem
                                  {...formItemLayout}
                                  label="标题：">
                                  <Input type="text" value={this.state.name} onChange={e => this.setState({name: e.target.value})}/>
                                  </FormItem>
                              </Col>
                          </Row>
                          <Row>
                              <Col span="24">
                                  <FormItem
                                  {...formItemLayout}
                                  label="链接：">
                                  <Input addonBefore="http://" type="text" value={this.state.target} onChange={e => this.setState({target: e.target.value})}/>
                                  </FormItem>
                              </Col>
                          </Row>
                          <Row>
                              <Col span="24">
                                  <FormItem
                                  {...formItemLayout}
                                  label="图标：">
                                  <BottomItemIcon ref="normalIcon" Auth={this.props.Auth} src={this.state.normal_icon} title="normal" Icons={this.props.origin_icon_pool_data} onListChange={this.props.onIconPoolListChange} onOK={u => this.setState({normal_icon: u})}/>
                                  <BottomItemIcon ref="activeIcon" Auth={this.props.Auth} src={this.state.active_icon} title="active" Icons={this.props.origin_icon_pool_data} onListChange={this.props.onIconPoolListChange} onOK={u => this.setState({active_icon: u})}/>
                                  </FormItem>
                              </Col>
                          </Row>
                      </Form>
                  </Modal>
              </div>
          )
      } else {
          return (
              <div></div>
          )
      }
    }
});

module.exports = AddBottomItemModal;