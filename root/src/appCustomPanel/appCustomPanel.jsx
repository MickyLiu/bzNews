import { Form, Input, Row, Col, Button, Icon, Popover, Modal } from 'antd';

const React = require('react');
const AppOverview = require('./appoverview');
const BottomItem = require('./bottomItem');
const AddBottomItemModal = require('./addBottomItemModal');
const ColorPicker = require('../components/colorPicker/colorPicker');
const DraggableList = require('../components/draggableList/draggableList');
const classnames = require('classnames');
const Const = require("./../_utils/const");
const ajaxEx = require('./../_utils/ajaxConfiger');
const $ = require('jquery');
require('./appCustomPanel.scss');
const overviewMask = document.createElement('img');
overviewMask.src = require('./../../assets/image/iphone6_border.png');
const overviewBGImg = document.createElement('img');
overviewBGImg.src = require('./../../assets/image/iphone6_background_2.png');
const titleImg = document.createElement('img');
titleImg.src = require('./../../assets/image/iphone6_top.png');

const AppCustomPanel = React.createClass({
  getInitialState() {
      return {
        overview_show: false,
        overview_scale: 0.5,
        overview_mask: overviewMask.src,
        overview_backgroundImg: overviewBGImg.src,
        overview_border: [136, 546, 1039, 38],
        overview_titleImg: titleImg.src,
        // overview data
        overview_titleBackgroundColor: [0, 0, 0, 1],
        overview_bottomItems: [],
        // origin data
        origin_data: null,
        origin_top_background_data: null,
        origin_bottom_icon_data: null,
        origin_icon_pool_data: [],
        // control
        top_save_loading: false,
        bottom_save_loading: false,
        bottom_icon_dragable_lock: true,
      }
  },
  componentDidMount() {
      var self = this;
      setTimeout(function() {
          self.setState({overview_show: true});
      }, 0);
      var auth = this.props.Auth || window.auth;
      auth.ajax(Const.API.getSettings)
        .done(function(data) {
            self.dataInit(data);
        })
  },
  dataInit(data) {
    this.setState({origin_data: data});
    var setting = data.filter(v => v.key == "COLOR");
    if (setting && setting[0]) {
        this.topTitleBackgroundInit(setting[0]);
    }
    setting = data.filter(v => v.key == "TAB_ITEMS");
    if (setting && setting[0]) {
        this.bottomItemInit(setting[0]);
    }
    this.refs.overview.unloading();
  },
  // top setting
  topTitleBackgroundInit(setting) {
      this.setState({origin_top_background_data: setting});
      this.onTopTitleBackgroundColorChange({rgb: setting.value.current});
  },
  onTopTitleBackgroundColorChange(color) {
      color = color.rgb;
      this.refs.topTitleBackgroundColorpicker.setColor(color);
      color = [color.r, color.g, color.b, color.a];
      this.setState({overview_titleBackgroundColor: color});
      this.refs.overview.changeColor(color);
  },
  saveTopTitleBackgroundColor() {
      var color = this.state.overview_titleBackgroundColor;
      var origin_color = this.state.origin_top_background_data.value;
      if (color[0] == origin_color.current.r
            && color[1] == origin_color.current.g
            && color[2] == origin_color.current.b
            && color[3] == origin_color.current.a) {
          return;
      }
      this.setState({ top_save_loading: true });
      var auth = this.props.Auth || window.auth;
      var self = this;
      var data = {
          current: {r: color[0], g: color[1], b: color[2], a: color[3]},
          history: [origin_color.current].concat(origin_color.history)
      };
      if (data.history.length > 5) {
          data.history = data.history.slice(0, 5);
      }

      auth.ajax({
        method: "PUT",
        url: Const.API.putSettings+ `/${self.state.origin_top_background_data.id}`,
        dataType: 'json',
        contentType: "application/json",
        data: JSON.stringify({
            setting: {
                value: data
            }
         })
      }).done(function(data) {
          self.topTitleBackgroundInit(data);
          self.setState({ top_save_loading: false });
      });
  },
  undoTopTitleBackgroundColor() {
      this.onTopTitleBackgroundColorChange({rgb: this.state.origin_top_background_data.value.current});
  },
  resetTitleBackgroundColor() {
      this.onTopTitleBackgroundColorChange({rgb: {r: 0, g: 0, b: 0, a: 1}});
  },
  // bottom setting
  bottomItemInit(setting) {
      this.setState({origin_bottom_icon_data: setting});
      var data = $.extend(true, {}, setting);
      // concat and sort
      data = data.value.primary_items.map(v => (v.type = "primary", v)).concat(data.value.secondary_items.map(v => (v.type = "secondary", v)));
      data.sort((a, b) => a.order - b.order);
      this.setState({overview_bottomItems: data});
      // icon data
      this.IconPoolInit();
  },
  IconPoolInit() {
      var auth = this.props.Auth || window.auth;
      var self = this;
      auth.ajax(Const.API.projectIcons)
        .done(function(v) {
            self.setState({origin_icon_pool_data: v.data});
        });
  },
  onIconPoolListChange() {
      this.IconPoolInit();
  },
  addBottomItem(data) {
    var overview_data = this.state.overview_bottomItems;
    data.order = overview_data.length;
    overview_data.push(data);
    this.setState({overview_bottomItems: overview_data});
  },
  changeBottomItemIcon(url, data, key) {
      var overview_data = this.state.overview_bottomItems;
      var d = overview_data.filter(v => v.name == data.name)[0];
      if (d) {
          d.icons[key] = url
      }
      this.setState({overview_bottomItems: overview_data});
  },
  changeBottomItemName(name, data) {
    var overview_data = this.state.overview_bottomItems;
    var d = overview_data.filter(v => v.name == data.name)[0];
    if (d) {
      d.name = name;
    }
    this.setState({overview_bottomItems: overview_data});
  },
  changeBottomItemTarget(name, target) {
    var overview_data = this.state.overview_bottomItems;
    var idx = overview_data.findIndex(v => v.name == name);
    overview_data[idx].target = target;
    this.setState({overview_bottomItems: overview_data});
  },
  deleteBottomItem(name) {
    var overview_data = this.state.overview_bottomItems;
    var idx = overview_data.findIndex(v => v.name == name);
    overview_data.splice(idx, 1);
    for (var i = 0; i < overview_data.length; i++) {
      overview_data[i].order = i;
    }
    this.setState({overview_bottomItems: overview_data});
  },
  bottomDataChangeHandle(data) {
    for (var i = 0; i < data.length; i++) {
      data[i].order = i;
    }
    this.setState({overview_bottomItems: data});
  },
  saveBottomItems() {
    this.setState({ bottom_save_loading: true });
    var auth = this.props.Auth || window.auth;
    var self = this;
    var overview_data = this.state.overview_bottomItems;
    var primary_items = overview_data.filter(v => v.type == 'primary').map(v => ({
      icons: v.icons,
      name: v.name,
      order: v.order,
      target: v.target
    }));
    var secondary_items = overview_data.filter(v => v.type == 'secondary').map(v => ({
      icons: v.icons,
      name: v.name,
      order: v.order,
      target: v.target
    }));;
    var data = $.extend(true, {}, this.state.origin_bottom_icon_data.value);
    data.count = overview_data.length;
    primary_items.sort((a, b) => {
      var sortby = ["home", "community", "account"];
      return sortby.indexOf(a.target) - sortby.indexOf(b.target);
    });
    data.primary_items = primary_items;
    data.secondary_items = secondary_items;
    auth.ajax({
      method: "PUT",
      url: Const.API.putSettings + `/${self.state.origin_bottom_icon_data.id}`,
      dataType: 'json',
      contentType: "application/json",
      data: JSON.stringify({
          setting: {
              value: data
          }
        })
    }).done(function(data) {
        self.bottomItemInit(data);
        self.setState({ bottom_save_loading: false });
    });
  },
  undoBottomItems() {
    this.bottomItemInit(this.state.origin_bottom_icon_data);
  },
  resetBottomItems() {
    var data = {
      id: this.state.origin_bottom_icon_data.id,
      key: "TAB_ITEMS",
      value: {
        count: 3,
        secondary_items: [],
        primary_items: [{
          icons: {
            active: "http://7xs8p5.com2.z0.glb.qiniucdn.com/home_active.png",
            normal: "http://7xs8p5.com2.z0.glb.qiniucdn.com/home.png"
          },
          name: '首页',
          order: 0,
          target: 'home'
        }, {
          icons: {
            active: "http://7xs8p5.com2.z0.glb.qiniucdn.com/community_active.png",
            normal: "http://7xs8p5.com2.z0.glb.qiniucdn.com/community.png"
          },
          name: '社区',
          order: 1,
          target: "community"
        }, {
          icons: {
            active: "http://7xs8p5.com2.z0.glb.qiniucdn.com/account_active.png",
            normal: "http://7xs8p5.com2.z0.glb.qiniucdn.com/account.png"
          },
          name: '我的',
          order: 2,
          target: "account"
        }]
      }
    }
    this.bottomItemInit(data);
  },
  render() {
    let overviewClasses = classnames('app-overview-container', {show: this.state.overview_show});
    let topTitleBackgroundColorHistory = (
        <div className="app-custom-opt-item">
            历史记录<Popover overlay={(<div style={{textAlign: 'center'}}>当前无记录</div>)}>
                <Icon type="exclamation-circle-o" />
            </Popover>：
        </div>
    );
    if (this.state.origin_top_background_data) {
        topTitleBackgroundColorHistory = this.state.origin_top_background_data.value.history.map((v, i) => (
            <div className="colorpicker" key={i}><div className="colorpicker-bt">
                <div className="colorpicker-bt-color" style={{background: `rgb(${v.r}, ${v.g}, ${v.b})`}} onClick={e => this.onTopTitleBackgroundColorChange({rgb: v})}></div>
            </div></div>
        ));
        topTitleBackgroundColorHistory = (
            <div className="app-custom-opt-item">
                历史记录<Popover overlay={(<div style={{textAlign: 'center'}}>点击历史记录可直接替换</div>)}>
                    <Icon type="question-circle-o" />
                </Popover>：{topTitleBackgroundColorHistory}
            </div>
        )
    }
    return (
      <div className="app-custom-panel">
        <div className="app-custom-left">
            <h1>APP样式定制</h1>
            <div className="app-custom-opt">
                <div className="app-custom-opt-group">
                    <div className="app-custom-opt-title"><Icon type="paper-clip" />顶部标题栏</div>
                    <div className="app-custom-opt-item">
                        顶部背景色：<ColorPicker ref="topTitleBackgroundColorpicker" onColorChange={this.onTopTitleBackgroundColorChange}/>
                    </div>
                    {topTitleBackgroundColorHistory}
                    <div className="app-custom-opt-submit">
                        <Button type="primary" loading={this.state.top_save_loading} onClick={this.saveTopTitleBackgroundColor}>保存</Button>
                        <Button onClick={this.undoTopTitleBackgroundColor}>重置本次操作</Button>
                        <Button onClick={this.resetTitleBackgroundColor}>恢复默认样式</Button>
                    </div>
                </div>
                <div className="app-custom-opt-group">
                    <div className="app-custom-opt-title"><Icon type="paper-clip" />底部导航栏
                      <Popover overlay={(<div style={{textAlign: 'center'}}>拖动版块标题改变顺序</div>)}>
                        <Icon type="question-circle-o" />
                      </Popover>
                    </div>
                    <div className="app-custom-opt-item">
                        <DraggableList ref="bottomItemDraggableList"
                          data={this.state.overview_bottomItems}
                          placeholderInit={p => $(p).append('<div>拖放到这里</div>')}
                          lock={this.state.bottom_icon_dragable_lock}
                          onDataChange={this.bottomDataChangeHandle}
                        >
                          {this.state.overview_bottomItems.map(v => (
                            <BottomItem parent={this} data={v} key={v.name} />
                          ))}
                        </DraggableList>
                        <AddBottomItemModal
                          data={this.state.overview_bottomItems}
                          Auth={this.props.Auth || window.auth}
                          origin_icon_pool_data={this.state.origin_icon_pool_data}
                          onIconPoolListChange={this.onIconPoolListChange}
                          onOK={this.addBottomItem}
                        />
                    </div>
                    <div className="app-custom-opt-submit">
                        <Button type="primary" loading={this.state.bottom_save_loading} onClick={this.saveBottomItems}>保存</Button>
                        <Button onClick={this.undoBottomItems}>重置本次操作</Button>
                        <Button onClick={this.resetBottomItems}>恢复默认样式</Button>
                    </div>
                </div>
            </div>
        </div>
        <div className={overviewClasses}>
            <AppOverview ref="overview"
                Scale={this.state.overview_scale}
                Mask={this.state.overview_mask}
                Background={this.state.overview_backgroundImg}
                TitleBackgroundColor={this.state.overview_titleBackgroundColor}
                BottomItems={this.state.overview_bottomItems}
                Title={this.state.overview_titleImg}
                Border={this.state.overview_border}/>
            <Popover overlay={(<div style={{textAlign: 'center'}}>请以实际效果为准</div>)}>
                <Icon type="exclamation-circle-o" />
            </Popover>
        </div>
      </div>
    );
  }
});

module.exports = AppCustomPanel;
