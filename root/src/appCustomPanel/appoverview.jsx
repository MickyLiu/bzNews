import { Icon } from 'antd';
let React = require('react');
let classnames = require('classnames');
require('./appoverview.scss');

const AppOverview = React.createClass({
  getInitialState() {
    return {
        theme: 'black',
        titleBackgroundColor: this.props.TitleBackgroundColor,
        loading: true,
    }
  },
  componentDidMount() {
      this.changeColor(this.state.titleBackgroundColor);
  },
  changeColor(color) {
      var b = 0.3 * color[0] + 0.6 * color[1] + 0.1 * color[2];
      if (b > 135) {
          this.setState({theme: 'white'});
      } else {
          this.setState({theme: 'black'});
      }
      this.setState({titleBackgroundColor: color});
  },
  loading() {
      this.setState({loading: true});
  },
  unloading() {
      this.setState({loading: false});
  },
  render() {
    let classes = classnames('app-overview', this.state.theme, {loading: this.state.loading});
    return (
    <div className={classes} style={{transform: `scale(${this.props.Scale})`}}>
        <img className="app-overview-mask" src={this.props.Mask} alt="预览"/>
        <div className="app-overview-content" style={{
            top: this.props.Border[0],
            left: this.props.Border[3],
            transform: `scale(${(this.props.Border[1] - this.props.Border[3]) / 750})`
        }}>
            <div className="app-overview-loading app-overview-element"><Icon type="loading" /></div>
            <img className="app-overview-bg app-overview-element" src={this.props.Background} />
            <div className="app-overview-top app-overview-element" style={{
                backgroundImage: `url(${this.props.Title})`,
                backgroundColor: `rgba(${this.state.titleBackgroundColor.join(',')})`
            }}></div>
            <div className="app-overview-bottom app-overview-element">
            {this.props.BottomItems.map(v => 
                <div key={v.order} className="app-overview-bottom-item" style={{width: `${100 / this.props.BottomItems.length}%`}}>
                    <div className="app-overview-bottom-item-icon"><img className="normal" src={v.icons.normal}/><img className="active" src={v.icons.active}/></div>
                    <div className="app-overview-bottom-item-title">{v.name}</div>
                </div>
            )}
            </div>
        </div>
    </div>
    );
  }
});

module.exports = AppOverview;
