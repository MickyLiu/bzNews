import { Icon, Popover, Col, Row, Modal, Input } from 'antd';
const React = require('react');
const ReactDOM = require('react-dom');
const classnames = require('classnames');
const IconPool = require('./iconPool');
const BottomItemIcon = require('./bottomItemIcon');
const EditableLabel = require('../components/editableLabel/editableLabel');
const confirm = Modal.confirm;
require('./bottomItem.scss');

const iconOrder = ['normal', 'active'];

function sortIcon(a, b) {
  return iconOrder.indexOf(a) - iconOrder.indexOf(b);
}

const BottomItem = React.createClass({
    getInitialState() {
        return {
        }
    },
    deleteHandle(name) {
      var self = this;
      confirm({
        title: '确认删除',
        content: '删除模块操作将不可逆，一旦提交，则无法恢复，您可以使用底端的“重置本次操作”按钮来恢复所有提交前的操作。',
        onOk() {
          self.props.parent.deleteBottomItem(name);
        },
        onCancel() {}
      });
    },
    render() {
      let v = this.props.data;
      let setting;
      let title;
      let edit_title = false;
      if (v.type == "primary") {
        title = (
          <div className="app-custom-bottomItems-block-title" onMouseDown={v => this.props.parent.setState({bottom_icon_dragable_lock: false})} onMouseUp={v => this.props.parent.setState({bottom_icon_dragable_lock: true})} onMouseLeave={v => this.props.parent.setState({bottom_icon_dragable_lock: true})}>
            <EditableLabel placeholder="2-4字" style={{ width: 100 }} value={v.name} onOK={name => this.props.parent.changeBottomItemName(name, v)}/>
          </div>
        )
        setting = (
          <div className="app-custom-bottomItems-block-setting-item">
            <Row>
              <Col span="4">链接：</Col>
              <Col span="20">{v.target}<Popover overlay={(<div style={{textAlign: 'center'}}>默认版块无法更改链接</div>)}><Icon type="question-circle-o" /></Popover></Col>
            </Row>
          </div>
        )
      } else {
        title = (
          <div className="app-custom-bottomItems-block-title" onMouseDown={v => this.props.parent.setState({bottom_icon_dragable_lock: false})} onMouseUp={v => this.props.parent.setState({bottom_icon_dragable_lock: true})} onMouseLeave={v => this.props.parent.setState({bottom_icon_dragable_lock: true})}>
            <EditableLabel placeholder="2-4字" style={{ width: 100 }} value={v.name} onOK={name => this.props.parent.changeBottomItemName(name, v)}/>
            <div className="app-custom-bottomItems-block-delete" onClick={() => this.deleteHandle(v.name)}><Icon type="cross-circle-o" /></div>
          </div>
        )
        setting = (
          <div className="app-custom-bottomItems-block-setting-item">
            <Row>
              <Col span="4">链接：</Col>
              <Col span="20"><Input addonBefore="http://" type="text" value={v.target} onChange={e => this.props.parent.changeBottomItemTarget(v.name, e.target.value)}/></Col>
            </Row>
          </div>
        )
      }
      return (
        <div className={`app-custom-bottomItems-block ${v.type}`}>
          {title}
          <div className="app-custom-bottomItems-block-setting">
            {setting}
            <div className="app-custom-bottomItems-block-setting-item">
              <Row>
                <Col span="4">图标：</Col>
                <Col span="20">
                {
                  Object.keys(v.icons).sort(sortIcon).map(key => 
                    <BottomItemIcon
                    Auth={this.props.parent.props.Auth}
                    src={v.icons[key]}
                    title={key}
                    key={key}
                    Icons={this.props.parent.state.origin_icon_pool_data}
                    onListChange={this.props.parent.onIconPoolListChange}
                    onOK={u => this.props.parent.changeBottomItemIcon(u, v, key)}/> 
                  )
                }
                </Col>
              </Row>
            </div>
          </div>
        </div>
      )
    }
});

module.exports = BottomItem;