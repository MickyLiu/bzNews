import { Icon, Modal, Input, Upload } from 'antd';
let React = require('react');
let Const = require('./../_utils/const');
let ReactDOM = require('react-dom');
let classnames = require('classnames');
let IconPool = require('./iconPool');
require('./bottomItemIcon.scss');

const BottomItemIcon = React.createClass({
    getInitialState() {
        return {
            visible: false,
            src: this.props.src
        }
    },
    onHandler() {
        if (this.props.onOK) {
            this.props.onOK(this.state.src);
        }
        this.setState({visible: false});
    },
    cancelHandler() {
        if (this.props.onCancel) {
            this.props.onCancel();
        }
        this.setState({visible: false, src: this.props.src});
    },
    openMadal() {
        if (this.refs.iconpool) {
            this.refs.iconpool.setSelect(this.props.src);
        }
        this.setState({visible: true});
    },
    iconSelectHandle(url) {
        this.setState({src: url});
    },
    render() {
        var self = this;
        const props = {
            action: Const.API.projectIcons,
            listType: 'picture-card',
            headers: {
                'Authorization': 'Token ' + (this.props.Auth || window.auth).token()
            },
            showUploadList: false,
            accept: 'image/jpeg,image/png,image/jpg,image/bmp',
            data: {
                'icon': {
                    'name': 'mmmm',
                }
            }
            // defaultFileList: [{
            //     uid: -1,
            //     name: 'xxx.png',
            //     status: 'done',
            //     url: 'https://os.alipayobjects.com/rmsportal/NDbkJhpzmLxtPhB.png',
            //     thumbUrl: 'https://os.alipayobjects.com/rmsportal/NDbkJhpzmLxtPhB.png',
            // }]
        };
        return (
            <div className="bottom-item-icon" onClick={this.openMadal}>
                <div className="bottom-item-icon-img">
                {(function() {
                  if (this.state.src) {
                    return <img src={this.state.src}/>
                  } else {
                    return <div className="bottom-item-icon-add-bt"><Icon type="plus" /></div>
                  }
                }).call(this)}
                </div>
                <div className="bottom-item-icon-title">
                    {this.props.title}
                </div>
                <Modal title="更改图标" visible={this.state.visible} onOk={this.onHandler} onCancel={this.cancelHandler}>
                    <IconPool ref="iconpool" Auth={(this.props.Auth || window.auth)} Icons={this.props.Icons} Selected={this.state.src} onListChange={this.props.onListChange} onSelect={this.iconSelectHandle}/>
                </Modal>
            </div>
        )
    }
});

module.exports = BottomItemIcon;
