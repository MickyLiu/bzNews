import { Icon, Modal, Row, Col, Input, Upload, Spin, Button, message } from 'antd';
let React = require('react');
let ReactDOM = require('react-dom');
let classnames = require('classnames');
let Const = require('./../_utils/const');
let $ = require('jquery');
require('./iconPool.scss');

const IconPool = React.createClass({
    upload_promiss: null,
    getInitialState() {
        return {
            selected: this.props.Selected,
            add_modal_visible: false,
            uploading: false,
            upload_data: {
                name: Math.random()
            }
        }
    },
    clickHandle(url) {
        this.setState({selected: url});
        if (this.props.onSelect) {
            this.props.onSelect(url);
        }
    },
    openIconNameModal(p) {
        this.setState({add_modal_visible: true});
        this.upload_promiss = p;
    },
    okModalHandler() {
        this.setState({ uploading: true });
        this.upload_promiss.upload();
    },
    cancelModalHandler() {
        this.setState({add_modal_visible: false});
        this.upload_promiss = null;
    },
    setSelect(url) {
        this.setState({selected: url});
    },
    render() {
        var self = this;
        var data = this.props.Icons.map(function(v) {
            v.selected = self.state.selected == v.url;
            return v
        });
        const props = {
            action: Const.API.projectIcons,
            headers: {
                'Authorization': 'Token ' + this.props.Auth.token()
            },
            showUploadList: false,
            accept: 'image/jpeg,image/png,image/jpg,image/bmp',
            onChange(info) {
                if (info.file.status == 'uploading') {
                    // console.log(info.file, info.fileList);
                } else if (info.file.status === 'done' || (info.file.status === 'error' && info.file.error.status == 201)) {
                    message.success(`${info.file.name} 上传成功。`);
                    self.setState({ uploading: false, add_modal_visible: false });
                    if (self.props.onListChange) {
                        self.props.onListChange();
                    }
                } else if (info.file.status === 'error') {
                    message.error(`${info.file.name} 上传失败。`);
                    self.setState({ uploading: false });
                }
            },
            beforeUpload(file) {
                var p = {
                    upload: null,
                    then: function(func) {
                        this.upload = func
                    }
                }
                self.upload_promiss = p;
                return p;
            }
        };
        return (
            <div className="icon-pool">
                {data.map(v =>
                    <div className={v.selected ? "icon-pool-item selected" : "icon-pool-item"} onClick={e => self.clickHandle(v.url, e)} key={`${Math.random()}`}>
                        <img src={v.url} /><div className="icon-pool-item-selected"><Icon type="check" /></div>
                    </div>
                )}
                <div onClick={this.openIconNameModal} className="icon-pool-item-add"><Icon type="plus" />
                    <Modal
                        className="icon-pool-item-add-modal"
                        title="上传图标" visible={this.state.add_modal_visible}
                        onOk={this.okModalHandler}
                        onCancel={this.cancelModalHandler}
                        footer={[
                            <Button key="back" type="ghost" size="large" onClick={this.cancelModalHandler}>返 回</Button>,
                            <Button key="submit" type="primary" size="large" loading={this.state.uploading} onClick={this.okModalHandler}>
                            提 交
                            </Button>
                        ]}
                    >
                        <Row>
                            <Col span="18">
                                <Input type="text" onChange={e => this.setState({upload_data: {name: e.target.value}})} placeholder="输入一个好记的名字"/>
                            </Col>
                            <Col span="6">
                                <Upload ref="uploader" {...props} data={this.state.upload_data}>
                                    <Button type="ghost">
                                        <Icon type="upload" /> 选择文件
                                    </Button>
                                </Upload>
                            </Col>
                        </Row>
                    </Modal>
                </div>
            </div>
        )
    }
});

module.exports = IconPool;
