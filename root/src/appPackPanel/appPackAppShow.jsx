import { Form, Input, Row, Col, Button, Icon, Popover, Modal } from 'antd';

const React = require('react');
const Const = require("./../_utils/const");
const ajaxEx = require('./../_utils/ajaxConfiger');
const events = require('./../_utils/eventSubscriber');
const $ = require('jquery');
require('./appPackAppShow.scss');

const AppItem = React.createClass({
    getInitialState() {
        return {
            status:'打包中',
            statusKey:'pending',
            statusMapping:{'pending':'打包中','fail':'打包错误','success':'打包完成','running':'打包中'},
            timer:0
        }
    },
    componentDidMount() {
          var _this = this;
          this.fetchStatus();
          this.setState({timer:setInterval(function(){
               _this.fetchStatus();
          },60000)});
    },
    componentWillUnmount(){
        clearInterval(this.state.timer);
    },
    gotoUpdateApp(){
        this.props.gotoUpdatePage(this.props.appType);
        events.publish('initAppPackSubmit',this.props.data);
    },
    fetchStatus(){
        var _this = this,appTypeStr = (this.props.appType==0?"android":"ios");

        ajaxEx.get({url: Const.API.urlPrefix + 'apps/'+ appTypeStr +'/' + this.props.data.id +'/pack_status'},function(d){ 
            _this.setState({status:_this.state.statusMapping[d.status],statusKey:d.status});
        });
    },
    render() {
      return (
          <div className="app-info">
                <a className="app-edit-btn" onClick={this.gotoUpdateApp}>编辑</a>
                <p><img src={this.props.data.icon}/></p>
                <p className="app-name"><Icon type={this.props.appType==0?"android":"apple"} /><span>{this.props.data.name}</span></p>
                <p className="app-version">{this.props.data.version}</p>
                <hr />
                <p className="app-create-time"><span>创建于：</span>{new Date(this.props.data.created_at).toLocaleString()}</p>
                <p className="app-status">状态：<span className={this.state.statusKey}>{this.state.status}</span></p>
                <p><Button><a href={this.props.data.package_url}>下载安装</a></Button></p>
          </div>
      );
    }
});

const AppPackAppShow = React.createClass({
  getInitialState() {
      return {
          apps: this.props.apps,
          timer:0
      }
  },
  componentDidMount() {
      var _this = this;
      this.setState({timer:setInterval(function(){
           _this.props.refreshApp();
      },61000)});
  },
  componentWillUnmount(){
        clearInterval(this.state.timer);
  },
  gotoUpdatePage(appType){
        this.props.gotoStep(1,function(){
            this.setState({appType:appType,submitType:'put'});
            $('#appCreateForm').attr('method','put');
        });
  },
  gotoCreatePage(){
       var _this = this;
       $('#appCreateForm').attr('method','post');
       this.props.gotoStep(1,function(){
            this.setState({appType:_this.props.apps[0]?1:0,submitType:'post'});
            events.publish('resetAppPackSubmit');
       });
  },
  render() {
    return (
        <div className={"app-pack-app-show "+ this.props.aminateClass}>
              {(this.props.apps[0] && this.props.apps[1])?null:<Button type="primary" onClick={this.gotoCreatePage}>创建应用</Button>}
              {this.props.apps[0]?<div><AppItem appType={0} data={this.props.apps[0]} gotoUpdatePage={this.gotoUpdatePage}/><hr /></div>:null}
              {this.props.apps[1]?<AppItem appType={1} data={this.props.apps[1]} gotoUpdatePage={this.gotoUpdatePage}/>:null}
        </div>
    );
  }
});

module.exports = AppPackAppShow;
