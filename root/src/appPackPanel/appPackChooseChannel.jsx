import { Select,Button } from 'antd';
const Option = Select.Option;
const React = require('react');
const Const = require("./../_utils/const");
const ajaxEx = require('./../_utils/ajaxConfiger');
const $ = require('jquery');
require('./appPackChooseChannel.scss');

const AppPackChooseChannel = React.createClass({
  getInitialState() {
      return {
        
      }
  },
  componentDidMount() {

  },
  handleChannelChange(values){
     console.log(values);
  },
  nextStep(){
     this.props.nextStep();
  },
  render() {
    return (
        <div className={"app-pack-choose-channel "+ this.props.aminateClass}>
             <h1>选择平台</h1>
             <Select tags
                style={{ width: '100%' }}
                searchPlaceholder="点击添加渠道"
                onChange={this.handleChannelChange}>
                    <Option value="0">应用宝</Option>
                    <Option value="1">豌豆荚</Option>
              </Select>
              <div><Button type="primary" onClickCapture={this.nextStep}>提交</Button></div>
        </div>
    );
  }
});

module.exports = AppPackChooseChannel;
