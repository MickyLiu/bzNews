import { Form, Input, Row, Col, Button, Icon, Popover, Modal, Spin } from 'antd';

const React = require('react');
const Const = require("./../_utils/const");
const ajaxEx = require('./../_utils/ajaxConfiger');
const $ = require('jquery');
require('./appPackChoosePlatform.scss')

const AppPackChoosePlatform = React.createClass({
  getInitialState() {
      return {
          android:true,
          apple:false,
          loading:false
      }
  },
  choosePlatform(e){
      if(!$(e.currentTarget).hasClass('disabled'))
      {
         if($(e.currentTarget).attr('value')=='0')
         {
             this.setState({android:true,apple:false});
             this.props.changeAppType(0);
         }
         else
         {
             this.setState({apple:true,android:false});
             this.props.changeAppType(1);
         }
       }
  },
  componentDidMount() {

  },
  nextStep(){
     var _this =this;
     _this.props.nextStep();
  },
  render() {
    return (
       <div> 
         <Spin spining={this.state.loading}>
            <div className={"app-pack-choose-platform "+ this.props.aminateClass}>
                 <h1>选择平台</h1>
                 <div className="platform-type">
                    <div className={this.props.apps[0]?'disabled ':'' + (this.state.android?'chosen':'')} onClickCapture={this.choosePlatform} value="0">
                        <Icon type="android" />
                        <p>手机移动应用&nbsp;&nbsp;安卓</p>
                    </div>
                    <div className={this.props.apps[1]?'disabled ':'' + (this.state.apple?'chosen':'')} onClickCapture={this.choosePlatform} value="1">
                         <Icon type="apple" />
                         <p>手机移动应用&nbsp;&nbsp;IOS</p>
                    </div>
                 </div>
                 <div className="clearfix"></div>
                 <div><Button type="primary" onClickCapture={this.nextStep}>创建应用</Button></div>
            </div>
          </Spin>
        </div>
    );
  }
});

module.exports = AppPackChoosePlatform;
