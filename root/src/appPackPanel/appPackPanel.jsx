import { Form, Input, Row, Col, Button, Icon, Popover, Modal,Steps,Spin } from 'antd';

const React = require('react');
const Const = require("./../_utils/const");
const ajaxEx = require('./../_utils/ajaxConfiger');
const $ = require('jquery');
let AppPackWelcome = require('./appPackWelcome');
let AppPackChoosePlatform = require('./appPackChoosePlatform')
let AppPackSubmit = require('./appPackSubmit')
let AppPackChooseChannel = require('./appPackChooseChannel')
let AppPackAppShow = require('./appPackAppShow')
require('./appPackPanel.scss');

const Step = Steps.Step;
const StepsInfo = React.createClass({
   getInitialState() {
      return {
          steps: this.props.steps
      };
  },
  prev() {
     this.props.prev();
  },
  next() {
     this.props.next();
  },
  render() {
    return (
      <div className="app-pack-steps">
        <Steps current={this.props.current}>
          {this.state.steps.map((s, i) => <Step key={i} title={s} />)}
        </Steps>
        <div className={this.props.stepButton?'':'invisible'}>
          {this.props.current>0?<Button onClick={this.prev}>上一步</Button>:null}
          {this.props.current<this.props.steped?<Button onClick={this.next}>下一步</Button>:null}
        </div>
      </div>
    );
  }
});


const AppPackPanel = React.createClass({
  getInitialState() {
      return {
          welcome:false,
          steps:[1,0,0],
          stepsTitle:['选择平台','填写应用信息','打包'],
          currentStep:0,
          steped:0,
          appType:0,
          submitType:'post',
          apps:{},
          loading:false,
          stepButton:true,
          stepAnimate:['move-left-enter move-left-enter-active',
                       'move-right-enter',
                       'move-right-enter',
                       'move-right-enter']
      }
  },
  componentDidMount() {
      var _this = this; 
      var data= {};
      this.setState({
              loading:true
      });
      ajaxEx.get({url: Const.API.projectPrefix + '/get_app'},function(d){ 
          if(d.meta && d.meta.have_app)
          {
               data[0]= d.project_app.android_app;
               data[1]= d.project_app.ios_app;
               data[0]?(data[0].icon=data[0].icon.icon.url):'';
               data[1]?(data[1].icon=data[1].icon.icon.url):'';
               if(data[0])
               {
                  _this.setState({
                      apps:data,
                      stepButton:false
                  });
               }
               else{
                  if(data[1])
                  {
                     _this.setState({
                        apps:data,
                        appType:1,
                        stepButton:false
                     });
                  }
               }
               if(data[0] || data[1])
               {
                  _this.gotoStep(2)
               } 
          }else{
             _this.setState({
                welcome:true
             });
          }
      }).complete(function(){
         _this.setState({
              loading:false
         });
      });
  },
  changeAppType(appType){
       this.setState({appType:appType});
  },
  changeSubmitType(submitType){
       this.setState({submitType:submitType});
  },
  refreshApp(cb){
      var _this = this,appType=this.state.appType,data={}; 
      ajaxEx.get({url: Const.API.projectPrefix + '/get_app'},function(d){ 
               data[0]= d.project_app.android_app;
               data[1]= d.project_app.ios_app;
               data[0]?(data[0].icon=data[0].icon.icon.url):'';
               data[1]?(data[1].icon=data[1].icon.icon.url):'';

               if(typeof cb=="function") cb.call(_this,data[appType]);
               _this.setState({
                     apps:data
               });  
      });
  },
  nextStep(){
      this.gotoStep(this.state.currentStep+1);
  },
  prevStep(){
      this.gotoStep(this.state.currentStep-1);
  },
  gotoStep(step,cb){
     let steps = [0,0,0],oldCurrent=this.state.currentStep,currentStep = step,stepAnimate=this.state.stepAnimate,steped=this.state.steped;
     if(step>steped){
          steped=step;
     }
     steps[currentStep] = 1;
     oldCurrent>currentStep?(stepAnimate[oldCurrent] = 'move-right-leave move-right-leave-active',stepAnimate[currentStep] = 'move-left-enter move-left-enter-active')
                            :(stepAnimate[oldCurrent] = 'move-left-leave move-left-leave-active',stepAnimate[currentStep] = 'move-right-enter move-right-enter-active');
     this.setState({
          steps:steps,
          currentStep: currentStep,
          stepAnimate:stepAnimate,
          steped:steped
      });
     if(typeof cb=="function") cb.call(this);
  },
  render() {
    return (
        <div className="app-pack-panel">
           <Spin spining={this.state.loading}>
               {this.state.welcome?<AppPackWelcome />:
                   <div className={this.state.loading?'invisible':''}>
                       <StepsInfo stepButton={this.state.stepButton} current={this.state.currentStep} steped={this.state.steped} steps={this.state.stepsTitle} prev={this.prevStep} next={this.nextStep}/>
                       <AppPackChoosePlatform apps={this.state.apps} changeAppType={this.changeAppType} aminateClass={this.state.stepAnimate[0]} nextStep={this.nextStep}/>
                       <AppPackSubmit refreshApp={this.refreshApp} submitType={this.state.submitType} appType={this.state.appType} aminateClass={this.state.stepAnimate[1]} nextStep={this.nextStep}/>
                       <AppPackAppShow apps={this.state.apps} refreshApp={this.refreshApp} gotoStep={this.gotoStep} changeSubmitType={this.changeSubmitType} aminateClass={this.state.stepAnimate[2]} nextStep={this.nextStep}/>
                   </div>
               }
           </Spin>
        </div>
    );
  }
});

module.exports = AppPackPanel;

/*<AppPackChooseChannel aminateClass={this.state.stepAnimate[2]} nextStep={this.nextStep}/>*/
