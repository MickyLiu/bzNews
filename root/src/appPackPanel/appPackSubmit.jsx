import { Form, Input, Button, Icon, Spin,notification,Modal } from 'antd';

const React = require('react');
const Const = require("./../_utils/const");
const ajaxEx = require('./../_utils/ajaxConfiger');
const events = require('./../_utils/eventSubscriber');
const $ = require('jquery');
require('./appPackSubmit.scss');

const FormItem = Form.Item;
let AppPackSubmit = React.createClass({
  getInitialState() {
      return {
          submitLoading:false,
          packLoading:false,
          data:{}
      }
  },
  componentDidMount() {
       events.subscribe('resetAppPackSubmit',(function(){
               this.setState({
                  data: {}
               });
               $('#appCreateForm').resetForm();
       }).bind(this));

       events.subscribe('initAppPackSubmit',(function(data){
            this.setState({
               data: data
            });
       }).bind(this));

       $('.appIconFormItem').find('label').addClass('ant-form-item-required');
  },
  componentWillReceiveProps(nextProps){
         if(/active/.test(nextProps.aminateClass) && this.props.submitType=='post' && this.props.aminateClass!=nextProps.aminateClass){        
                  this.props.form.resetFields();
                  this.setState({
                      data: {}
                  });
         }
    },
  componentWillUnmount: function(){
       events.unsubscribe('resetAppPackSubmit');
       events.unsubscribe('initAppPackSubmit');
  },
  handleSubmit(e){
      e.preventDefault();
       var _this= this;
       this.props.form.validateFields((errors, values) => {
                if (!!errors) {
                  console.log('Errors in form!!!');
                  e.preventDefault();
                  return;
                }
            let url = '',msg='';

            if(_this.props.submitType=='post')
            {
                 if(!_this.state.data.icon) {
                      Modal.error({
                        title: '请选择app图标！',
                        content: ''
                      });
                     return;
                  }
                  msg = 'APP信息保存成功！需点击“提交打包”，才能生成APP！';
                  if(_this.props.appType==0)
                  {
                     url = Const.API.projectApp +'/android';
                  }
                  else
                  {
                       url = Const.API.projectApp +'/ios';
                  }
            }
            else
            {
                  msg = 'APP信息保存成功！需点击“提交打包”，才能同步到APP！';
                  if(_this.props.appType==0)
                  {
                     url = Const.API.urlPrefix + 'apps/android/' + _this.state.data.id;
                  }
                  else
                  {
                     url = Const.API.urlPrefix + 'apps/ios/' + _this.state.data.id;
                  }
            }

           _this.setState({submitLoading:true});
           ajaxEx.submit({url:url,formEle:$('#appCreateForm')[0],enabledErrorMsg:true},function(d){
                _this.props.refreshApp((d)=>{
                     _this.setState({submitLoading:false,data:d});
                      notification['success']({
                              message: msg,
                              description: ''
                      });
                });
           },function(){
                _this.setState({submitLoading:false});
           });
           return false;
      });
  },
  handlePack(e){
      var _this = this,appTypeStr = (this.props.appType==0?"android":"ios");;
      if(!$.isEmptyObject(this.state.data))
      {
          _this.setState({packLoading:true});
          ajaxEx.post({url:Const.API.urlPrefix +'apps/'+ appTypeStr +'/' + this.state.data.id + '/pack',enabledErrorMsg:true},function(d){
               _this.setState({packLoading:false});
               _this.props.nextStep();
                notification['success']({
                    message: '打包任务在后台运行中...',
                    description: '请耐心等待，注意查看app状态！'
                });
          });
      }
      else
      {
          Modal.error({
            title: '请先保存app信息再打包！',
            content: ''
          });
      }
  },
  handleInputChange(e) {
      var _this=this,data = this.state.data;
      var name = e.target.attributes['name'].value.match(/\[\S+\]/)[0].replace(/\[|\]/g,'');
     
      if(e.target.type=='file')
      {
          var fr = new FileReader(),fileEle = e.target;
          fr.readAsDataURL(e.target.files[0]);
          fr.onload = function(e) {
             //$('#app-icon-pack').attr('src',e.target.result);
             var img = document.createElement('img');
             img.src = e.target.result;
             if(img.naturalWidth!=1024 || img.naturalWidth!=1024)
             {
                  Modal.error({
                    title: '请选择1024*1024的png图片！',
                    content: ''
                  });
                  
                  fileEle.value='';
                  return;
             }
             else{
                  data[name]= e.target.result;
                  _this.setState({
                      data: data
                  });
             }
          };
      }
      else{
          data[name]= e.target.value;
          this.setState({
              data: data
          });
      }
  },
  render() {
    var _this = this;
    const { getFieldProps,getFieldError } = this.props.form;
    const keyP = {name:'应用名称',version:'版本号',description:'应用简介',image:'图标'};
    const rule = (key)=>{ 
        return {
          validate: [{
            rules: [
              { required: true, message: key + '不能为空'}
            ],
            trigger: 'onBlur',
          }],
          initialValue: 'demo'
      };
    };
    const nameProps = getFieldProps('name', rule(keyP['name']));
    const versionProps = getFieldProps('version', rule(keyP['version']));
    const descriptionProps = getFieldProps('description', rule(keyP['description']));
    return (
      <div> 
          <Spin spining={this.state.packLoading}>
              <div className={"app-pack-submit " + this.props.aminateClass}>
                  <h1>{this.props.appType==0?'安卓应用':'IOS应用'}</h1>
                  <Form horizontal id={'appCreateForm'} form={this.props.form} method={'post'}>
                      <FormItem
                        label="应用名称：" labelCol={{ span: 3 }} wrapperCol={{ span: 17 }}>
                        <Input placeholder="" {...nameProps}  name={(this.props.appType==0?'android_':'ios_') + 'app[name]'} value={this.state.data.name} onChange={this.handleInputChange}/>
                      </FormItem>

                      <FormItem label="应用简介：" labelCol={{ span: 3 }} wrapperCol={{ span: 17 }}>
                        <Input name={(this.props.appType==0?'android_':'ios_') + "app[description]"} type="textarea" {...descriptionProps} id="description" value={this.state.data.description} onChange={this.handleInputChange}/>
                      </FormItem>

                      <FormItem
                        label="图标：" labelCol={{ span: 3 }} wrapperCol={{ span: 17 }} className="appIconFormItem">
                        <img id="app-icon-pack" src={this.state.data.icon}/>
                        <div>
                          <div>1024*1024</div>
                          <a className="file-upload">
                             选择文件
                             <Input type="file" name={(this.props.appType==0?'android_':'ios_') + 'app[icon]'} onChange={this.handleInputChange} />
                          </a>
                        </div>
                      </FormItem>
                     
                      <FormItem wrapperCol={{ offset: 3 }}>
                          <Button loading={this.state.submitLoading} type="primary" onClick={this.handleSubmit}>保存</Button>
                          <Button type="primary" onClick={this.handlePack}>提交打包</Button>
                      </FormItem>
                  </Form>
              </div>
          </Spin>    
      </div>
    );
  }
});

AppPackSubmit = Form.create()(AppPackSubmit);
module.exports = AppPackSubmit;
