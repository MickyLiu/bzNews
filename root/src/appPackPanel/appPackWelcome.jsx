import { Form, Input, Row, Col, Button, Icon, Popover, Modal } from 'antd';

const React = require('react');
const Const = require("./../_utils/const");
const ajaxEx = require('./../_utils/ajaxConfiger');
const $ = require('jquery');
require('./appPackWelcome.scss')

const AppPackWelcome = React.createClass({
  getInitialState() {
      return {
        
      }
  },
  componentDidMount() {

  },
  render() {
    return (
        <div className="app-pack-welcome">
            创建移动应用需预先进行商务沟通，我们的联系方式是：xxx@baozou.com。欢迎合作！
        </div>
    );
  }
});

module.exports = AppPackWelcome;
