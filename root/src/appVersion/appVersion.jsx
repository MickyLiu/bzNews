import {Form, Input, Button} from 'antd';
let React = require('react');
let Const = require('../_utils/const');
let events = require('../_utils/eventSubscriber');
let ajaxEx = require('../_utils/ajaxConfiger');
require('./appVersion.scss');

const FormItem = Form.Item;

const AppVersion = React.createClass({
  getInitialState() {
    return {
      versionNum: '0.0',
      description: '',
      sourceUrl: '',
      editable: false,
      id: '',
      errInfo: ''
    };
  },
  componentDidMount() {
    let _this = this;
    ajaxEx.get({url: Const.API.appVersion}, function(d){
      _this.setState({id: d.id, versionNum: d.version, description: d.description, sourceUrl: d.package_url});
    });
  },
  handleVersionChange(e) {
    this.setState({versionNum: e.target.value.trim()});
  },
  handledescriptionChange(e) {
    this.setState({description: e.target.value.trim()});
  },
  handleSourceUrlChange(e) {
    this.setState({sourceUrl: e.target.value.trim()});
  },
  versionSubmit(e) {
    e.preventDefault();
    let _this = this;
    if(this.state.editable) {
      if(this.state.version === '' || this.state.description === '' || this.state.sourceUrl === '') {
        this.setState({errInfo: '填写内容不完整，请补充'});
        return;
      }
      let data = {
        android_version: {
          version: this.state.version,
          description: this.state.description,
          package_url: this.state.sourceUrl
        }
      }
      ajaxEx.put({url: Const.API.updateAppVersion + this.state.id, form: data, enabledLoadingMsg:true, enabledErrorMsg:true}, function(d) {
        _this.setState({editable: false, errInfo: ''});

      });
    } else {
      this.setState({editable: true});
    }
  },
  render() {
    return (
      <div >
        <h1 className='app-version-title'>App版本管理</h1>
        <Form horizontal className='app-version-form'>
          <p className='app-version-err'>{this.state.errInfo}</p>
          <FormItem label="版本号:" required>
            <Input type='text' value={this.state.versionNum} onChange={this.handleVersionChange} disabled={!this.state.editable} />
          </FormItem>
          <FormItem label="版本描述:" required>
            <Input type="textarea" value={this.state.description} onChange={this.handledescriptionChange} disabled={!this.state.editable} />
          </FormItem>
          <FormItem label="安装包地址:" required>
            <Input type='text' value={this.state.sourceUrl} onChange={this.handleSourceUrlChange} disabled={!this.state.editable} />
          </FormItem>
          <FormItem>
            <Button type="primary" onClick={this.versionSubmit}>{this.state.editable ? '保存': '编辑'}</Button>
          </FormItem>
        </Form>
      </div>
    );
  }
});

module.exports = AppVersion;
