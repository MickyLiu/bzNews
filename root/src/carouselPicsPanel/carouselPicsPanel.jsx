let React = require('react');
let PanelTopBar = require('./panelTopBar/panelTopBar');
let PicsEditTable = require('./picsEditTable/picsEditTable');
let CreateBannerDialog = require('./createBannerDialog/createBannerDialog');

const carouselPicsPanel = React.createClass({
  render() {
    return (
      <div>
        <PanelTopBar />
        <PicsEditTable />
        <CreateBannerDialog />
      </div>
    );
  }
});

module.exports = carouselPicsPanel;
