import { Form, Input, Button, message } from 'antd';

let React = require('react');
let ajaxEx = require('./../../_utils/ajaxConfiger');
let events = require('./../../_utils/eventSubscriber');
let Const = require('./../../_utils/const');
require('./panelTopBar.scss');

const FormItem = Form.Item;
const PanelTopBar = React.createClass({
  getInitialState() {
     return {
       value: "",
       loading: false
     };
  },
  componentDidMount() {
    let _this = this;
    ajaxEx.get({url:Const.API.projectBanners + "/limit_number"},function(d){
      _this.setState({value: d.meta.limit_number});
    });
  },
  inputChange(e) {
    this.setState({value: e.target.value.trim()});
  },
  submitLimitNum(e) {
    e.preventDefault();

    let _this = this;
    let data = {limit_number: _this.state.value};
    ajaxEx.put({url:Const.API.projectBanners + '/change_limit_number', form: data},function(d){
      _this.setState({
        loading: false
      });
      message.success('更新成功', 3);
    },function(){
      message.error('更新出错,请稍候再试', 3);
    },function(d){
      _this.setState({
          loading: true
        });
    });
  },
  createBanner() {
    events.publish("showBannerDialog");
  },
  render() {
    return (
      <header className="carousel-pics-topbar">
        <h1 className="panel-title">轮播图</h1>
        <Button type="primary" className="btn" onClick={this.createBanner}>添加</Button>
        <Form inline>
           <FormItem label="数量限制：">
             <Input value={this.state.value} onChange={this.inputChange}/>
           </FormItem>
           <Button type="primary" onClick={this.submitLimitNum} loading={this.state.loading}>确定</Button>
        </Form>
      </header>
    );
  }
});

module.exports = PanelTopBar;
