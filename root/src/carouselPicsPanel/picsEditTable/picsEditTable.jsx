import { Table, Popconfirm, Select, Input} from 'antd';

let React = require('react');
let events = require('./../../_utils/eventSubscriber');
let ajaxEx = require('./../../_utils/ajaxConfiger');
let Const = require('./../../_utils/const');
let Preloader = require('./../../components/imagePreloader/imagePreloader');

const Option = Select.Option;

const EditButton = React.createClass({
  editBanner() {
    let _this = this;
    events.publish("showBannerDialog");
    setTimeout(function() {
      events.publish("editBanner",_this.props.bannerInfo);
    },100);
  },
  render() {
    return (
      <a href="javascript:void(0)" onClick={this.editBanner}>编辑</a>
    );
  }
});

const MoveUpButton = React.createClass({
  moveUp() {
    events.publish("recordMoveUp",this.props.bannerId);
  },
  render() {
    return (
      <a href="javascript:void(0)" onClick={this.moveUp}>上移</a>
    );
  }
});

const MoveDownButton = React.createClass({
  moveDown() {
    events.publish("recordMoveDown",this.props.bannerId);
  },
  render() {
    return (
      <a href="javascript:void(0)" onClick={this.moveDown}>下移</a>
    );
  }
});

const columns = [
  {
    title: 'ID',
    dataIndex: 'id',
    key: 'id'
  },{
    title: '类型',
    dataIndex: 'kind',
    key: 'kind',
    render(text) {
      if(text === "video") {
        return "视频";
      } else {
        return "推广链接";
      }
    }
  },{
    title: '视频ID',
    dataIndex: 'video_id',
    key: 'video_id'
  }, {
    title: '推广页面地址',
    key: 'source_url',
    dataIndex: 'source_url'
  }, {
    title: '图片',
    dataIndex: 'image',
    key: 'image',
    render(text) {
      return (<Preloader className="thumbnail" url={text} />);
    }
  },{
    title: '描述',
    dataIndex: 'description',
    key: 'description'
  },{
    title: '目标设备',
    dataIndex: 'device_type',
    key: 'device_type',
    render(text) {
      if(text === "whole"){
        return "全部";
      } else if(text === "ios") {
        return "苹果"
      } else {
        return "安卓"
      }
    }
  },{
    title: '操作',
    key: 'operation',
    render(text, record) {
      return (
        <span>
          <MoveUpButton bannerId={record.id}/>
          <span className="ant-divider"></span>
          <MoveDownButton bannerId={record.id}/>
          <span className="ant-divider"></span>
          <EditButton bannerInfo={record} />
          <span className="ant-divider"></span>
          <Popconfirm title="确定要删除这个轮播图么？" bannerId={record.id} onConfirm={delConfirm}>
            <a href="#">删除</a>
          </Popconfirm>
        </span>
      );
    }
  }];



let delConfirm =  function(){
  var _this =this;
  ajaxEx.delete({url:Const.API.banners+'/'+ _this.props.bannerId,enabledLoadingMsg:true,enabledErrorMsg:true},function(d){
    events.publish('BannersDataChange', _this.props.bannerId);
  });
};

const PicsEditTable = React.createClass({
  getInitialState() {
    return{
        dataSource: [],
        loading: true,
        timeoutSeed: ""
    };
  },
  componentDidMount() {
    let _this = this;
    ajaxEx.get({url:Const.API.projectBanners},function(d){
      let data = [];
      for(let i=0; i< d.data.length; i++) {
        data.push(d.data[i].attributes);
        data[i].id = d.data[i].id;
      }
      _this.setState({
         dataSource : data,
         loading:false
      });
    });

    events.subscribe("BannersDataChange",(function(id) {
      let dataSource = this.state.dataSource;
      for(let i in dataSource) {
        if(dataSource[i].id === id) {
          dataSource.splice(i, 1);
          this.setState({dataSource: dataSource});
          break;
        }
      }
    }).bind(this));

    events.subscribe("refreshTable",(function() {
      let _this = this;
      ajaxEx.get({url:Const.API.projectBanners},function(d){
        let data = [];
        for(let i=0; i< d.data.length; i++) {
          data.push(d.data[i].attributes);
          data[i].id = d.data[i].id;
        }
        _this.setState({
           dataSource : data,
           loading:false
        });
      });
    }).bind(this));

    events.subscribe("recordMoveUp",(function(id) {
      let dataSource = this.state.dataSource;
      let _this = this;
      if(dataSource[0].id === id) {
        return;
      }
      for(let i=1; i< dataSource.length; i++) {
        if(dataSource[i].id === id) {
          let temp = dataSource[i];
          dataSource[i] = dataSource[i-1];
          dataSource[i-1] = temp;
          break;
        }
      }
      if(this.state.timeoutSeed) {
        clearTimeout(this.state.timeoutSeed);
      }
      let seed = setTimeout(function(){
        _this.updateBannerPriority(dataSource);
      },5000);
      this.setState({dataSource: dataSource, timeoutSeed: seed});
    }).bind(this));
    events.subscribe("recordMoveDown",(function(id) {
      let dataSource = this.state.dataSource;
      let _this =this;
      if(dataSource[dataSource.length-1].id === id) {
        return;
      }
      for(let i=0; i< dataSource.length-1; i++) {
        if(dataSource[i].id === id) {
          let temp = dataSource[i];
          dataSource[i] = dataSource[i+1];
          dataSource[i+1] = temp;
          break;
        }
      }
      if(this.state.timeoutSeed) {
        clearTimeout(this.state.timeoutSeed);
      }
      let seed = setTimeout(function(){
        _this.updateBannerPriority(dataSource);
      },5000);
      this.setState({dataSource: dataSource, timeoutSeed: seed});
    }).bind(this));
  },
  updateBannerPriority(dataSource) {
    let data = {ids:[]};
    for(let i=0; i< dataSource.length; i++) {
      data.ids.push(dataSource[i].id);
    }
    ajaxEx.put({url:Const.API.projectBanners + "/update_priority", form:data},function() {
      console.log("priority change success!");
    });
    this.setState({timeoutSeed: ""});
  },
  componentWillUnmount: function(){
    events.unsubscribe('BannersDataChange');
    events.unsubscribe('refreshTable');
    events.unsubscribe('recordMoveUp');
    events.unsubscribe('recordMoveDown');
  },
  render() {
    return (
      <div>
        <Table id="bannersTable" columns={columns} dataSource={this.state.dataSource} loading={this.state.loading} pagination={false}/>
      </div>
    );
  }
});

module.exports = PicsEditTable;
