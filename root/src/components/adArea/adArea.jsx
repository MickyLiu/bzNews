let React = require('react');
import {Index as CanvasPic} from './../../lib/tsdist/index/index';
require('./adArea.scss');
require('./sprite.scss');

const AdArea = React.createClass({
  getInitialState: function() {
    return {
      searchText: '',
    }
  },
  getDefaultProps: function() {
    return {
      searchShow: true,
      login: false,
    };
  },
  handleSearch: function(e) {
    if (this.state.searchText.length == 0) {
      return;
    }
    let url = '';
    if (this.props.login) {
      url = `/?q=${encodeURI(this.state.searchText)}#/searchAuthor`;
    } else {
      url = `/searchAuthor?q=${encodeURI(this.state.searchText)}`;
    }
    window.open(url, "_blank");
  },
  render: function() {
    let img = document.createElement("img");
    img.src = require("./sprite.png");
    return (
      <div>
        <div className="search-area">
          <div className="home-canvas">
            <CanvasPic />
          </div>
          <div className="search-info" style={(!this.props.searchShow) ? {display:'none'} : {}}>
            <p>为视频内容创作者提供的专业推广联盟</p>
            <div className="search-form">
              <input className="search-input" placeholder="输入作者名查询" onChange={(e) => this.setState({searchText: e.target.value.trim()})}></input>
              <button className="search-btn" onClick={this.handleSearch}>搜索</button>
            </div>
          </div>
        </div>
        <div className="users-show" style={(!this.props.searchShow) ? {display:'none'} : {}}>
          <p className="ad-title">他们可能都会用</p>
          <p className="ad-description">1,000,000视频创作者的选择</p>
          <ul className="user-list">
            <li className="icon icon-baozou"></li>
            <li className="icon icon-bagua"></li>
            <li className="icon icon-big"></li>
            <li className="icon icon-chengxiang"></li>
            <li className="icon icon-feidie"></li>
            <li className="icon icon-guamo"></li>
            <li className="icon icon-hexiangu"></li>
            <li className="icon icon-huaxiubang"></li>
            <li className="icon icon-niunan"></li>
            <li className="icon icon-papi"></li>
          </ul>
        </div>
      </div>
    );
  }
});

module.exports = AdArea;
