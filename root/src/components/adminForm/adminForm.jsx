import { Button, Form, Input, message } from 'antd';

let React = require('react');
let ajaxEx = require('./../../_utils/ajaxConfiger');
let Const = require('./../../_utils/const');
require('./adminForm.scss');
const createForm = Form.create;
const FormItem = Form.Item;

function noop() {
  return false;
}

class AdminForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {loading: false};
  }

  handleSubmit(e) {
    e.preventDefault();
    let _this = this;
    this.props.form.validateFields((errors, values) => {
      if (!!errors) {
        console.log('表单中包含错误！');
        message.error('表单中包含错误！');
        return;
      }
      let data = {};
      data.administrator = values;
      data.administrator.role = "general";
      let url = (this.props.id === "") ? Const.API.admins : Const.API.admins + "/" + this.props.id;
      let method = (this.props.id === "") ? "post" : "put";
      ajaxEx[method]({url:url, form:data},function(d){
          _this.setState({
            loading: false
          });
          message.success('用户创建成功！', 3);
          _this.props.form.resetFields();
        },function(d){
          message.error('创建出错,请稍候再试', 3);
        },function(d){
        _this.setState({
            loading: true
          });
        });
      });
  }

  checkPass(rule, value, callback) {
    const { validateFields } = this.props.form;
    if (value) {
      let formatValidate = false;
      formatValidate = /^(?=.*[A-Z])(?=.*[!@#$&*])(?=.*[0-9])(?=.*[a-z]).{6,}/.test(value);
      if(!formatValidate) {
        callback('密码长度最少为6位, 至少包含一个数字, 一个大写字母, 一个小写字母、一个符号:! @ # $ & *');
      }
      validateFields(['password_confirmation'], { force: true });
    }
    callback();
  }

  checkPass2(rule, value, callback) {
    const { getFieldValue } = this.props.form;
    if (value && value !== getFieldValue('password')) {
      callback('两次输入密码不一致！');
    } else {
      callback();
    }
  }

  render() {
    const { getFieldProps, getFieldError, isFieldValidating } = this.props.form;
    const nameProps = getFieldProps('name', {
      rules: [
        { required: true }
      ],
    });
    const emailProps = getFieldProps('email', {
      validate: [{
        rules: [
          { required: true }
        ],
        trigger: ['onBlur']
      }, {
        rules: [
          { type: 'email', message: '请输入正确的邮箱地址' },
        ],
        trigger: ['onBlur', 'onChange'],
      }]
    });
    const passwdProps = getFieldProps('password', {
      rules: [
        { required: true, whitespace: true, min: 6, message: '请填写密码' },
        { validator: this.checkPass.bind(this) },
      ],
    });
    const rePasswdProps = getFieldProps('password_confirmation', {
      rules: [{
        required: true,
        whitespace: true,
        message: '请再次输入密码',
        min: 6
      }, {
        validator: this.checkPass2.bind(this),
      }],
    });
    const formItemLayout = {
      labelCol: { span: 3 },
      wrapperCol: { span: 7 },
    };
    return (
      <Form horizontal form={this.props.form}>

        <FormItem
          {...formItemLayout}
          label="用户名："
          hasFeedback
          help={isFieldValidating('name') ? '校验中...' : (getFieldError('name') || []).join(', ')}>
          <Input {...nameProps} />
        </FormItem>

        <FormItem
          {...formItemLayout}
          label="邮箱："
          hasFeedback>
          <Input {...emailProps} type="email" />
        </FormItem>

        <FormItem
          {...formItemLayout}
          label="密码："
          hasFeedback>
          <Input {...passwdProps} type="password" autoComplete="off"
            onContextMenu={noop} onPaste={noop} onCopy={noop} onCut={noop} />
        </FormItem>

        <FormItem
          {...formItemLayout}
          label="确认密码："
          hasFeedback>
          <Input {...rePasswdProps} type="password" autoComplete="off" placeholder="两次输入密码保持一致"
            onContextMenu={noop} onPaste={noop} onCopy={noop} onCut={noop} />
        </FormItem>
        <FormItem wrapperCol={{ span: 5, offset: 3 }}>
          <Button type="primary" onClick={this.handleSubmit.bind(this)} loading={this.state.loading}>确定</Button>
        </FormItem>
      </Form>
    );
  }
}


class CustomerFormEdit extends AdminForm {
  constructor(props) {
    super(props);
    this.state = {loading: false,data:{}};
  }

  componentDidMount() {
       var _this = this; 
       ajaxEx.get({url:Const.API.customers +'/'+ this.props.id},function(d){
            _this.setState({data:d.data.attributes});
       });
  }

  handleSubmit(e) {
    e.preventDefault();
    let _this = this;
    this.props.form.validateFields((errors, values) => {
        if (!!errors) {
          console.log('表单中包含错误！');
          message.error('表单中包含错误！');
          return;
        }
        _this.setState({
            loading: true
        });
        let data = {};
        data.customer = values;

        let url = Const.API.customers + "/" + this.props.id;

        ajaxEx.submit({url:url,formEle:$('#user-form')[0],enabledErrorMsg:true},function(d){
                    _this.setState({
                      loading: false
                    });
                    message.success('用户信息更改成功！', 3);
                    _this.props.form.resetFields();
          },function(){
               _this.setState({
                 loading: false
               });
          });

      });
  }

  handleInputChange(e) {
      var data = this.state.data;
      data[e.target.attributes['id'].value]= e.target.value;
      this.setState({
          data: data
      });

      if(e.target.type=='file')
      {
          var fr = new FileReader();
          fr.readAsDataURL(e.target.files[0]);
          fr.onload = function(e) {
             $('.user-avatar-icon').attr('src',e.target.result);
          };
      }
  }

  render() {
    var _this = this;
    const { getFieldProps, getFieldError, isFieldValidating } = this.props.form;
    const nameProps = getFieldProps('name', {
      rules: [
        { required: true }
      ],
      initialValue: _this.state.data.name
    });
    const passwdProps = getFieldProps('password', {
      rules: [
        { required: false, whitespace: true, min: 6, message: '请填写密码' },
        { validator: this.checkPass.bind(this) },
      ],
    });
    const rePasswdProps = getFieldProps('password_confirmation', {
      rules: [{
        required: false,
        whitespace: true,
        message: '请再次输入密码',
        min: 6
      }, {
        validator: this.checkPass2.bind(this),
      }],
    });
    const formItemLayout = {
      labelCol: { span: 3 },
      wrapperCol: { span: 7 },
    };
    return (
      <Form horizontal form={this.props.form} method={'put'} id="user-form">
         <FormItem
                label={<img className="user-avatar-icon" src={this.state.data.avatar_url} />} {...formItemLayout} className="user-avatar-item"> 
                <p><a className="file-upload">
                   选择新头像
                   <Input type="file" id={'customer_avatar'} name={'customer[avatar]'} onChange={this.handleInputChange.bind(this)}/>
                </a></p>
                <p>你可以选择png/jpg图（100*100）作为头像</p>
        </FormItem>

        <FormItem
          {...formItemLayout}
          label="用户名："
          hasFeedback
          help={isFieldValidating('name') ? '校验中...' : (getFieldError('name') || []).join(', ')}>
          <Input {...nameProps} name={'customer[name]'} value={this.state.data.name} onChange={this.handleInputChange.bind(this)}/>
        </FormItem>

        <FormItem
          {...formItemLayout}
          label="邮箱："
          hasFeedback>
          <Input type="email" value={this.state.data.email} disabled />
        </FormItem>

        <FormItem
          {...formItemLayout}
          label="密码："
          hasFeedback>
          <Input {...passwdProps} type="password" name={'customer[password]'} autoComplete="off"
            onContextMenu={noop} onPaste={noop} onCopy={noop} onCut={noop} />
        </FormItem>

        <FormItem
          {...formItemLayout}
          label="确认密码："
          hasFeedback>
          <Input {...rePasswdProps} type="password" name={'customer[password_confirmation]'} autoComplete="off" placeholder="两次输入密码保持一致"
            onContextMenu={noop} onPaste={noop} onCopy={noop} onCut={noop} />
        </FormItem>
        <FormItem wrapperCol={{ span: 12, offset: 3 }}>
          <Button type="primary" style={{width:'300px',backgroundColor:'#2fa9be',borderColor:'#2fa9be'}} onClick={this.handleSubmit.bind(this)} loading={this.state.loading}>保存</Button>
        </FormItem>
      </Form>
    );
  }
}


AdminForm = createForm()(AdminForm);
CustomerFormEdit = createForm()(CustomerFormEdit);

module.exports = {AdminForm:AdminForm,CustomerFormEdit:CustomerFormEdit};
