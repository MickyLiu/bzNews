const React = require('react');
import { DatePicker, InputNumber, Tabs, Select, Switch } from 'antd';
const RangePicker = DatePicker.RangePicker;
const TabPane = Tabs.TabPane;
require('./datetimePicker.scss');

// time helper
var timeHelper = {
  second: 1000,
  minute: 1000 * 60,
  hour: 1000 * 60 * 60,
  day: 1000 * 60 * 60 * 24,
  week: 1000 * 60 * 60 * 24 * 7,
  month: 1000 * 60 * 60 * 24 * 30,
  // compute
  computeDay: function (date, val) {
    date = new Date(date);
    return new Date(date.getTime() + this.day * val);
  },
  computeWeek: function (date, val) {
    return this.computeDay(date, val * 7);
  },
  computeMonth: function (date, val) {
    date = new Date(date);
    let tag = val / Math.abs(val);
    val = Math.abs(val);
    for (var i = 0; i < val; i++) {
      date.setMonth(date.getMonth() + tag);
    }
    return date;
  },
  // floor
  floorDay: function (date) {
    date = new Date(date);
    return new Date(date.toString().replace(/\d\d:\d\d:\d\d/, '00:00:00'));
  },
  floorWeek: function (date) {
    date = new Date(date);
    return this.floorDay(this.computeDay(date, 1 - date.getDay()));
  },
  floorMonth: function (date) {
    date = new Date(date);
    return this.floorDay(date.setDate(1));
  },
  // seil
  ceilDay: function (date) {
    return this.floorDay(this.computeDay(date, 1));
  },
  ceilWeek: function (date) {
    return this.floorWeek(this.computeWeek(date, 1));
  },
  ceilMonth: function (date) {
    return this.floorMonth(this.computeMonth(date, 1));
  },
  // format
  pad2(x) {
    return (x < 10 ? '0' : '') + x;
  },
  easyFormat(date, format) {
    format = format || '%y-%m-%d';
    format = format.replace(/%%/g, '%% ')
      .replace(/%y/g, date.getFullYear())
      .replace(/%m/g, this.pad2(date.getMonth() + 1))
      .replace(/%d/g, this.pad2(date.getDate()))
      .replace(/%H/g, this.pad2(date.getHours()))
      .replace(/%M/g, this.pad2(date.getMinutes()))
      .replace(/%S/g, this.pad2(date.getSeconds()))
      .replace('%% ', '%');
    return format;
  },
  // data span
  spanString(start, end, format) {
    // %d,%H,%M,%S
    format = format || {
      d: '%d天',
      H: '%H小时',
      M: '%M分',
      S: '%S秒'
    }
    let span = ['S', 'M', 'H', 'd'];
    span.d = span.H = span.M = 0;
    span.S = (end - start) / 1000;
    let max = format.d ? span.indexOf('d') :
              format.H ? span.indexOf('H') :
              format.M ? span.indexOf('M') :
              format.S ? span.indexOf('S') : -1;
    if (max == -1) {
      return '';
    }
    for (var i = 0; i < max; i++) {
      let sign = span[i]; // S M H d
      let nextSign = span[i + 1];
      let ary = (sign == 'S' || sign == 'M') ? 60 : 24;
      span[nextSign] = Math.floor(span[sign] / ary);
      span[sign] = span[sign] % ary;
    }
    let ret = '';
    for (var i = max; i >= 0; i--) {
      let sign = span[i];
      if (span[sign] == 0) {
        continue;
      }
      ret += format[sign].replace('%' + sign, span[sign]);
    }
    return ret;
  }
}

var DatetimePicker = React.createClass({
  getInitialState() {
    let ret = {
      mode: 0, // 0: quick 1: relative 2: absolute
      relativeScale: timeHelper.day,
      relativeNumber: 3,
      relativeDate: null,
      relativeKey: '',
      relativeFloor: false,
      absoluteStart: null,
      absoluteEnd: null,
      defaultValue: [null, null]
    };
    if (this.props.defaultValue) {
      ret.mode = 2;
      ret.defaultValue = [this.props.defaultValue.start, this.props.defaultValue.end];
    }
    return ret;
  },
  componentDidMount() {
  },
  // mode handle
  chooseQuick(datestart, dateend, e) {
    if (this.props.onChoose) {
      this.props.onChoose({
        start: datestart,
        end: dateend,
        key: e.target.innerHTML,
        mode: 0
      });
    }
  },
  chooseRelative(scale, num, floor) {
    let date = new Date((new Date()).getTime() - scale * num);
    if (floor) {
      date = timeHelper.floorDay(date);
    }
    let str = `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()} ${date.toString().match(/\d\d:\d\d:\d\d/)[0]}`
    this.setState({
      relativeDate: date,
      relativeKey: `从 ${str} 到现在`
    });
    if (this.props.onChoose) {
      this.props.onChoose({
        start: date,
        end: new Date(),
        key: `从 ${str} 到现在`,
        mode: 1
      });
    }
  },
  chooseAbsolute(date) {
    let str = '';
    let datestart = date[0];
    let dateend = date[1];
    if (datestart && dateend) {
      str += `${datestart.getFullYear()}-${datestart.getMonth() + 1}-${datestart.getDate()} ${datestart.toString().match(/\d\d:\d\d:\d\d/)[0]}`;
      str += ' 到 ';
      str += `${dateend.getFullYear()}-${dateend.getMonth() + 1}-${dateend.getDate()} ${dateend.toString().match(/\d\d:\d\d:\d\d/)[0]}`;
    }
    this.setState({
      absoluteStart: datestart,
      absoluteEnd: dateend
    })
    if (this.props.onChoose) {
      this.props.onChoose({
        start: datestart,
        end: dateend,
        key: str,
        mode: 2
      });
    }
  },
  // event
  handleTabChange(mode) {
    this.setState({
      mode: mode
    });
    if (mode == 1) {
      this.chooseRelative(this.state.relativeScale, this.state.relativeNumber, this.state.relativeFloor);
    };
    if (mode == 2) {
      this.chooseAbsolute([this.state.absoluteStart, this.state.absoluteEnd]);
    };
    if (this.props.onTabChange) {
      this.props.onTabChange(mode);
    };
  },
  handleRelativeSelectChange(val) {
    this.setState({
      relativeScale: val
    });
    this.chooseRelative(val, this.state.relativeNumber, this.state.relativeFloor);
  },
  handleRelativeInputChange(val) {
    this.setState({
      relativeNumber: val
    });
    this.chooseRelative(this.state.relativeScale, val, this.state.relativeFloor);
  },
  handleRelativeFloorChange(val) {
    this.setState({
      relativeFloor: val
    });
    this.chooseRelative(this.state.relativeScale, this.state.relativeNumber, val);
  },
  render() {
    return (
      <div className="datetime-picker">
        <Tabs tabPosition="left" onChange={this.handleTabChange}>
          <TabPane tab="快速选择" key="0">
            <div className="datetimep-quick-content">
              <div className="datetimep-quick-col">
                <ul>
                  <li onClick={e => this.chooseQuick(timeHelper.floorDay(Date()), timeHelper.ceilDay(Date()), e) }>今天</li>
                  <li onClick={e => this.chooseQuick(timeHelper.floorWeek(Date()), timeHelper.ceilWeek(Date()), e) }>本周</li>
                  <li onClick={e => this.chooseQuick(timeHelper.floorMonth(Date()), timeHelper.ceilMonth(Date()), e) }>本月</li>
                  <li onClick={e => this.chooseQuick(timeHelper.floorDay(Date()), new Date(), e) }>今天到现在</li>
                  <li onClick={e => this.chooseQuick(timeHelper.floorWeek(Date()), new Date(), e) }>本周到今天</li>
                  <li onClick={e => this.chooseQuick(timeHelper.floorMonth(Date()), new Date(), e) }>本月到今天</li>
                </ul>
              </div>
              <div className="datetimep-quick-col">
                <ul>
                  <li onClick={e => {
                    let date = timeHelper.computeDay(Date(), -1);
                    this.chooseQuick(timeHelper.floorDay(date), timeHelper.ceilDay(date), e)
                  } }>昨天</li>
                  <li onClick={e => {
                    let date = timeHelper.computeDay(Date(), -2);
                    this.chooseQuick(timeHelper.floorDay(date), timeHelper.ceilDay(date), e)
                  } }>前天</li>
                  <li onClick={e => {
                    let date = timeHelper.computeDay(Date(), -7);
                    this.chooseQuick(timeHelper.floorDay(date), timeHelper.ceilDay(date), e)
                  } }>上周同一天</li>
                  <li onClick={e => {
                    let date = timeHelper.computeWeek(Date(), -1);
                    this.chooseQuick(timeHelper.floorWeek(date), timeHelper.ceilWeek(date), e)
                  } }>上周</li>
                  <li onClick={e => {
                    let date = timeHelper.computeMonth(Date(), -1);
                    this.chooseQuick(timeHelper.floorMonth(date), timeHelper.ceilMonth(date), e)
                  } }>上个月</li>
                </ul>
              </div>
              <div className="datetimep-quick-col">
                <ul>
                  <li onClick={e => {
                    this.chooseQuick(timeHelper.computeDay(Date(), -1), new Date(), e)
                  } }>最近24小时</li>
                  <li onClick={e => {
                    this.chooseQuick(timeHelper.computeDay(Date(), -2), new Date(), e)
                  } }>最近48小时</li>
                  <li onClick={e => {
                    this.chooseQuick(timeHelper.computeDay(Date(), -7), new Date(), e)
                  } }>最近7天</li>
                  <li onClick={e => {
                    this.chooseQuick(timeHelper.computeDay(Date(), -15), new Date(), e)
                  } }>最近15天</li>
                  <li onClick={e => {
                    this.chooseQuick(timeHelper.computeDay(Date(), -30), new Date(), e)
                  } }>最近30天</li>
                  <li onClick={e => {
                    this.chooseQuick(timeHelper.computeDay(Date(), -60), new Date(), e)
                  } }>最近60天</li>
                </ul>
              </div>
            </div>
          </TabPane>
          <TabPane tab="从...到现在" key="1">
            <div className="datetimep-relative-content">
              <div className="datetimep-relative-text">{this.state.relativeKey}</div>
              <div className="datetimep-relative-input">
                <InputNumber min={1} max={30} style={{ width: 100, margin: '0 25px 0 0' }} defaultValue={this.state.relativeNumber} onChange={this.handleRelativeInputChange}/>
                <Select size="large" defaultValue={this.state.relativeScale} style={{ width: 80 }} onChange={this.handleRelativeSelectChange}>
                  <Option value={timeHelper.hour}>小时前</Option>
                  <Option value={timeHelper.day}>天前</Option>
                  <Option value={timeHelper.week}>周前</Option>
                </Select>
              </div>
              <div>
                <Switch checkedChildren="开" unCheckedChildren="关" onChange={this.handleRelativeFloorChange}/><span style={{ marginLeft: 10, verticalAlign: 'middle' }}>日期取整</span>
              </div>
            </div>
          </TabPane>
          <TabPane tab="自定义选择" key="2">
            <div style={{ fontWeight: 'bold', marginBottom: '40px' }}>请选择：</div>
            <RangePicker ref='rangepicker' showTime format="yyyy-MM-dd HH:mm:ss" disabledDate={c => c && c.getTime() > Date.now() } style={{ width: '100%' }} onChange={this.chooseAbsolute} defaultValue={this.state.defaultValue} />
          </TabPane>
        </Tabs>
      </div>
    )
  }
});
DatetimePicker.timeHelper = timeHelper;

module.exports = DatetimePicker;