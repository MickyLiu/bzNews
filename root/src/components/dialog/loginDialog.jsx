let React = require('react');
let events = require('./../../_utils/eventSubscriber');
let ajaxEx = require('./../../_utils/ajaxConfiger');
let Const = require('./../../_utils/const');
require('./../../_utils/startsWith');
require('./dialog.scss');

let LoginDialog = React.createClass({
  getDefaultProps: function() {
    return {
      showDel: true,
      title: "星辰大海",
      inviteLogin: false
    };
  },
  getInitialState: function() {
    let username = "",
    password = "",
    rememberPsd = false,
    messageShow = false,
    msg = "";

    if(localStorage.getItem("username")) {
      username = localStorage.getItem("username");
    }
    if(localStorage.getItem("rememberPsd") !== null) {
      rememberPsd = JSON.parse(localStorage.getItem("rememberPsd"));
      if(rememberPsd) {
        password = "aaaaaaaaaa";
      }
    }
    if(localStorage.getItem("expired") !== null && JSON.parse(localStorage.getItem("expired"))) {
      password = "";
      messageShow = true;
      msg= "登录过期，请重新登录";
    }

    return {
      name: username,
      rememberName: username,  // 记在浏览器中的常用登录名
      password: password,
      msg: msg,
      messageShow: messageShow,
      btvalue: '登录',
      rememberPsd: rememberPsd
    };
  },
  removeDialog: function() {
    events.publish("removeDialog");
  },
  handleNameChange: function(e) {
    this.setState({name: e.target.value});
  },
  handlePwdChange: function(e) {
    this.setState({password: e.target.value});
  },
  handlerememberPsdChange: function(e) {
    if(e.target.checked === false) {
      this.setState({password : ""});
      localStorage.removeItem("diao");
    }
    this.setState({rememberPsd: e.target.checked});
  },
  handleSubmit: function(e) {
    e.preventDefault();
    if (this.state.btvalue.startsWith('登录中')) {
      return;
    }
    let self = this;
    let auth = this.props.auth;
    let name = this.state.name.trim();
    let password = this.state.password.trim();

    //判断是不是记住密码下更换了用户再登录
    if(name !== this.state.rememberName) {
      localStorage.removeItem('diao');
    }

    if(JSON.parse(localStorage.getItem('rememberPsd')) === true  && localStorage.getItem('diao')) {
      auth.rememberPsdLogin(function() {
        self.setState({password: "", messageShow: true, msg: '登录过期，请重新登录'});
      });
      return;
    }

    let formData = {
      name: name,
      password: password,
      rememberPsd: this.state.rememberPsd
    };
    if (!password || !name) {
      this.setState({messageShow:true, msg: '登录邮箱或密码不能为空'});
      return;
    }
    this.setState({messageShow:false, msg: ''});
    var dot = 0;
    self.setState({btvalue: '登录中'});
    var s = setInterval(function(){
        if (!self.state.btvalue.startsWith('登录中')) {
          clearInterval(s);
          return;
        }
        dot = (dot + 1) % 4;
        self.setState({btvalue: '登录中' + '.'.repeat(dot)});
    }, 500);

    if(this.props.inviteLogin) {
      auth.inviteLogin(formData, function() {
        events.publish("loginSuccess");
      }, function() {
        self.setState({messageShow:true, msg: '登录邮箱或密码错误', btvalue: '登录'});
      });
    } else {
      auth.login(formData, function() {
        self.setState({messageShow:true, msg: '登录邮箱或密码错误', btvalue: '登录'});
      });
    }
  },
  jumpToRegister: function() {
    events.publish("showRegisterDialog");
  },
  render: function () {
    let deleteImg = document.createElement("img");
    deleteImg.src = require('./../../../assets/image/delete.png');
    return (
      <div id="loginDialog" className="dialog" style={this.props.visible ? {display:'block'} : {display:'none'}}>
        <div className="dialog-header">
          <span>{this.props.title}</span>
          <img style={this.props.showDel ? {display:"inline-block"} : {display: "none"}} src={deleteImg.src} onClick={this.removeDialog}></img>
        </div>
        <p className={this.state.messageShow ? "error-message show" :"error-message"}>
          {this.state.msg}
        </p>
        <form className="commentForm" onSubmit={this.handleSubmit} >
          <div className="ld-input-part">
            <div className="ld-group">
                <input
                    type="text"
                    placeholder="邮箱"
                    value={this.state.name}
                    onChange={this.handleNameChange}
                    className="ld-input ld-name"
                    autoComplete="off"
                />
            </div>
            <div className="ld-group">
                <input
                    type="password"
                    placeholder="密码"
                    value={this.state.password}
                    onChange={this.handlePwdChange}
                    className="ld-input ld-pwd"
                    autoComplete="off"
                />
            </div>
            <div className="ld-group checkbox-line">
                <input
                  type="checkbox"
                  checked={this.state.rememberPsd}
                  onChange={this.handlerememberPsdChange}
                  className="ld-input ld-pwd"
                />
                <label>记住密码
                </label>
            </div>
          </div>
          <div className="ld-submit-part">
            <input type="submit" value={this.state.btvalue} className="ld-submit" />
          </div>
        </form>
        <a className="jump-link" href="javascript:void(0)" onClick={this.jumpToRegister}>没有账户？立即注册→</a>
      </div>
    );
  }
});

module.exports =  LoginDialog;
