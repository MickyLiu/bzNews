let React = require('react');
let events = require('../../_utils/eventSubscriber');
let ajaxEx = require('../../_utils/ajaxConfiger');
let Const = require('../../_utils/const');
require('./../../_utils/startsWith');
require('./dialog.scss');
let RegisterDialog = React.createClass({
  getDefaultProps: function() {
    return {
      showDel: true,
      title: "星辰大海",
      inviteRegister: false
    };
  },
  getInitialState: function() {
    return {
      name: '',
      email: '',
      teamName: '',
      password: '',
      passwordConfirm: '',
      msg: '',
      messageShow: false,
      btvalue: '注册'
    };
  },
  removeDialog: function() {
    events.publish("removeDialog");
  },
  handleNameChange: function(e) {
    this.setState({name: e.target.value});
  },
  handleProjectNameChange: function(e) {
    this.setState({teamName: e.target.value});
  },
  handleEmailChange: function(e) {
    this.setState({email: e.target.value});
  },
  handlePwdChange: function(e) {
    this.setState({password: e.target.value});
  },
  handlePwdConfirmChange: function(e) {
    this.setState({passwordConfirm: e.target.value});
  },
  handleSubmit: function(e) {
    e.preventDefault();
    if (this.state.btvalue.startsWith('注册中')) {
        return;
    }
    let self = this;
    let data = this.initFormData();
    if(!this.validateForm(data)) {
      return;
    }

    this.setState({messageShow:false, msg: ''});
    if(this.props.inviteRegister) {
      delete data.team;
      data.invitation_code = this.props.auth.token;
      this.props.auth.inviteRegister(data, function(data) {
        let errMessage = "";
        for(let key in data.message) {
          if(data.message.hasOwnProperty(key)){
            errMessage = data.message[key][0];
          }
        }
        self.setState({messageShow:true, msg: errMessage, btvalue: '注册'});
      });
    } else {
      this.props.auth.register(data, function(data) {
        let errMessage = "";
        for(let key in data.message) {
          if(data.message.hasOwnProperty(key)){
            errMessage = data.message[key][0];
          }
        }
        self.setState({messageShow:true, msg: errMessage, btvalue: '注册'});
      });
    }
  },
  initFormData: function() {
    let name = this.state.name.trim();
    let email = this.state.email.trim();
    let teamName = this.state.teamName.trim();
    let password = this.state.password.trim();
    let passwordConfirm = this.state.passwordConfirm.trim();
    let data = {
      customer: {
        name: name,
        email: email,
        password: password,
        password_confirmation: passwordConfirm,
      },
      team: {
        name: teamName
      }
    };

    return data;
  },
  validateForm: function(data) {
    // validate empty input.
    if(this.props.inviteRegister) {
      if(!data.customer.name || !data.customer.email || !data.customer.password || !data.customer.password_confirmation) {
        this.setState({messageShow:true, msg: '请填写完整的表单'});
        return false;
      }
    } else {
      if(!data.customer.name || !data.customer.email || !data.team.name || !data.customer.password || !data.customer.password_confirmation) {
        this.setState({messageShow:true, msg: '请填写完整的表单'});
        return false;
      }
    }

    //validate email format.
    let emailExp= /^([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+@([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+\.[a-zA-Z]{2,3}$/;
    if(!emailExp.test(data.customer.email)) {
      this.setState({messageShow:true, msg: '请输入正确的邮箱地址'});
      return false;
    }

    //validate password format.
    let psdExp = /^(?=.*[A-Z])(?=.*[!@#$&*])(?=.*[0-9])(?=.*[a-z]).{6,}/;
    if(!psdExp.test(data.customer.password)) {
      this.setState({messageShow:true, msg: '密码长度最少为6位, 至少包含一个数字, 一个大写字母, 一个小写字母、一个符号:! @ # $ & *'});
      return false;
    }

    //validate password confirmation.
    if(data.customer.password !== data.customer.password_confirmation) {
      this.setState({messageShow:true, msg: '两次输入的密码不匹配'});
      return false;
    }

    return true;
  },
  jumpToLogin: function() {
    events.publish("showLoginDialog");
  },
  render: function () {
    let deleteImg = document.createElement("img");
    deleteImg.src = require('./../../../assets/image/delete.png');
    return (
      <div id="registerDialog" className="dialog" style={this.props.visible ? {display:'block'} : {display:'none'}}>
        <div className="dialog-header">
          <span>{this.props.title}</span>
          <img style={this.props.showDel ? {display:"inline-block"} : {display: "none"}} src={deleteImg.src} onClick={this.removeDialog}></img>
        </div>
        <p className={this.state.messageShow ? "error-message show": "error-message"}>{this.state.msg}</p>
        <form className="commentForm" onSubmit={this.handleSubmit} >
          <div className="ld-input-part">
            <div className="ld-group">
                <input
                    type="text"
                    placeholder="用户名"
                    value={this.state.name}
                    onChange={this.handleNameChange}
                    className="ld-input ld-name"
                    autoComplete="off"
                />
            </div>
            <div className="ld-group" style={this.props.inviteRegister ? {display:"none"} : {display: "block"}}>
                <input
                    type="text"
                    placeholder="团队名"
                    value={this.state.teamName}
                    onChange={this.handleProjectNameChange}
                    className="ld-input"
                    autoComplete="off"
                />
            </div>
            <div className="ld-group">
                <input
                    type="text"
                    placeholder="邮箱，请输入正确的邮箱"
                    value={this.state.email}
                    onChange={this.handleEmailChange}
                    className="ld-input"
                    autoComplete="off"
                />
            </div>
            <div className="ld-group">
                <input
                    type="password"
                    placeholder="密码，至少包含6位字符"
                    value={this.state.password}
                    onChange={this.handlePwdChange}
                    className="ld-input ld-pwd"
                    autoComplete="off"
                />
            </div>
            <div className="ld-group">
                <input
                    type="password"
                    placeholder="密码确认"
                    value={this.state.passwordConfirm}
                    onChange={this.handlePwdConfirmChange}
                    className="ld-input"
                    autoComplete="off"
                />
            </div>
          </div>
          <div className="ld-msg">{this.state.msg}</div>
          <div className="ld-submit-part">
            <input type="submit" value={this.state.btvalue} className="ld-submit" />
          </div>
        </form>
        <a className="jump-link" href="javascript:void(0)" onClick={this.jumpToLogin}>已有账户，立即登陆→</a>
      </div>
    );
  }
});

module.exports = RegisterDialog;
