var React = require('react');
let classnames = require('classnames');
import {render} from 'react-dom';

var placeholder = document.createElement("li");
placeholder.className = "draggable-placeholder";

var DraggableList = React.createClass({
  getInitialState() {
    this.props.placeholderInit(placeholder);
    return {};
  },
  dragStart(e) {
    e.currentTarget.parentNode.style.height = `${e.currentTarget.parentNode.offsetHeight}px`;
    this.dragged = e.currentTarget;
    e.dataTransfer.effectAllowed = 'move';
    // Firefox requires dataTransfer data to be set
    e.dataTransfer.setData("text/html", e.currentTarget);
    // fix offset
    this.dragoffset = 0;
    var target = e.target.offsetParent;
    while (target.tagName.toLowerCase() != "body") {
        this.dragoffset += target.offsetTop;
        target = target.offsetParent;
    }
    // give placeholder height
    placeholder.style.height = `${this.dragged.offsetHeight}px`;
  },
  dragEnd(e) {
    this.dragged.style.display = "block";
    this.dragged.parentNode.removeChild(placeholder);
    // update data
    var data = this.props.data;
    var from = Number(this.dragged.dataset.id);
    var to = Number(this.over.dataset.id);
    if(from < to) to--;
    if(this.nodePlacement == "after") to++;
    data.splice(to, 0, data.splice(from, 1)[0]);
    if (this.props.onDataChange) {
        this.props.onDataChange(data);
    }
    e.currentTarget.parentNode.style.height = '';
  },
  dragOver(e) {
    e.preventDefault();
    this.dragged.style.display = "none";
    var target = e.target;
    while (target && target.className != "draggable-list-item" && target.className != "draggable-placeholder") {
        target = target.parentNode;
    }
    // deal margin
    if(!target) return;
    // deal placeholder
    if(target.className == "draggable-placeholder") return;
    this.over = target;
    
    var relY = e.clientY - this.over.offsetTop - this.dragoffset;
    var height = this.over.offsetHeight / 2;
    var parent = this.over.parentNode;
    
    if(relY > height) {
      this.nodePlacement = "after";
      parent.insertBefore(placeholder, this.over.nextElementSibling);
    }
    else if(relY < height) {
      this.nodePlacement = "before"
      parent.insertBefore(placeholder, this.over);
    }
  },
  render() {
    return <ul onDragOver={this.dragOver} className="draggable-list">
      {React.Children.map(this.props.children, (element, idx) => {
          return React.cloneElement(<li
                className="draggable-list-item"
                data-id={idx}
                key={idx}
                draggable={!!!this.props.lock}
                onDragEnd={this.dragEnd}
                onDragStart={this.dragStart}
            >
                {element}
            </li>, { ref: 'opt' + idx, Auth: this.props.Auth });
      })}
    </ul>
  }
});

module.exports = DraggableList;