// 引入 ECharts 主模块
let echarts = require('echarts');
let classnames = require('classnames');
let React = require('react');

const Echarts = React.createClass({
  chart: null,
  shouldResize: false,
  getInitialState() {
      return {}
  },
  componentDidMount() {
      var $this = this.refs.chart;
      var chart = echarts.init($this);
      this.chart = chart;
      this.name = this.props.name;
      chart.setOption(this.props.Option);
  },
  componentDidUpdate() {
      if (this.shouldResize) {
          this.chart.resize();
      }
      this.shouldResize = false;
  },
  setOption(opt, notMerge) {
      notMerge = notMerge || false;
      var chart = this.chart;
      chart.setOption(opt, notMerge);
  },
  getOption() {
      return this.chart.getOption();
  },
  resize() {
      this.shouldResize = true;
  },
  render() {
    let classes = classnames('bz-echarts', this.props.className);
    return (
      <div ref='chart' className={classes} style={this.props.style}>
      </div>
    );
  }
});

Echarts.graphic = echarts.graphic;


module.exports = Echarts;
