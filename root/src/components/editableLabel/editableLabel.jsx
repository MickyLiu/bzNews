import { Icon, Input, Button } from 'antd';
const InputGroup = Input.Group;
const React = require('react');
const classnames = require('classnames');
import {render} from 'react-dom';
require('./editableLabel.scss');

const EditableLabel = React.createClass({
  getInitialState() {
    return {
      value: this.props.value,
      focus: false,
      edit: false,
    };
  },
  handleInputChange(e) {
    this.setState({
      value: e.target.value,
    });
  },
  handleFocusBlur(e) {
    this.setState({
      focus: e.target === document.activeElement,
    });
  },
  handleOK() {
    if (!!this.state.value.trim()) {
      var value = this.state.value.trim().slice(0, 4);
      this.setState({value: value, edit: false});
      if (this.props.onOK) {
        this.props.onOK(value);
      }
    }
  },
  handleEdit() {
    this.setState({edit: true});
  },
  render() {
    const btnCls = classnames({
      'ant-search-btn': true,
      'ant-search-btn-noempty': !!this.state.value.trim(),
    });
    const searchCls = classnames({
      'ant-search-input': true,
      'ant-search-input-focus': this.state.focus,
      'editable-label-editor': true
    });
    const labelCls = classnames({
      'editable-label': true,
      'edit': this.state.edit,
    })
    return (
      <div className={labelCls}>
        <span className="editable-label-label">
          <span className="editable-label-span">{this.state.value}</span><Icon type="edit" className="editable-label-edit-bt" onClick={this.handleEdit}/>
        </span>
        <InputGroup className={searchCls} style={this.props.style}>
          <Input {...this.props} value={this.state.value} onChange={this.handleInputChange}
            onFocus={this.handleFocusBlur} onBlur={this.handleFocusBlur} validateStatus="error"/>
            <div className="ant-input-group-wrap">
              <Button className={btnCls} size={this.props.size} onClick={this.handleOK}>
                <Icon type="check" />
              </Button>
            </div>
          </InputGroup>
      </div>
    );
  }
});

module.exports = EditableLabel;