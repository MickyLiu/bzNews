let React = require('react');

require('./footer.scss');

const Footer = React.createClass({
  render: function() {
    let qrcode = document.createElement("img");
    qrcode.src = require("./../../../assets/image/qrcode.jpg");
    return (
      <footer className="project-footer">
        <div className="footer-main-area">
          <div className="main-text-area">
            <div className="text-area">
              <h3>关于我们</h3>
              <section>
                星辰大海是国内最专业的视频互推平台！
                国内第一视频多渠道分发、数据分析平台。
                提供最权威的PGC榜单排名数据，全方位
                的视频推广方案，让视频推广更简单！
              </section>
            </div>
            <div className="text-area">
              <h3>联系我们</h3>
              <ul>
                <li>商务合作：13800000000</li>
                <li>意见反馈：000000000@baozou.com</li>
                <li>联系电话：021-00000000</li>
              </ul>
            </div>
            <div className="qr-code-area text-area">
              <span>星辰大海</span>
              <img src={qrcode.src} alt="qrcode"></img>
            </div>
          </div>
        </div>
        <p className="copyright">©XXX All Rights Reserved.</p>
      </footer>
    );
  }
});

module.exports = Footer;
