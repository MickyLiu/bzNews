let React = require('react');
let classnames = require('classnames');
let Const = require('./../../_utils/const');
let globalVars = require('./../../_utils/global');

const PreLoader = React.createClass({
   getInitialState() {
    return{
       url: this.props.defaultUrl || require('../../../assets/image/default-pic.png'),
       showClass:'',
       image: document.createElement('img')
    };
  },
  componentDidMount() {
       this.loadingImage();
  },
  loadingImage(isChange){
       if(document.createElement)
       {
            let _this=this,img = this.state.image;
            if(isChange) _this.setState({url:this.props.defaultUrl || require('../../../assets/image/default-pic.png')});
            img.src = this.props.url;
            img.onload = ()=>{
                   _this.setState({url:_this.props.url,showClass:'fade-enter fade-enter-active '});
            };
            img.onerror = ()=>{
                   _this.setState({url:_this.props.errorUrl || require('../../../assets/image/default-error.png')});
            };
       }
       else
       {
            this.setState({url:this.props.url});
       }
  },
  componentWillReceiveProps(nextProps){
        if(this.props.url==nextProps.url) return;
        this.props = nextProps;
        this.loadingImage(true);
  },
  componentWillUnmount(){
        let img = this.state.image;
        img.onload = null;
        img.onerror = null;
        img = null;
  },
  render() { 
    return (
          <img src={this.state.url} {...this.props} className={this.state.showClass + this.props.className} />
    );
  }
});

module.exports = PreLoader;
