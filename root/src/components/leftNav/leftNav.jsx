import { Router,RouterContext } from 'react-router';
import { Menu, Icon } from 'antd';
let UIChangeMixIn = require('./../../_utils/uiChangeMixIn');
let React = require('react');
let classnames = require('classnames');
let Link = require('../link/link');
require('./leftNav.scss');

const SubMenu = Menu.SubMenu;
const LeftNav = React.createClass({
  mixins:[UIChangeMixIn.routeOnNavMixIn,UIChangeMixIn.UiInfoChangeMixIn],
  getDefaultProps() {
    return {}
  },
  getInitialState() {
    return {
      theme: 'light',
      current: '1',
      full: true,
      projectName:localStorage.getItem('projectName'),
      projectImage:localStorage.getItem('projectImage')
    };
  },
  handleClick(e) {
    /*this.setState({
      current: e.key
    });*/
  },
  render() {
    let classes = classnames('left-nav-container', {'left-nav-full': this.state.full});
    return (
      <div className={classes}>
        <div className='project-title'>
            <div className='project-opt-area'>
                <Link to="member"><span className="index-iconfont icon-member"></span></Link>
                {
                    globalVars.get('_currentRole_') == 'admin' || globalVars.get('_currentRole_') == 'super' ?
                    <Link to="setting"><span className="index-iconfont icon-setting"></span></Link>
                    :
                    undefined
                }
            </div>
            <div className="project-image"><img src={this.state.projectImage} /></div>
            <div className="project-name" title={this.state.projectName}><div>{this.state.projectName}</div></div>
        </div>
        <Menu className="left-nav" theme={this.state.theme}
            onClick={this.handleClick}
            defaultOpenKeys={['sub1', 'sub2']}
            selectedKeys={[this.state.current]}
            mode="inline">
            <SubMenu key="sub1" title={<span className="left-nav-menu-title">基础功能</span>}>
                <Menu.Item key="g1">
                    <Link to="series" className="left-nav-menu-item">
                        <span className="index-iconfont icon-series"></span>
                        <span className="left-nav-menu-item-title">系列</span>
                    </Link>
                    <div className="left-nav-tip">
                        <div className="left-nav-tip-arrow"></div>
                        <div className="left-nav-tip-text">系列</div>
                    </div>
                </Menu.Item>
                <Menu.Item key="g2">
                    <Link to="videos" className="left-nav-menu-item">
                        <span className="index-iconfont icon-video"></span>
                        <span className="left-nav-menu-item-title">视频</span>
                    </Link>
                    <div className="left-nav-tip">
                        <div className="left-nav-tip-arrow"></div>
                        <div className="left-nav-tip-text">视频</div>
                    </div>
                </Menu.Item>
                <Menu.Item key="g3">
                    <Link to="groups" className="left-nav-menu-item">
                        <span className="index-iconfont icon-group"></span>
                        <span className="left-nav-menu-item-title">群组</span>
                    </Link>
                    <div className="left-nav-tip">
                        <div className="left-nav-tip-arrow"></div>
                        <div className="left-nav-tip-text">群组</div>
                    </div>
                </Menu.Item>
                <Menu.Item key="g4">
                    <Link to="carouselPics" className="left-nav-menu-item">
                        <span className="index-iconfont icon-lunbotu"></span>
                        <span className="left-nav-menu-item-title">首页轮播图</span>
                    </Link>
                    <div className="left-nav-tip">
                        <div className="left-nav-tip-arrow"></div>
                        <div className="left-nav-tip-text">首页轮播图</div>
                    </div>
                </Menu.Item>
                <Menu.Item key="g5">
                    <Link to="navy" className="left-nav-menu-item">
                        <span className="index-iconfont icon-shuijun"></span>
                        <span className="left-nav-menu-item-title">水军管理</span>
                    </Link>
                    <div className="left-nav-tip">
                        <div className="left-nav-tip-arrow"></div>
                        <div className="left-nav-tip-text">水军管理</div>
                    </div>
                </Menu.Item>
                <Menu.Item key="g6">
                    <Link to="appVersion" className="left-nav-menu-item">
                        <span className="index-iconfont icon-version"></span>
                        <span className="left-nav-menu-item-title">APP版本管理</span>
                    </Link>
                    <div className="left-nav-tip">
                        <div className="left-nav-tip-arrow"></div>
                        <div className="left-nav-tip-text">APP版本管理</div>
                    </div>
                </Menu.Item>
            </SubMenu>
            {
                globalVars.get('_currentRole_') == 'admin' || globalVars.get('_currentRole_') == 'super' ?
                <SubMenu key="sub2" title={<span className="left-nav-menu-title">管理员功能</span>}>
                    <Menu.Item key="a1">
                        <Link to="statistics" className="left-nav-menu-item">
                            <span className="index-iconfont icon-statistics"></span>
                            <span className="left-nav-menu-item-title">数据统计</span>
                        </Link>
                        <div className="left-nav-tip">
                            <div className="left-nav-tip-arrow"></div>
                            <div className="left-nav-tip-text">数据统计</div>
                        </div>
                    </Menu.Item>
                    <Menu.Item key="a2">
                        <Link to="appcustom" className="left-nav-menu-item">
                            <span className="index-iconfont icon-custom"></span>
                            <span className="left-nav-menu-item-title">APP样式定制</span>
                        </Link>
                        <div className="left-nav-tip">
                            <div className="left-nav-tip-arrow"></div>
                            <div className="left-nav-tip-text">APP样式定制</div>
                        </div>
                    </Menu.Item>
                    <Menu.Item key="a3">
                        <Link to="apppack" className="left-nav-menu-item">
                            <span className="index-iconfont icon-app"></span>
                            <span className="left-nav-menu-item-title">创建应用</span>
                        </Link>
                        <div className="left-nav-tip">
                            <div className="left-nav-tip-arrow"></div>
                            <div className="left-nav-tip-text">创建应用</div>
                        </div>
                    </Menu.Item>
                </SubMenu>
                :
                <div style={{display: 'none'}}></div>
            }
        </Menu>
      </div>
    );
  }
});


module.exports = LeftNav;
