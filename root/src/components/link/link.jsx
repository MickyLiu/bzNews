let React = require('react');
let classnames = require('classnames');
let Const = require('./../../_utils/const');
let globalVars = require('./../../_utils/global');

const MyLink = React.createClass({
   getInitialState() {
   return{
       urlPrefix: '#/projects/'+ globalVars.get('_projectId_')
    };
  },
  render() { 
    return (
          <a href={this.state.urlPrefix + '/' +this.props.to} {...this.props}>
               {this.props.children}
          </a>
    );
  }
});

module.exports = MyLink;
