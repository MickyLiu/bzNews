import { Router,RouterContext } from 'react-router';
import { Menu, Icon } from 'antd';
let UIChangeMixIn = require('./../../_utils/uiChangeMixIn');
let React = require('react');
let classnames = require('classnames');
let Link = require('../link/link');
require('./popOut.scss');


const PopOut = React.createClass({
  render() {
    return (
      <div>
         {this.props.visible?<div {...this.props} className={"pop-out pop-out-placement-bottom "} style={{top:(this.props.top || 0),left:(this.props.left || 0),width:(this.props.width || 210)}}>
              <div className="pop-out-arrow"></div>
              <div className="pop-out-inner">
                  {this.props.children}
              </div>
         </div>:null}
      </div>
    );
  }
});


module.exports = PopOut;
