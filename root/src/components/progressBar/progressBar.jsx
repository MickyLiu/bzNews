import { Progress } from 'antd';

let React = require('react');

const ProgressBar = React.createClass({
  getInitialState() {
    return {
      current: 0,
      show:false
    };
  },
  render() {
    return (
        <Progress.Line style={{"display":this.state.show?"":"none","position":"fixed","top":"-11px","zIndex":"9999999"}} percent={this.state.current}ß strokeWidth={2} showInfo={false}/>
    );
  }
});

module.exports = ProgressBar;
