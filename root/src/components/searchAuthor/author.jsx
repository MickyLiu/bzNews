import { Icon, Select, Table, Popover } from 'antd';

let React = require('react');
let Echarts = require('../echarts/echarts');
let classnames = require('classnames');
let Const = require('./../../_utils/const');
let Ganggang = require('./../../dataStatisticsPanel/utils/ganggang');
let timeHelper = require('./../../dataStatisticsPanel/utils/datetimePickerModal2').timeHelper;
let seriesHelper = require('./../../dataStatisticsPanel/utils/seriesHelper');
let source = require('./../../dataStatisticsPanel/utils/source');
let chartOption = require('./chartOption').chartOption;
let chartOptionHandler = require('./chartOption').chartOptionHandler;
let timeTableOption = require('./tableOption').timeTableOption;
let siteTableOption = require('./tableOption').siteTableOption;
require('./author.scss');

const Option = Select.Option;
const OptGroup = Select.OptGroup;

const Author = React.createClass({
  // lifecycle
  getInitialState: function () {
    let state = {
      author: this.props.author,
      overview: [],
      overviewCount: "0",
      resources: [],
      selectResources: 'sum',
      date: {
        start: null,
        end: null,
      },
      sourceData: [],
      chartDiffData: [],
      chartNormalData: [],
      updateChart: true,
      timeTableData: [],
      siteTableData: [],
    }
    return state;
  },
  componentDidMount() {
    this.setState({
      date: {
        start: timeHelper.computeWeek(new Date(), -1),
        end: new Date()
      }
    }, this.init);
  },
  // event
  init() {
    let auth = this.props.Auth;
    let self = this;
    var aid, start, end, url;
    // set total data
    aid = this.state.author.id;
    start = timeHelper.easyFormat(this.state.date.start, '%y-%m-%d %H:%M:%S');
    end = timeHelper.easyFormat(this.state.date.end, '%y-%m-%d %H:%M:%S');
    url = Const.API.playcounts + `?from=${start}&to=${end}&type=author&query_id=${aid}&site=all`;
    auth.ajax(url).done(function (data) {
      data = seriesHelper.cleanResData(data);
      let sum = 0;
      data = data.map((v) => {
        let p = v.playcounts[v.playcounts.length - 1].playcount;
        sum += p;
        return {
          value: p,
          name: v.site
        }
      });
      self.setState({
        overview: data,
        overviewCount: sum
      });
    });
    // set detail data
    this.requsetDetail();
  },
  requsetDetail() {
    var self = this;
    let auth = this.props.Auth;
    var aid, start, end, url;
    aid = this.state.author.id;
    start = timeHelper.easyFormat(this.state.date.start, '%y-%m-%d %H:%M:%S');
    end = timeHelper.easyFormat(this.state.date.end, '%y-%m-%d %H:%M:%S');
    url = Const.API.playcounts + `?from=${start}&to=${end}&type=author&query_id=${aid}&site=${source[this.state.selectResources].site}`;
    auth.ajax(url).done(function (data) {
      data = seriesHelper.cleanResData(data);
      let sourceData = data;
      // 处理sum数据
      if (self.state.selectResources == 'sum') {
        // 将所有数据加起来
        let source = data;
        data = {};
        for (var i = 0; i < source.length; i++) {
          var pcs = source[i].playcounts;
          for (var j = 0; j < pcs.length; j++) {
            var node = pcs[j];
            if (data[node.created_at]) {
              data[node.created_at] += node.playcount;
            } else {
              data[node.created_at] = node.playcount;
            }
          }
        }
        source = data;
        data = [{
          playcounts: [],
          site: 'sum'
        }]
        data[0].playcounts = Object.keys(source).map((t) => {
          return {
            created_at: t,
            playcount: source[t]
          }
        });
        data[0].playcounts.sort((a, b) => {
          a = a.created_at;
          b = b.created_at;
          if (a == b) {
            return 0;
          }
          if (a > b) {
            return 1;
          }
          if (a < b) {
            return -1
          }
        });
      }
      // 处理结束
      let diffData = [];
      let normalData = [];
      for (var i = 0; i < data.length; i++) {
        var _data = data[i];
        let _diffData = seriesHelper.getSeries(_data.site, 'line', seriesHelper.buildDiffData(_data.playcounts));
        let _normalData = seriesHelper.getSeries(_data.site, 'line', seriesHelper.buildNormalData(_data.playcounts));
        diffData.push(_diffData);
        normalData.push(_normalData);
      }
      let timeTableData = [];
      if (normalData.length > 0) {
        timeTableData = normalData[0].data.map((v, i) => {
          return {
            key: i + 1,
            time: v.name,
            increase: i == 0 ? -1 : diffData[0].data[i - 1].value[1],
            total: v.value[1]
          }
        });
      }
      let siteTableData = [];
      if (self.state.selectResources == 'all' && normalData.length > 0) {
        siteTableData = sourceData.map((site, i) => {
          let ret = {
            key: site.site,
            name: site.site
          };
          let pcs = site.playcounts;
          if (pcs.length == 1) {
            ret.total = pcs[0].playcount;
            ret.increase = -1;
          } else {
            ret.total = pcs[pcs.length - 1].playcount;
            ret.increase = pcs[pcs.length - 1].playcount - pcs[0].playcount;
          }
          ret.children = pcs.map((pc, i) => {
            return {
              key: site.site + i,
              name: timeHelper.easyFormat(new Date(pc.created_at), '%y-%m-%d %H:%M'),
              total: pc.playcount,
              increase: -1
            }
          });
          for (var i = 1; i < ret.children.length; i++) {
            ret.children[i].increase = ret.children[i].total - ret.children[i - 1].total;
          }
          return ret
        });
      }
      self.setState({
        sourceData: sourceData,
        chartDiffData: diffData,
        chartNormalData: normalData,
        timeTableData: timeTableData,
        siteTableData: siteTableData
      }, self.handleDetail)
    });
  },
  handleDetail() {
    let option = this.chart.getOption();
    option = chartOptionHandler(this, option);
    this.chart.setOption(option, this.state.updateChart);
  },
  render() {
    return (
      <div className='author-tab-content'>


        <div className="author-overview">
          <div className="author-info">
            <div className="author-avatar"><img src={this.state.author.avatar} /></div>
            <div className="author-name">{this.state.author.name}</div>
          </div>
          <div className="author-playcount">
            <div className="author-playcount-text">当前视频总播放数<span className="count">{seriesHelper.toChineseString(this.state.overviewCount) }</span></div>
            <Ganggang dataSource={this.state.overview}/>
          </div>
        </div>


        <hr/>


        <div className="author-tab-select">
          <Select className="site-select" defaultValue="sum" onChange={(v) => this.setState({ selectResources: v, updateChart: true }, this.requsetDetail) } style={{ width: '150px' }}>
            {Object.keys(source).map((k) => <Option value={k} key={k}>{source[k].chinese}播放趋势</Option>) }
          </Select>
          <Popover
            overlay={(<div style={{textAlign: 'center', padding: '5px 0'}}>选择时间区间为付费功能</div>)}
          >
            <span className="timespan">
              近7天<Icon type="exclamation-circle-o" />
            </span>
          </Popover>
        </div>


        <div className="tab-h">
          <h3><Icon type="line-chart" />&nbsp; 播放量增长图表</h3>
        </div>
        <div className="author-chart">
          <Echarts ref={(c) => this.chart = c} name='viewcountChart' Option={chartOption} className="viewcount-chart" />
        </div>


        

        {
          this.state.selectResources == 'all' ? ([
            <div className="tab-h" key="0">
              <h3><Icon type="line-chart" />&nbsp; 来源明细数据</h3>
            </div>,
            <div className="author-table" key="1">
              <Table className="viewcount-daily-analysis-table" columns={siteTableOption} dataSource={this.state.siteTableData} />
            </div>
          ]) : ([
            <div className="tab-h" key="0">
              <h3><Icon type="line-chart" />&nbsp; 时间明细数据</h3>
            </div>,
            <div className="author-table" key="1">
              <Table className="viewcount-daily-analysis-table" columns={timeTableOption} dataSource={this.state.timeTableData} />
            </div>
          ])
        }

      </div>
    );
  }
});

module.exports = Author;
