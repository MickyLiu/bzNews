const source = require('./../../dataStatisticsPanel/utils/source');

var chartOption = {
  tooltip: {
    trigger: 'axis',
    formatter: function (params) {
      var ret = [];
      for (var i = 0; i < params.length; i++) {
        let param = params[i];
        if (param.value == undefined) {
          ret.push(`<div class="chart-tooltip"><span class="chart-tooltip-name" style="background: ${source[param.seriesName].color}">${source[param.seriesName].chinese}</span> 无数据</div>`);
        } else {
          ret.push(`<div class="chart-tooltip"><span class="chart-tooltip-name" style="background: ${source[param.seriesName].color}">${source[param.seriesName].chinese}</span><span class="chart-tooltip-time">${param.name}</span> ${parseInt(params[i].value[1]).toLocaleString()}</div>`);
        }
      }
      return ret.join('');
    },
    axisPointer: {
      animation: false
    }
  },
  grid: {
    right: 10,
    left: 60,
    top: 50,
  },
  toolbox: {
    show: true,
    feature: {
      dataZoom: {},
      dataView: { readOnly: false },
      restore: {},
      saveAsImage: {}
    },
  },
  xAxis: {
    // type: 'category',
    // boundaryGap: false,
    // data: [
    //     '00:00','01:00','02:00','03:00','04:00','05:00',
    //     '06:00','07:00','08:00','09:00','10:00','11:00',
    //     '12:00','13:00','14:00','15:00','16:00','17:00',
    //     '18:00','19:00','20:00','21:00','22:00','23:00',
    // ]
    type: 'time',
    splitLine: {
      show: false
    },
  },
  yAxis: {
    type: 'value',
    axisLabel: {
      formatter: function (value, index) {
        if (value < 1000) {
          return value.toString();
        } else if (value < 1000000) {
          return (value / 1000).toFixed(1) + 'K';
        } else if (value < 1000000000) {
          return (value / 1000000).toFixed(1) + 'M';
        } else if (value < 1000000000000) {
          return (value / 1000000000).toFixed(1) + 'B';
        } else {
          return value.toString();
        }
      }
    },
    scale: true, // 脱离0
    // boundaryGap: ['30%', '30%']
  },
  series: []
};

var chartOptionHandler = function (self, option) {
  if (option.xAxis && option.xAxis.length > 0) {
    option.xAxis[0].min = self.state.date.start.getTime();
    option.xAxis[0].max = self.state.date.end.getTime();
  }
  option.series = self.state.chartDiffData;
  option.legend = {
    data: self.state.chartDiffData.map((d) => source[d.name].chinese),
    bottom: 0
  };
  option.color = option.series.map((s) => source[s.name].color);
  return option;
}

export { chartOption, chartOptionHandler }