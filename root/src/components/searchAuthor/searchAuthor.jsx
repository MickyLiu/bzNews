import { Spin, Icon } from 'antd';

let React = require('react');
let ReactDOM = require('react-dom');
let locationHelper = require('../../_utils/locationHelper');
let Const = require('./../../_utils/const');
let Auth = require('../../_utils/auth');
let Author = require('./author');

require('./searchAuthor.scss');

const SearchAuthor = React.createClass({
  getDefaultProps: function() {
    return {
      auth: window.auth,
    };
  },
  getInitialState: function() {
    let ret = {
      q: locationHelper.getSearchVar('q'),
      searching: true,
      result: [],
      selected: false,
      selectedAuthor: null,
    }
    return ret;
  },
  componentDidMount: function() {
    let self = this;
    this.props.auth.ajax(`${Const.API.teamsSearch}?text=${this.state.q}&size=30`)
      .done((data) => {
        data = data.data;
        data = data.map((d) => {
          return {
            name: d.attributes.name,
            id: d.id,
            avatar: d.avatar || 'http://7xs8p5.com2.z0.glb.qiniucdn.com/homeImg_default.jpg'
          }
        })
        self.setState({
          searching: false,
          result: data,
        });
      });
  },
  selectAuthor(author) {
    this.setState({
      selected: true,
      selectedAuthor: author
    });
  },
  render: function() {
    return (
      <div className="search-author">
        {this.state.selected ? (
          <Author Auth={this.props.auth} author={this.state.selectedAuthor}/>
        ) : (
          <div>
            <div className="search-title">与<span className="q">{this.state.q}</span>相关的作者有：</div>
            <div className="search-result">
              <Spin spining={this.state.searching}>
                {this.state.result.map((r, i) => 
                  <div className="author-container" key={i}>
                    <div className="author" onClick={() => this.selectAuthor(r)}>
                      <div className="avatar"><img src={r.avatar} alt={r.name}/></div>
                      <div className="name">
                        <span>{r.name}</span>
                      </div>
                    </div>
                  </div>
                )}
                {this.state.result.length == 0 ?
                  <div className="video-empty">
                    <Icon type="frown" />暂无结果
                  </div> : undefined
                }
              </Spin>
            </div>
          </div>
        )}
      </div>
    );
  }
});

module.exports = SearchAuthor;