import { Input, Select, Button, Icon } from 'antd';

let React = require('react');
let classNames = require('classnames');
let querystring = require('querystring');
const Option = Select.Option;

let timeout;
let currentValue;

function fetch(value, searchFunc, callback) {
  if (timeout) {
    clearTimeout(timeout);
    timeout = null;
  }
  currentValue = value;

  function fake() {
    searchFunc(value, callback);
  }

  timeout = setTimeout(fake, 500);
}

const SearchInput = React.createClass({
  getInitialState() {
    return {
      data: [],
      value: '',
      focus: false,
    };
  },
  handleChange(value, label) {
    this.setState({ value });
    if (value == label) {
        fetch(value, this.props.searchCall, (data) => this.setState({ data }));
    }
  },
  handleSubmit() {
    this.props.onSubmit(this.state);
  },
  handleFocusBlur(e) {
    this.setState({
      focus: e.target === document.activeElement,
    });
  },
  render() {
    const btnCls = classNames({
      'ant-search-btn': true,
      'ant-search-btn-noempty': !!this.state.value.trim(),
    });
    const searchCls = classNames(this.props.className, {
      'ant-search-input': true,
      'ant-search-input-focus': this.state.focus,
    });
    const options = this.state.data.map(d => this.props.dataMap(d));
    return (
      <Input.Group className={searchCls} style={this.props.style}>
        <Select
          combobox
          value={this.state.value}
          searchPlaceholder={this.props.placeholder}
          notFoundContent="未找到"
          defaultActiveFirstOption={false}
          showArrow={false}
          filterOption={false}
          onChange={this.handleChange}
          onFocus={this.handleFocusBlur}
          onBlur={this.handleFocusBlur}>
          {options}
        </Select>
        <div className="ant-input-group-wrap">
          <Button className={btnCls} onClick={this.handleSubmit}>
            <Icon type="check" />
          </Button>
        </div>
      </Input.Group>
    );
  },
});

module.exports = SearchInput;