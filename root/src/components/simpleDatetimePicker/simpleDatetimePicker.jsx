const React = require('react');
import { DatePicker, InputNumber, Select, Radio } from 'antd';
const RangePicker = DatePicker.RangePicker;
const RadioButton = Radio.Button;
const RadioGroup = Radio.Group;
const timeHelper = require('./../datetimePicker/datetimePicker').timeHelper;
require('./simpleDatetimePicker.scss');

var SimpleDatetimePicker = React.createClass({
  getInitialState() {
    let ret = {
      radio: 'today',
      start: this.props.start,
      end: this.props.end,
      text: '今天',
      custom: false,
    };
    return ret;
  },
  componentDidMount() {
    this.setTimespan(this.state.radio);
  },
  onRadioChange(e) {
    let value = e.target.value;
    if (value == 'custom') {
      this.setCustom();
    } else {
      this.setTimespan(value);
    }
    this.setState({
      radio: value,
    });
  },
  _timespan: {
    today() {return [timeHelper.computeDay(new Date(), -1), new Date()]},
    yesterday() {return [timeHelper.computeDay(new Date(), -2), timeHelper.computeDay(new Date(), -1)]},
    d7() {return [timeHelper.computeDay(new Date(), -7), new Date()]},
    d30() {return [timeHelper.computeDay(new Date(), -30), new Date()]},
  },
  _text: {
    today: '今天',
    yesterday: '昨天',
    d7: '近7天',
    d30: '近30天',
    custom: '自定义'
  },
  setTimespan(sign) {
    let arr = this._timespan[sign]();
    this.setState({
      start: arr[0],
      end: arr[1],
      text: this._text[sign],
      custom: false,
    }, this.onChange);
  },
  setCustom() {
    this.setState({
      custom: true,
      text: `${timeHelper.easyFormat(this.state.start, '%y-%m-%d')} - ${timeHelper.easyFormat(this.state.end, '%y-%m-%d')}`,
      start: timeHelper.floorDay(this.state.start),
      end: new Date(timeHelper.ceilDay(this.state.end).getTime() - timeHelper.second)
    }, this.onChange);
  },
  chooseCustom(date) {
    date[0] = timeHelper.floorDay(date[0]);
    date[1] = new Date(timeHelper.ceilDay(date[1]).getTime() - timeHelper.second);
    this.setState({
      start: date[0],
      end: date[1],
      text: `${timeHelper.easyFormat(date[0], '%y-%m-%d')} - ${timeHelper.easyFormat(date[1], '%y-%m-%d')}`
    }, this.onChange);
  },
  onChange() {
    if (this.props.onChange) {
      this.props.onChange([this.state.start, this.state.end], this.state.text);
    }
  },
  render() {
    return (
      <div className="datetime-picker">
        <RadioGroup defaultValue="today" size="large" onChange={this.onRadioChange} value={this.state.radio}>
          {Object.keys(this._text).map((k) => <RadioButton value={k} key={k}>{this._text[k]}</RadioButton>)}
        </RadioGroup>
        {
          this.state.custom ?
            <RangePicker ref='rangepicker' format="yyyy-MM-dd" disabledDate={c => c && c.getTime() > Date.now() } style={{ width: '330px', marginTop: '20px' }} onChange={this.chooseCustom} defaultValue={[this.state.start, this.state.end]} />
            :
            null
        }
      </div>
    )
  }
});

SimpleDatetimePicker.timeHelper = timeHelper;
module.exports = SimpleDatetimePicker;