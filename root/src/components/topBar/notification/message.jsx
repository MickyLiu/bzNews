let React = require('react');
let $ = require('jquery');
let formater = require('./../../../_utils/timeFormater');
let _ = require('lodash');

const Message = React.createClass({
  getDefaultProps() {
    return {
      message: 'XXX',
      linkText: '现在去处理',
      time: new Date(),
      total: 0,
      index: 0,
    }
  },
  getInitialState() {
    return{

    };
  },
  componentDidMount() {
    
  },
  componentWillUnmount(){

  },
  render() {
    return (
      <div className="messagebox-message">
        <div className="body"><span>{this.props.message}，</span><a>{this.props.linkText}</a></div>
        <div className="time">{formater(this.props.time).fromNow()}</div>
        {
          this.props.total == this.props.index ? undefined : <hr color="#ddd" size="1" />
        }
      </div>
    );
  }
});

module.exports = Message;