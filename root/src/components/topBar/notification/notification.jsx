import { Badge, Icon, Dropdown } from 'antd';
import {Link} from 'react-router';

let React = require('react');
let classnames = require('classnames');
let events = require('./../../../_utils/eventSubscriber');
let ajaxEx = require('./../../../_utils/ajaxConfiger');
let Const = require('./../../../_utils/const');
let $ = require('jquery');
let _ = require('lodash');

require('./notification.scss');

let Message = require('./message');

const Notification = React.createClass({
  getInitialState() {
    return{
      count: 0,
      messages: []
    };
  },
  componentDidMount() {
    let data = [{
      message: '呵呵呵呵',
      linkText: '现在去处理',
      time: new Date()
    }, {
      message: 'hhahahahah',
      linkText: '现在去处理',
      time: new Date()
    }, {
      message: 'wwuwuwuwuwuwu',
      linkText: '现在去处理',
      time: new Date()
    }, {
      message: '这条很长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长',
      linkText: '现在去处理',
      time: new Date()
    }, {
      message: 'hhahahahah',
      linkText: '现在去处理',
      time: new Date()
    }, {
      message: 'wwuwuwuwuwuwu',
      linkText: '现在去处理',
      time: new Date()
    }, {
      message: 'wwuwuwuwuwuwu',
      linkText: '现在去处理',
      time: new Date()
    }, {
      message: '这条很长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长',
      linkText: '现在去处理',
      time: new Date()
    }, {
      message: 'hhahahahah',
      linkText: '现在去处理',
      time: new Date()
    }, {
      message: 'wwuwuwuwuwuwu',
      linkText: '现在去处理',
      time: new Date()
    }]
    this.setState({
      count: data.length,
      messages: data
    });
    // 定时更新
    setInterval(() => {
      this.forceUpdate();
    }, 30000)
  },
  componentWillUnmount(){
    
  },
  remove(idx) {
    if (idx == 'all') {
      this.setState({
        count: 0,
        messages: []
      });
    } else {
      let data = this.state.messages;
      data.splice(idx, 1);
      this.setState({
        count: data.length,
        messages: data
      });
    }
  },
  messageBox() {
    return (
      <div id="messageBox" className="messagebox-menu">
        <div className="messagebox-title">通知</div>
        <hr color="#ddd" size="1" />
        <div className="messagebox-list">
          {this.state.count == 0 ?
            <div className="empty">- 没有新通知 -</div>
            :
            this.state.messages.map((v, i) => 
              <Message {...v} key={i} index={i + 1} total={this.state.messages.length}/>
            )
          }
        </div>
        <div className="messagebox-total"><a href="#/messages" onClick={() => this.remove('all')}>查看全部</a></div>
      </div>
    )
  },
  render() {
    return (
      <Dropdown overlay={this.messageBox()} trigger={['click']} className="messagebox-dropdown" getPopupContainer={() => $(".top-nav-con")[0]}>
        <Badge count={this.state.count} className="ant-badge messagebox-dropdown-link">
          <Icon type="notification" />
        </Badge>
      </Dropdown>
    );
  }
});

module.exports = Notification;