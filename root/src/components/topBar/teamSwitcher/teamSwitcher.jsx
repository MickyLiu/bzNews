import { Menu, Icon, Row, Dropdown,Select } from 'antd';
import {Link} from 'react-router';
const Option = Select.Option;
let React = require('react');
let classnames = require('classnames');
let events = require('./../../../_utils/eventSubscriber');
let ajaxEx = require('./../../../_utils/ajaxConfiger');
let Const = require('./../../../_utils/const');
let $ = require('jquery');
let _ = require('lodash');

require('./teamSwitcher.scss');

const TeamSwitcher = React.createClass({
  getInitialState() {
   return{
      data:[],
      value: "",
      title: ""
    };
  },
  componentDidMount: function () {
    this.updateData();
  },
  updateData() {
    let _this = this;
    ajaxEx.get({url:Const.API.urlPrefix + 'teams'},function(d){

            _this.setState({
              data:d.data
           });

           d.data.forEach(function(o,i){
              if(o.id==globalVars.get("_teamId_"))
              {
                  _this.setState({
                    value: o.id,
                    title: o.attributes.name
                 });
              }
           });
    });
  },
  componentWillUnmount: function(){

  },
  handleChange(id, name){
      var _this = this,data = this.state.data;
      if(id!='disabled')
      {

          globalVars.set('_teamId_',id);
          globalVars.set('_teamName_', name);

          let ability = JSON.parse(localStorage.getItem('ability'));
          for(let i=0; i< ability.length; i++) {
            if(ability[i].team_id == id) {
              globalVars.set('_currentRole_',ability[i].role);
              break;
            }
          }

          data.forEach(function(o,i){
              if(o.id==id)
              {
                  _this.setState({
                    value: o.id,
                    title: o.attributes.name
                 });
              }
          });
          window.location = '#/';
          events.publish('refreshProjectList');
      }
  },
  menuBoard() { return (
    <div id="teamMenu" className="team-menu">
      {globalVars.get('_currentRole_') == 'super' ?
        [<Link to='/team' key="1"><div className="team-page" key="1">团队账户</div></Link>,
        <div className="separate-title" key="2"><span>切换团队</span></div>,
        <hr color="#ddd" size="1" key="3"/>,
        <div className="separate-title" key="4"></div>]
        : undefined
      }
      <div className="team-item-container">
        {_.map(this.state.data,(o,index)=>
          <div className="team-item" key={o.id} onClick={() => this.handleChange(o.id, o.attributes.name)} title={o.attributes.name}>{o.attributes.name}</div>
        )}
      </div>
      <hr color="#ddd" size="1"/>
      <div className="team-manage" onClick={() => window.location = "/teams" }>创建/管理团队</div>
    </div>
  )},
  render() {
    return (
      <Dropdown overlay={this.menuBoard()} trigger={['click']} className="team-dropdown" getPopupContainer={() => $(".top-nav-con")[0]}>
        <a className="team-dropdown-link" href="#">
          {this.state.title} <Icon type="down" />
        </a>
      </Dropdown>
    );
  }
});

module.exports = TeamSwitcher;