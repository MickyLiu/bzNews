import { Menu, Icon, Row, Dropdown,Select,Input,Col,Button } from 'antd';
import {Link} from 'react-router';

let React = require('react');
let classnames = require('classnames');
let UIChangeMixIn = require('./../../_utils/uiChangeMixIn');
let events = require('./../../_utils/eventSubscriber');
let ajaxEx = require('./../../_utils/ajaxConfiger');
let Const = require('./../../_utils/const');
let TeamSwitcher = require('./teamSwitcher/teamSwitcher');
let Notification = require('./notification/notification');
let $ = require('jquery');
require('./topBar.scss');
const Option = Select.Option;
const UserMenu = (
      <Menu>
        <Menu.Item key="1"><Link to="user">个人信息</Link></Menu.Item>
        <Menu.Item key="2"><Link to="logout">注销</Link></Menu.Item>
      </Menu>
    );

const TopBar = React.createClass({
  teamSwitcher: null,
  mixins:[UIChangeMixIn.routeOnTopMixIn,UIChangeMixIn.UiInfoChangeMixIn],
  getDefaultProps() {
    return {
      showRight: true
    };
  },
  getInitialState() {
   return{
      userName: "",
      currentRoute:'',
      searchAuthorText: '',
    };
  },
  componentDidMount: function () {
    let _this = this;
    ajaxEx.get({url:Const.API.customers + "/" + localStorage.getItem("currentUserId")},function(d){
      _this.setState({userName: d.data.attributes.name});
    });
    $('#searchInput').attr('placeholder', '输入作者名查询');
    events.subscribe('renderTopBar',(function(){
      this.teamSwitcher.updateData();
      this.forceUpdate();
    }).bind(this));
  },
  componentWillUnmount: function(){
    //events.unsubscribe('setCurrentUserName');
  },
  handleChange(){

  },
  logout: function() {
      var auth = this.props.Auth;
      auth.logout(function() {
          localStorage.removeItem('_projectId_');
          localStorage.removeItem('_teamId_');
          localStorage.removeItem('_teamName_');
          localStorage.removeItem('_currentRole_');
      });
  },
  searchAuthor: function() {
    if (this.state.searchAuthorText.length == 0) {
      return;
    }
    window.open(`/?q=${encodeURI(this.state.searchAuthorText)}#/searchAuthor`, "_blank");
  },
  render() {
    let classes = classnames('top-nav', {'nav-full': this.props.full},this.props.style);

    return (
      <div className="top-nav-con">
          <div className={classes}>
             <Link to='/home' className={"logo-bt"}>星辰大海</Link>
             {this.props.navFull?
              <div>
                   <div className="top-left-info">
                       <Link to='/home' className={this.state.currentRoute=='home'?'current':''}>首页</Link>
                       <TeamSwitcher ref={(c) => this.teamSwitcher = c }/>
                       <Link to='/' className={this.state.currentRoute==''?'current':''}>项目</Link>
                       <Link to='/members' className={this.state.currentRoute=='members'?'current':''}>团队成员</Link>
                   </div>

                   <div className="top-right-info">
                     <Input id='searchInput' placeholder="输入作者名查询" className={'nav-search-input'} onChange={(e) => this.setState({searchAuthorText: e.target.value.trim()})}/><Button className="nav-search-input-btn" onClick={this.searchAuthor}><Icon type="search" /></Button>
                     <Notification />
                     <Select size="large" value={this.state.userName} style={{height: '30px'}} onChange={this.handleChange}>
                          <Option value="1">
                                 <Link to='/user'>
                                    <span>账户设置</span>
                                 </Link>
                          </Option>
                          <Option value="2">
                               <a onClick={this.logout}>
                                 退出
                               </a>
                          </Option>
                     </Select>
                   </div>
                </div>
               :<div className="top-right-info" style={(!this.props.showRight) ? {display: 'none'} : {display: 'block'}}>
                   <span className="user-name">Hi,{this.state.userName}</span>
                   <a onClick={this.logout}>
                             退出
                   </a>
                </div>}
          </div>
      </div>
    );
  }
});

module.exports = TopBar;
