import { Tabs, Icon, Select, notification, Switch } from 'antd';

let React = require('react');
let classnames = require('classnames');
let ViewCountTab = require('./viewCountTab/viewCountTab');
let AuthorTab = require('./authorTab/authorTab');
let VideoSearch = require('./videoTab/videoSearch');
let SearchInput = require('../components/searchInput/searchInput');
let Const = require('./../_utils/const');
require('./dataStatisticsPanel.scss');

const Option = Select.Option;
const TabPane = Tabs.TabPane;
const DataStatisticsPanel = React.createClass({
  currentKey: 1,
  getInitialState () {
      return {
      }
  },
  componentDidMount() {
  },
  resize() {
      var tab = this.refs['tab' + this.currentKey];
      if (tab.resize) {
          tab.resize();
      }
  },
  onChange(key) {
      this.currentKey = key;
  },
  render() {
    return (
    <div className="data-statistics-panel">
        <Tabs defaultActiveKey="1" className="data-statistics-tab" onChange={this.onChange}>
            <TabPane tab={<span><Icon type="line-chart" />作者数据</span>} key="1">
                <AuthorTab ref="tab2" Auth={this.props.Auth}/>
            </TabPane>
            <TabPane tab={<span><Icon type="line-chart" />视频数据</span>} key="2">
                <VideoSearch ref="tab3" Auth={this.props.Auth}/>
            </TabPane>
        </Tabs>
    </div>
    );
  }
});

module.exports = DataStatisticsPanel;
