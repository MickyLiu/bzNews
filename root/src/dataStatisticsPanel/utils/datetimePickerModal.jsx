import { Modal, Button, message, Icon } from 'antd';
const React = require('react');
const DatetimePicker = require('../../components/datetimePicker/datetimePicker');
const timeHelper = DatetimePicker.timeHelper;

var DatetimePickerModal = React.createClass({
  getInitialState() {
    let ret = {
      mode: 0,
      start: null,
      end: null,
      visible: false,
      text: '最近24小时',
      _text: '最近24小时'
    };
    ret.end = new Date();
    ret.start = timeHelper.computeDay(ret.end, -1);
    return ret;
  },
  componentDidMount() {
    if (this.props.onChoose) {
      this.props.onChoose({
        start: this.state.start,
        end: this.state.end
      })
    }
  },
  showModal() {
    this.setState({
      visible: true
    });
  },
  handleOk() {
    if (this.state.mode == 0) {
      message.error('请选择一个时间段');
      return;
    }
    if (this.state.mode == 2 && this.state._text.length == 0) {
      message.error('请选择一个时间段');
      return;
    }
    this.setState({
      visible: false,
      text: this.state._text
    });
    if (this.props.onChoose) {
        this.props.onChoose({
          start: this.state.start,
          end: this.state.end
        });
      }
  },
  handleCancel() {
    this.setState({
      visible: false
    });
  },
  handleDatetimeChoose(data) {
    if (data.mode == 0) {
      this.setState({
        visible: false,
        text: data.key,
      });
      if (this.props.onChoose) {
        this.props.onChoose({
          start: data.start,
          end: data.end
        });
      }
    }
    this.setState({
      mode: data.mode,
      start: data.start,
      end: data.end,
      _text: data.key
    })
  },
  handlePickerTabChange(mode) {
    this.setState({
      mode: mode
    });
  },
  render() {
    return (
      <div style={{display: 'inline-block'}}>
        <Button type="ghost" onClick={this.showModal}><Icon type="calendar" />{this.state.text}</Button>
        <Modal title="选择时间" visible={this.state.visible}
          onOk={this.handleOk} onCancel={this.handleCancel} maskClosable={false}>
          <DatetimePicker onChoose={this.handleDatetimeChoose} onTabChange={this.handlePickerTabChange} defaultValue={this.props.defaultValue}/>
        </Modal>
      </div>
    )
  }
});
DatetimePickerModal.timeHelper = timeHelper;

module.exports = DatetimePickerModal;