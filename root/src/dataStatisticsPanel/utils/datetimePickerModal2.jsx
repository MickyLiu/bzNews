import { Modal, Button, message, Icon } from 'antd';
const React = require('react');
const SimpleDatetimePicker = require('../../components/simpleDatetimePicker/simpleDatetimePicker');
const timeHelper = SimpleDatetimePicker.timeHelper;

var DatetimePickerModal2 = React.createClass({
  getInitialState() {
    let ret = {
      mode: 0,
      start: null,
      end: null,
      text: '今天',
      _start: null,
      _end: null,
      _text: '今天',
      visible: false,
    };
    ret.end = new Date();
    ret.start = timeHelper.computeDay(ret.end, -1);
    return ret;
  },
  componentDidMount() {
    this.onChange();
  },
  onChange() {
    if (this.props.onChange) {
      this.props.onChange([this.state.start, this.state.end], this.state.text);
    }
  },
  showModal() {
    this.setState({
      visible: true
    });
  },
  handleOk() {
    this.setState({
      start: this.state._start,
      end: this.state._end,
      text: this.state._text,
      visible: false
    }, this.onChange);
  },
  handleCancel() {
    this.setState({
      visible: false
    });
  },
  handleDatetimeChoose(data, text) {
    this.setState({
      _start: data[0],
      _end: data[1],
      _text: text
    })
  },
  render() {
    return (
      <div style={{ display: 'inline-block' }} className={this.props.className}>
        <Button type="ghost" onClick={this.showModal}><Icon type="calendar" />{this.state.text}</Button>
        <Modal title="选择时间" visible={this.state.visible}
          onOk={this.handleOk} onCancel={this.handleCancel} maskClosable={false}>
          <SimpleDatetimePicker onChange={this.handleDatetimeChoose} start={this.state.start} end={this.state.end} text={this.state._text}/>
        </Modal>
      </div>
    )
  }
});
DatetimePickerModal2.timeHelper = timeHelper;

module.exports = DatetimePickerModal2;