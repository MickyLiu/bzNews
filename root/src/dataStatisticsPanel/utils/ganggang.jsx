import { Popover } from 'antd';
const React = require('react');
const $ = require('jquery');
const source = require('./source');
require('./ganggang.scss');

var Ganggang = React.createClass({
  getInitialState() {
    let state = {
      minWidth: 16,
    }
    return state
  },
  buildPopover(data) {
    return (
      <div>
        播放量: {data.value} <br/>
        占比: {`${(data.percent * 100).toFixed(2)}%`}
      </div>
    )
  },
  buildGang(data, index, datas) {
    return (
      <Popover key={data.name} overlay={this.buildPopover(data)} title={source[data.name].chinese} >
        <div className="ganggang-gang" style={{background: source[data.name].color, width: data.width}}>
          <div className="ganggang-title"><div className={"icon " + data.name}></div></div>
          <div className="ganggang-detail">{data.value}<br/>{`${(data.percent * 100).toFixed(2)}%`}</div>
        </div>
      </Popover>
    )
  },
  buildGangs() {
    let sum = 0;
    let datas = this.props.dataSource;
    let computed = [];
    for (var i = 0; i < datas.length; i++) {
      sum += datas[i].value;
      computed.push({
        value: datas[i].value,
        name: datas[i].name
      });
    }
    let width = $(this.dom).width();
    // percent
    for (var i = 0; i < computed.length; i++) {
      var site = computed[i];
      site.percent = site.value / sum;
      site.shouldCompute = width * site.percent > this.state.minWidth;
      if (!site.shouldCompute) {
        site.width = `${this.state.minWidth}px`
      }
    }
    // compute new sum and percent
    let shouldCompute = computed.filter(c => c.shouldCompute);
    sum = 0;
    for (var i = 0; i < shouldCompute.length; i++) {
      sum += shouldCompute[i].value;
    }
    for (var i = 0; i < shouldCompute.length; i++) {
      var site = shouldCompute[i];
      site.width = `calc(${site.value / sum * 100}% - ${(computed.length - shouldCompute.length) * this.state.minWidth * site.value / sum}px)`
    }
    // console.log(width, computed);
    return computed.map(this.buildGang);
  },
  render() {
    return (
      <div className="ganggang" ref={(c) => this.dom = c}>
        {this.buildGangs()}
      </div>
    )
  }
});

module.exports = Ganggang;