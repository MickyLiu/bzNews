const timeHelper = require('./datetimePickerModal2').timeHelper;
const graphic = require('./../../components/echarts/echarts').graphic;
const source = require('./source');

module.exports = {
  cleanResData: function (data) {
    let ret = [];
    // 取得有效来源
    for (var i = 0; i < data.length; i++) {
      var res = data[i];
      if (res.playcounts.length > 0) {
        ret.push(res);
      }
    }
    // 对返回数据修饰，目的是为了统一数据数量，生成单调递增函数:
    // 0. 清洗数据，将小于前方数据的数据均去除
    // 1. 前方缺失数据补齐
    // 2. 后方缺失数据补齐
    // 3. 中间缺失数据前后求平均

    // 清洗数据
    for (var i = 0; i < ret.length; i++) {
      var site = ret[i];
      let psc = [];
      for (var j = 0; j < site.playcounts.length; j++) {
        if (psc.length == 0 || psc[psc.length - 1].playcount <= site.playcounts[j].playcount) {
          psc[psc.length] = site.playcounts[j];
        }
      }
      site.playcounts = psc;
    }
    // 补数据前寻找最大和最小时间
    let maxtime = "";
    let mintime = "2100-01-01T00:00:00.000+08:00";
    for (var i = 0; i < ret.length; i++) {
      var psc = ret[i].playcounts;
      for (var j = 0; j < psc.length; j++) {
        var c = psc[j].created_at;
        if (c > maxtime) {
          maxtime = c;
        }
        if (c < mintime) {
          mintime = c;
        }
      }
    }
    // 补数据
    for (var i = 0; i < ret.length; i++) {
      var psc = ret[i].playcounts;
      // 补齐前方数据
      if (psc[0].created_at != mintime) {
        let _mintime = new Date(mintime);
        let timespan = new Date(psc[0].created_at).getTime() - _mintime.getTime();
        let num = Math.floor(timespan / timeHelper.hour);
        let fill = [];
        for (var j = 0; j < num; j++) {
          fill.push({
            created_at: timeHelper.easyFormat(new Date(_mintime.getTime() + j * timeHelper.hour), '%y-%m-%dT%H:00:00.000+08:00'),
            playcount: psc[0].playcount
          })
        }
        psc.splice(0, 0, ...fill);
      }
      // 补齐后方数据
      if (psc[psc.length - 1].created_at != maxtime) {
        let _maxtime = new Date(maxtime);
        let timespan = _maxtime.getTime() - new Date(psc[psc.length - 1].created_at).getTime();
        let num = Math.floor(timespan / timeHelper.hour);
        let fill = [];
        for (var j = 0; j < num; j++) {
          fill.push({
            created_at: timeHelper.easyFormat(new Date(_maxtime.getTime() - (num - 1 - j) * timeHelper.hour), '%y-%m-%dT%H:00:00.000+08:00'),
            playcount: psc[psc.length - 1].playcount
          })
        }
        psc.splice(psc.length, 0, ...fill);
      }
      // 补齐中间缺失数据
      if (psc.length > 2) {
        let j = 1, length = psc.length;
        for(;j < length; j++) {
          let timespan = new Date(psc[j].created_at).getTime() - new Date(psc[j - 1].created_at).getTime();
          if (timespan != timeHelper.hour) {
            let num = Math.floor(timespan / timeHelper.hour);
            let piece = (psc[j].playcount - psc[j - 1].playcount) / num;
            let fill = [];
            for (var k = 0; k < num - 1; k++) {
              fill.push({
                created_at: timeHelper.easyFormat(new Date(new Date(psc[j - 1].created_at).getTime() + (k + 1) * timeHelper.hour), '%y-%m-%dT%H:00:00.000+08:00'),
                playcount: Math.round(psc[j - 1].playcount + (k + 1) * piece)
              });
            }
            psc.splice(j, 0, ...fill);
            length = psc.length;
            j += num - 1;
          }
        }
      }
    }
    return ret;
  },
  aggregationByDay(data) {
    let tmp = {}
    data.map(v => {
      let date = v.created_at.slice(0, 10);
      if (!tmp[date]) {
        // 获取每日第一个数据，这个数据将作为前一天的最终数据
        tmp[date] = v;
      }
    });
    // 将每一天第一个数据的日期往前平移一天
    tmp = Object.keys(tmp).map(v => tmp[v]).sort((a, b) => {
      a = a.created_at;
      b = b.created_at;
      if (a == b) {
        return 0;
      }
      if (a > b) {
        return 1;
      }
      if (a < b) {
        return -1
      }
    });
    for (var i = 0; i < tmp.length; i++) {
      var d = tmp[i];
      d.created_at = timeHelper.easyFormat(timeHelper.computeDay(d.created_at, -1), '%y-%m-%dT%H:00:00.000+08:00');
    }
    // 将最后一个数据作为最后一日的数据
    tmp.push(data[data.length - 1]);
    return tmp;
  },
  buildNormalData(data, format) {
    format = format || '%y-%m-%d %H:%M'
    if (data == undefined || data.length == 0) {
      return [];
    }
    var dataret = data.map(v => {
      let date = new Date(v.created_at);
      date = timeHelper.easyFormat(date, format);
      return {
        name: `${date}`,
        value: [
          date,
          v.playcount
        ]
      }
    });
    return dataret;
  },
  buildDiffData(data, format) {
    var data = this.buildNormalData(data, format);
    var dataret = [];
    for (var i = 1; i < data.length; i++) {
      let hour = data[i]
      dataret.push({
        name: hour.name,
        value: [
          hour.value[0],
          hour.value[1] - data[i - 1].value[1]
        ]
      });
    }
    return dataret;
  },
  getSeries(name, type, data) {
    return {
      name: name,
      data: data,
      type: type,
      // markPoint: {
      //     data: [
      //         {type: 'max', name: '最大值'},
      //         {type: 'min', name: '最小值'}
      //     ]
      // },
      // markLine: {
      //     data: [
      //         {type: 'average', name: '平均值'}
      //     ]
      // }
      areaStyle: {
        normal: {
          color: new graphic.LinearGradient(0, 0, 0, 1, [{
            offset: 0,
            color: source[name].color
          }, {
            offset: 1,
            color: 'rgba(255, 255, 255, 0)'
          }])
        }
      },
      itemStyle: {
        normal: {
          opacity: 0
        },
        emphasis: {
          opacity: 1
        }
      },
    }
  },
  toChineseString(num) {
    if (num < 10000) {
      return num.toString();
    } else if (num < 100000000) {
      return `${(num / 10000).toFixed(2)}万`;
    } else {
      return `${(num / 100000000).toFixed(2)}亿`
    }
  }
}