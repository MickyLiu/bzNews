module.exports = {
  sum: {
    color: '#FFC24D',
    site: 'all',
    chinese: '全平台总和',
  },
  all: {
    color: '#FFC24D',
    site: 'all',
    chinese: '全平台各源',
  },
  qq: {
    color: '#EE8F06',
    site: 'qq',
    chinese: '腾讯视频',
  },
  bilibili: {
    color: '#CF4D73',
    site: 'bilibili',
    chinese: 'B站',
  },
  acfun: {
    color: '#FD4C5B',
    site: 'acfun',
    chinese: 'A站',
  },
  iqiyi: {
    color: '#5AA700',
    site: 'iqiyi',
    chinese: '爱奇艺',
  },
  youku: {
    color: '#4FB4E9',
    site: 'youku',
    chinese: '优酷',
  },
}