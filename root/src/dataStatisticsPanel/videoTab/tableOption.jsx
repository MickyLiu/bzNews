let React = require('react');

var timeTableOption = [{
  title: '时间',
  dataIndex: 'time',
  key: 'time',
  className: 'table1Columns-time',
}, {
  title: '播放增量',
  dataIndex: 'increase',
  key: 'increase',
  className: 'table1Columns-increase',
  render: (t) => {
    return <span>{t >= 0 ? t : '--'}</span>;
  }
}, {
  title: '播放总量',
  dataIndex: 'total',
  key: 'total',
  className: 'table1Columns-total',
  render: (t) => {
    return <span>{t}</span>;
  }
}];

var siteTableOption = [{
  title: '平台/时间',
  dataIndex: 'name',
  key: 'name',
  className: 'table2Columns-name'
}, {
  title: '播放增量',
  dataIndex: 'increase',
  key: 'increase',
  className: 'table2Columns-increase',
  render: (t) => {
    return <span>{t >= 0 ? t : '--'}</span>
  }
}, {
  title: '播放总量',
  dataIndex: 'total',
  key: 'total',
  className: 'table2Columns-total',
  render: (t) => {
    return <span>{t}</span>;
  }
}];

export { timeTableOption, siteTableOption }