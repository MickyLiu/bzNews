import { Button, Icon, Input } from 'antd';
let React = require('react');
let VideoTab = require('./videoTab');
let Const = require('./../../_utils/const');
require('./videoSearch.scss');

let timeout;
let currentValue;

function fetch(value, searchFunc, callback) {
  if (timeout) {
    clearTimeout(timeout);
    timeout = null;
  }
  currentValue = value;

  function fake() {
    searchFunc(value, callback);
  }

  timeout = setTimeout(fake, 500);
}

const VideoSearch = React.createClass({
  // lifecycle
  getInitialState: function () {
    let state = {
      // search
      searching: false,
      text: '',
      searchResult: [],
      // video
      selected: false,
      selected_video: null,
    }
    return state;
  },
  componentDidMount() {
    this.init()
  },
  // event
  init() {

  },
  // search video
  onSearchTextChange(value) {
    value = value.target.value;
    if (value.length == 0) {
      return;
    }
    this.setState({ text: value });
    fetch(value, this.searchVideo, this.showSearchResult);
  },
  searchVideo(str, callback) {
    var auth = this.props.Auth;
    auth.ajax(Const.API.videoSearch + `?key_word=${str}&page=1&per=20`)
      .done(function (data) {
        callback(data.data);
      });
  },
  showSearchResult(data) {
    data = data.map((d) => {
      return {
        title: d.attributes.title,
        thumbnail: d.attributes.thumbnail,
        id: d.id,
        published_at: d.attributes.published_at
      }
    });
    this.setState({
      searchResult: data,
    })
  },
  selectVideo(video) {
    this.setState({
      selected_video: video,
      selected: true,
    });
  },
  render() {
    return (
      <div className='video-tab-content'>
        {this.state.selected ?
          (
            <VideoTab Auth={this.props.Auth} video={this.state.selected_video} onReturn={() => this.setState({selected: false})}/>
          ) : (
            <div className={'video-search' + (this.state.searchResult.length == 0 ? ' empty' : '').toString() }>
              <div className="video-search-area">
                <Input id="largeInput" size="large" placeholder="请输入视频标题关键字" className="video-text" onChange={this.onSearchTextChange} defaultValue={this.state.text}/>
              </div>
              <div className="video-search-result">
                {
                  this.state.searchResult.map((r, i) =>
                    <div className="search-video-container" key={i}>
                      <div className="search-video" onClick={() => this.selectVideo(r)}>
                        <div className="thumbnail"><img src={r.thumbnail} alt={r.title}/></div>
                        <div className="title" title={r.title}>{r.title}</div>
                      </div>
                    </div>
                  )
                }
              </div>
              <div className="video-empty">
                <Icon type="frown" />暂无结果
              </div>
            </div>
          )
        }
      </div>
    );
  }
});

module.exports = VideoSearch;
