import { Tabs, Icon, DatePicker, Switch, Select, Table, Button } from 'antd';

let React = require('react');
let Echarts = require('../../components/echarts/echarts');
let classnames = require('classnames');
let Const = require('./../../_utils/const');
let DatetimePickerModal2 = require('./../utils/datetimePickerModal2');
let Ganggang = require('./../utils/ganggang');
let timeHelper = DatetimePickerModal2.timeHelper;
let seriesHelper = require('./../utils/seriesHelper');
let source = require('./../utils/source');
let chartOption = require('./chartOption').chartOption;
let chartOptionHandler = require('./chartOption').chartOptionHandler;
let timeTableOption = require('./tableOption').timeTableOption;
let siteTableOption = require('./tableOption').siteTableOption;
require('./videoTab.scss');

const Option = Select.Option;
const OptGroup = Select.OptGroup;

const VideoTab = React.createClass({
  // lifecycle
  getInitialState: function () {
    let state = {
      overview: [],
      overviewCount: "0",
      resources: [],
      selectResources: 'sum',
      date: {
        start: new Date(0),
        end: new Date(0),
      },
      sourceData: [],
      chartDiffData: [],
      chartNormalData: [],
      shouldAggregation: false,
      updateChart: true,
      timeTableData: [],
      siteTableData: []
    }
    return state;
  },
  componentDidMount() {
    this.setState({
      date: {
        start: timeHelper.computeDay(new Date(), -1),
        end: new Date()
      }
    }, this.init);
  },
  // event
  init() {
    let auth = this.props.Auth;
    let self = this;
    
    var vid, start, end, url;
    // set total data
    vid = this.props.video.id;
    start = timeHelper.easyFormat(this.state.date.start, '%y-%m-%d %H:%M:%S');
    end = timeHelper.easyFormat(this.state.date.end, '%y-%m-%d %H:%M:%S');
    url = Const.API.playcounts + `?from=${start}&to=${end}&type=video&query_id=${vid}&site=all`;
    auth.ajax(url).done(function (data) {
      data = seriesHelper.cleanResData(data);
      let sum = 0;
      data = data.map((v) => {
        let p = v.playcounts[v.playcounts.length - 1].playcount;
        sum += p;
        return {
          value: p,
          name: v.site
        }
      });
      self.setState({
        overview: data,
        overviewCount: sum
      });
    });
    // set detail data
    this.requsetDetail();
  },
  requsetDetail() {
    var self = this;
    var vid, start, end, url;
    vid = this.props.video.id;
    start = timeHelper.easyFormat(this.state.date.start, '%y-%m-%d %H:%M:%S');
    end = timeHelper.easyFormat(this.state.date.end, '%y-%m-%d %H:%M:%S');
    url = Const.API.playcounts + `?from=${start}&to=${end}&type=video&query_id=${vid}&site=${source[this.state.selectResources].site}`;
    let shouldAggregation = (this.state.date.end - this.state.date.start) > timeHelper.day;
    auth.ajax(url).done(function (data) {
      data = seriesHelper.cleanResData(data);
      let sourceData = data;
      // 处理sum数据
      if (self.state.selectResources == 'sum') {
        // 将所有数据加起来
        let source = data;
        data = {};
        for (var i = 0; i < source.length; i++) {
          var pcs = source[i].playcounts;
          for (var j = 0; j < pcs.length; j++) {
            var node = pcs[j];
            if (data[node.created_at]) {
              data[node.created_at] += node.playcount;
            } else {
              data[node.created_at] = node.playcount;
            }
          }
        }
        source = data;
        data = [{
          playcounts: [],
          site: 'sum'
        }]
        data[0].playcounts = Object.keys(source).map((t) => {
          return {
            created_at: t,
            playcount: source[t]
          }
        });
        data[0].playcounts.sort((a, b) => {
          a = a.created_at;
          b = b.created_at;
          if (a == b) {
            return 0;
          }
          if (a > b) {
            return 1;
          }
          if (a < b) {
            return -1
          }
        });
      }
      let timeFormat = '%y-%m-%d %H:%M';
      if (shouldAggregation) {
        for (var i = 0; i < data.length; i++) {
          data[i].playcounts = seriesHelper.aggregationByDay(data[i].playcounts);
        }
        timeFormat = '%y-%m-%d';
      }
      // 处理结束
      let diffData = [];
      let normalData = [];
      for (var i = 0; i < data.length; i++) {
        var _data = data[i];
        let _diffData = seriesHelper.getSeries(_data.site, 'line', seriesHelper.buildDiffData(_data.playcounts, timeFormat));
        let _normalData = seriesHelper.getSeries(_data.site, 'line', seriesHelper.buildNormalData(_data.playcounts, timeFormat));
        diffData.push(_diffData);
        normalData.push(_normalData);
      }
      let timeTableData = [];
      if (normalData.length > 0) {
        timeTableData = normalData[0].data.map((v, i) => {
          return {
            key: i + 1,
            time: v.name,
            increase: i == 0 ? -1 : diffData[0].data[i - 1].value[1],
            total: v.value[1]
          }
        });
      }
      let siteTableData = [];
      if (self.state.selectResources == 'all' && normalData.length > 0) {
        siteTableData = sourceData.map((site, i) => {
          let ret = {
            key: site.site,
            name: site.site
          };
          let pcs = site.playcounts;
          if (pcs.length == 1) {
            ret.total = pcs[0].playcount;
            ret.increase = -1;
          } else {
            ret.total = pcs[pcs.length - 1].playcount;
            ret.increase = pcs[pcs.length - 1].playcount - pcs[0].playcount;
          }
          ret.children = pcs.map((pc, i) => {
            return {
              key: site.site + i,
              name: timeHelper.easyFormat(new Date(pc.created_at), '%y-%m-%d %H:%M'),
              total: pc.playcount,
              increase: -1
            }
          });
          for (var i = 1; i < ret.children.length; i++) {
            ret.children[i].increase = ret.children[i].total - ret.children[i - 1].total;
          }
          return ret
        });
      }
      self.setState({
        shouldAggregation: shouldAggregation,
        sourceData: sourceData,
        chartDiffData: diffData,
        chartNormalData: normalData,
        timeTableData: timeTableData,
        siteTableData: siteTableData
      }, self.handleDetail)
    });
  },
  handleDetail() {
    let option = this.chart.getOption();
    option = chartOptionHandler(this, option);
    this.chart.setOption(option, this.state.updateChart);
  },
  render() {
    return (
      <div className='video-tab-content'>


        <div className="video-overview">
          <div className="video-info">
            <div className="video-avatar"><img src={this.props.video.thumbnail} /></div>
            <div className="video-name" style={{fontSize: `${Math.min(24, Math.max(12, Math.floor(300 / this.props.video.title.length)))}px`}} title={this.props.video.title}>{this.props.video.title}</div>
          </div>
          <div className="video-playcount">
            <div className="video-playcount-text">当前视频总播放数<span className="count">{seriesHelper.toChineseString(this.state.overviewCount)}</span></div>
            <Ganggang dataSource={this.state.overview}/>
          </div>
          <div className="return-search">
            <Button type="primary" onClick={this.props.onReturn}>
              <Icon type="search" />返回搜索
            </Button>
          </div>
        </div>


        <hr/>


        <div className="video-tab-select">
          <Select className="site-select" defaultValue="sum" onChange={(v) => this.setState({selectResources: v, updateChart: true}, this.requsetDetail)} style={{width: '150px'}}>
            {Object.keys(source).map((k) => <Option value={k} key={k}>{source[k].chinese}播放趋势</Option>)}
          </Select>
          <DatetimePickerModal2 className="date-select" onChange={(date) => this.setState({date: {start: date[0], end: date[1]}, updateChart: false}, this.requsetDetail) } defaultValue={this.state.date}/>
        </div>


        <div className="tab-h">
          <h3><Icon type="line-chart" />&nbsp; 播放量增长图表</h3>
        </div>
        <div className="video-chart">
          <Echarts ref={(c) => this.chart = c} name='viewcountChart' Option={chartOption} className="viewcount-chart" />
        </div>

        {
          this.state.selectResources == 'all' ? ([
            <div className="tab-h" key="0">
              <h3><Icon type="line-chart" />&nbsp; 来源明细数据</h3>
            </div>,
            <div className="author-table" key="1">
              <Table className="viewcount-daily-analysis-table" columns={siteTableOption} dataSource={this.state.siteTableData} />
            </div>
          ]) : ([
            <div className="tab-h" key="0">
              <h3><Icon type="line-chart" />&nbsp; 时间明细数据</h3>
            </div>,
            <div className="video-table" key="1">
              <Table className="viewcount-daily-analysis-table" columns={timeTableOption} dataSource={this.state.timeTableData} />
            </div>
          ])
        }

      </div>
    );
  }
});

module.exports = VideoTab;
