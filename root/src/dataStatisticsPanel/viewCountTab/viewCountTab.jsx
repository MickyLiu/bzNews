import { viewchartOption, analysischart_1_Option, analysischart_2_Option } from './viewCountTabChartOptions';
import { totalTableOption, dailyTableOption } from './viewCountTabTableOptions'
import { Tabs, Icon, DatePicker, Switch, Select, Table } from 'antd';

let React = require('react');
let Echarts = require('../../components/echarts/echarts');
let classnames = require('classnames');
let Const = require('./../../_utils/const');
let DatetimePickerModal = require('./../utils/datetimePickerModal');
let Ganggang = require('./../utils/ganggang');
let timeHelper = DatetimePickerModal.timeHelper;
require('./viewCountTab.scss');

const Option = Select.Option;
const OptGroup = Select.OptGroup;

const ViewCountTab = React.createClass({
    chartData: {
        id: null,
        viewcountSeries: [],
        mode: 'author',
        date: {
            start: new Date(0),
            end: new Date(0),
        },
        analysis_chart: {
            total_distribution: [],
            increase_distribution: []
        },
        analysis_table: {
            total: [],
            daily: [],
        }
    },
    getInitialState: function () {
        window.timeHelper = timeHelper;
        return this.chartData;
    },
    componentDidMount() {
        this.updateAllChart();
    },
    resize() {
        this.refs.viewcountChart.resize();
        this.refs.analysisChart1.resize();
    },
    setVideoId(id) {
        this.setState({
            id: id
        }, this.updateAllChart);
    },
    setMode(mode) {
        this.setState({
            mode: mode
        }, this.updateAllChart);
    },
    series(name, type, data) {
        return {
            name: name,
            data: data,
            type: type,
            // markPoint: {
            //     data: [
            //         {type: 'max', name: '最大值'},
            //         {type: 'min', name: '最小值'}
            //     ]
            // },
            // markLine: {
            //     data: [
            //         {type: 'average', name: '平均值'}
            //     ]
            // }
        }
    },
    updateSeries(callback) {
        if (this.state.mode == 'author' && this.state.date.start.getFullYear() > 2010) {
            this.updateAuthorSeries(callback);
        } else if (this.state.mode == 'video' && this.state.id != null) {
            this.updateVideoSeries(callback);
        }
    },
    updateAuthorSeries(callback) {
        var ret = []
        var self = this;
        // set start and end
        var start = timeHelper.easyFormat(this.state.date.start, '%y-%m-%d %H:%M:%S');
        var end = timeHelper.easyFormat(this.state.date.end, '%y-%m-%d %H:%M:%S');
        // TODO: set author id
        var aid = window.localStorage.getItem('_teamId_');
        var auth = this.props.Auth;
        var url = Const.API.playcounts + `?from=${start}&to=${end}&type=author&query_id=${aid}&site=all`;
        auth.ajax(url).done(function (data) {
            data = data.filter(v => {
                return v.playcounts.length > 0
            });
            if (data) {
                for (var i = 0; i < data.length; i++) {
                    var site = data[i];
                    ret.push(self.series(site.site, 'line', self.buildNormalData(site.playcounts)));
                }
            } else {
                ret = []
            }
            self.setState({
                viewcountSeries: ret,
                viewcountLegend: ret.map(v => v.name)
            }, () => {
                self.updateAnalysisData.call(self, callback);
            });
        });
    },
    updateVideoSeries(callback) {
        var ret = []
        var self = this;
        // set start and end
        var start = timeHelper.easyFormat(this.state.date.start, '%y-%m-%d %H:%M:%S');
        var end = timeHelper.easyFormat(this.state.date.end, '%y-%m-%d %H:%M:%S');
        var vid = this.state.id;
        var auth = this.props.Auth;
        var url = Const.API.playcounts + `?from=${start}&to=${end}&type=video&query_id=${vid}&site=all`;
        auth.ajax(url).done(function (data) {
            data = data.filter(v => {
                return v.playcounts.length > 0
            });
            if (data) {
                for (var i = 0; i < data.length; i++) {
                    var site = data[i];
                    ret.push(self.series(site.site, 'line', self.buildNormalData(site.playcounts)));
                }
            } else {
                ret = []
            }
            self.setState({
                viewcountSeries: ret,
                viewcountLegend: ret.map(v => v.name)
            }, () => {
                self.updateAnalysisData.call(self, callback);
            });
        });
    },
    cleanData(data) {
        let ret = [];
        for (let i = 0; i < data.length; i++) {
            let element = data[i];
            if (ret.length == 0 || ret[ret.length - 1].playcount <= element.playcount) {
                ret[ret.length] = element;
            }
        }
        return ret;
    },
    buildNormalData(data) {
        data = this.cleanData(data);
        if (data == undefined || data.length == 0) {
            return [];
        }
        var dataret = data.map(v => {
            let date = new Date(v.created_at);
            date = timeHelper.easyFormat(date, '%y-%m-%d %H:%M');
            return {
                name: `${date}`,
                value: [
                    date,
                    v.playcount
                ]
            }
        });
        return dataret;
    },
    updateAnalysisData(callback) {
        let self = this;
        // analysis_chart.total_distribution
        let total_distribution = this.state.viewcountSeries.map(v => {
            return {
                value: v.data.length == 0 ? 0 : parseInt(v.data[v.data.length - 1].value[1]),
                name: v.name
            }
        });
        // analysis_chart.increase_distribution
        let increase_distribution = this.state.viewcountSeries.map(v => {
            if (v.data.length < 2) {
                return {
                    value: 0,
                    name: v.name
                }
            }
            let last = parseInt(v.data[v.data.length - 1].value[1]);
            let increase = last - parseInt(v.data[0].value[1]);
            return {
                value: increase,
                name: v.name
            }
        });
        // analysis_table.total
        let table_total = [];
        table_total.increase = 0;
        table_total.total = 0;
        this.state.viewcountSeries.map((v, i) => {
            if (v.data.length == 0) {
                table_total.push({
                    name: v.name,
                    increase: 0,
                    total: 0,
                    increasespeed: '-'
                })
            } else {
                let last = parseInt(v.data[v.data.length - 1].value[1]);
                let increase = last - parseInt(v.data[0].value[1]);
                let start = new Date(v.data[0].value[0]);
                let end = new Date(v.data[v.data.length - 1].value[0]);
                table_total.push({
                    name: v.name,
                    increase: increase,
                    total: last,
                    increasespeed: (end - start) == 0 ? '-' : `${(increase / ((end - start) / 1000 / 60 / 60)).toFixed(2)}/小时`
                })
                table_total.increase += increase;
                table_total.total += last;
            }
        });
        if (table_total.total == 0) {
            table_total = []
        } else if (table_total.increase == 0) {
            for (var i = 0; i < table_total.length; i++) {
                var data = table_total[i];
                data.increase = '-';
                data.total = `${data.total.toLocaleString()}|(${(data.total * 100 / table_total.total).toFixed(2)}%)`;
            }
        } else {
            for (var i = 0; i < table_total.length; i++) {
                var data = table_total[i];
                data.increase = `${data.increase.toLocaleString()}|(${(data.increase * 100 / table_total.increase).toFixed(2)}%)`;
                data.total = `${data.total.toLocaleString()}|(${(data.total * 100 / table_total.total).toFixed(2)}%)`;
            }
        }
        // analysis_table.daily
        let table_daily = [];
        this.state.viewcountSeries.map((v, j) => {
            for (var i = 0; i < v.data.length; i++) {
                var data = v.data[i];
                var date = data.name.slice(0, 10);
                // 增加日期到表中
                if (table_daily[date]) {
                } else {
                    let arr = {
                        _key: date,
                        key: table_daily.length + 1,
                        name: date,
                        increase: 0,
                        increasep: 1,
                        total: 0,
                        totalp: 1,
                        children: []
                    }
                    table_daily[date] = arr;
                    table_daily.push(arr);
                }
                // 增加这个源到表中
                if (table_daily[date].children[v.name]) {
                    // 取同一天同一个源的最后的数据
                    let site = table_daily[date].children[v.name];
                    if (site.total < parseInt(data.value[1])) {
                        site.total = parseInt(data.value[1]);
                    }
                } else {
                    let site = {
                        key: table_daily[date].key * 100 + table_daily[date].children.length + 1,
                        _key: `${date}*${v.name}`,
                        name: v.name,
                        increase: 0,
                        increasep: 1,
                        total: parseInt(data.value[1]),
                        totalp: 1,
                    }
                    table_daily[date].children.push(site)
                    table_daily[date].children[v.name] = site;
                }
            }
        });
        // 按日期排序
        table_daily.sort((a, b) => {
            a = a.name; b = b.name;
            return a == b ? 0 : a > b ? 1 : -1;
        });
        // 计算增量及总量
        for (var i = 0; i < table_daily.length; i++) {
            var day = table_daily[i];
            var prevday = table_daily[i - 1];
            for (var j = 0; j < day.children.length; j++) {
                var children = day.children[j];
                if (prevday) {
                    var prevkey = prevday._key + '*' + children._key.split('*')[1];
                    var prevtotal = prevday.children.filter(v => v._key == prevkey);
                    if (prevtotal.length == 1) {
                        prevtotal = prevtotal[0].total
                        children.increase = children.total - prevtotal;
                    }
                }
                day.increase += children.increase;
                day.total += children.total;
            }
            delete table_daily[day.name];
        }
        for (var i = 0; i < table_daily.length; i++) {
            // 计算占比
            var day = table_daily[i];
            for (var j = 0; j < day.children.length; j++) {
                var children = day.children[j];
                if (i > 0) {
                    children.increasep = children.increase / day.increase;
                }
                children.totalp = children.total / day.total;
                delete children._key;
            }
            delete day._key;
        }
        this.setState({
            analysis_chart: {
                total_distribution: total_distribution,
                increase_distribution: increase_distribution
            },
            analysis_table: {
                total: table_total,
                daily: table_daily
            }
        }, () => {
            callback.call(self);
        })
    },
    optionHander: {
        viewcountChart: function (self, option) {
            option.series = self.state.viewcountSeries;
            if (option.xAxis && option.xAxis.length > 0) {
                option.xAxis[0].min = self.state.date.start.getTime();
                option.xAxis[0].max = self.state.date.end.getTime();
            }
            option.legend = {
                data: self.state.viewcountLegend,
                bottom: 0
            }
            return option;
        },
        analysisChart1: function (self, option) {
            option.series[0].data = self.state.analysis_chart.total_distribution;
            return option;
        },
        analysisChart2: function (self, option) {
            option.series[0].data = self.state.analysis_chart.increase_distribution;
            return option;
        }
    },
    updateOption(chart) {
        var option = chart.getOption();
        this.optionHander[chart.name](this, option);
        return option;
    },
    updateChart(chart, option, marge) {
        chart = chart.chart || chart;
        option = option || {};
        marge = marge || false;
        chart.setOption(option, marge);
    },
    updateAllChart() {
        var self = this;
        this.updateSeries(() => {
            var option = self.updateOption(self.refs.viewcountChart);
            self.updateChart(self.refs.viewcountChart, option, true);
            option = self.updateOption(self.refs.analysisChart1);
            self.updateChart(self.refs.analysisChart1, option, true);
            option = self.updateOption(self.refs.analysisChart2);
            self.updateChart(self.refs.analysisChart2, option, true);
        });
    },
    onDateChange(value) {
        // console.log(1)
        if (value.start.getTime() == this.state.date.start.getTime() && value.end.getTime() == this.state.date.end.getTime()) {
            return;
        }
        this.setState({ date: value }, this.updateAllChart);
    },
    render() {
        return (
            <div className='viewcount-tab-content'>
                <div className="viewcount-chart-area">
                    <Echarts ref='viewcountChart' name='viewcountChart' Option={viewchartOption} className="viewcount-chart" />
                    <div className="viewcount-chart-top-opt">
                        <DatetimePickerModal onChoose={this.onDateChange} defaultValue={this.state.date}/>
                    </div>
                    <div className="viewcount-chart-bottom-opt">
                    </div>
                </div>
                <div className='viewcount-tab-h3'>
                    <h3><Icon type="line-chart" />&nbsp; 总体数据</h3>
                </div>
                <div className="viewcount-analysis">
                    <Echarts ref='analysisChart1' name='analysisChart1' Option={analysischart_1_Option} className="viewcount-analysis-chart chart1"/>
                    <Echarts ref='analysisChart2' name='analysisChart2' Option={analysischart_2_Option} className="viewcount-analysis-chart chart2"/>
                    <Table className="viewcount-analysis-table" columns={totalTableOption} dataSource={this.state.analysis_table.total} pagination={false} size="middle" rowKey={(r, i) => i} useFixedHeader />
                </div>
                <div className='viewcount-tab-h3'>
                    <h3><Icon type="line-chart" />&nbsp; 每日数据</h3>
                </div>
                <div className="viewcount-daily-analysis">
                    <Table className="viewcount-daily-analysis-table" columns={dailyTableOption} dataSource={this.state.analysis_table.daily} pagination={false} />
                </div>
            </div>
        );
    }
});

module.exports = ViewCountTab;
