var colors = ['#c23531','#2f4554', '#61a0a8', '#d48265', '#91c7ae','#749f83',  '#ca8622', '#bda29a','#6e7074', '#546570', '#c4ccd3'];
var viewchartOption = {
    tooltip: {
        trigger: 'axis',
        formatter: function (params) {
            var ret = [];
            for (var i = 0; i < params.length; i++) {
                let param = params[i];
                if (param.value == undefined) {
                    ret.push(`<div class="chart-tooltip"><span class="chart-tooltip-name" style="background: ${colors[i % colors.length]}">${param.seriesName}</span> 无数据</div>`);
                } else {
                    ret.push(`<div class="chart-tooltip"><span class="chart-tooltip-name" style="background: ${colors[i % colors.length]}">${param.seriesName}</span><span class="chart-tooltip-time">${param.name}</span> ${parseInt(params[i].value[1]).toLocaleString()}</div>`);
                }
            }
            return ret.join('');
        },
        axisPointer: {
            animation: false
        }
    },
    grid: {
        right: 10,
        left: 60,
        top: 50,
    },
    toolbox: {
        show: true,
        feature: {
            dataZoom: {},
            dataView: {readOnly: false},
            restore: {},
            saveAsImage: {}
        },
    },
    xAxis:  {
        // type: 'category',
        // boundaryGap: false,
        // data: [
        //     '00:00','01:00','02:00','03:00','04:00','05:00',
        //     '06:00','07:00','08:00','09:00','10:00','11:00',
        //     '12:00','13:00','14:00','15:00','16:00','17:00',
        //     '18:00','19:00','20:00','21:00','22:00','23:00',
        // ]
        type: 'time'
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            formatter: function(value, index) {
                if (value < 1000) {
                    return value.toString();
                } else if (value < 1000000) {
                    return (value / 1000).toFixed(1) + 'K';
                } else if (value < 1000000000) {
                    return (value / 1000000).toFixed(1) + 'M';
                } else if (value < 1000000000000) {
                    return (value / 1000000000).toFixed(1) + 'B';
                } else {
                    return value.toString();
                }
            }
        },
        scale: true, // 脱离0
        // boundaryGap: ['30%', '30%']
    },
    series: []
};

var analysischart_1_Option = {
    tooltip: {
        trigger: 'item',
        formatter: "{b} <br/>{c}<br/> {d}%"
    },
    toolbox: {
        show: true,
        feature: {
            saveAsImage: {}
        }
    },
    title : {
        text: '播放量分布',
        y: 'bottom',
        x: 'center',
        textStyle: {
            fontSize: '16'
        }
    },
    series: [
        {
            name:'播放量分布',
            type:'pie',
            radius: ['50%', '70%'],
            avoidLabelOverlap: false,
            label: {
                normal: {
                    show: false,
                    position: 'center'
                },
                emphasis: {
                    show: true,
                    textStyle: {
                        fontSize: '24',
                        fontWeight: 'bold'
                    }
                }
            },
            labelLine: {
                normal: {
                    show: false
                }
            },
            data: [
                // {value:335, name:'直接访问'},
                // {value:310, name:'邮件营销'},
                // {value:234, name:'联盟广告'},
                // {value:135, name:'视频广告'},
                // {value:1548, name:'搜索引擎'}
            ]
        }
    ]
};

var analysischart_2_Option = {
    tooltip: {
        trigger: 'item',
        formatter: "{b} <br/>{c}<br/> {d}%"
    },
    toolbox: {
        show: true,
        feature: {
            saveAsImage: {}
        }
    },
    title : {
        text: '新增量分布',
        y: 'bottom',
        x: 'center',
        textStyle: {
            fontSize: '16'
        }
    },
    series: [
        {
            name:'新增量分布',
            type:'pie',
            radius: ['50%', '70%'],
            avoidLabelOverlap: false,
            label: {
                normal: {
                    show: false,
                    position: 'center'
                },
                emphasis: {
                    show: true,
                    textStyle: {
                        fontSize: '24',
                        fontWeight: 'bold'
                    }
                }
            },
            labelLine: {
                normal: {
                    show: false
                }
            },
            data: [
                // {value:335, name:'直接访问'},
                // {value:310, name:'邮件营销'},
                // {value:234, name:'联盟广告'},
                // {value:135, name:'视频广告'},
                // {value:1548, name:'搜索引擎'}
            ]
        }
    ]
};

// var analysischart_3_Option = {
//     tooltip: {
//         trigger: 'axis'
//     },
//     grid: {
//         right: 10,
//         left: 60,
//         top: 50,
//     },
//     toolbox: {
//         show: true,
//         feature: {
//             saveAsImage: {}
//         }
//     },
//     title : {
//         text: '常用数据',
//         y: 'bottom',
//         x: 'center',
//         textStyle: {
//             fontSize: '16'
//         }
//     },
//     xAxis:  {
//         type: 'category',
//         boundaryGap: true,
//         data: ['最小值', '平均值', '最大值']
//     },
//     yAxis: {
//         type: 'value',
//         axisLabel: {
//             formatter: function(value, index) {
//                 if (value < 1000) {
//                     return value.toString();
//                 } else if (value < 1000000) {
//                     return (value / 1000).toFixed(1) + 'K';
//                 } else if (value < 1000000000) {
//                     return (value / 1000000).toFixed(1) + 'M';
//                 } else if (value < 1000000000000) {
//                     return (value / 1000000000).toFixed(1) + 'B';
//                 } else {
//                     return value.toString();
//                 }
//             }
//         },
//         // scale: true, // 脱离0
//         // boundaryGap: ['30%', '30%']
//     },
//     series: []
// };

export { viewchartOption, analysischart_1_Option, analysischart_2_Option };