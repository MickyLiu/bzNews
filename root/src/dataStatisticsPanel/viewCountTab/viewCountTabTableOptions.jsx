let React = require('react');

var totalTableOption = [{
    title: '平台',
    dataIndex: 'name',
    key: 'name',
    width: '16%',
    className: 'table1Columns-name',
}, {
    title: '播放增量',
    dataIndex: 'increase',
    key: 'increase',
    width: '28%',
    className: 'table1Columns-increase',
    render: (t) => {
        let split = t.split('|');
        return <span>{split[0]}<br/>{split[1]}</span>;
    }
}, {
    title: '增量速度',
    dataIndex: 'increasespeed',
    key: 'increasespeed',
    width: '28%',
    className: 'table1Columns-increasespeed',
    render: (t) => {
        return <span>{t}</span>;
    }
}, {
    title: '播放总量',
    dataIndex: 'total',
    key: 'total',
    width: '28%',
    className: 'table1Columns-total',
    render: (t) => {
        let split = t.split('|');
        return <span>{split[0]}<br/>{split[1]}</span>;
    }
}];

var dailyTableOption = [{
    title: '时间/平台',
    dataIndex: 'name',
    key: 'name',
    className: 'table2Columns-name',
    width: '20%',
}, {
    title: '播放增量',
    dataIndex: 'increase',
    key: 'increase',
    className: 'table2Columns-increase',
    render: (t) => t == '0' ? '--' : parseInt(t).toLocaleString(),
    width: '30%',
}, {
    title: '占比',
    dataIndex: 'increasep',
    key: 'increasep',
    className: 'table2Columns-increasep',
    render: (t) => t == '1' ? '--' : `${parseFloat(t * 100).toFixed(2)}%`,
    width: '10%',
}, {
    title: '播放总量',
    dataIndex: 'total',
    key: 'total',
    className: 'table2Columns-total',
    render: (t) => t == '0' ? '--' : parseInt(t).toLocaleString(),
    width: '30%',
}, {
    title: '占比',
    dataIndex: 'totalp',
    key: 'totalp',
    className: 'table2Columns-totalp',
    render: (t) => t == '1' ? '--' : `${parseFloat(t * 100).toFixed(2)}%`,
    width: '10%',
}];

export { totalTableOption, dailyTableOption }