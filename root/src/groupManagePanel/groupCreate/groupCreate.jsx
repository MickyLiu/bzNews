import { Table, Icon, Spin,Form,Input,Button } from 'antd';
let Link = require('./../../components/link/link');

let React = require('react');
let events = require('./../../_utils/eventSubscriber');
let ajaxEx = require('./../../_utils/ajaxConfiger');
let Const = require('./../../_utils/const');
let $ = require('jquery');
require('./../groupEdit/groupEdit.scss');

const FormItem = Form.Item;
let groupCreateView = React.createClass({
  getInitialState() {
       return { title:"新建群组"};
  },
  componentDidMount: function () {
  },
  componentWillUnmount: function(){
  },
  handleSubmit(e) {
       e.preventDefault();
       var _this= this;
       this.props.form.validateFields((errors, values) => {
                if (!!errors) {
                  console.log('Errors in form!!!');
                  e.preventDefault();
                  return;
                }

          ajaxEx.submit({url:Const.API.projectGroups,formEle:$('#groupForm')[0],enabledLoadingMsg:true,enabledErrorMsg:true},function(d){
                   _this.props.history.goBack();
          });
          return false;
      });
  },
  handleInputChange(e) {
      if(e.target.type=='file')
      {
          var fr = new FileReader();
          fr.readAsDataURL(e.target.files[0]);
          fr.onload = function(e) {
             $('#groupImage').attr('src',e.target.result);
          };
      }
  },
  render() {
    var _this = this;
    const { getFieldProps,getFieldError } = this.props.form;
    const nameProps = getFieldProps('name', {
        validate: [{
          rules: [
            { required: true, message: '群组名不能为空'},
            { max: 15, message: '群组名不能超过15个字符'}
          ],
          trigger: 'onBlur',
        }]
    });

    return (
    <div>
       <div className="ant-breadcrumb custom">
          <span>
            <span className="ant-breadcrumb-link"><Link to='groups'>群组列表</Link></span>
            <span className="ant-breadcrumb-separator">/</span>
          </span>
          <span>
            <span className="ant-breadcrumb-link">{this.state.title}</span>
          </span>
       </div>
       <div className="group-con">
          <Form horizontal id={'groupForm'} form={this.props.form} method="post">
              <FormItem
                label="群组名：" labelCol={{ span: 3 }} wrapperCol={{ span: 14 }}>
                <Input placeholder="" {...nameProps}  name={'group[name]'} />
              </FormItem>
              <FormItem
                label="群组头像：" labelCol={{ span: 3 }} wrapperCol={{ span: 14 }}>
                <img id="groupImage" />
                <div><a className="file-upload">
                   选择文件
                   <Input type="file" name={'group[image]'} onChange={this.handleInputChange}/>
                </a></div>
              </FormItem>
              <FormItem
                label="群类型：" labelCol={{ span: 3 }} wrapperCol={{ span: 8 }}>
               <select size="large" style={{width:290,border:'none',height:'30'}} name="group[kind]">
                    <option value="star">明星</option>
                    <option value="nostar">非明星</option>
                </select>
              </FormItem>
              <FormItem wrapperCol={{ offset: 3 }}>
                <Button type="primary" onClick={this.handleSubmit}>保存</Button>
              </FormItem>
          </Form>
       </div>
     </div>
    );
  }
});

groupCreateView = Form.create()(groupCreateView);

module.exports = groupCreateView;
