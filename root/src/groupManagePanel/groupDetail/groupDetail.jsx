import { Table, Icon, Spin,Form,Input,InputNumber,Button,Upload } from 'antd';

let React = require('react');
let events = require('./../../_utils/eventSubscriber');
let ajaxEx = require('./../../_utils/ajaxConfiger');
let Link = require('./../../components/link/link');
let Preloader = require('./../../components/imagePreloader/imagePreloader');
let groupManageStore = require('../store/groupManageStore');
let GroupPosts = require('../groupPosts/groupPosts');

let Const = require('./../../_utils/const');
require('./groupDetail.scss');


const FormItem = Form.Item;
const GroupDetailView = React.createClass({
  getInitialState() {
     if(groupManageStore.getDataSource().length)
     {
        var itemData = groupManageStore.getItemByProperty('id',this.props.params.id);
        return this.getShowData(itemData[0]);
     }
     else
     {
        return  {
           data:{title:'',id:this.props.params.id},
           dataSource:[],
           columns:[]
        };
     }
  },
  getShowData(dataIn){
      var columns = [{
        title: '',
        dataIndex: 'd1'
      }, {
        title: '',
        dataIndex: 'd2',
        render(text,record) {
          if('群头像'==record.d1)
          {
             return (<Preloader className="group-avatar" url={text} defaultUrl={require('../../../assets/image/default-group-avatar.png')}/>);
          }
          else if('群类型'==record.d1)
          {
             return text=='star'?'明星':'非明星';
          }
          else
          {
             return text;
          }
        }
      }];

      var dataSource =  [];
      var mapping = groupManageStore.getItemOnMappingKey(dataIn);
      for(var i in mapping)
      {
         if(i=='群名' || i=='群头像' || i=='群类型')
         {
            dataSource.push({
              d1: i,
              d2: mapping[i]
            });
         }
         else{
           
         }
      }
      return {
          data:dataIn,
          dataSource:dataSource,
          columns:columns
      };
  },
  fetchGroup(gid){
       var _this=this;
       var groupId = gid? gid:this.props.params.id;
       if(!groupManageStore.getDataSource().length)
       {
             ajaxEx.get({url:Const.API.groups+'/'+ groupId},function(d){
                   var  mappingData= groupManageStore.mapping([d.data]);
                  _this.setState(_this.getShowData(mappingData[0]));
             });
       }
       else
       {
           var itemData = groupManageStore.getItemByProperty('id',groupId);
           _this.setState(_this.getShowData(itemData[0]));
       }
  },
  componentDidMount() {
        this.fetchGroup();
  },
  componentWillReceiveProps(nextProps){
        if(this.props.params.id==nextProps.params.id) return;
        this.fetchGroup(nextProps.params.id);
  },
  componentWillUnmount: function(){
     //events.unsubscribe('progressChange');
  },
  render() {
    return (
    <div>
       <div className="ant-breadcrumb custom">
          <span>
            <span className="ant-breadcrumb-link"><Link to='groups'>群组列表</Link></span>
            <span className="ant-breadcrumb-separator">/</span>
          </span>
          <span>
            <span className="ant-breadcrumb-link">{this.state.data.name}</span>
          </span>
       </div>
       <div>
          <Table columns={this.state.columns}  dataSource={this.state.dataSource} size="middle" pagination={false} showHeader={false}/>
          <div className="group-option">
                 <Link to={'groups/'+this.state.data.id +'/edit'}><Button type="primary" className="group-edit-btn">修改</Button></Link>
          </div>
       </div>

       <div className="group-posts">
            <div className="group-option post-top">
                <h3 className="group-post-title">下属帖子</h3>
                <Link to={'groups/'+ this.state.data.id + '/posts/create'}><Button type="primary" className="post-add-btn">新建</Button></Link>
            </div>
            <GroupPosts groupId={this.state.data.id}/>
       </div>
     </div>
    );
  }
});

module.exports = GroupDetailView;
