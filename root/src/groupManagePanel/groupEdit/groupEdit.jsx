import { Table, Icon, Spin,Form,Input,Button,Upload, Tabs } from 'antd';

let React = require('react');
let events = require('./../../_utils/eventSubscriber');
let ajaxEx = require('./../../_utils/ajaxConfiger');
let Const = require('./../../_utils/const');
let $ = require('jquery');
let groupManageStore = require('../store/groupManageStore');
let Link = require('./../../components/link/link');
require('./groupEdit.scss');

const FormItem = Form.Item;
const TabPane = Tabs.TabPane;
let groupEditView = React.createClass({
  getInitialState() {
      if(groupManageStore.getDataSource().length)
      {
          var itemData = groupManageStore.getItemByProperty('id',this.props.routeParams.id);
          return {
              data:itemData[0],
              title:itemData[0].name
          };
      }
      else
      {
          return { data:{id:this.props.params.id}};
      }
  },
  fetchGroup(gid){
       var _this=this;
       var groupId = gid? gid:this.props.routeParams.id;
       if(!groupManageStore.getDataSource().length)
       {
             ajaxEx.get({url:Const.API.groups+'/'+ groupId},function(d){
                    var  mappingData= groupManageStore.mapping([d.data]);
                    _this.setState({data:mappingData[0],title:mappingData[0].name});
             });
       }
       else
       {
           var itemData = groupManageStore.getItemByProperty('id',groupId);
           _this.setState({data:itemData[0],title:itemData[0].name});
       }
  },
  componentDidMount: function () {
       this.fetchGroup();
  },
  componentWillReceiveProps(nextProps){
        if(this.props.params.id==nextProps.params.id) return;
        this.fetchGroup(nextProps.params.id);
  },
  componentWillUnmount: function(){
     //events.unsubscribe('progressChange');
  },
  handleSubmit(e) {
       e.preventDefault();
       var _this= this;
       this.props.form.validateFields((errors, values) => {
                if (!!errors) {
                  console.log('Errors in form!!!');
                  e.preventDefault();
                  return;
                }

          ajaxEx.submit({url:Const.API.groups+'/'+ _this.state.data.id,formEle:$('#groupForm')[0],enabledLoadingMsg:true,enabledErrorMsg:true},function(d){
                   groupManageStore.clearData();
                   window.history.back();
          });
          return false;
      });
  },
  handleInputChange(e) {
      var data = this.state.data;
      data[e.target.attributes['id'].value]= e.target.value;
      this.setState({
          data: data
      });

      if(e.target.type=='file')
      {
          var fr = new FileReader();
          fr.readAsDataURL(e.target.files[0]);
          fr.onload = function(e) {
             $('#groupImageShow').attr('src',e.target.result);
          };
      }
  },
  render() {
    var _this = this;
    const { getFieldProps,getFieldError } = this.props.form;
    const nameProps = getFieldProps('name', {
        validate: [{
          rules: [
            { required: true, message: '群组名不能为空'}
          ],
          trigger: 'onBlur',
        }],
        initialValue: _this.state.data.name
    });
    return (
    <div>
       <div className="ant-breadcrumb custom">
          <span>
            <span className="ant-breadcrumb-link"><Link to='groups'>群组列表</Link></span>
            <span className="ant-breadcrumb-separator">/</span>
          </span>
          <span>
            <span className="ant-breadcrumb-link">{this.state.title}</span>
          </span>
       </div>
       <div className="group-con">
          <Form horizontal id={'groupForm'} form={this.props.form} method="put" action={Const.API.groups+'/'+ this.state.data.id}>
              <FormItem
                label="群组名：" labelCol={{ span: 3 }} wrapperCol={{ span: 14 }}>
                <Input placeholder="" {...nameProps}  name={'group[name]'} value={this.state.data.name} onChange={this.handleInputChange} />
              </FormItem>
              <FormItem
                label="群组头像：" labelCol={{ span: 3 }} wrapperCol={{ span: 14 }}>
                <img id="groupImageShow" src={this.state.data.image}/>
                <div><a className="file-upload">
                   选择文件
                   <Input type="file" id="groupImage" name={'group[image]'} onChange={this.handleInputChange}/>
                </a></div>
              </FormItem>
              <FormItem
                label="群类型：" labelCol={{ span: 3 }} wrapperCol={{ span: 8 }}>
               <select size="large" style={{width:290,border:'none',height:'30'}} name="group[kind]" {...getFieldProps('kind')} onChange={this.handleInputChange} value={this.state.data.kind}>
                    <option value="star">明星</option>
                    <option value="nostar">非明星</option>
                </select>
              </FormItem>
              <FormItem wrapperCol={{ offset: 3 }}>
                <Button type="primary" onClick={this.handleSubmit}>保存</Button>
              </FormItem>
          </Form>
       </div>
     </div>
    );
  }
});

groupEditView = Form.create()(groupEditView);

module.exports = groupEditView;
