import { Table, Icon, Spin, Popconfirm,InputNumber,Modal,Form } from 'antd';

let React = require('react');
let Link = require('./../../components/link/link');
let Preloader = require('./../../components/imagePreloader/imagePreloader');
let events = require('./../../_utils/eventSubscriber');
let ajaxEx = require('./../../_utils/ajaxConfiger');
let Const = require('./../../_utils/const');
require('./groupPosts.scss');
let postManageStore = require('./../../groupManagePanel/store/postManageStore');

const FormItem = Form.Item;
const columns = [{
    title: '帖子ID',
    dataIndex: 'id',
    key: 'id'
  },{
    title: '用户',
    dataIndex: 'creator_name',
    key: 'creator_name',
  },{
    title: '帖子',
    dataIndex: 'title',
    key: 'title',
    render(text, record) {
        var groupId = postManageStore.getRelations().group.data.id;
        var showLink = "groups/" + groupId + "/" + record.id;
        var content = record.content;
        return (
            <Link to={showLink} className="post-title">{text || content || (<Preloader url={record.pictures[0]}/>) }</Link>
        );
    }
  }, {
    title: '点赞数',
    dataIndex: 'likings_count',
    key: 'likings_count',
  }, {
    title: '回复数',
    dataIndex: 'comments_count',
    key: 'comments_count',
  },{
    title: '优先级',
    dataIndex: 'priority',
    key: 'priority',
  },{
    title: '发布时间',
    dataIndex: 'created_at',
    key: 'created_at',
  },{
    title: '操作',
    key: 'operation',
    render(text, record) {
      var groupId = postManageStore.getRelations().group.data.id;
      //var creator = record.user_id,current_user = localStorage.getItem('currentUserId');
      var showLink = "groups/" + groupId + "/" + record.id;
      var resetPostPriority = ()=>{ events.publish('showResetBox',{postId:record.id,priority:record.priority}); };
      return (
        <span>
          <Link to={showLink}>查看</Link>
          <span className="ant-divider"></span>
          <a href="javascript:void(0)" onClick={resetPostPriority}>置顶</a>
          <span className="ant-divider"></span>
          <Popconfirm title="确定要删除这个帖子么？" postId={record.id} onConfirm={delPostConfirm}>
            <a href="#">删除</a>
          </Popconfirm>
        </span>
      );
    }
  }];

var delPostConfirm =  function(){
      var _this =this;
      var groupId = postManageStore.getRelations().group.data.id;
      ajaxEx.delete({url:Const.API.posts + '/' + this.props.postId,enabledLoadingMsg:true,enabledErrorMsg:true},function(d){
                   events.publish('groupPostsDataChange');
      });
}

const TableView = React.createClass({
  getInitialState() {
     return{
        dataSource :[],
        pageSize:10,
        total:1,
        loading: true,
        resetVisible:false,
        priority:0,
        validateStatus:'',
        resetErrorMsg:'',
        confirmLoading:false,
        currentPostId:0,
        currentPage:1
    };
  },
  componentDidMount: function () {
       events.subscribe('groupPostsDataChange',(function(filter){
             this.getDataSource(this,filter);
       }).bind(this));

       events.subscribe('groupPostDelete',(function(postId){
              ajaxEx.delete({url:Const.API.groups + '/' + this.props.groupId + '/posts/' + postId,enabledLoadingMsg:true,enabledErrorMsg:true},function(d){
                   events.publish('groupPostsDataChange');
              });
       }).bind(this));

       events.subscribe('showResetBox',(function(data){
              this.setState({resetVisible:true,currentPostId:data.postId,priority:data.priority});
       }).bind(this));

       this.getDataSource(this);
  },
  getDataSource(_this,filter){
      var page = 1;
      if(filter)
      {
         page = filter.page?filter.page:1;
      }
      _this.setState({loading:true});
      ajaxEx.get({url:Const.API.groups + '/' +  _this.props.groupId +"/posts?page="+ page +"&per="+_this.state.pageSize},function(d){
           postManageStore.clearData().pushData(postManageStore.mapping(d.data));
           _this.setState({
              dataSource : postManageStore.getDataSource(),
              total: d.meta.total_pages*_this.state.pageSize,
              currentPage:page,
              loading:false
           });
       }).complete(function(){
             _this.setState({loading:false});
       });
  },
  pageChange:function(current){
       this.getDataSource(this,{"page":current});
  },
  componentWillUnmount: function(){
       postManageStore.clearData();
       events.unsubscribe('groupPostsDataChange');
       events.unsubscribe('groupPostDelete');
       events.unsubscribe('showResetBox');
  },
  handleInputChange(value) {
      this.setState({
          priority: value
      });
  },
  handleCancel(){
      this.setState({
        resetVisible: false,
        resetErrorMsg:'',
        validateStatus:''
      });
  },
  handleOk(){
    var _this = this,priority=_this.state.priority.toString().trim();
    if(priority)
    {
        this.setState({
          confirmLoading: true,
          resetErrorMsg:'',
          validateStatus:''
        });

        ajaxEx.post({url:Const.API.posts + '/' + this.state.currentPostId + '/set_priority',form:{priority:priority}},function(d){
               _this.setState({
                resetVisible: false,
                confirmLoading: false
              });
               _this.getDataSource(_this,{page:_this.state.currentPage});
        },function(er){
              _this.setState({
                confirmLoading: false,
                resetErrorMsg:er.responseJSON?er.responseJSON.errors[0].detail:'error',
                validateStatus:'error'
             });
        });
    }
    else{
        _this.setState({
            confirmLoading: false,
            resetErrorMsg:'请填优先级！',
            validateStatus:'error'
        });
    }
  },
  render() {
    return (
      <div>
            <Table columns={columns} dataSource={this.state.dataSource} loading={this.state.loading} pagination={{ onChange:this.pageChange, current:this.state.currentPage, pageSize: this.state.pageSize, total: this.state.total, showQuickJumper: true }} />
            <Modal title="重置优先级"
                  visible={this.state.resetVisible}
                  onOk={this.handleOk}
                  confirmLoading={this.state.confirmLoading}
                  onCancel={this.handleCancel}
                  okText="确定">
                      <FormItem
                        label="优先级：" labelCol={{ span: 3 }} wrapperCol={{ span: 14 }} help={this.state.resetErrorMsg} validateStatus={this.state.validateStatus}>
                        <InputNumber value={this.state.priority} onChange={this.handleInputChange}/>
                      </FormItem>
                      <div style={{clear:'both'}}></div>
            </Modal>
      </div>
    );
  }
});

module.exports = TableView;
