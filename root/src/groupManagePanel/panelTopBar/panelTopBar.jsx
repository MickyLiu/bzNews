import { Button } from 'antd';
let Link = require('./../../components/link/link');

let React = require('react');
require('./panelTopBar.scss');

const PanelTopBar = React.createClass({
  render() {
    return (
      <header className="panel-top-bar group-top">
        <h1 className="panel-title">{this.props.panelTitle}</h1>
        <Link to='groups/create/show'><Button type="primary" className="group-add-btn">新建</Button></Link>
      </header>
    );
  }
});

module.exports = PanelTopBar;
