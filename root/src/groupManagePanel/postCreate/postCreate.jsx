import { Icon,Spin,Form,Input,Button,DatePicker,Upload,notification,message,Select,Modal } from 'antd';

let React = require('react');
let Link = require('./../../components/link/link');
let events = require('./../../_utils/eventSubscriber');
let ajaxEx = require('./../../_utils/ajaxConfiger');
let Const = require('./../../_utils/const');
let groupManageStore = require('../store/groupManageStore');
let $ = require('jquery') ;
require('./../postEdit/postEdit.scss');

const FormItem = Form.Item;
const Option = Select.Option;
let postCreateView = React.createClass({
  getInitialState() {
        return {
          data: {},
          fileList: [],
          pics: [],
          uploadToken: '',
          requesting:false,
          content:'',
          user_id:'',
          armies:[]
        };
  },
  componentDidMount: function () {
      var _this = this;
      this.fetchGroup();
      ajaxEx.get({url:Const.API.urlPrefix +'upload_token',form:{}},function(d){
                      _this.setState({"uploadToken":d.upload_token});
      });
      ajaxEx.get({url:Const.API.water_armies},function(d){
                _this.setState({armies:d.data});
      });
  },
  fetchGroup(gid){
       var _this = this;
       var groupId = gid? gid:this.props.params.id;
       if(!groupManageStore.getDataSource().length)
       {
             ajaxEx.get({url:Const.API.groups+'/'+ groupId},function(d){
                   var  mappingData= groupManageStore.mapping([d.data]);
                  _this.setState({title:mappingData[0].name,data:mappingData[0]});
             });
       }
       else
       {
           var itemData = groupManageStore.getItemByProperty('id',groupId);
           _this.setState({title:itemData[0].name,data:itemData[0]});
       }
  },
  componentWillUnmount: function(){
     //events.unsubscribe('progressChange');
  },
  contentChange(e) {
    this.setState({content: e.target.value});
  },
  handleSelectChange(uid) {
      this.setState({
          user_id: uid
      });
  },
  handleSubmit(e) {
       e.preventDefault();
       var _this= this;
       this.props.form.validateFields((errors, values) => {
                if (!!errors) {
                  console.log('Errors in form!!!');
                  e.preventDefault();
                  return;
                }
             var postData = {
              "post": {
                "title": values.title,
                "content": _this.state.content,
                "pictures": _this.state.pics.toString().replace(/,/ig,'，'),
                "user_id": values.user_id,
                "group_id": _this.props.params.id
              }
            };

           if(!postData.post.title && !postData.post.content && !postData.post.pictures)
           {
                Modal.error({
                        title: '标题，正文，图片不可都为空！',
                        content: ''
                });
                return false;
           }

           ajaxEx.post({url:Const.API.projectPrefix + '/water_armies/posts',form:postData,enabledLoadingMsg:true,enabledErrorMsg:true},function(d){
                   $('#postForm').resetForm();
                   window.history.back();
           }).complete(function(){
               _this.setState({requesting:false});
           });
           return false;
      });
  },
  render() {
    var _this = this;
    const { getFieldProps,getFieldError } = this.props.form;

    const titleProps = getFieldProps('title', {
        validate: [{
          rules: [
            { max: 40, message: '标题不能超过40个字符' }
          ],
          trigger: 'onBlur',
        }],
        initialValue: ''
    });

    const majiaProps = getFieldProps('user_id', {
        validate: [{
          rules: [
            { required: true, message: '马甲不能为空'}
          ]
        }],
        initialValue: ''
    });

    const props = {
        action:'https://up.qbox.me',
        data:{
         'token':this.state.uploadToken
        },
        listType: 'picture-card',
        accept: 'image/jpeg,image/png,image/jpg,image/bmp',
        onChange(info) {
              var files = _this.state.fileList,
                pics = _this.state.pics;

               if (info.file.status == 'uploading') {

               } else if (info.file.status === 'error') {
                  notification['error']({
                    message: '错误',
                    description: info.file.error
                  });
              } else if (info.file.status === 'removed') {
                 //
                  pics.withOut(function(i,o){return o==info.file.url});

                 _this.setState({
                      pics: pics
                  });
              } else if (info.file.status === 'done') {
                  this.data.key = '';
                  files.push({
                      uid: info.file.uid,
                      name: info.file.name,
                      status: 'done',
                      url: 'http://' + Const.API.qiniuUpload + '/' + info.file.response.key,
                      thumbUrl: 'http://' + Const.API.qiniuUpload+ '/' + info.file.response.key
                  });

                  pics.push('http://' + Const.API.qiniuUpload+ '/' + info.file.response.key );

                  _this.setState({
                      fileList: files,
                      pics: pics
                  });
              } else if (info.file.status === 'error') {
                _this.setState({
                    fileList: files,
                    pics: pics
                });
              } else if (!info.file.status) {
                //_this.setState({ loading: true });
              }
        },
        beforeUpload(file) {
            var self = this,flag = _this.state.fileList.length<9,promise={};
            if (!flag) {
                message.error('图片最多添加9张！');
            }else{
               var then = function(cb){
                   var fr = new FileReader();
                    fr.readAsDataURL(file);
                    fr.onload = function(e) {
                        var img = document.createElement('img');
                        img.src= e.target.result;
                        img.onload = function(){
                            self.data.key = new Date().getTime()+ '/width=' + img.width + 'px' + '/height=' + img.height + 'px';
                            cb();
                        };
                    };
                }
                promise.then = then;
                return promise;
            }
            return flag;
        }
    };

    return (
    <div>
       <div className="ant-breadcrumb custom">
          <span>
            <span className="ant-breadcrumb-link"><Link to='groups'>群组列表</Link></span>
            <span className="ant-breadcrumb-separator">/</span>
          </span>
          <span>
            <span className="ant-breadcrumb-link"><Link to={'groups/'+this.state.data.id}>{this.state.title}</Link></span>
            <span className="ant-breadcrumb-separator">/</span>
          </span>
          <span>
            <span className="ant-breadcrumb-link">新建帖子</span>
          </span>
       </div>
       <div className="post-con">
          <Form horizontal id={'postForm'} form={this.props.form} method="post">
              <FormItem
                       label="马甲：" labelCol={{ span: 3 }} wrapperCol={{ span: 14 }}>
                         <Select showSearch
                          style={{ width: 165 }}
                          placeholder="请选择马甲"
                          optionFilterProp="children"
                          notFoundContent="无法找到"
                          searchPlaceholder="输入关键词"
                          name={'post[user_id]'}
                          onChange={this.handleSelectChange} value={this.state.user_id} {...majiaProps}>
                            {_.map(this.state.armies,(o,index)=>
                                <Option value={o.id} key={o.id}>{o.attributes.name}</Option>
                            )}
                        </Select>
              </FormItem>
              <FormItem
                label="标题：" labelCol={{ span: 3 }} wrapperCol={{ span: 14 }}>
                <Input placeholder="" {...titleProps} name={'post[title]'} />
              </FormItem>
              <FormItem
                label="正文：" labelCol={{ span: 3 }} wrapperCol={{ span: 14 }}>
                 <Input type="textarea" id="control-textarea"  {...getFieldProps('content')} name={'post[content]'} value={this.state.content} onChange={this.contentChange} rows="3" />
              </FormItem>
              <FormItem
                    label="图片（最多添加9张）：" labelCol={{ span: 3 }} wrapperCol={{ span: 14 }}>
                      <Upload ref="uploader" {...props} fileList={this.state.fileList}>
                          <Icon type="plus" />
                          <div className="ant-upload-text">上传照片</div>
                      </Upload>
                  </FormItem>
                   <FormItem wrapperCol={{ offset: 3 }}>
                        <Button type="primary" onClick={this.handleSubmit} loading={this.state.requesting}>保存</Button>
                  </FormItem>
          </Form>

       </div>
     </div>
    );
  }
});

postCreateView = Form.create()(postCreateView);

module.exports = postCreateView;
