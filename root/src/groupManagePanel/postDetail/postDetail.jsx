import { Table, Icon, Spin,Form,Input,InputNumber,Button,Upload,Popconfirm,Pagination,Popover,Select,Modal } from 'antd';
let _ = require('lodash');
let React = require('react');
let Link = require('./../../components/link/link');
let Preloader = require('./../../components/imagePreloader/imagePreloader');
let PopOut = require('./../../components/popOut/popOut');
let events = require('./../../_utils/eventSubscriber');
let ajaxEx = require('./../../_utils/ajaxConfiger');
let postManageStore = require('../store/postManageStore');
let Const = require('./../../_utils/const');
let timeFormater = require('./../../_utils/timeFormater');
let $ = require('jquery');
let ReplyBox = require('./replyBox');
let ReplyView = require('./replyView');
require('./postDetail.scss');


const FormItem = Form.Item;
const Option = Select.Option;

let CommentItem = React.createClass({
  getInitialState() {
      return {
         loading:false,
         pageSize:8,
         totalComments:0,
         currentPage:1,
         comments:[]
      };
  },
  componentDidMount: function () {
      if(this.props.level==0)
      {       
         this.getCommentsData(1);

         events.subscribe('commentCommentDataChange_' + this.props.data.id.toString(),(function(){
            this.getCommentsData(this.state.currentPage);
         }).bind(this));
      }
  },
  getCommentsData(page){
       var _this=this,per=this.state.pageSize;
       _this.setState({loading:true});
       ajaxEx.get({url:Const.API.comments +'/'+ this.props.data.id +'/children?comment_id='+ this.props.data.id +'&page='+page + '&per=' + per},function(d){
                _this.setState({comments:d.data,totalComments:d.meta.total_counts,currentPage:!d.data.length?(page-1):page});
       }).complete(function(){
           _this.setState({loading:false});
       });
  },
  pageChange:function(current){
        this.getCommentsData(current);
  },
  componentWillUnmount: function(){
      if(this.props.level==0)
      {  
         events.unsubscribe('commentCommentDataChange_' + this.props.data.id.toString());
      }
  },
  deleteItem(){
       var _this =this;
       var commentId= this.props.data.id;
       ajaxEx.delete({url:Const.API.urlPrefix +'comments/'+ commentId,enabledErrorMsg:true},function(d){
               if(_this.props.level==0){
                     events.publish('postCommentDataChange');
               }
               else if(_this.props.level==1)
               {
                     events.publish('commentCommentDataChange_' + _this.props.rootCommentId.toString());
               }
       });
  },
  render() {
    var _this = this;
    var replyStr = ()=>{
       if(this.props.data.attributes.reply_user_name)
       {
         return <span><span className="reply-str">回复:</span><a>{this.props.data.attributes.reply_user_name}</a></span>      
       }
       else
       {
          return null;
       }
    };
    var replyPics = ()=>{ return (this.props.level==0 && this.props.data.attributes.image_url)?this.props.data.attributes.image_url.split('，'):[]};
    return (
          <div className="comment_con">
            {this.props.level<=1?   
                <div>
                   <div className="user_avatar">
                      <p><Preloader url={this.props.data.attributes.user_avatar_url} defaultUrl={require('../../../assets/image/default-avatar.png')}/></p>
                  </div>
                  <div className="comment_content">
                     <p><a>{this.props.data.attributes.user_name}</a>{replyStr()}</p>
                     <p>{timeFormater(this.props.data.attributes.created_at).fromNow()}</p>
                     <p>{this.props.data.attributes.content}</p>
                     <p>{_.map(replyPics(),(o,index)=><Preloader url={o} />)}</p>
                     <div>
                       <Icon type="like" /><span className="vote-count">{this.props.data.attributes.vote_count}</span>
                       <Popconfirm title="确定要删除这条评论么？" onClick={(e)=>{ e.stopPropagation()}} onConfirm={this.deleteItem}>
                          <a href="javascript:void()" style={{marginLeft:5}}>删除</a>
                       </Popconfirm>
                       <ReplyBox armies={this.props.armies} rootCommentId={this.props.rootCommentId} postId={this.props.postId} data={this.props.data} level={this.props.level}/>
                      </div>
                  </div>
                  {this.props.level<1?
                     <ul> 
                         <Spin spining={this.state.loading}>
                             {this.state.comments.length>0?
                                <li>
                                     <h3><span>共{this.state.totalComments}条</span></h3>
                                     {_.map(this.state.comments,(o,index)=>
                                          <CommentItem key={o.id} data={o} level={1} postId={this.props.postId} armies={this.props.armies} rootCommentId={this.props.rootCommentId}/>
                                     )}
                                </li>
                             :null}
                            {(this.props.level==0 && this.state.totalComments>0)?<Pagination size="small" className="comments-pagination" onChange={this.pageChange} current={this.state.currentPage} pageSize={this.state.pageSize} total={this.state.totalComments} />:null}
                         </Spin>
                     </ul>:null}
                </div> :null}
          </div>
    );
  }
});

const postDetailView = React.createClass({
  getInitialState() {
     if(postManageStore.getDataSource().length)
     {
        var itemData = postManageStore.getItemByProperty('id',this.props.routeParams.pid);
        return {
             data:itemData[0],
             loading:true,
             pageSize:8,
             totalComments:0,
             currentPage:1,
             comments:[],
             user:{},
             armies:[]
        };
     }
     else
     {
        return  {
           data:{"pictures":""},
           title:'',
           loading:true,
           pageSize:8,
           totalComments:0,
           currentPage:1,
           comments:[],
           user:{},
           armies:[]
         }
     }
  },
  fetchPost(pid){
       var _this=this;
       var postId = pid? pid:this.props.routeParams.postId,per=this.state.pageSize,page=this.state.currentPage;

       if(!postManageStore.getDataSource().length)
       {
             ajaxEx.get({url:Const.API.posts + '/' + postId},function(d){
                    var  mappingData= postManageStore.mapping([d.data]);
                    _this.setState({data:mappingData[0],title:mappingData[0].name});
             });
       }
       else
       {
           var itemData = postManageStore.getItemByProperty('id',postId);
           _this.setState({data:itemData[0],title:itemData[0].name});
       }
       this.getCommentsData(1);
  },
  getCommentsData(page){
       var _this=this,postId = this.props.routeParams.postId,per=this.state.pageSize;
       _this.setState({loading:true});
       ajaxEx.get({url:Const.API.posts +'/'+ postId + '/comments' +'?page='+page + '&per=' + per},function(d){
                    _this.setState({comments:d.data,totalComments:d.meta.total_counts,currentPage:!d.data.length?(page-1):page});
       }).complete(function(){
           _this.setState({loading:false});
       });
  },
  pageChange:function(current){
        this.getCommentsData(current);
  },
  componentDidMount() {
        var _this = this;

        events.subscribe('postCommentDataChange',(function(){
              this.getCommentsData(this.state.currentPage);
        }).bind(this));

        this.fetchPost();
        ajaxEx.get({url:Const.API.water_armies},function(d){
                _this.setState({armies:d.data});
        });
  },
  cancelPopOutBox(){
     // $('.reply-box .pop-out:not(.pop-out-hidden)').addClass('pop-out-hidden');
     events.publish('CancelReplyBox');
  },
  componentWillReceiveProps(nextProps){
        if(this.props.params.pid==nextProps.params.pid) return;
        this.fetchPost(nextProps.params.pid);
  },
  componentWillUnmount: function(){
     //events.unsubscribe('progressChange');
     events.unsubscribe('CancelReplyBox');
     events.unsubscribe('postCommentDataChange');
  },
  render() {
    return (
    <div onClick={this.cancelPopOutBox}>
     <div className="ant-breadcrumb custom">
          <span>
            <span className="ant-breadcrumb-link"><Link to='groups'>群组列表</Link></span>
            <span className="ant-breadcrumb-separator">/</span>
          </span>
         <span>
            <span className="ant-breadcrumb-link"><Link to={'groups/'+ this.props.params.id}>返回群组</Link></span>
            <span className="ant-breadcrumb-separator">/</span>
          </span>
          <span>
            <span className="ant-breadcrumb-link">帖子详情</span>
          </span>
       </div>
       <div className="post-detail-con">
               <div>
                  <div className="post-user">
                        <span className="user_info">
                          <img src={this.state.data.avatar} />
                          <span>{this.state.data.creator_name}</span>
                          <span>{this.state.data.created_at}</span>
                        </span>
                        
                   </div>
                   <div className="post-info">
                         <span><Icon type="like" /><span>{this.state.data.likings_count}</span></span>
                         <span><Icon type="message" /><span>{this.state.data.comments_count}</span></span>
                   </div>
                   <p className="post-title">{this.state.data.title}</p>
                   <p className="post-content">{this.state.data.content}</p>
                    {_.map(this.state.data.pictures,(o,index)=>
                       <p className="post-img" key={o.id}><Preloader url={o} /></p>
                    )}
               </div>
               <ul> 
                   <Spin spining={this.state.loading}>
                       {this.state.comments.length>0?
                          <li>
                               <h3><span>共{this.state.totalComments}条</span></h3>
                               {_.map(this.state.comments,(o,index)=>
                                    <CommentItem key={o.id} data={o} level={0} armies={this.state.armies} postId={this.props.routeParams.postId} rootCommentId={o.id}/>
                               )}
                          </li>
                       :null}
                      {this.state.totalComments>0?<Pagination size="small" className="comments-pagination" onChange={this.pageChange} current={this.state.currentPage} pageSize={this.state.pageSize} total={this.state.totalComments} />:null}
                   </Spin>
               </ul>  
               <ReplyView postId={this.props.routeParams.postId} armies={this.state.armies}/>
       </div>
     </div>
    );
  }
});

module.exports = postDetailView;
