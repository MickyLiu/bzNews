import { Icon,Spin,Form,Input,Button,Pagination,Select,Upload,Modal } from 'antd';
let _ = require('lodash');
let React = require('react');
let Link = require('./../../components/link/link');
let PopOut = require('./../../components/popOut/popOut');
let events = require('./../../_utils/eventSubscriber');
let ajaxEx = require('./../../_utils/ajaxConfiger');
let Const = require('./../../_utils/const');
let $ = require('jquery');


const FormItem = Form.Item;
const Option = Select.Option;

const ReplyView = React.createClass({
  getInitialState() {
      return {
         loading:false,
         fileList: [],
         pics: [],
         data:{content:''},
         uploadToken: '',
      };
  },
  componentDidMount: function () {
      var _this = this;
      ajaxEx.get({url:Const.API.urlPrefix +'upload_token',form:{}},function(d){
                      _this.setState({"uploadToken":d.upload_token});
      });
  },
  componentWillUnmount: function(){
  },
  handleSubmit(e) {
       e.preventDefault();
       var _this= this;

         var postData = {
          "comment": {
            "upvotable_type": 'Post',
            "content": _this.state.data.content,
            "upvotable_id":_this.props.postId,
            "image_url": _this.state.pics.toString().replace(/,/ig,'，'),
            "user_id":_this.state.data.user_id
          }
        };
       if(!postData.comment.user_id)
       {
            Modal.error({
                    title: '请选择一个水军！',
                    content: ''
            });
            return false;
       }
       else if(!postData.comment.user_id && !postData.comment.image_url){
            Modal.error({
                    title: '请填写回复内容或者图片！',
                    content: ''
            });
            return false;
       }
       _this.setState({loading:true});
       ajaxEx.post({url:Const.API.projectPrefix +'/water_armies/comments',form:postData,enabledErrorMsg:true},function(d){
                  _this.setState({data:{content:''},fileList:[],pics:[]});
                   events.publish('postCommentDataChange');
       }).complete(function(){
              _this.setState({loading:false});
       });
  },
  handleInputChange(e) {
      var data = this.state.data;
      var name = e.target.attributes['name'].value.match(/\[\S+\]/)[0].replace(/\[|\]/g,'');
      data[name]= e.target.value;
      this.setState({
          data: data
      });
  },
  handleSelectChange(uid) {
      var data = this.state.data;
      data.user_id = uid;
      this.setState({
          data: data
      });
  },
  render() {
    var _this = this;

    const props = {
        action:'https://up.qbox.me',
        data:{
         'token':this.state.uploadToken
        },
        listType: 'picture-card',
        accept: 'image/jpeg,image/png,image/jpg,image/bmp',
        onChange(info) {
              var files = _this.state.fileList,
                pics = _this.state.pics;
              if (info.file.status == 'uploading') {

              } else if (info.file.status === 'error') {
                  notification['error']({
                    message: '错误',
                    description: info.file.error
                  });
              } else if (info.file.status === 'removed') {
                 //
                  pics.withOut(function(i,o){return o==info.file.url});

                 _this.setState({
                      pics: pics
                  });
              } else if (info.file.status === 'done') {
                  this.data.key = '';
                  files.push({
                      uid: info.file.uid,
                      name: info.file.name,
                      status: 'done',
                      url: 'http://' + Const.API.qiniuUpload+ '/' + info.file.response.key,
                      thumbUrl: 'http://' + Const.API.qiniuUpload+ '/' + info.file.response.key
                  });
                  pics.push('http://' + Const.API.qiniuUpload + '/' + info.file.response.key);
                  _this.setState({
                      fileList: files,
                      pics: pics
                  });
              } else if (info.file.status === 'error') {
                _this.setState({
                    fileList: files,
                    pics: pics
                });
              } else if (!info.file.status) {
                //_this.setState({ loading: true });
              }
        },
        beforeUpload(file) {
            var self = this,flag = _this.state.fileList.length<9,promise={};
            if (!flag) {
                message.error('图片最多添加9张！');
            }else{
               var then = function(cb){
                   var fr = new FileReader();
                    fr.readAsDataURL(file);
                    fr.onload = function(e) {
                        var img = document.createElement('img');
                        img.src= e.target.result;
                        img.onload = function(){
                            self.data.key = new Date().getTime()+ '/width=' + img.width + 'px' + '/height=' + img.height + 'px';
                            cb();
                        };
                    };
                }
                promise.then = then;
                return promise;
            }
            return flag;
        }
    };
    return (
      <div className="reply-view">
          <Form horizontal id={'replyViewForm'} method="post" style={{marginTop:20}}>
                    <FormItem
                       wrapperCol={{ span: 20 }}>
                         <Select showSearch
                          style={{ width: 165 }}
                          placeholder="请选择马甲"
                          optionFilterProp="children"
                          notFoundContent="无法找到"
                          searchPlaceholder="输入关键词"
                          name={'comment[user_id]'}
                          onChange={this.handleSelectChange} value={this.state.data.user_id}>
                            {_.map(this.props.armies,(o,index)=>
                                <Option value={o.id} key={o.id}>{o.attributes.name}</Option>
                            )}
                        </Select>
                    </FormItem>
                    <FormItem
                       wrapperCol={{ span: 20 }}>
                       <Input type="textarea" name={'comment[content]'} placeholder={'请填写回复内容'} value={this.state.data.content} onChange={this.handleInputChange}/>
                    </FormItem>
                     <FormItem
                          label="图片（最多添加9张）：" labelCol={{ span: 3 }} wrapperCol={{ span: 14 ,offset: 1 }}>
                            <Upload ref="uploader" {...props} fileList={this.state.fileList} style={{marginLeft:10}}>
                                <Icon type="plus" />
                                <div className="ant-upload-text">上传图片</div>
                            </Upload>
                        </FormItem>
                    <FormItem wrapperCol={{ offset: 2 }}>
                          <Button type="primary" onClick={this.handleSubmit} loading={this.state.loading} style={{marginRight:20}}>发送</Button>
                    </FormItem>
                </Form>
      </div>
    );
  }
});

module.exports = ReplyView;
