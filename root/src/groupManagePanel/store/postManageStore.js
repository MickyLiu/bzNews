let $ = require('jquery');
let timeFormater = require('./../../_utils/timeFormater');
var PostManageStore = {

   clearData:function(){
      this.dataSource=[];
      return this;
   },
   pushData:function(data) {
      if(!this.dataSource) this.dataSource=[];
      this.dataSource = this.dataSource.concat(data);
      return this;
   },
   mapping:function(data){
        var result=[];
        var relation = data[0]?data[0].relationships:{};
        data.forEach(function(o,i){
               $.isNumeric(o.id)?o.attributes.id = o.id.toString():'';
               $.isNumeric(o.id)?o.attributes.key =  o.id.toString():'';
               o.attributes.likings_count = $.isNumeric(o.attributes.likings_count)?o.attributes.likings_count.toString():'0';
               o.attributes.comments_count = $.isNumeric(o.attributes.comments_count)?o.attributes.comments_count.toString():'0';
               o.attributes.pictures = $.isEmptyObject(o.attributes.pictures)?[]:o.attributes.pictures.split('，');
               o.attributes.created_at = o.attributes.created_at?timeFormater(o.attributes.created_at).fromNow():'';
               //o.attributes.user_id = o.relationships.creator.data?o.relationships.creator.data.id:'';
               result.push(o.attributes);
        });
        this.relationships = relation;
        return result;
   },
   getRelations:function(){
        return this.relationships;
   },
   getDataSource:function() {
      return !this.dataSource?[]:this.dataSource;
   },
   getItemByProperty:function(key,value){
        return this.dataSource.filter(function(item){
               return item[key]==value;
        });
   },
   getMappingKeyData:function(){
         return {
             "id":"帖子ID",
             "user_id":"用户",
             "title":"帖子",
             "likings_count":"点赞数",
             "replies_count":"回复数",
             "priority":"优先级",
             "created_at":'发布时间'
         }
   },
   getItemOnMappingKey:function(item){
      var result = {};
      var mapping = this.getMappingKeyData();
      for(var key in mapping)
      {
          result[mapping[key]] = item[key];
      }
        return result;
   }
};

module.exports = PostManageStore;
