import {Link} from 'react-router';
import { Menu, Icon, Switch, Table, Row, Col, Spin, Dropdown } from 'antd';
let React = require('react');
let classnames = require('classnames');
let events = require('../_utils/eventSubscriber');
let LeftNav = require('../components/leftNav/leftNav');
require('./../_utils/startsWith');

require('./index.scss');
require('./index_custom.scss');
require('./components_common.scss');
require('./animate.scss');

const SubMenu = Menu.SubMenu;

var Index = React.createClass({
    getInitialState() {
        return {
          theme: 'dark',
          show: false,
          full: true,
      };
    },
    componentDidMount: function () {
          var path = this.props.location.pathname.replace(/\/$/,'').replace(/^\//,'');
          events.publish('routeOnNav', path.split('/')[2] || '');
    },
    onFullChange: function(state) {
        this.setState({full: state});
        this.traversalChildren(ref => ref.resize ? ref.resize() : 0);
    },
    traversalChildren: function(callback) {
        for (var key in this.refs) {
            if (key.startsWith('opt') && this.refs.hasOwnProperty(key)) {
                var ref = this.refs[key];
                callback(ref);
            }
        }
    },
    componentWillReceiveProps(nextProps){
         if(nextProps.params.pid!=this.props.params.pid){
            globalVars.set('_projectId_',nextProps.params.pid);
            window.location.reload();
         }
    },
    render: function () {
        let optClasses = classnames('opt-container', {'side-bar-full': this.state.full});
        return (
             <div className={'opt-area'}>
                <LeftNav onFullChange={this.onFullChange} />
                <div className={optClasses}>
                    {React.Children.map(this.props.children, (element, idx) => {
                            return React.cloneElement(element, { ref: 'opt' + idx, Auth: window.auth });
                    })}
                </div>
             </div>
          )
      }
});

module.exports = Index;
