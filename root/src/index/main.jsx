// import {Link} from 'react-router';
let React = require('react');
let classnames = require('classnames');

let events = require('../_utils/eventSubscriber');
let TopBar = require('../components/topBar/topBar');
let ProgressBar = require('../components/progressBar/progressBar');

const MainPanel = React.createClass({
  getInitialState() {
      return {
        full: true
    };
  },
  componentDidMount: function () {
      var path = this.props.location.pathname.replace(/\/$/,'').replace(/^\//,'');
      events.publish('routeOnTop', path.split('/')[0] || '');
  },
  render() {
    return (
      <div>
        <ProgressBar/>
        <TopBar navFull={true} full={this.state.full} Auth={window.auth}/>
        <div>
           {this.props.children}
        </div>
      </div>
    );
  }
});

module.exports = MainPanel;
