import { Router, Route, hashHistory } from 'react-router';
import {render} from 'react-dom';

let React = require('react');
let routes = require('./../../routes/routes');
let events = require('../_utils/eventSubscriber');
let Auth = require('../_utils/auth');
let $ = require('jquery');

require('./index.scss');
require('./index_custom.scss');
require('./components_common.scss');


window.auth = new Auth({
  page: 'index',
  target: 'home',
  expect: true
});


auth.verify(function(data) {
  localStorage.setItem("currentUserId",data.current_customer);

  auth.verifyIf(function(){return !$.isNumeric(globalVars.get('_teamId_'))},'teams',function(data) {

          hashHistory.listen(function(ev) {
    		       /*var route = ev.pathname.length?ev.pathname[0]=="/"?ev.pathname.split('/')[3]:ev.pathname.split('/')[2]:'';*/
               var leftRoute = null,topRoute = null,path = ev.pathname.replace(/\/$/,'').replace(/^\//,'');
               topRoute = path.split('/')[0] || '';
               if(topRoute=='projects' && ev.pathname.split('/').length>1)
               {
                  leftRoute = path.split('/')[2] || '';
               }
    		       events.publish('routeOnNav', leftRoute);
               events.publish('routeOnTop', topRoute);
    		  });

    		  render(<Router history={hashHistory}>{routes}</Router>, document.getElementById("container"));

  	  });

});
