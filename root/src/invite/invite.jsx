let React = require('react');
let ReactDOM = require('react-dom');
let Footer = require('./../components/footer/footer');
let AdArea = require('./../components/adArea/adArea');
let TopBar = require('../components/topBar/topBar');
let RegisterDialog = require('../components/dialog/registerDialog');
let LoginDialog = require('../components/dialog/loginDialog');
let Auth = require('../_utils/auth');
let ajaxEx = require('./../_utils/ajaxConfiger');
let Const = require('./../_utils/const');
let events = require('./../_utils/eventSubscriber');
let locationHelper = require('./../_utils/locationHelper');
let global = require('./../_utils/global');

require('./invite.scss');

let auth = new Auth({
  page: 'invite',
  target: '/',
  expect: false,
});


const InviteDialog = React.createClass({
  getInitialState() {
    return {
      applyInfo: ''
    }
  },
  handleApplyInfo(e) {
    this.setState({applyInfo: e.target.value.trim()});
  },
  submitHandler() {
    let _this = this;
    let url = Const.API.team_applications;
    let data = {
      team_application: {
        content: this.state.applyInfo
      },
      invitation_code: this.props.inviteToken
    };
    ajaxEx.post({url:url, form:data}, function(d){
      window.location.href = "/teams";
    });
  },
  render: function() {
    return (
      <div className="invite-dialog" style={this.props.visible ? {display: "block"} : {display: "none"}} >
        <h3>{this.props.title}</h3>
        <p className='sub-title'>你需要发送认证申请,等管理员通过</p>
        <textarea onchange={this.handleApplyInfo} />
        <button className="apply-btn" onClick={this.submitHandler}>申请加入</button>
      </div>
    );
  }
});

const InvitePage = React.createClass({
  getInitialState: function() {
    return {
      isLogin: false,
      loginDialogShow: false,
      registerDialogShow: true,
      teamName: "",
      inviteToken: "",
      teamId: 0,
    };
  },
  componentDidMount: function() {
    let _this = this;
    auth.verifyLoginInvite(function(isLogin) {
      _this.setState({isLogin: isLogin});
    });

    let teamName = locationHelper.getSearchVar("teamName");
    let teamId = locationHelper.getSearchVar("teamId");
    let inviteToken = locationHelper.getSearchVar("token");

    this.setState({teamName: teamName, inviteToken: inviteToken, teamId: teamId});

    events.subscribe("showLoginDialog", function() {
      _this.setState({loginDialogShow: true, registerDialogShow: false});
    });

    events.subscribe("showRegisterDialog", function() {
      _this.setState({loginDialogShow: false, registerDialogShow: true});
    });

    events.subscribe("loginSuccess", function() {
      _this.setState({loginDialogShow: false, isLogin: true});
    });
  },
  render: function(){
    let inviteDialogTitle = `加入［${this.state.teamName}］`;
    let dialogTitle = `加入［${this.state.teamName}］团队`;
    auth.token = this.state.inviteToken;
    let adArea = this.state.isLogin ? <div className='blank-main-area'></div> : <AdArea searchShow={false} />;
    return (
      <div>
        <TopBar navFull={false} showRight={this.state.isLogin} Auth={auth}/>
        {adArea}
        <Footer />
        <InviteDialog title={inviteDialogTitle} visible={this.state.isLogin} teamId={this.state.teamId} inviteToken={this.state.inviteToken} inviteLogin={true}/>
        <RegisterDialog title={dialogTitle} auth={auth} visible={!this.state.isLogin && this.state.registerDialogShow} showDel ={false} inviteRegister={true} />
        <LoginDialog title={dialogTitle} auth={auth} visible={!this.state.isLogin && this.state.loginDialogShow} showDel ={false} inviteLogin={true} />
      </div>
    );
  }
});

ReactDOM.render(<InvitePage />, document.getElementById('container'));
