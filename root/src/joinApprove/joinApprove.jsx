import {hashHistory} from 'react-router';
let React = require('react');
let Footer = require('./../components/footer/footer');
let ajaxEx = require('./../_utils/ajaxConfiger');
let Const = require('./../_utils/const');
let events = require('./../_utils/eventSubscriber');
require('./approve.scss');

const JoinApprove = React.createClass({
  getInitialState() {
    return {
      selectAll: false,
      approves: [],
      rejectDialogShow: false
    }
  },
  componentDidMount() {
    let url = `${Const.API.teams}/${localStorage.getItem('_teamId_')}/team_applications`;
    let _this = this;
    ajaxEx.get({url: url},function(d) {
      let approves = d.data;
      for(let i=0; i< approves.length; i++) {
        approves[i].checked = false;
      }
      _this.setState({approves: approves});
    });
  },
  selectAllChange() {
    let selectAll = !this.state.selectAll;
    let approves = this.state.approves;
    let checked;
    if(selectAll) {
      checked = true;
    } else {
      checked = false;
    }
    for(let i = 0; i< approves.length; i++) {
      approves[i].checked = checked;
    }
    this.setState({approves: approves, selectAll: selectAll});
  },
  selectChange(e) {
    let approves = this.state.approves;
    for(let i=0; i< approves.length; i++) {
      if(approves[i].id === e.target.name) {
        approves[i].checked = !approves[i].checked;
        break;
      }
    }
    this.setState({approves: approves});
  },
  handleAgree() {
    let data = [], approves = this.state.approves;
    for(let i=0; i< approves.length; i++) {
      if(approves[i].checked) {
        data.push(approves[i]);
      }
    }

    hashHistory.push({
      pathname: '/joinApproveDetail',
      state: { approvesData: data }
    });
  },
  handleReject() {
    this.setState({rejectDialogShow: true});
  },
  rejectMembers() {
    let denyIds = [], denyIdsString = '', approves = this.state.approves, _this = this;

    for(let i=0; i< approves.length; i++) {
      if(approves[i].checked) {
        denyIds.push(approves[i].id);
      }
    }
    denyIdsString = denyIds.join(',');

    ajaxEx.delete({url: `${Const.API.teamDeny}?team_application_ids=${denyIds}`, enabledLoadingMsg:true, enabledErrorMsg:true},(d) => {
      let approves = this.state.approves, newApproves=[];
      for(let i=0; i< approves.length; i++) {
        if(!denyIds.includes(approves[i].id)) {
          newApproves.push(approves[i]);
        }
      }
      _this.setState({approves: newApproves, rejectDialogShow: false});

      if(newApproves.length === 0) {
        hashHistory.push('/joinApproveAll');
      }
    });
  },
  hideRejectDialog() {
    this.setState({rejectDialogShow: false});
  },
  render() {
    let approveItems = this.state.approves.map((item) => {
      return(
        <li>
          <input type='radio' name= {item.id} checked={item.checked} onChange={this.selectChange}/>
          <img src={item.attributes.customer_avatar} />
          <div className='content-container'>
            <h3>{item.attributes.customer_name}（{item.attributes.customer_email}）</h3>
            <p>{item.attributes.content}</p>
          </div>
        </li>
      );
    });

    let deleteBtn = document.createElement('img');
    deleteBtn.src = require('./../../assets/image/invite-delete.png');

    return(
      <div>
        <div className='join-approve-container'>
          <h1>加入申请</h1>
          <ul className='approves-list'>
            {approveItems}
          </ul>
          <hr />
          <div className='approve-submit-area'>
            <input type='radio' name="selectAll" checked={this.state.selectAll} onChange={this.selectAllChange}/>
            <span>全选</span>
            <button className='approve-btns approve-agree' onClick={this.handleAgree}>审批通过</button>
            <button className='approve-btns approve-reject' onClick={this.handleReject}>拒绝申请</button>
          </div>
          <div className={this.state.rejectDialogShow ? 'reject-dialog show' : 'reject-dialog'}>
            <img src={deleteBtn.src} onClick={this.hideRejectDialog}/>
            <h4>确定要拒绝他们的申请吗？</h4>
            <button className='reject-btn' onClick={this.rejectMembers}>确定</button>
            <a href='javascript:void(0)' onClick={this.hideRejectDialog}>取消</a>
          </div>
        </div>
        <Footer />
      </div>
    );
  }
});

module.exports = JoinApprove;
