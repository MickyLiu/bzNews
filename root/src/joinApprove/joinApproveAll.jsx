let React = require('react');
let Footer = require('./../components/footer/footer');
import {Link} from 'react-router';
require('./approve.scss');

const JoinApproveAll = React.createClass({
  render() {
    let approveAll = document.createElement('img');
    approveAll.src = require('../../assets/image/approveAll.png');
    return(
      <div>
        <div className='approve-all-container'>
          <h1>申请处理完毕</h1>
          <img src={approveAll.src} />
          <p>你可以<Link to='/projects'>回到项目主页</Link></p>
        </div>
        <Footer />
      </div>
    );
  }
});

module.exports = JoinApproveAll;
