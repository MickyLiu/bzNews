let React = require('react');
let Footer = require('./../components/footer/footer');
let ajaxEx = require('./../_utils/ajaxConfiger');
let Const = require('./../_utils/const');
let events = require('./../_utils/eventSubscriber');
import {hashHistory} from 'react-router';
require('./approve.scss');

const ProjectInput = React.createClass({
  getInitialState() {
    return {
      checked: true
    };
  },
  handleChange() {
    this.setState({checked: !this.state.checked});
  },
  render() {
    return (
      <input type='checkbox' name={`${this.props.index}-${this.props.itemId}`} checked={this.state.checked} onChange={this.handleChange}/>
    );
  }
});

const ProjectItems = React.createClass({
  render() {
    let _this = this;
    let projectItems = this.props.teamProjects.map((item) => {
      return (
        <span>
          <ProjectInput index={_this.props.index} itemId={item.id} /> <span className='checkbox-label'>{item.name}</span>
        </span>
      );
    });
    return(
      <div>
        {projectItems}
      </div>
    );
  }
});

const JoinApproveDetail = React.createClass({
  getInitialState() {
    return {
      teamProjects: [],
      approveObjects: []
    };
  },
  componentWillMount() {
    let _this = this;
    let approvesData = this.props.location.state.approvesData;

    ajaxEx.get({url: Const.API.teamProjects}, (d)=> {
      let projects = [], projectIds =[];
      for(let i=0; i< d.data.length; i++) {
        projects.push({id: d.data[i].id, name: d.data[i].attributes.name});
        projectIds.push(d.data[i].id);
      }

      for(let i= 0; i< approvesData.length; i++) {
        approvesData[i]['project_ids'] = projectIds;
        approvesData[i]['isMember'] = true;
      }
      _this.setState({approveObjects: approvesData, teamProjects: projects});
    });
  },
  handleApproveSetting(e) {
    let approveObjects = this.state.approveObjects;
    if(e.target.type === 'radio') {
      let index = parseInt(e.target.name);
      approveObjects[index].isMember = !approveObjects[index].isMember;
    } else if(e.target.type === 'checkbox') {
      let attrs = e.target.name.split('-');
      let [item, projectId] = attrs;
      if(e.target.checked) {
        approveObjects[item]['project_ids'].push(projectId);
      } else {
        let i = approveObjects[item]['project_ids'].indexOf(projectId);
        if(i >= 0) {
          approveObjects[item]['project_ids'].splice(i,1);
        }
      }
    }
    this.setState({approveObjects: approveObjects});
  },
  handleSave() {
    let items = [], approveObjects = this.state.approveObjects,  projectIdsStr ='', form, role;
    for(let i=0; i< approveObjects.length; i++) {
      if(approveObjects[i].isMember) {
        role = 'member';
      } else {
        role = 'admin';
      }

      if(approveObjects[i].project_ids.length > 0) {
        projectIdsStr = approveObjects[i].project_ids.join(',');
      }

      items.push({
        team_application_id: approveObjects[i].id,
        role: role,
        project_ids: projectIdsStr
      });
    }
    form = {
      approval_objects: items
    }

    ajaxEx.post({url: Const.API.teamApprove ,contentType: 'application/json',form: JSON.stringify(form), enabledLoadingMsg:true, enabledErrorMsg:true},(d) => {
      hashHistory.push('/joinApproveAll');
    });
  },
  render() {
    let approveItems = this.state.approveObjects.map((item, index) => {
      return (
        <li>
          <div className='main-detail-container'>
            <img src={item.attributes.customer_avatar} />
            <div className='content-container'>
              <h3>{item.attributes.customer_name}</h3>
              <p>{item.attributes.customer_email}</p>
            </div>
            <div className='role-container'>
              <input type='radio' name={index} checked={!this.state.approveObjects[index].isMember} /><span className='span-divide'>管理员</span>
              <input type='radio' name={index} checked={this.state.approveObjects[index].isMember} /><span>成员</span>
            </div>
          </div>
          <div className='projects-container'>
            <h3>参与的项目</h3>
            <ProjectItems teamProjects = {this.state.teamProjects} index={index} />
          </div>
          <hr className='item-divide'/>
        </li>
      );
    });

    return(
      <div>
        <div className='join-approve-detail-container'>
          <h1>审批加入申请</h1>
          <ul className='approve-detail-list' onClick={this.handleApproveSetting}>
            {approveItems}
          </ul>
          <button className='approve-detail-btn' onClick={this.handleSave}>保存设置</button>
        </div>
        <Footer />
      </div>
    );
  }
});

module.exports = JoinApproveDetail;
