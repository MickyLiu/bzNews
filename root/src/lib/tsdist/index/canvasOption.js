"use strict";
const layers_1 = require('./layers');
const $ = require('jquery');
exports.canvasOption = {
    width: 1920,
    height: 680,
    layers: layers_1.layers,
    onResize: (canvas) => {
        // console.log(canvas);
    },
    clear: false
};
