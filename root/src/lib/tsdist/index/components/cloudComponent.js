"use strict";
const canvasComponent_1 = require('./../../reactCanvas/canvasComponent');
const reactCanvas_1 = require('./../../reactCanvas/reactCanvas');
const const_1 = require('./../const');
const Utils = reactCanvas_1.ReactCanvas.Utils;
class CloudComponent extends canvasComponent_1.ReactCanvasImageComponent {
    constructor(count) {
        super(const_1.CloudImageSrc);
        this.count = count;
        this.options = {
            scale: [.3, .8],
            speed: [.1, .5],
            alpha: [.3, 1],
            x: 0,
            y: 0,
            width: 1920,
            height: 400,
        };
        this.clouds = [];
        for (let i = 0; i < count; i++) {
            this.clouds.push(this.buildCloud(i, count));
        }
    }
    buildCloud(index, count) {
        let interval = this.options.width / count;
        let ret = {
            x: Utils.round(Math.random() * interval + index * interval),
            y: Utils.round(Math.random() * this.options.height + this.options.y),
            alpha: 1,
            scale: Math.random() * (this.options.scale[1] - this.options.scale[0]) + this.options.scale[0],
            zindex: Math.random(),
            speed: 1
        };
        // 313 x 119
        ret.width = Utils.round(313 * ret.scale);
        ret.height = Utils.round(119 * ret.scale);
        ret.speed = ret.zindex * (this.options.speed[1] - this.options.speed[0]) + this.options.speed[0];
        ret.alpha = ret.zindex * (this.options.alpha[1] - this.options.alpha[0]) + this.options.alpha[0];
        return ret;
    }
    draw(context) {
        for (let i = 0; i < this.clouds.length; i++) {
            let cloud = this.clouds[i];
            context.globalAlpha = cloud.alpha;
            context.drawImage(this.image, 0, 0, 313, 119, cloud.speed < .25 ? cloud.x : Utils.round(cloud.x), cloud.y, cloud.width, cloud.height);
        }
    }
    clear(context) {
        for (var i = 0; i < this.clouds.length; i++) {
            var cloud = this.clouds[i];
            context.clearRect(cloud.x - 1, cloud.y - 1, cloud.width + 2, cloud.height + 2);
        }
    }
    update() {
        for (let i = 0; i < this.clouds.length; i++) {
            let cloud = this.clouds[i];
            cloud.x += cloud.speed;
            if (cloud.x > this.options.width) {
                cloud.x = this.options.x - cloud.width - 1;
                cloud.y = Utils.round(Math.random() * this.options.height + this.options.y);
            }
        }
    }
}
exports.CloudComponent = CloudComponent;
