"use strict";
const canvasComponent_1 = require('./../../reactCanvas/canvasComponent');
const const_1 = require('./../const');
const $ = require('jquery');
require('./island2Component.scss');
class Island2Component extends canvasComponent_1.ReactCanvasDivComponent {
    constructor() {
        super();
        this.image = new Image();
        this.image.src = const_1.IslandImageSrc;
        let self = this;
        this.image.addEventListener('load', this.handleLoaded.bind(this), false);
    }
    build(context, canvas, reactCanvas) {
        let $dom = $(this.dom);
        $dom.css('width', reactCanvas.props.width).css('height', reactCanvas.props.height).addClass('canvas-island');
    }
}
exports.Island2Component = Island2Component;
