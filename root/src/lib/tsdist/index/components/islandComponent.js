"use strict";
const canvasComponents_1 = require('./../../reactCanvas/canvasComponent');
const const_1 = require('./../const');
class IslandComponent extends canvasComponents_1.ReactCanvasImageComponent {
    constructor() {
        super(const_1.IslandImageSrc, 0, 0, 1920, 680, 0, 0, 1920, 680);
    }
}
exports.IslandComponent = IslandComponent;
