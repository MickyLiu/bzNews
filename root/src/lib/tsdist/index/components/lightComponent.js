"use strict";
const $ = require('jquery');
const canvasComponent_1 = require('./../../reactCanvas/canvasComponent');
const const_1 = require('./../const');
class LightComponent extends canvasComponent_1.ReactCanvasImageComponent {
    constructor() {
        // 317 × 66
        super(const_1.LightImageSrc);
        this.frames = [{
                alpha: 1,
                duration: 7000,
            }, {
                alpha: .5,
                duration: 300
            }, {
                alpha: .8,
                duration: 100,
            }, {
                alpha: .6,
                duration: 100,
            }, {
                alpha: .9,
                duration: 100,
            }, {
                alpha: .7,
                duration: 100,
            }, {
                alpha: 1,
                duration: 100,
            }];
        this.x = 229;
        this.y = 123;
        this.alpha = 1;
        this.open = true;
        // 设置xya
        let self = this;
        function stateLoop(current) {
            // 设置动画起点
            let currentFrame = self.frames[current];
            self.alpha = currentFrame.alpha;
            // 设置目标关键帧
            setTimeout(() => {
                stateLoop((current + 1) % self.frames.length);
            }, currentFrame.duration);
        }
        stateLoop(0);
    }
    draw(context) {
        if (this.open) {
            context.globalAlpha = this.alpha;
            context.drawImage(this.image, this.x, this.y, 317, 66);
        }
    }
    clear(context) {
        context.clearRect(this.x, this.y, 317, 66);
    }
    onClick(e, canvas, reactCanvas) {
        let x = e.pageX - $(canvas).offset().left;
        let y = e.pageY - $(canvas).offset().top;
        if ((x < this.x) ||
            (x > this.x + 39) ||
            (y < this.y) ||
            (y > this.y + 39)) {
            return;
        }
        this.open = !this.open;
        return false;
    }
}
exports.LightComponent = LightComponent;
