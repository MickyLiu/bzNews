"use strict";
const $ = require('jquery');
const canvasComponent_1 = require('./../../reactCanvas/canvasComponent');
const reactCanvas_1 = require('./../../reactCanvas/reactCanvas');
const const_1 = require('./../const');
const Utils = reactCanvas_1.ReactCanvas.Utils;
class StarComponent extends canvasComponent_1.ReactCanvasImageComponent {
    constructor(count) {
        super(const_1.StarImageSrc);
        this.count = count;
        this.options = {
            scale: [.1, .4],
            x: 0,
            y: 0,
            width: 1920,
            height: 520,
        };
        this.stars = [];
        for (let i = 0; i < count; i++) {
            let x = Utils.round(Math.random() * this.options.width + this.options.x);
            let y = Utils.round(Math.random() * this.options.height + this.options.y);
            this.stars.push(this.buildStar(x, y));
        }
    }
    buildStar(x, y, halo) {
        let ret = {
            x: x,
            scale: Math.random() * (this.options.scale[1] - this.options.scale[0]) + this.options.scale[0],
            y: y,
            alpha: Math.random(),
            shiningSpeed: Math.random() * .015,
            shiningDirection: Math.random() > .5 ? 1 : -1
        };
        ret.height = Utils.round(60 * ret.scale);
        ret.width = Utils.round(60 * ret.scale);
        if (halo) {
            ret.halo = halo;
        }
        return ret;
    }
    draw(context) {
        for (let i = 0; i < this.stars.length; i++) {
            let star = this.stars[i];
            context.globalAlpha = star.alpha;
            context.drawImage(this.image, 0, 0, 60, 60, star.x, star.y, star.width, star.height);
            // 绘制光晕
            if (star.halo) {
                let halo = star.halo;
                context.globalAlpha = 1;
                context.beginPath();
                context.strokeStyle = `rgba(255,255,255,${halo.alpha})`;
                context.lineWidth = 1;
                let radius = halo.radius * star.scale;
                let offset = 30 * star.scale;
                context.arc(star.x + offset, star.y + offset, radius, (Math.PI / 180) * 0, (Math.PI / 180) * 360, false);
                context.stroke();
                context.closePath();
            }
        }
    }
    clear(context) {
        for (var i = 0; i < this.stars.length; i++) {
            var star = this.stars[i];
            if (star.halo) {
                context.clearRect(star.x - 10, star.y - 10, star.width + 20, star.height + 20);
            }
            else {
                context.clearRect(star.x, star.y, star.width, star.height);
            }
        }
    }
    update() {
        for (let i = 0; i < this.stars.length; i++) {
            let star = this.stars[i];
            star.alpha = star.alpha + star.shiningSpeed * star.shiningDirection;
            if (star.alpha > 1) {
                star.alpha = 1;
                star.shiningDirection = -1;
            }
            else if (star.alpha < 0) {
                star.alpha = 0;
                star.shiningDirection = 1;
            }
            // 光晕控制
            if (star.halo) {
                let halo = star.halo;
                halo.alpha -= halo.alphaSpeed;
                halo.radius += halo.radiusSpeed;
                if (halo.alpha < 0) {
                    delete star.halo;
                }
            }
        }
    }
    onClick(e, canvas, reactCanvas) {
        let x = e.pageX - $(canvas).offset().left;
        let y = e.pageY - $(canvas).offset().top;
        if ((x < this.options.x) ||
            (x > this.options.x + this.options.width) ||
            (y < this.options.y) ||
            (y > this.options.y + this.options.height)) {
            return;
        }
        let star = this.buildStar(x, y, {
            alpha: 1,
            alphaSpeed: .025,
            radius: 10,
            radiusSpeed: 1
        });
        star.alpha = 1;
        star.shiningDirection = -1;
        star.x -= star.scale * 30;
        star.y -= star.scale * 30;
        star.x = Utils.round(star.x);
        star.y = Utils.round(star.y);
        this.stars.push(star);
    }
}
exports.StarComponent = StarComponent;
