"use strict";
const canvasComponent_1 = require('./../../reactCanvas/canvasComponent');
const const_1 = require('./../const');
const $ = require('jquery');
require('./wave2Component.scss');
class Wave2Component extends canvasComponent_1.ReactCanvasDivComponent {
    constructor() {
        super();
        this.loaded = false;
        this.waves = [];
        this.image = new Image();
        this.image.src = const_1.WaveImageSrc;
        let self = this;
        this.image.addEventListener('load', this.handleLoaded.bind(this), false);
        // build waves
        for (var i = 0; i < 6; i++) {
            this.waves.push({
                width: 1920,
                height: 220,
                offsetX: 0,
                offsetY: 1320 - (i + 1) * 220 + 1,
                direction: i % 2 * 2 - 1,
                transform: {
                    position: {
                        x: 0,
                        y: 0
                    }
                },
                speed: {
                    x: 1,
                    y: 1
                }
            });
        }
    }
    build(context, canvas, reactCanvas) {
        let $dom = $(this.dom);
        $dom.css('width', reactCanvas.props.width).css('height', reactCanvas.props.height);
        for (var i = 0; i < this.waves.length; i++) {
            let wave = this.waves[i];
            let $wave = $(`<div class="canvas-wave w${i}"></div>`);
            let $subwave1 = $('<div class="canvas-sub-wave"></div>')
                .css('backgroundPosition', `0px ${-wave.offsetY}px`)
                .css('left', '0');
            let $subwave2 = $('<div class="canvas-sub-wave"></div>')
                .css('backgroundPosition', `0px ${-wave.offsetY}px`)
                .css('left', 1920);
            let $subwave3 = $('<div class="canvas-sub-wave"></div>')
                .css('backgroundPosition', `0px ${-wave.offsetY}px`)
                .css('left', -1920);
            $wave.append($subwave1).append($subwave2).append($subwave3);
            $dom.append($wave);
            wave.dom = $wave[0];
        }
    }
    draw() {
        // for (var j = 0; j < this.waves.length; j++) {
        //   let wave = this.waves[j];
        //   let $wave = $(wave.dom);
        //   $wave.css('transform', `translateX(${wave.transform.position.x}px)`);
        // }
    }
    update() {
        // for (var j = 0; j < this.waves.length; j++) {
        //   var wave = this.waves[j];
        //   wave.transform.position.x += wave.speed.x * wave.direction;
        //   if (wave.transform.position.x > 1920) {
        //     wave.transform.position.x = 0;
        //   }
        //   if (wave.transform.position.x < -1920) {
        //     wave.transform.position.x = 0
        //   }
        // }
    }
}
exports.Wave2Component = Wave2Component;
