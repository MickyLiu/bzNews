"use strict";
const canvasComponent_1 = require('./../../reactCanvas/canvasComponent');
const const_1 = require('./../const');
class WaveComponent extends canvasComponent_1.ReactCanvasImageComponent {
    constructor() {
        super(const_1.WaveImageSrc);
        this.waves = [];
        this.frames = [];
        this.currentFrame = 0;
        this.frameLength = 0;
        for (var i = 0; i < 6; i++) {
            this.waves.push({
                width: 1920,
                height: 220,
                offsetX: 0,
                offsetY: 1320 - (i + 1) * 220 + 1,
                direction: i % 2 * 2 - 1,
                transform: {
                    position: {
                        x: 0,
                        y: 0
                    }
                },
                speed: {
                    x: 1,
                    y: 1
                }
            });
        }
    }
    beforeLoaded() {
        this.frameLength = 1920;
        for (var i = 0; i < this.frameLength; i++) {
            let frame = document.createElement("canvas");
            frame.width = 1920;
            frame.height = 220;
            let context = frame.getContext('2d');
            // update
            for (var j = 0; j < this.waves.length; j++) {
                var wave = this.waves[j];
                wave.transform.position.x += wave.speed.x * wave.direction;
                if (wave.transform.position.x > 1920) {
                    wave.transform.position.x = 0;
                }
                if (wave.transform.position.x < -1920) {
                    wave.transform.position.x = 0;
                }
            }
            // draw
            for (var j = 0; j < this.waves.length; j++) {
                var wave = this.waves[j];
                context.drawImage(this.image, wave.offsetX, wave.offsetY, wave.width, wave.height, wave.transform.position.x, wave.transform.position.y, wave.width, wave.height);
                context.drawImage(this.image, wave.offsetX, wave.offsetY, wave.width, wave.height, wave.transform.position.x - wave.direction * 1920, wave.transform.position.y, wave.width, wave.height);
            }
            this.frames[i] = frame;
        }
    }
    draw(context) {
        context.drawImage(this.frames[this.currentFrame], 0, 460);
    }
    update(context) {
        this.currentFrame = (this.currentFrame + 1) % this.frameLength;
    }
}
exports.WaveComponent = WaveComponent;
