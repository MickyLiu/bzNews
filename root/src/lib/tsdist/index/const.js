"use strict";
exports.WaveImageSrc = require('./../../../../assets/image/wave.png');
exports.IslandImageSrc = require('./../../../../assets/image/island.png');
exports.StarImageSrc = require('./../../../../assets/image/star.png');
exports.SprayImageSrc = require('./../../../../assets/image/spray.png');
exports.CloudImageSrc = require('./../../../../assets/image/cloud.png'); // 313 x 119
exports.LightImageSrc = require('./../../../../assets/image/light.png'); // 317 × 66
