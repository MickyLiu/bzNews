"use strict";
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
const React = require("react");
const reactCanvas_1 = require("./../reactCanvas/reactCanvas");
const canvasOption_1 = require('./canvasOption');
var play = true;
class Index extends React.Component {
    render() {
        return (React.createElement(reactCanvas_1.ReactCanvas, __assign({ref: (c) => this.canvas = c}, canvasOption_1.canvasOption)));
    }
}
exports.Index = Index;
