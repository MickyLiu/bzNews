"use strict";
const canvasComponent_1 = require('./../reactCanvas/canvasComponent');
const canvasLayer_1 = require('./../reactCanvas/canvasLayer');
// import { WaveComponent } from './components/waveComponent';
const wave2Component_1 = require('./components/wave2Component');
// import { IslandComponent } from './components/islandComponent';
const island2Component_1 = require('./components/island2Component');
const starComponent_1 = require('./components/starComponent');
const sprayComponent_1 = require('./components/sprayComponent');
const cloudComponent_1 = require('./components/cloudComponent');
const lightComponent_1 = require('./components/lightComponent');
const layers = [];
exports.layers = layers;
/**
 * Layer1: background
 */
// add background
const BackgroundComponent = new canvasComponent_1.ReactCanvasLinearGradientFillRectComponent({ x0: 0, y0: 0, x1: 0, y1: 680 }, { x: 0, y: 0, width: 1920, height: 680 }, [0, '#72c2d2'], [1, '#cbecd9']);
BackgroundComponent.zindex = -1;
// add cloud
let cloud = new cloudComponent_1.CloudComponent(8);
cloud.zindex = 20;
// add star
let star = new starComponent_1.StarComponent(20);
star.zindex = 10;
let layer1 = new canvasLayer_1.ReactCanvasLayer(BackgroundComponent, cloud, star);
layer1.zindex = 1;
layer1.static = true;
layers.push(layer1);
// /**
//  * Layer2: star, cloud
//  */
// add star
let star2 = new starComponent_1.StarComponent(0);
star.zindex = 10;
let layer2 = new canvasLayer_1.ReactCanvasLayer(star2);
layer2.zindex = 2;
layer2.ifclear = true;
// layer2.static = true;
layers.push(layer2);
/**
 * Layer3: island
 */
// add island
let island = new island2Component_1.Island2Component();
island.zindex = 50;
let layer3 = new canvasLayer_1.ReactCanvasLayer(island);
layer3.zindex = 3;
layer3.static = true;
layers.push(layer3);
/**
 * Layer4: spray, waves, light
 */
// add spray
let spray = new sprayComponent_1.SprayComponent();
spray.zindex = 60;
// add wave
// let waves = new WaveComponent();
// waves.zindex = 100;
let waves = new wave2Component_1.Wave2Component();
waves.zindex = 100;
// add light
let light = new lightComponent_1.LightComponent();
light.zindex = 200;
let layer4 = new canvasLayer_1.ReactCanvasLayer(spray, light, waves);
layer4.zindex = 4;
layer4.ifclear = true;
layers.push(layer4);
