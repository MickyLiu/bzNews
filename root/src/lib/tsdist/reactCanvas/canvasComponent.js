"use strict";
class ReactCanvasComponent {
    constructor() {
        this.loaded = false;
        this.zindex = 0;
    }
    beforeLoaded() { }
    init(context, canvas, reactCanvas) { }
    update(context, canvas, reactCanvas) { }
    clear(context, canvas, reactCanvas) { }
    draw(context, canvas, reactCanvas) { }
    onLoaded(callback) {
        this._loaded = callback;
        if (this.loaded) {
            this.handleLoaded();
        }
    }
    handleLoaded() {
        this.beforeLoaded();
        this.loaded = true;
        if (this._loaded) {
            this._loaded(this);
        }
    }
    onClick(e, canvas, reactCanvas) { }
    ;
}
exports.ReactCanvasComponent = ReactCanvasComponent;
const reactCanvasLinearGradientFillRectComponent_1 = require('./components/reactCanvasLinearGradientFillRectComponent');
exports.ReactCanvasLinearGradientFillRectComponent = reactCanvasLinearGradientFillRectComponent_1.ReactCanvasLinearGradientFillRectComponent;
const reactCanvasImageComponent_1 = require('./components/reactCanvasImageComponent');
exports.ReactCanvasImageComponent = reactCanvasImageComponent_1.ReactCanvasImageComponent;
const reactCanvasDivComponent_1 = require('./components/reactCanvasDivComponent');
exports.ReactCanvasDivComponent = reactCanvasDivComponent_1.ReactCanvasDivComponent;
