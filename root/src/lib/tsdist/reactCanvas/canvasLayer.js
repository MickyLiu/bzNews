"use strict";
class ReactCanvasLayer {
    constructor(...components) {
        // array like
        this.length = 0;
        this.loaded = false;
        this.zindex = 0;
        this.static = false;
        this.ifclear = false;
        this.length = components.length;
        let self = this;
        components.sort((a, b) => a.zindex - b.zindex);
        for (var i = 0; i < components.length; i++) {
            this[i] = components[i];
        }
        for (var i = 0; i < this.length; i++) {
            var component = this[i];
            if (component._loaded) {
                ((component) => {
                    component.onLoaded(() => {
                        component._loaded(component);
                        self._checkLoaded();
                    });
                })(component);
            }
            else {
                component.onLoaded(this._checkLoaded.bind(this));
            }
        }
    }
    beforeLoaded() { }
    init(context, canvas, reactCanvas) {
        for (var i = 0; i < this.length; i++) {
            this[i].init(context, canvas, reactCanvas);
        }
    }
    update(context, canvas, reactCanvas) {
        for (var i = 0; i < this.length; i++) {
            this[i].update(context, canvas, reactCanvas);
        }
    }
    clear(context, canvas, reactCanvas) {
        if (!this.static) {
            for (var i = 0; i < this.length; i++) {
                this[i].clear(context, canvas, reactCanvas);
            }
        }
    }
    draw(context, canvas, reactCanvas) {
        for (var i = 0; i < this.length; i++) {
            context.save();
            this[i].draw(context, canvas, reactCanvas);
            context.restore();
        }
    }
    _checkLoaded() {
        // console.log(this);
        let loaded = true;
        for (var i = 0; i < this.length; i++) {
            var component = this[i];
            loaded = loaded && component.loaded;
        }
        this.loaded = loaded;
        if (this.loaded && this._loaded) {
            this._loaded(this);
        }
    }
    onLoaded(callback) {
        this._loaded = callback;
        if (this.loaded) {
            this.handleLoaded();
        }
    }
    handleLoaded() {
        this.beforeLoaded();
        this.loaded = true;
        if (this._loaded) {
            this._loaded(this);
        }
    }
    onClick(e, canvas, reactCanvas) {
        for (var i = this.length - 1; i >= 0; i--) {
            var component = this[i];
            if (component.onClick(e, canvas, reactCanvas) == false) {
                return false;
            }
        }
    }
}
exports.ReactCanvasLayer = ReactCanvasLayer;
