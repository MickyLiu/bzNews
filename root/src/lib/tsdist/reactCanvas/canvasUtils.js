"use strict";
exports.CanvasUtils = {
    round(x) {
        return (0.5 + x) << 0;
    },
};
