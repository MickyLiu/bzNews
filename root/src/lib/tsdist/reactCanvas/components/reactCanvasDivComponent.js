"use strict";
const canvasComponent_1 = require('./../canvasComponent');
const reactCanvas_1 = require('./../reactCanvas');
const $ = require('jquery');
const Utils = reactCanvas_1.ReactCanvas.Utils;
class ReactCanvasDivComponent extends canvasComponent_1.ReactCanvasComponent {
    constructor(...args) {
        super(...args);
        this.loaded = true;
    }
    init(context, canvas, reactCanvas) {
        let $dom = $('<div style="position: absolute; overflow: hidden;"></div>');
        let $canvas = $(canvas);
        if (canvas.width != parseInt(canvas.style.width)) {
            $dom.css('width', canvas.style.width);
        }
        else {
            $dom.css('width', canvas.width + 'px');
        }
        if (canvas.height != parseInt(canvas.style.height)) {
            $dom.css('height', canvas.style.height);
        }
        else {
            $dom.css('height', canvas.height + 'px');
        }
        $(canvas).after($dom);
        this.dom = $dom[0];
        this.build(context, canvas, reactCanvas);
    }
    build(context, canvas, reactCanvas) { }
}
exports.ReactCanvasDivComponent = ReactCanvasDivComponent;
