"use strict";
const canvasComponent_1 = require('./../canvasComponent');
const reactCanvas_1 = require('./../reactCanvas');
const Utils = reactCanvas_1.ReactCanvas.Utils;
class ReactCanvasImageComponent extends canvasComponent_1.ReactCanvasComponent {
    constructor(source, offsetX, offsetY, width, height, canvasOffsetX, canvasOffsetY, canvasImageWidth, canvasImageHeight) {
        super();
        this.source = source;
        this.offsetX = offsetX;
        this.offsetY = offsetY;
        this.width = width;
        this.height = height;
        this.canvasOffsetX = canvasOffsetX;
        this.canvasOffsetY = canvasOffsetY;
        this.canvasImageWidth = canvasImageWidth;
        this.canvasImageHeight = canvasImageHeight;
        this.image = new Image();
        this.image.src = this.source;
        let self = this;
        this.image.addEventListener('load', this.handleLoaded.bind(this), false);
        this.drawMode = 0;
        if (offsetX != undefined) {
            this.drawMode = 1;
            this.offsetX = Utils.round(this.offsetX);
            this.offsetY = Utils.round(this.offsetY);
        }
        if (width != undefined) {
            this.drawMode = 2;
            this.width = Utils.round(this.width);
            this.height = Utils.round(this.height);
        }
        if (canvasOffsetX != undefined) {
            this.drawMode = 3;
            this.canvasOffsetX = Utils.round(this.canvasOffsetX);
            this.canvasOffsetY = Utils.round(this.canvasOffsetY);
            this.canvasImageWidth = Utils.round(this.canvasImageWidth);
            this.canvasImageHeight = Utils.round(this.canvasImageHeight);
        }
    }
    draw(context) {
        switch (this.drawMode) {
            case 1:
                context.drawImage(this.image, this.offsetX, this.offsetY);
                break;
            case 2:
                context.drawImage(this.image, this.offsetX, this.offsetY, this.width, this.height);
                break;
            case 3:
                context.drawImage(this.image, this.offsetX, this.offsetY, this.width, this.height, this.canvasOffsetX, this.canvasOffsetY, this.canvasImageWidth, this.canvasImageHeight);
                break;
            default:
                break;
        }
    }
}
exports.ReactCanvasImageComponent = ReactCanvasImageComponent;
