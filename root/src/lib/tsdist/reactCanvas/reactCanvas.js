"use strict";
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
const React = require("react");
const $ = require("jquery");
require("./../../../_utils/hidpi-canvas");
const canvasUtils_1 = require('./canvasUtils');
function ReactCanvasEvent(target, propertyKey, descriptor) {
    return {
        value: function (...args) {
            if (!this.state.play) {
                return;
            }
            else {
                var result = descriptor.value.apply(this, args);
                return result;
            }
        }
    };
}
class ReactCanvas extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.drawList = [];
        this.canvasList = [];
        this.state = {
            loading: true,
            play: true
        };
    }
    // lifecycle
    componentWillMount() {
        let self = this;
        for (var i = 0; i < this.props.layers.length; i++) {
            var layer = this.props.layers[i];
            layer.onLoaded(() => {
                let loaded = true;
                for (var l in self.props.layers) {
                    if (self.props.layers.hasOwnProperty(l)) {
                        var component = self.props.layers[l];
                        loaded = loaded && component.loaded;
                    }
                }
                self.setState({
                    loading: !loaded
                }, () => {
                    if (!self.state.loading) {
                        // init
                        let drawList = self.drawList;
                        for (var i = 0; i < drawList.length; i++) {
                            var unit = drawList[i];
                            unit.layer.init(unit.context, unit.canvas, self);
                        }
                    }
                });
            });
        }
    }
    componentDidMount() {
        this.updateLayers();
        var self = this;
        // window.onresize
        if (this.props.onResize) {
            $(window).resize(() => { self.props.onResize(self); });
        }
        // renderLoop
        function renderLoop() {
            if (self.state.play) {
                let drawList = self.drawList;
                for (var i = 0; i < drawList.length; i++) {
                    let unit = drawList[i];
                    let layer = unit.layer;
                    if (!layer.static) {
                        // clear canvas
                        if (layer.ifclear) {
                            layer.clear(unit.context, unit.canvas, self);
                        }
                        // update
                        layer.update(unit.context, unit.canvas, self);
                        // draw
                        unit.layer.draw(unit.context, unit.canvas, self);
                    }
                }
            }
            window.requestAnimationFrame(renderLoop);
        }
        // draw static layers
        (() => {
            let drawList = self.drawList;
            for (var i = 0; i < drawList.length; i++) {
                if (drawList[i].layer.static) {
                    let unit = drawList[i];
                    unit.layer.update(unit.context, unit.canvas, self);
                    unit.layer.draw(unit.context, unit.canvas, self);
                }
            }
        })();
        // loading
        (function loadingDetect() {
            if (self.state.loading) {
                window.requestAnimationFrame(loadingDetect);
            }
            else {
                window.requestAnimationFrame(renderLoop);
            }
        })();
    }
    render() {
        let canvasOne = {
            width: this.props.width || 320,
            height: this.props.height || 640,
            maxWidth: this.props.maxWidth || 1920,
            maxHeight: this.props.maxHeight || 640,
            style: {
                position: 'absolute',
            }
        };
        let style = this.props.style || {};
        style.width = style.width || canvasOne.width;
        style.height = style.height || canvasOne.height;
        style.position = style.position || 'relative';
        this.canvasList.length = this.props.layers.length;
        let self = this;
        return (React.createElement("div", {className: "canvas-app", style: style, onClick: this.handleClick.bind(this)}, this.props.layers.map((layer, i) => {
            return (React.createElement("canvas", __assign({ref: (dom) => { self.canvasList[i] = dom; }}, canvasOne, {key: i}), "Your browser does not support HTML5 Canvas"));
        })));
    }
    // event
    handleClick(e) {
        for (var i = this.drawList.length - 1; i >= 0; i--) {
            let unit = this.drawList[i];
            var layer = unit.layer;
            if (layer.onClick) {
                if (layer.onClick(e, unit.canvas, this) == false) {
                    break;
                }
            }
        }
    }
    // methods
    updateLayers() {
        let drawList = this.drawList;
        drawList.length = 0;
        for (var i = 0; i < this.props.layers.length; i++) {
            drawList[drawList.length] = {
                layer: this.props.layers[i],
                canvas: null,
                context: null
            };
        }
        drawList.sort((a, b) => a.layer.zindex - b.layer.zindex);
        for (var i = 0; i < this.props.layers.length; i++) {
            let unit = drawList[i];
            unit.canvas = this.canvasList[i];
            unit.context = unit.canvas.getContext('2d');
            unit.context.clearRect(0, 0, this.props.width, this.props.height);
        }
    }
    pause() {
        this.setState({
            play: false
        });
    }
    play() {
        this.setState({
            play: true
        });
    }
}
// static members
ReactCanvas.Utils = canvasUtils_1.CanvasUtils;
__decorate([
    ReactCanvasEvent
], ReactCanvas.prototype, "handleClick", null);
exports.ReactCanvas = ReactCanvas;
