let React = require('react');
let MovingFace = require('./MovingFace');

let LoginDialog = React.createClass({
  getInitialState: function() {
    return {name: '', password: '', msg: '', btvalue: '登录'};
  },
  handleNameChange: function(e) {
    this.setState({name: e.target.value});
  },
  handlePwdChange: function(e) {
    this.setState({password: e.target.value});
  },
  handleSubmit: function(e) {
    e.preventDefault();
    if (this.state.btvalue.startsWith('登录中')) {
        return;
    }
    console.log(this.updater.enqueueSetState);
    var name = this.state.name.trim();
    var password = this.state.password.trim();
    if (!password || !name) {
      this.setState({msg: '邮箱或密码不能为空'});
      return;
    };
    this.setState({msg: ''});
    var dot = 0;
    var self = this;
    self.setState({btvalue: '登录中'});
    var s = setInterval(function(){
        if (!self.state.btvalue.startsWith('登录中')) {
          clearInterval(s);
          return;
        }
        dot = (dot + 1) % 4;
        self.setState({btvalue: '登录中' + '.'.repeat(dot)});
    }, 500);
    var auth = this.props.Auth;
    auth.login(name, password, function() {
      self.setState({msg: '邮箱或密码错误', btvalue: '登录'});
    });
  },
  render: function () {
    return (
      <div id="loginDialog">
        <MovingFace radius={12} pradius={5} />
        <form className="commentForm" onSubmit={this.handleSubmit} >
          <div className="ld-input-part">
            <div className="ld-group">
                <input
                    type="text"
                    placeholder="尼玛的邮箱"
                    value={this.state.name}
                    onChange={this.handleNameChange}
                    className="ld-input ld-name"
                />
            </div>
            <div className="ld-divid"></div>
            <div className="ld-group">
                <input
                    type="password"
                    placeholder="尼玛的密码"
                    value={this.state.password}
                    onChange={this.handlePwdChange}
                    className="ld-input ld-pwd"
                />
            </div>
          </div>
          <div className="ld-msg">{this.state.msg}</div>
          <div className="ld-submit-part">
            <input type="submit" value={this.state.btvalue} className="ld-submit" />
          </div>
        </form>
      </div>
    )
  }
});

module.exports =  LoginDialog;
