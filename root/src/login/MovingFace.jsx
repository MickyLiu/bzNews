let $ = require('jquery');
let React = require('react');
let ReactDOM = require('react-dom');
let head = document.createElement('img');
head.src = require('./../../assets/image/head.png');

var MovingFace = React.createClass({
  getInitialState: function() {
    var ret = {
      LTop: 0,
      LLeft: 0,
      RTop: 0,
      RLeft: 0,
      LCTop: 0,
      LCLeft: 0,
      RCTop: 0,
      RCLeft: 0,
      _init: true
    };
    return ret;
  },
  componentDidMount: function () {
    var self = this;
    var radius = self.props.radius;
    var pradius = self.props.pradius;
    $('body').mousemove(function(e) {
      if (self.state._init) {
        self.updateCenter();
      }
      var lx = e.pageX - self.state.LCLeft;
      var ly = e.pageY - self.state.LCTop;
      var ld = Math.sqrt(lx * lx + ly * ly);
      var rx = e.pageX - self.state.RCLeft;
      var ry = e.pageY - self.state.RCTop;
      var rd = Math.sqrt(rx * rx + ry * ry);
      if (ld > radius) {
        ld = ld / radius;
        lx = lx / ld;
        ly = ly / ld;
      }
      if (rd > radius) {
        rd = rd / radius;
        rx = rx / rd;
        ry = ry / rd;
      }
      self.setState({
        LLeft: lx - pradius,
        LTop: ly - pradius,
        RLeft: rx - pradius,
        RTop: ry - pradius,
      });
    });
    setInterval(function(){
        self.updateCenter();
    }, 2000);
  },
  updateCenter: function() {
    var l = $(".wnm-eye-l").offset();
    var r = $(".wnm-eye-r").offset();
    this.setState({
      LCTop: l.top,
      LCLeft: l.left,
      RCTop: r.top,
      RCLeft: r.left,
    });
    this.state._init = false;
  },
  render: function () {
    return (
      <div id="wangnima">
        <img src={head.src}/>
        <div className="wnm-eye-l">
          <div className="wnm-eye-pupil" style={{top: this.state.LTop + "px", left: this.state.LLeft + "px"}}></div>
        </div>
        <div className="wnm-eye-r">
          <div className="wnm-eye-pupil" style={{top: this.state.RTop + "px", left: this.state.RLeft + "px"}}></div>
        </div>
      </div>
    )
  }
});


module.exports = MovingFace;
