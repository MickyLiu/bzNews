let React = require('react');
let ReactDOM = require('react-dom');
let Auth = require('../_utils/auth');
let LoginDialog = require('./LoginDialog');
let particlesInit = require('./particles');
require('particles.js');
require('./login.scss');

let auth = new Auth({
  page: 'login',
  target: 'teams',
  expect: false,
});
auth.verify();

particlesInit();

const topLogo = document.createElement('img');
topLogo.src = require('./../../assets/image/top_logo.png');
const LoginPanel = React.createClass({
  render() {

    return (
      <div id="login">
        <div id="header">
            <a href="#" title="首页"></a>
            <img src={topLogo.src} />
        </div>
        <div id="container">
            <div id="content">
              <LoginDialog Auth={auth} />
            </div>
        </div>
        <div id="footer">© 暴走漫画
            <div className="left">
                <a href="#" className="iconfont icon-android" title="安卓客户端"></a>
                <a href="#" className="iconfont icon-apple" title="苹果客户端"></a>
            </div>
            <div className="right">
                <a href="https://dsjadmin.baomihua.tv/auth/weibo" className="iconfont icon-xlweibo" title="新浪微博"></a>
            </div>
        </div>
      </div>
    );
  }
});

ReactDOM.render(<LoginPanel />, document.getElementById('block'));
