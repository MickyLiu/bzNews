let React = require('react');

const MembersMain = React.createClass({
  render() {
    return(
      <div>
        {this.props.children}
      </div>
    );
  }
});

module.exports = MembersMain;
