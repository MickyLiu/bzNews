let React = require('react');
let ajaxEx = require('./../_utils/ajaxConfiger');
let Const = require('./../_utils/const');
let events = require('./../_utils/eventSubscriber');
let Footer = require('./../components/footer/footer');
require('./membersPanel.scss');

const MemberEditPanel = React.createClass({
  getInitialState() {
    return {
      role: 'member',
      projects: [],
      isAdmin: false,
      showDelete: false
    };
  },
  componentDidMount() {
    let _this = this;
    let url = `${Const.API.customers}/${this.props.params.id}/settings?team_id=${localStorage.getItem('_teamId_')}`;
    ajaxEx.get({url: url}, (d)=> {
      let isAdmin = false
      if(d.role === 'admin') {
        isAdmin = true;
      }
      _this.setState({role: d.role, projects: d.projects, isAdmin: isAdmin});
    });
  },
  saveEdit() {
    let project_ids = [];
    for(let i= 0; i< this.state.projects.length; i++) {
      if(this.state.projects[i].in_project) {
        project_ids.push(this.state.projects[i].id)
      }
    }
    let form = {
      role: this.state.role,
      project_ids: project_ids
    }
    ajaxEx.post({url: `${Const.API.team}/customers/${this.props.params.id}/setting_other`, form: form, enabledLoadingMsg:true,enabledErrorMsg:true});
  },
  roleChange() {
    let isAdmin = !this.state.isAdmin;
    let role = '';
    if(isAdmin) {
      role = 'admin';
    } else {
      role = 'member';
    }
    this.setState({isAdmin: isAdmin, role: role});
  },
  handleCheckBox(e) {
    let projects = this.state.projects;
    for(let i=0; i< projects.length; i++) {
      if(projects[i].id === parseInt(e.target.id)) {
        projects[i].in_project = !projects[i].in_project;
        break;
      }
    }
    this.setState({projects: projects});
  },
  showDeleteDialog() {
    this.setState({showDelete: true});
  },
  hideDeleteDialog() {
    this.setState({showDelete: false});
  },
  deleteMember() {
    let _this = this;
    ajaxEx.post({url:`${Const.API.team}/delete_member?customer_id=${this.props.params.id}`, enabledLoadingMsg:true,enabledErrorMsg:true }, function() {
      window.location.href = '#/members';
    });
  },
  render() {
    let projectsCheckBox = this.state.projects.map((project) => {
      return (
        <li>
          <input type='checkbox' checked={project.in_project} id={project.id} onChange={this.handleCheckBox}/>
          <span>{project.name}</span>
        </li>
      );
    });

    let deleteBtn = document.createElement('img');
    deleteBtn.src = require('./../../assets/image/invite-delete.png');

    return(
      <div>
        <div className='member-edit-container'>
          <h1 className='member-edit-title'>{this.props.location.query.memberName} 的设置</h1>
          <div className='remove-member-panel'>
            <h4 className='member-sub-title'>从团队中移除</h4>
            <p>被移除的成员，将不能再访问 星辰大海 上</p>
            <a href='javascript:void(0)' onClick={this.showDeleteDialog}>了解,从团队中移除 {this.props.location.query.memberName}</a>
          </div>
          <div className='priority-panel'>
            <h4 className='member-sub-title'>权限</h4>
            <input type='radio' name="role" value='admin' checked={this.state.isAdmin} onChange={this.roleChange}/> 管理员 <br />
            <input type='radio' name="role" value='member' checked={!this.state.isAdmin} onChange={this.roleChange}/> 成员
          </div>
          <div className='involve-projects'>
            <h4 className='member-sub-title'>参与的项目</h4>
            <ul className='projects-list'>
              {projectsCheckBox}
            </ul>
          </div>
          <button className='save-edit member-edit-btn' onClick={this.saveEdit}>保存</button>
          <div className={this.state.showDelete ? 'delete-member-dialog show' : 'delete-member-dialog'}>
            <img src={deleteBtn.src} onClick={this.hideDeleteDialog}/>
            <h4>确定要移除 {this.props.location.query.memberName} 吗</h4>
            <p>跟 {this.props.location.query.memberName} 相关的数据不会被移除，你还可以重新邀请TA加入团队</p>
            <button className='member-edit-btn' onClick={this.deleteMember}>确定</button>
            <a href='javascript:void(0)' onClick={this.hideDeleteDialog}>取消</a>
          </div>
        </div>
        <Footer />
      </div>
    );
  }
});

module.exports = MemberEditPanel;
