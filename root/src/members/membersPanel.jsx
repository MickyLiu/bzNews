import {Link} from 'react-router';
let React = require('react');
let Footer = require('./../components/footer/footer');
let ajaxEx = require('./../_utils/ajaxConfiger');
let Const = require('./../_utils/const');
let events = require('./../_utils/eventSubscriber');
let Clipboard = require('clipboard');
require('./membersPanel.scss');

const CreateCustomerButton = React.createClass({
  render: function() {
    let addIcon = document.createElement('img');
    addIcon.src = require('./../../assets/image/addMember.png');
    return (
        <li>
          <img src={addIcon.src} onClick={this.props.clickHandler}/>
          <p>添加成员</p>
        </li>
    );
  }
});

const InviteDialog = React.createClass({
  getInitialState: function() {
    return {
      inviteUrl: ""
    };
  },
  componentDidMount: function() {
    new Clipboard('#copyBtn');
    let _this = this;
    events.subscribe("getInviteURL", function(){
      let url = Const.API.customers + "/" + localStorage.getItem("currentUserId") + "/quick_invite"+ "?team_id=" + localStorage.getItem("_teamId_");
      ajaxEx.get({url: url}, function(d){
        let inviteUrl = Const.API.inviteUrl + "?token=" + d.invitation_code + "&teamName=" + localStorage.getItem("_teamName_") + "&teamId=" + localStorage.getItem("_teamId_");
        _this.setState({inviteUrl: inviteUrl});
      });
    });
    events.subscribe("removeInviteURL", function(){
      _this.setState({inviteUrl: ""});
    });
  },
  render: function() {
    let deleteBtn = document.createElement('img');
    deleteBtn.src = require('./../../assets/image/invite-delete.png');
    return (
      <div className={this.props.visible ? "invite-dialog show": "invite-dialog"}>
        <h3>添加新成员</h3>
        <input
            id="inviteUrl"
            type="text"
            placeholder="邀请链接"
            value={this.state.inviteUrl}
        />
        <div className="handle-area">
          <button id="copyBtn" data-clipboard-target="#inviteUrl">复制</button>
          <span>将公共邀请链发送给需要邀请的人</span>
        </div>
        <img className="delete-btn" src={deleteBtn.src} onClick={this.props.removeDialog} />
      </div>
    );
  }
});

const MembersPanel = React.createClass({
  getInitialState: function() {
    return {
      members: [],
      dialogShow: false
    };
  },
  addMember: function() {
    if(localStorage.getItem('_currentRole_') === 'member') {
      return;
    } else {
      this.setState({dialogShow: true});
      events.publish("getInviteURL");
    }
  },
  removeDialog: function() {
    this.setState({dialogShow: false});
    events.publish("removeInviteURL");
  },
  componentDidMount: function() {
    let _this = this;
    ajaxEx.get({url: Const.API.customers + "?team_id=" + localStorage.getItem("_teamId_")}, function(d) {
      let adminsList = d.data.map(function(item) {
        return {
          id: item.id,
          name: item.attributes.name,
          avatar: item.attributes.avatar_url,
          role: item.attributes.role
        };
      });
      _this.setState({members: adminsList});
    });
  },
  render: function () {
    let customerNodes = this.state.members.map((customer) => {
      let roleTitle = '';
      switch(customer.role) {
        case 'super':
          roleTitle = '超级管理员';
          break;
        case 'admin':
          roleTitle = '管理员';
          break;
        case 'member':
          roleTitle = '成员';
          break;
        default:
          roleTitle = '成员';
          break;
      }

      let linkSrc = `members/${customer.id}/edit`;
      if(localStorage.getItem('_currentRole_') === 'member' || roleTitle === '超级管理员' || customer.id === localStorage.getItem('currentUserId')) {
        linkSrc = 'members/';
      }

      return (
          <li>
            <Link to={{ pathname: linkSrc, query: { memberName: customer.name } }}>
              <img src={customer.avatar}/>
            </Link>
            <p style={{marginBottom: '4px'}}>{customer.name}</p>
            <p style={roleTitle === '超级管理员' ? {color: '#FF8B00'} : {color: '#c8c8c8'}}>{roleTitle}</p>
          </li>
      );
    });
    customerNodes.unshift(<CreateCustomerButton clickHandler={this.addMember} />);
      return (
          <div>
            <p className='msg-line'>你有3条未处理申请，<Link to="/joinApprove" >现在去处理</Link></p>
            <div className="admin-panel">
              <ul className="admin-list">
                {customerNodes}
              </ul>
            </div>
            <Footer />
            <InviteDialog removeDialog={this.removeDialog} visible={this.state.dialogShow}/>
          </div>
        )
    }
});

module.exports = MembersPanel;
