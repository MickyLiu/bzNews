let React = require('react');
let ReactDom = require('react-dom');
let events = require('./../_utils/eventSubscriber');

require('./messages.scss');

const Message = require('./../components/topBar/notification/message');

const Messages = React.createClass({
  getInitialState() {
    return{
      messages: [],
      total: 0,
      page: 0,
    };
  },
  componentDidMount() {
    let data = [{
      message: '呵呵呵呵',
      linkText: '现在去处理',
      time: new Date()
    }, {
      message: 'hhahahahah',
      linkText: '现在去处理',
      time: new Date()
    }, {
      message: 'wwuwuwuwuwuwu',
      linkText: '现在去处理',
      time: new Date()
    }, {
      message: '这条很长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长',
      linkText: '现在去处理',
      time: new Date()
    }, {
      message: 'hhahahahah',
      linkText: '现在去处理',
      time: new Date()
    }, {
      message: 'wwuwuwuwuwuwu',
      linkText: '现在去处理',
      time: new Date()
    }, {
      message: 'wwuwuwuwuwuwu',
      linkText: '现在去处理',
      time: new Date()
    }, {
      message: '这条很长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长长',
      linkText: '现在去处理',
      time: new Date()
    }, {
      message: 'hhahahahah',
      linkText: '现在去处理',
      time: new Date()
    }, {
      message: 'wwuwuwuwuwuwu',
      linkText: '现在去处理',
      time: new Date()
    }]
    this.setState({
      messages: data
    });

  },

  render() {
    return (
      <div className="messages">
        <div className="title">全部通知</div>
        {this.state.messages.map((v, i) => 
          <Message {...v} key={i} index={i + 1} total={this.state.messages.length}/>
        )}
      </div>
    )
  }
});

module.exports = Messages;

