import {Modal, Form, Input, Button} from 'antd';

let React = require('react');
let ajaxEx = require('./../../_utils/ajaxConfiger');
let events = require('./../../_utils/eventSubscriber');
let Const = require('./../../_utils/const');
let $ = require('jquery');
require('./../../lib/jquery.form.js');
require('./navyDialog.scss');

const FormItem = Form.Item;

const NavyDialogForm = React.createClass({
  getInitialState() {
    return {
      formMethod: "post",
      id: "",
      name: "",
      errInfo: ""
    };
  },
  componentDidMount() {
    events.subscribe("editNavy",(function(navyInfo){
      this.setState({
        formMethod: "put",
        id: navyInfo.id,
        name: navyInfo.name,
        errInfo: ""
      });
      $('#avatar').attr('src',navyInfo.avatar);
    }).bind(this));

    events.subscribe("createNavy",(function(navyInfo){
      this.setState({
        formMethod: "post",
        id: '',
        name: '',
        errInfo: ""
      });
      $('#avatar').attr('src','');
    }).bind(this));

    events.subscribe("nameErr",(function(){
      this.setState({
        errInfo: "水军名字不要超过8个字符"
      });
    }).bind(this));
  },
  componentWillUnmount: function(){
    events.unsubscribe('editNavy');
    events.unsubscribe('createNavy');
    events.unsubscribe('nameErr');
  },
  handleInputChange(e) {
    if(e.target.type=='file')
    {
      var fr = new FileReader();
      fr.readAsDataURL(e.target.files[0]);
      fr.onload = function(e) {
         $('#avatar').attr('src',e.target.result);
      };
    }
  },
  handleNameChange: function(e) {
    this.setState({name: e.target.value.trim()});
  },
  render() {
    return (
      <Form horizontal className="dialogForm navyDialog" id="createDialogForm" method={this.state.formMethod}>
        <p className='err-info'>{this.state.errInfo}</p>
        <FormItem label="名字:"
        >
          <Input name="user[name]" value={this.state.name} onChange={this.handleNameChange} id="name" />
        </FormItem>
        <FormItem
          label="头像" >
          <img id="avatar" />
          <div>
            <a className="file-upload">
               选择文件
               <Input type="file" id="bannerThumbnail" name='user[avatar]' onChange={this.handleInputChange}/>
            </a>
          </div>
        </FormItem>
      </Form>
    );
  }
});


const NavyDialog = React.createClass({
  getInitialState() {
    return {
      loading: false,
      visible: false,
      create: true  // false for edit type
    };
  },
  componentDidMount() {
    events.subscribe("showCreateNavyDialog",(function(data){
      this.setState({
        visible: true
      });
    }).bind(this));
    events.subscribe("showEditNavyDialog",(function(data){
      this.setState({
        visible: true,
        create: false
      });
    }).bind(this));
  },
  componentWillUnmount: function(){
    events.unsubscribe('showCreateNavyDialog');
    events.unsubscribe('showEditNavyDialog');
  },
  handleOk() {
    if(this.refs.navyDialogForm.state.name.length > 8) {
      events.publish('nameErr');
      return;
    }
    ajaxEx.submit({url:Const.API.water_armies + "/" + this.refs.navyDialogForm.state.id ,formEle:$('#createDialogForm')[0],enabledLoadingMsg:true,enabledErrorMsg:true},function() {
      events.publish("refreshNavyTable");
    });
    this.resetDialog();
  },
  handleCancel() {
    this.resetDialog();
  },
  resetDialog() {
    this.setState({
      visible: false,
      create: true
    });
    $('#createDialogForm').get(0).reset();
  },
  render() {
    return (
        <Modal ref="modal"
          visible={this.state.visible}
          title={this.state.create ? "创建水军" : "编辑信息"} onOk={this.handleOk} onCancel={this.handleCancel}
          footer={[
            <Button key="back" type="ghost" size="large" onClick={this.handleCancel}>返 回</Button>,
            <Button key="submit" type="primary" size="large" loading={this.state.loading} onClick={this.handleOk}>
              确定
            </Button>
          ]}>
          <NavyDialogForm ref="navyDialogForm" />
        </Modal>
    );
  }
});

module.exports = NavyDialog;
