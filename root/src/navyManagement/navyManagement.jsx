let React = require('react');
let Const = require('../_utils/const');
let events = require('../_utils/eventSubscriber');
let ajaxEx = require('../_utils/ajaxConfiger');
let NavyDialog = require('./navyDialog/navyDialog');
let Preloader = require('./../components/imagePreloader/imagePreloader');
require('./navyManagement.scss');

import {Table, Button} from 'antd';

const NavyHeader = React.createClass({
  createNavy: function(e) {
    events.publish("showCreateNavyDialog");
    events.publish("createNavy");
  },
  render: function() {
    return (
      <header className='navy-header'>
        <h1>水军信息</h1>
        <Button className='btn' type="primary" onClick={this.createNavy}>新建</Button>
      </header>
    );
  }
});

const EditButton = React.createClass({
  editNavy() {
    let _this = this;
    events.publish("showEditNavyDialog");
    setTimeout(function() {
      events.publish("editNavy",_this.props.navyInfo);
    },100);
  },
  render() {
    return (
      <a href="javascript:void(0)" onClick={this.editNavy}>编辑</a>
    );
  }
});

const columns = [
  {
    title: 'ID',
    dataIndex: 'id',
    key: 'id'
  },
  {
    title: '头像',
    dataIndex: 'avatar',
    key: 'avatar',
    render(text) {
      return (<Preloader className="list-avatar" url={text} defaultUrl={require('../../assets/image/default-avatar.png')}/>);
    }
  },
  {
    title: '名字',
    dataIndex: 'name',
    key: 'name'
  },
  {
    title: '操作',
    key: 'operation',
    render(text, record) {
      return (
        <EditButton navyInfo={record}/>
      );
    }
  }
];

const NavyList = React.createClass({
  getInitialState: function() {
    return {
      loading: true,
      dataSource: []
    }
  },
  componentDidMount: function() {
    let _this = this;
    ajaxEx.get({url:Const.API.water_armies},function(d) {
      let dataSource = [];
      if(d.data && d.data.length > 0) {
        for(let i= 0; i< d.data.length; i++) {
          dataSource.push({
              id: d.data[i].id,
              name: d.data[i].attributes.name,
              avatar: d.data[i].attributes.avatar
            });
        }
      }
      _this.setState({dataSource: dataSource, loading: false});
    });

    events.subscribe("refreshNavyTable",(function() {
      let _this = this;
      ajaxEx.get({url:Const.API.water_armies},function(d){
        let dataSource = [];
        if(d.data && d.data.length > 0) {
          for(let i= 0; i< d.data.length; i++) {
            dataSource.push({
                id: d.data[i].id,
                name: d.data[i].attributes.name,
                avatar: d.data[i].attributes.avatar
              });
          }
        }
        _this.setState({dataSource: dataSource, loading: false});
      });
    }).bind(this));
  },
  render: function() {
    return (
      <div>
        <Table id="navyTable" columns={columns} dataSource={this.state.dataSource} loading={this.state.loading} />
      </div>
    );
  }
});



const NavyManagement = React.createClass({
  render: function() {
    return (
      <div>
        <NavyHeader />
        <NavyList />
        <NavyDialog />
      </div>
    );
  }
});

module.exports = NavyManagement;
