let React = require('react');
let classnames = require('classnames');
import { Icon,Spin,Upload } from 'antd';
let ajaxEx = require('../_utils/ajaxConfiger');
let Const = require('../_utils/const');
require('./imageUploader.scss');

const ImageUploader = React.createClass({
  getInitialState() {
    let projectLogo = require('./../../assets/image/yellow-grass.png');
    return {
      projectImage: this.props.project.attributes.image || projectLogo,
      loading:false
    };
  },
  handleFileChange(e){
       var _this = this,target= e.target;
       e.stopPropagation();
       _this.setState({loading:true});
        
       ajaxEx.submit({url:Const.API.urlPrefix + 'projects/' + this.props.project.id,formEle:$('#projectImageForm_' + this.props.project.id)[0],enabledErrorMsg:true},function(d){
              if(target.type=='file')
              {
                  var fr = new FileReader();
                  fr.readAsDataURL(target.files[0]);
                  fr.onload = function(e) {
                     _this.setState({projectImage:e.target.result});
                     _this.props.changeProjectImage(_this.props.project.id,e.target.result);
                  };
              }  
              _this.setState({loading:false});
       },function(){
              _this.setState({loading:false});
       });
  },
  render() {
    const props = {
      action: Const.API.urlPrefix + 'projects' + this.props.project.id,
      listType: 'picture-card',
      defaultFileList: [{
        uid: -1,
        name: 'xxx.png',
        status: 'done',
        url: this.state.projectImage,
        thumbUrl: this.state.projectImage,
      }],
      onChange: this.handleFileChange,
      showUploadList:false
    };
    return (
     <div className={"ant-tooltip  ant-tooltip-placement-bottom  ant-tooltip-hidden"} onClick={(e)=>{ e.stopPropagation()}}>
        <div className="ant-tooltip-content">
          <div className="ant-tooltip-arrow"></div>
          <div className="ant-tooltip-inner">
            <div className="image-uploader-con">
                <div className="choose-project-image">  
                 <ul>
                   <Spin spining={this.state.loading}>
                     <li className="chosen">
                        <img src={this.state.projectImage}/>
                     </li>
                     <span className="plus-btn">
                        <form id={'projectImageForm_' + this.props.project.id} method="put">
                           <input type="file" className="project-image-file" name={'project[image]'} onChange={this.handleFileChange}/>
                        </form>  
                          <i className=" anticon anticon-plus"></i>
                     </span>
                   </Spin>  
                  </ul>
                </div> 
                <div className="clearfix"></div> 
            </div>
        </div> 
      </div>
     </div> 
    );
  },
});


module.exports = ImageUploader;
