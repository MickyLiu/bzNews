import { Table, Icon, Spin,Form,Input,Button,Upload,Select,Modal } from 'antd';
let Link = require('./../../components/link/link');

let React = require('react');
let events = require('./../../_utils/eventSubscriber');
let ajaxEx = require('./../../_utils/ajaxConfiger');
let Const = require('./../../_utils/const');
let $ = require('jquery');
require('./projectCreate.scss');
let Footer = require('./../../components/footer/footer');
let Preloader = require('./../../components/imagePreloader/imagePreloader');
const FormItem = Form.Item;
const Option = Select.Option;

let ProjectsCreateView = React.createClass({
  getInitialState() {
       return { projectName:'',members:[],membersAdded:[],masks:[],membersAddedOb:{}};
  },
  componentDidMount: function () {
       var _this =this;
       ajaxEx.get({url: Const.API.customers + "?team_id=" + localStorage.getItem("_teamId_")}, function(d) {
          let adminsList = d.data.map(function(item) {
            return {
              name: item.attributes.name,
              avatar: item.attributes.avatar_url,
              id:item.id,
              role: item.attributes.role
            };
          });
          _this.setState({members: adminsList});
    });
  },
  componentWillUnmount: function(){
     //events.unsubscribe('progressChange');
  },
  handleSubmit(e) {
       e.preventDefault();
       var _this= this;
      if(!this.state.projectName)
      {
          Modal.error({
            title: '请填写项目名称！',
            content: ''
          });
          return false;
      }
      let ids = this.state.membersAdded.map(function(item) {
            return item.id;
      });
      var data = {
           project:{
              name:this.state.projectName,
              customer_ids:ids.toString()
           }
      };
      ajaxEx.post({url:Const.API.team + "/projects",form:data,enabledErrorMsg:true},function(d){
               _this.props.history.goBack();
      });
     
  },
  handleInputChange(e) {
      var value = $.trim(e.target.value);
      this.setState({
          projectName: value
      });
  },
  handleChange(v) {
        var ob = _.filter(this.state.members, function(item) {
              return item.id==v;
        });
        var membersAdd = this.state.membersAdded,membersOb = this.state.membersAddedOb;
        if(!membersOb[ob[0].id])
        {
           membersAdd.push(ob[0]);
           membersOb[ob[0].id] = true;
           this.setState({membersAdded:membersAdd,membersAddedOb:membersOb,masks:[]});
        }
  },
  deleteMemberChoosed(i){
        var membersAdd = this.state.membersAdded,membersOb = this.state.membersAddedOb;
        delete membersOb[membersAdd[i].id];
        membersAdd.withOut(function(index,o){
               return index == i;
        });
        this.setState({membersAdded:membersAdd,membersAddedOb:membersOb});
  },
  addAllMember(){
       var members = this.state.members,membersAdd = this.state.membersAdded,membersOb = this.state.membersAddedOb;
       members.forEach(function(o,i){
             if(!membersOb[o.id])
             {  
                 membersAdd.push(o);
                 membersOb[o.id]= true;
             }
       });

       this.setState({membersAdded:membersAdd,membersAddedOb:membersOb,masks:[]});
  },
  showDeleteMember(i){
        let masks = this.state.masks;
        masks[i] =true;
        this.setState({masks:masks});
  },
  hideDeleteMember(i){
        let masks = this.state.masks;
        masks[i] =false;
        this.setState({masks:masks});
  },
  render() {
    var _this = this;

    let roleTextMap = (role)=>{
                let roleTitle = '';
                switch(role) {
                  case 'super':
                    roleTitle = '超级管理员';
                    break;
                  case 'admin':
                    roleTitle = '管理员';
                    break;
                  case 'member':
                    roleTitle = '成员';
                    break;
                  default:
                    roleTitle = '成员';
                    break;
                }
                return roleTitle;
    };

    return (
        <div>
            <div className='project-create-con'>
               <Form horizontal id={'projectCreateForm'} form={this.props.form} method="post">
                    <FormItem>
                      <p className="title">创建新项目</p>
                      <Input placeholder="项目名称" name={'series[title]'} className="project-name" value={this.state.projectName} onChange={this.handleInputChange}/>
                    </FormItem>
                    <FormItem>
                       <p className="title">选择项目成员</p>
                       <p className="sub-title">团队成员</p>
                       <Select style={{ maxWidth: 180 }} placeholder="点击添加成员" className="member-select" onChange={this.handleChange}>
                           {_.map(this.state.members,(o,index)=>
                              <Option value={o.id} key={o.id}><a title={o.name}><Preloader className="member-avatar-opt" url={o.avatar} defaultUrl={require('../../../assets/image/default-avatar.png')}/>{o.name}</a></Option>
                           )}
                       </Select>

                       <Button type="primary" onClick={this.addAllMember} disabled={this.state.members.length==this.state.membersAdded.length?'disabled':''}>所有人</Button>
                       <div className="member-card-list"> 
                           {_.map(this.state.membersAdded,(o,index)=>
                              <li onMouseEnter={(o)=>{this.showDeleteMember(index)}} onMouseLeave={(o)=>{this.hideDeleteMember(index)}}>
                                <div className="member-content" >
                                   <Preloader url={o.avatar} defaultUrl={require('../../../assets/image/default-avatar.png')}/>
                                   <div>
                                       <p className="member-name">{o.name}</p>
                                       <p className="member-role">{roleTextMap(o.role)}</p>
                                   </div>
                                 </div>  
                                 <div className={"member-mask " + (this.state.masks[index]?"active":'')} onClick={(o)=>{this.deleteMemberChoosed(index)}}>点击移除成员</div>
                              </li>
                           )}
                       </div>
                    </FormItem>
                    <FormItem>
                      <Button type="primary" onClick={this.handleSubmit} className="submit-project-btn">创建项目</Button>
                      <a className="cancel-project-btn" href="#/">取消</a>
                    </FormItem>
                </Form>
           </div>
           <Footer />
       </div>    
    );
  }
});

ProjectsCreateView = Form.create()(ProjectsCreateView);

module.exports = ProjectsCreateView;
