import { Table, Icon, Spin,Form,Input,Button,Upload,Select,Modal } from 'antd';
let InLink = require('./../../components/link/link');
import {Link} from 'react-router';
let React = require('react');
let events = require('./../../_utils/eventSubscriber');
let ajaxEx = require('./../../_utils/ajaxConfiger');
let Const = require('./../../_utils/const');
let $ = require('jquery');
require('./projectMember.scss');
require('../projectCreate/projectCreate.scss');
let Footer = require('./../../components/footer/footer');
let Preloader = require('./../../components/imagePreloader/imagePreloader');
const FormItem = Form.Item;
const Option = Select.Option;

let ProjectsMemberView = React.createClass({
  getInitialState() {
       return { listOrEdit:1,teamMembers:[],members:[],membersAdded:[],masks:[],membersAddedOb:{}};
  },
  componentDidMount: function () {
       var _this =this,membersOb=this.state.membersAddedOb;
       let adminsListNoMe=[],currentUserId = localStorage.getItem('currentUserId');
       ajaxEx.get({url: Const.API.projectPrefix + "/members" }, function(d) {
              d.data.forEach(function(item,index) {
                   if(currentUserId!=item.id)
                   {
                        adminsListNoMe.push({
                          name: item.attributes.name,
                          avatar: item.attributes.avatar_url,
                          id:item.id,
                          role: item.attributes.role
                        });
                        membersOb[item.id] = true;
                   }
              });
          _this.setState({members:$.extend(true,[],adminsListNoMe),membersAdded:adminsListNoMe,membersAddedOb:membersOb});
      });
  },
  componentWillUnmount: function(){
     //events.unsubscribe('progressChange');
  },
  handleSubmit(e) {
       e.preventDefault();
       var _this= this;
      let ids = this.state.membersAdded.map(function(item) {
            return item.id;
      });
      var data = {
           customer_ids:ids.toString()
      };
      ajaxEx.put({url:Const.API.projectPrefix + "/member_setting",form:data,enabledErrorMsg:true},function(d){
               _this.setState({members: $.extend(true,[],_this.state.membersAdded)});
               _this.switchListOrEdit();
      });
  },
  switchListOrEdit(){
       var _this = this,membersOb=this.state.membersAddedOb,membersAdded=this.state.membersAdded,currentUserId = localStorage.getItem('currentUserId');;
       if(this.state.listOrEdit && !this.state.teamMembers.length)
       {
           ajaxEx.get({url: Const.API.customers + "?team_id=" + localStorage.getItem("_teamId_")}, function(d) {
                 let adminsList =[];
                 d.data.forEach(function(item,index) {
                     if(currentUserId!=item.id)
                     {
                          adminsList.push({
                            name: item.attributes.name,
                            avatar: item.attributes.avatar_url,
                            id:item.id,
                            role: item.attributes.role
                          });
                     }
                });
                _this.setState({teamMembers: adminsList});
           });
       }

       this.setState({listOrEdit:!this.state.listOrEdit,membersAdded: membersAdded});
  },
  handleChange(v) {
        var ob = _.filter(this.state.teamMembers, function(item) {
              return item.id==v;
        });
        var membersAdd = this.state.membersAdded,membersOb = this.state.membersAddedOb;
        if(!membersOb[ob[0].id])
        {
           membersAdd.push(ob[0]);
           membersOb[ob[0].id] = true;
           this.setState({membersAdded:membersAdd,membersAddedOb:membersOb,masks:[]});
        }
  },
  deleteMemberChoosed(i){
        var membersAdd = this.state.membersAdded,membersOb = this.state.membersAddedOb;
        delete membersOb[membersAdd[i].id];
        membersAdd.withOut(function(index,o){
               return index == i;
        });
        this.setState({membersAdded:membersAdd,membersAddedOb:membersOb});
  },
  addAllMember(){
       var members = this.state.teamMembers,membersAdd = this.state.membersAdded,membersOb = this.state.membersAddedOb;
       members.forEach(function(o,i){
             if(!membersOb[o.id])
             {  
                 membersAdd.push(o);
                 membersOb[o.id]= true;
             }
       });

       this.setState({membersAdded:membersAdd,membersAddedOb:membersOb,masks:[]});
  },
  showDeleteMember(i){
        let masks = this.state.masks;
        masks[i] =true;
        this.setState({masks:masks});
  },
  hideDeleteMember(i){
        let masks = this.state.masks;
        masks[i] =false;
        this.setState({masks:masks});
  },
  confirmLeaveProject(){
        Modal.confirm({
          title: '确定要退出这个项目吗？',
          content: '',
          okText: '确认',
          cancelText: '取消',
          onOk(){
                ajaxEx.delete({url: Const.API.projectPrefix + "/quit",enabledErrorMsg:true}, function(d) {
                     window.location = "#/";
                });
          }
        });
  },
  clearDataAndBack(){
      let membersOb = {};
      this.state.members.forEach(function(o,i){
             if(!membersOb[o.id])
             {  
                 membersOb[o.id]= true;
             }
       });
      this.setState({listOrEdit:!this.state.listOrEdit,membersAdded:$.extend(true,[],this.state.members),masks:[],membersAddedOb:membersOb});
  },
  render() {
    var _this = this;

    let roleTextMap = (role)=>{
                let roleTitle = '';
                switch(role) {
                  case 'super':
                    roleTitle = '超级管理员';
                    break;
                  case 'admin':
                    roleTitle = '管理员';
                    break;
                  case 'member':
                    roleTitle = '成员';
                    break;
                  default:
                    roleTitle = '成员';
                    break;
                }
                return roleTitle;
    };

    let customerNodes = this.state.members.map((customer) => {
          let roleTitle = '';
          roleTitle = roleTextMap(customer.role);
          let linkSrc = `members/${customer.id}/edit`;
          if(localStorage.getItem('_currentRole_') === 'member') {
            linkSrc = 'members/';
          }
          else if(roleTitle === '超级管理员') {
            linkSrc = 'members/';
          }

          return (
              <li>
                <Link to={{ pathname: linkSrc, query: { memberName: customer.name } }}>
                  <Preloader url={customer.avatar} defaultUrl={require('../../../assets/image/default-avatar.png')}/>
                </Link>
                <p style={{marginBottom: '4px'}}>{customer.name}</p>
                <p style={roleTitle === '超级管理员' ? {color: '#FF8B00'} : {color: '#c8c8c8'}}>{roleTitle}</p>
              </li>
          );
    });
    return (
        <div className="project-member-con">
             <div>
                 <span className="title">项目成员</span>
                 <Button type="primary" onClick={this.switchListOrEdit} disabled={this.state.listOrEdit?'':'disabled'}>编辑</Button>
                 <a className={"leave-project-btn " + (this.state.listOrEdit?"invisible":'')} onClick={this.confirmLeaveProject}>退出项目</a>
             </div>
             <div className={'project-member-list '+ (this.state.listOrEdit?"":'invisible')}>
                  <ul className="member-list">
                    {customerNodes}
                  </ul>
             </div>
             <div className={'project-member-setting ' + (this.state.listOrEdit?"invisible":'')}>
                <Form horizontal id={'projectMemberForm'} form={this.props.form} method="post">
                    <FormItem>
                       <p className="title">选择项目成员</p>
                       <p className="sub-title">团队成员</p>
                       <Select style={{ maxWidth: 180 }} placeholder="点击添加成员" className="member-select" onChange={this.handleChange}>
                           {_.map(this.state.teamMembers,(o,index)=>
                              <Option value={o.id} key={o.id}><a title={o.name}><Preloader className="member-avatar-opt" url={o.avatar} defaultUrl={require('../../../assets/image/default-avatar.png')}/>{o.name}</a></Option>
                           )}
                       </Select>

                       <Button type="primary" onClick={this.addAllMember} disabled={this.state.teamMembers.length==this.state.membersAdded.length?'disabled':''}>所有人</Button>
                       <div className="member-card-list"> 
                           {_.map(this.state.membersAdded,(o,index)=>
                              <li onMouseEnter={(o)=>{this.showDeleteMember(index)}} onMouseLeave={(o)=>{this.hideDeleteMember(index)}}>
                                <div className="member-content" >
                                   <Preloader url={o.avatar} defaultUrl={require('../../../assets/image/default-avatar.png')}/>
                                   <div>
                                       <p className="member-name">{o.name}</p>
                                       <p className="member-role">{roleTextMap(o.role)}</p>
                                   </div>
                                 </div>  
                                 <div className={"member-mask " + (this.state.masks[index]?"active":'')} onClick={(o)=>{this.deleteMemberChoosed(index)}}>点击移除成员</div>
                              </li>
                           )}
                       </div>
                    </FormItem>
                    <FormItem>
                      <Button type="primary" onClick={this.handleSubmit} className="submit-project-btn">保存设置</Button>
                      <a className="cancel-project-btn" href="javascript:void(0)" onClick={this.clearDataAndBack}>取消</a>
                    </FormItem>
                </Form>
             </div>
       </div>    
    );
  }
});

ProjectsMemberView = Form.create()(ProjectsMemberView);

module.exports = ProjectsMemberView;
