import { Table, Icon, Spin,Form,Input,Button,Upload,Select,Modal } from 'antd';
let InLink = require('./../../components/link/link');
import {Link} from 'react-router';
let React = require('react');
let events = require('./../../_utils/eventSubscriber');
let ajaxEx = require('./../../_utils/ajaxConfiger');
let Const = require('./../../_utils/const');
let $ = require('jquery');
require('./projectSetting.scss');
let Footer = require('./../../components/footer/footer');
let Preloader = require('./../../components/imagePreloader/imagePreloader');
const FormItem = Form.Item;
const Option = Select.Option;

let ProjectSettingView = React.createClass({
  getInitialState() {
       return { projectName:localStorage.getItem("projectName"),errorMsg:'',password:'',confirmLoading:false,visible:false};
  },
  componentDidMount: function () {
       var _this =this;
  },
  componentWillUnmount: function(){
     //events.unsubscribe('progressChange');
  },
  handleSubmit(e) {
       e.preventDefault();
        var _this= this,projectName = $.trim(this.state.projectName);
        if(!projectName)
        {
            Modal.error({
              title: '请填写项目名称！',
              content: ''
            });
            return false;
        }

      var data = {
            project:{
                name:projectName
            }
      };
      
      ajaxEx.put({url: Const.API.projectPrefix,form:data,enabledErrorMsg:true,enabledSuccessMsg:true},function(d){
               localStorage.setItem("projectName",projectName);
               events.publish('UiInfoChange',{projectName:projectName});
      });
     
  },
  handleProjectNameChange(e) {
      var value = $.trim(e.target.value);
      this.setState({
          projectName: value
      });
  },
  handlePasswordChange(e) {
      var value = $.trim(e.target.value);
      this.setState({
          password: value
      });
  },
  confirmDelete(){
       this.setState({visible:true,password:''});
  },
  handleOk(){
        let _this = this;
        if(this.state.password)
        {
            this.setState({confirmLoading:true});
            ajaxEx.delete({url:Const.API.projectPrefix,form:{
                  password: _this.state.password
              }},function(d){
                   _this.setState({errorMsg:'',visible:false});
                    window.location = '#/';
           },function(e){
                _this.setState({errorMsg:e.statusText || e.responseJSON.errors[0].detail});
           }).complete(function(){
                 _this.setState({confirmLoading:false});
           });
       }
       else
       {
           _this.setState({errorMsg:'请输入你的登录密码!'});
       }
  },
  handleCancel(){
        this.setState({visible:false,errorMsg:'',password:''});
  },
  render() {
    var _this = this;

    return (
        <div className="project-setting-con">
              <Form horizontal id={'projectEditForm'} form={this.props.form} method="post">
                    <FormItem>
                      <p className="title">项目设置</p>
                      <Input placeholder="项目名称" name={'project[name]'} className="project-name" value={this.state.projectName} onChange={this.handleProjectNameChange}/>
                    </FormItem>

                    <FormItem>
                       <Button type="primary" onClick={this.handleSubmit} className="submit-project-btn">保存设置</Button>
                    </FormItem>
              </Form>
              <div>
                   <span className="title">删除项目</span>
                   <p className="delete-info">
                       <span>删除项目后,所有内容也将被立即删除，不能恢复。请谨慎操作</span>
                       <a href="javascript:void(0)" onClick={this.confirmDelete}>删除当前项目</a>
                   </p>
              </div>
              <Modal
                visible={this.state.visible}
                onOk={this.handleOk}
                confirmLoading={this.state.confirmLoading}
                onCancel={this.handleCancel} okText="确认删除" width={500}>
                    <div className="delete-info-box" >
                     <div>
                       <p className="title">删除项目：{this.state.projectName}</p>
                       <p className="info"><i>重要提示：</i>该项目所有的内容都将被删除，不可恢复。如果你确定要这样做,请在下面输入登录密码确认。</p>
                       <Input placeholder="请输入你的登录密码" ref="deleteInfoBoxInput" onFocus={()=>{this.refs.deleteInfoBoxInput.refs.input.type='password'}} name={'password'} value={this.state.password} onChange={this.handlePasswordChange}/>
                       <div className="ant-form-explain error-msg">{this.state.errorMsg}</div>
                     </div>  
                     <Icon type="exclamation-circle" />
                  </div>
              </Modal>
        </div>    
    );
  }
});

ProjectSettingView = Form.create()(ProjectSettingView);

module.exports = ProjectSettingView;
