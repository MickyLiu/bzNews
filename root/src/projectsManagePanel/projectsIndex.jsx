import { Router, hashHistory } from 'react-router';
import {render} from 'react-dom';

let Const = require('../_utils/const');
let React = require('react');
let events = require('../_utils/eventSubscriber');
let ajaxEx = require('../_utils/ajaxConfiger');
let Link = require('../components/link/link');
let ImageUploader = require('./imageUploader');
let $ = require('jquery');
let _ = require('lodash');
require('./projectsIndex.scss');
let Footer = require('../components/footer/footer');
require('./../components/adArea/adArea.scss');
require('./../components/adArea/sprite.scss');
require('./../lib/tsdist/index/components/island2Component.scss');
require('./../lib/tsdist/index/components/wave2Component.scss');

import { Spin,Modal,Form,Input,Tooltip,Upload,Icon } from 'antd';

const FormItem = Form.Item;

const ProjectsManagePanel = React.createClass({
  getInitialState() {
   return{
      data:[],
      loading:false,
      listType:1,
      showUploaders:{}
    };
  },
  componentDidMount: function () {
       events.subscribe('refreshProjectList',(function(){
               this.fetchProjects();
       }).bind(this));

      this.fetchProjects();
  },
  fetchProjects(){
       var _this = this;
      _this.setState({loading:true});
      ajaxEx.get({url:Const.API.urlPrefix + 'teams/' + globalVars.get('_teamId_') + '/projects'},function(d){
           _this.setState({
              data: d.data
           });
      }).complete(function(){
            _this.setState({loading:false});
      });
  },
  componentWillUnmount: function(){
      events.unsubscribe('refreshProjectList');
  },
  toggleUploaderPanel(e){
    e.stopPropagation();
    var target = $(e.currentTarget).next();
    this.hideUploader(target);
    if(target.hasClass('ant-tooltip-hidden')){
         target.removeClass('ant-tooltip-hidden')
               .addClass('zoom-big-enter zoom-big-enter-active');
    }
    else{
         target.removeClass('zoom-big-enter zoom-big-enter-active')
               .addClass('ant-tooltip-hidden');
    }
  },
  hideUploader(targetTokeep){
    $('.ant-tooltip:not(.ant-tooltip-hidden)').not(targetTokeep).removeClass('zoom-big-enter zoom-big-enter-active')
                                              .addClass('ant-tooltip-hidden');
  },
  changeProject(o){
       globalVars.set('_projectId_', o.id);
       localStorage.setItem('projectName', o.attributes.name);
       localStorage.setItem('projectImage', o.attributes.image);
       window.location.href= "#/projects/"+  o.id;
  },
  changeProjectImage(id,image){
        var data =this.state.data;
        _.map(data,(o,index)=>{
            if(o.id==id)
            {
               o.attributes.image = image;
            }
        });

        this.setState({data:data});
  },
  render() {
    return (
      <div>
         <div className="project-container" onClick={this.hideUploader}>
             <Spin spining={this.state.loading}>
                <div className="project-list">
                    {this.state.listType?
                      <div>
                         <a className="project-item-grid project-new" href={"#/projects/create/show"}>
                                   <div><Icon type="plus" /></div>
                                   <p>新建项目</p>
                         </a>
                         {_.map(this.state.data,(o,index)=>
                             <a key={o.id} className="project-item-grid" onClick={() => this.changeProject(o)} name={o.attributes.name} id={o.id}>
                                  <Icon type="setting" onClick={this.toggleUploaderPanel} />
                                  <ImageUploader project={o} changeProjectImage={this.changeProjectImage}/>
                                  <img src={o.attributes.image} />
                                  <p>{o.attributes.name}</p>
                              </a>
                         )}
                      </div>:
                      <div>
                      {_.map(this.state.data,(o,index)=>
                          <p key={o.id}>
                            <Tooltip title={<ImageUploadList projectId={o.id}/>}>
                                <a className="project-item-list" href={"#/projects/"+ o.id} onClick={this.changeProject} name={o.id}>
                                   {o.attributes.name}
                                </a>
                             </Tooltip>
                          </p>
                       )}
                      </div>}
                </div>
             </Spin>
          </div>
          <Footer />
        </div>
    );
  }
});

module.exports = ProjectsManagePanel;
