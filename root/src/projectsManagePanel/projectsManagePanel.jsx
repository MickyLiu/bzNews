let React = require('react');

const ProjectsManagePanel = React.createClass({
  componentDidMount: function () {

  },
  render() {
    return (
        <div>
             {this.props.children}
        </div>
    );
  }
});

module.exports = ProjectsManagePanel;
