let React = require('react');
let ReactDOM = require('react-dom');
let Auth = require('../_utils/auth');
let LoginDialog = require('../components/dialog/loginDialog');
let RegisterDialog = require('../components/dialog/registerDialog');
let events = require('./../_utils/eventSubscriber');
let SearchPage = require('../components/searchAuthor/searchAuthor');

require('./searchAuthor.scss');
require('./../index/index.scss');

let auth = new Auth({
  page: 'searchAuthor',
  target: 'index',
  expect: false
});
auth.verify();

const SearchAuthor = React.createClass({
  getInitialState: function() {
    let dialogVisible = false,
    loginDialogShow = false;

    if(localStorage.getItem("expired") !== null && JSON.parse(localStorage.getItem("expired"))) {
      dialogVisible = true;
      loginDialogShow = true;
    }

    return {
      dialogVisible: dialogVisible,
      loginDialogShow: loginDialogShow,
      registerDialogShow: false
    };
  },
  componentDidMount: function() {
    let _this = this;
    events.subscribe("removeDialog", function() {
      _this.setState({dialogVisible: false});
    });

    events.subscribe("showLoginDialog", function() {
      _this.setState({dialogVisible: true, loginDialogShow: true, registerDialogShow: false});
    });

    events.subscribe("showRegisterDialog", function() {
      _this.setState({dialogVisible: true, loginDialogShow: false, registerDialogShow: true});
    });
  },
  componentWillUnmount: function() {
    events.unsubscribe("removeDialog");
  },
  login: function() {
    this.setState({dialogVisible: true, loginDialogShow: true, registerDialogShow: false});
  },
  register: function() {
    this.setState({dialogVisible: true, registerDialogShow: true, loginDialogShow: false});
  },
  render: function() {
    return (
      <div>
        <nav className="home-nav">
          <div className="main-area">
            <h2 className="home-title">星辰大海</h2>
            <a className="home-nav-btn" href="javascript:void(0)" onClick= {this.login}>登录</a>
            <a className= "home-nav-btn register-btn" href="javascript:void(0)" onClick= {this.register}>注册</a>
          </div>
        </nav>
        <SearchPage auth={auth}/>

        <div className={this.state.dialogVisible ? 'hover-container show' : 'hover-container'}>
          <LoginDialog auth={auth} visible={this.state.loginDialogShow}/>
          <RegisterDialog auth={auth} visible={this.state.registerDialogShow}/>
        </div>
      </div>
    );
  }
});

ReactDOM.render(<SearchAuthor />, document.getElementById('container'));
