import { Button } from 'antd';
let Link = require('./../../components/link/link');

let React = require('react');
require('./panelTopBar.scss');

const PanelTopBar = React.createClass({
  render() {
    return (
      <header className="panel-top-bar series-top">
        <h1 className="panel-title">{this.props.panelTitle}</h1>
        <Link to='series/create'><Button type="primary" className="series-add-btn">添加</Button></Link>
      </header>
    );
  }
});

module.exports = PanelTopBar;
