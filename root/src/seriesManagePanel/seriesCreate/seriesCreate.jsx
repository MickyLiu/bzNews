import { Table, Icon, Spin,Form,Input,Button,Upload } from 'antd';
let Link = require('./../../components/link/link');

let React = require('react');
let events = require('./../../_utils/eventSubscriber');
let ajaxEx = require('./../../_utils/ajaxConfiger');
let Const = require('./../../_utils/const');
let $ = require('jquery');
require('./../seriesEdit/seriesEdit.scss');

const FormItem = Form.Item;
let seriesCreateView = React.createClass({
  getInitialState() {
       return { title:"新建系列"};
  },
  componentDidMount: function () {
  },
  componentWillUnmount: function(){
     //events.unsubscribe('progressChange');
  },
  handleSubmit(e) {
       e.preventDefault();
       var _this= this;
       this.props.form.validateFields((errors, values) => {
                if (!!errors) {
                  console.log('Errors in form!!!');
                  e.preventDefault();
                  return;
                }

          ajaxEx.submit({url:Const.API.projectSeries,formEle:$('#seriesForm')[0],enabledLoadingMsg:true,enabledErrorMsg:true},function(d){
                   _this.props.history.goBack();
          });
          return false;
      });
      // console.log('收到表单值：', this.props.form.getFieldsValue());
  },
  handleInputChange(e) {
      if(e.target.type=='file')
      {
          var fr = new FileReader();
          fr.readAsDataURL(e.target.files[0]);
          fr.onload = function(e) {
             $('#videoThumbnailImg').attr('src',e.target.result);
          };
      }
  },
  render() {
    var _this = this;
    const { getFieldProps,getFieldError } = this.props.form;
    const titleProps = getFieldProps('title', {
        validate: [{
          rules: [
            { required: true, message: '标题不能为空'}
          ],
          trigger: 'onBlur',
        }]
    });

    return (
    <div>
       <div className="ant-breadcrumb custom">
          <span>
            <span className="ant-breadcrumb-link"><Link to='series'>系列列表</Link></span>
            <span className="ant-breadcrumb-separator">/</span>
          </span>
          <span>
            <span className="ant-breadcrumb-link">{this.state.title}</span>
          </span>
       </div>
       <div className="series-con">
          <Form horizontal id={'seriesForm'} form={this.props.form} method="post" action={Const.API.series}>
              <FormItem
                label="标题：" labelCol={{ span: 3 }} wrapperCol={{ span: 14 }}>
                <Input placeholder="" {...titleProps}  name={'series[title]'} />
              </FormItem>
              <FormItem
                label="缩略图：" labelCol={{ span: 3 }} wrapperCol={{ span: 14 }}>
                <img id="videoThumbnailImg" />
                <div><a className="file-upload">
                   选择文件
                   <Input type="file" id="seriesThumbnail" name={'series[thumbnail]'} onChange={this.handleInputChange}/>
                </a></div>
              </FormItem>
              <FormItem
                label="排序：" labelCol={{ span: 3 }} wrapperCol={{ span: 8 }}>
               <select  size="large" style={{width:290,border:'none',height:'30'}} name="series[video_sort]">
                    <option value="asc">升序</option>
                    <option value="desc">降序</option>
                </select>
              </FormItem>
              <FormItem wrapperCol={{ offset: 3 }}>
                <Button type="primary" onClick={this.handleSubmit.bind(this)}>保存</Button>
              </FormItem>
          </Form>
       </div>
     </div>
    );
  }
});

seriesCreateView = Form.create()(seriesCreateView);

module.exports = seriesCreateView;
