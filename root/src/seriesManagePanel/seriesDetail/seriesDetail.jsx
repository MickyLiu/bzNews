import { Table, Icon, Spin,Form,Input,InputNumber,Button,Upload } from 'antd';

let React = require('react');
let events = require('./../../_utils/eventSubscriber');
let ajaxEx = require('./../../_utils/ajaxConfiger');
let SeriesManageStore = require('../store/seriesManageStore');
let SeriesVideoBatch = require('../seriesVideoBatch/seriesVideoBatch');
let SeriesVideoAdd = require('../seriesVideoAdd/seriesVideoAdd');
let SeriesVideosTable = require('../seriesVideos/seriesVideos');
let Const = require('./../../_utils/const');
let Link = require('./../../components/link/link');
let Preloader = require('./../../components/imagePreloader/imagePreloader');
require('./seriesDetail.scss');


const FormItem = Form.Item;
const seriesDetailView = React.createClass({
  getInitialState() {
     if(SeriesManageStore.getDataSource().length)
     {
        var itemData = SeriesManageStore.getItemByProperty('id',this.props.params.id);
        return this.getShowData(itemData[0]);
     }
     else
     {
        return  {
           data:{title:'',id:this.props.params.id},
           dataSource:[],
           columns:[]
        };
     }
  },
  getShowData(dataIn){
      var columns = [{
        title: '',
        dataIndex: 'd1'
      }, {
        title: '',
        dataIndex: 'd2',
        render(text,record) {
          if('缩略图'==record.d1)
          {
             return (<Preloader className="thumbnail" url={text} />);
          }
          else
          {
             return text;
          }
        }
      }];

      var dataSource =  [];
      var mapping = SeriesManageStore.getItemOnMappingKey(dataIn);
      for(var i in mapping)
      {
         if(i=='创建时间' || i=='系列描述' || i=='系列ID' || i=='视频数目' || i=='更新时间' || i=='播放量')
         {}
         else{
            dataSource.push({
              d1: i,
              d2: mapping[i]
            });
         }
      }
      return {
          data:dataIn,
          dataSource:dataSource,
          columns:columns
      };
  },
  fetchSeries(sid){
       var _this=this;
       var seriesId = sid? sid:this.props.params.id;
       if(!SeriesManageStore.getDataSource().length)
       {
             ajaxEx.get({url:Const.API.series+'/'+ seriesId},function(d){
                   var  mappingData= SeriesManageStore.mapping([d.data]);
                  _this.setState(_this.getShowData(mappingData[0]));
             });
       }
       else
       {
           var itemData = SeriesManageStore.getItemByProperty('id',seriesId);
           _this.setState(_this.getShowData(itemData[0]));
       }
  },
  componentDidMount() {
        this.fetchSeries();
  },
  componentWillReceiveProps(nextProps){
        if(this.props.params.id==nextProps.params.id) return;
        this.fetchSeries(nextProps.params.id);
  },
  componentWillUnmount: function(){
     //events.unsubscribe('progressChange');
  },
  render() {
    return (
    <div>
       <div className="ant-breadcrumb custom">
          <span>
            <span className="ant-breadcrumb-link"><Link to='series'>系列列表</Link></span>
            <span className="ant-breadcrumb-separator">/</span>
          </span>
          <span>
            <span className="ant-breadcrumb-link">{this.state.data.title}</span>
          </span>
       </div>
       <div className="series-con">
          <Table columns={this.state.columns}  dataSource={this.state.dataSource} size="middle" pagination={false} showHeader={false}/>
          <div className="series-option">
                 <Link to={'series/'+this.state.data.id +'/edit'}><Button type="primary" className="series-edit-btn">修改</Button></Link>
          </div>
       </div>

       <div className="video-con series-video">
             <div className="series-option">
               <h3 className="series-video-title">下属视频</h3>
               <SeriesVideoAdd seriesId={this.state.data.id} />
               <SeriesVideoBatch seriesId={this.state.data.id} />
             </div>  
             <SeriesVideosTable seriesId={this.state.data.id} />
       </div>
     </div>
    );
  }
});

module.exports = seriesDetailView;
