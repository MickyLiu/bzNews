let Link = require('./../../components/link/link');
import { Table, Icon, Spin,Form,Input,Button,Upload, Tabs } from 'antd';

let React = require('react');
let events = require('./../../_utils/eventSubscriber');
let ajaxEx = require('./../../_utils/ajaxConfiger');
let Const = require('./../../_utils/const');
let $ = require('jquery');
let SeriesManageStore = require('../store/seriesManageStore');
require('./seriesEdit.scss');

const FormItem = Form.Item;
const TabPane = Tabs.TabPane;

let seriesEditView = React.createClass({
  getInitialState() {
      if(SeriesManageStore.getDataSource().length)
      {
          var itemData = SeriesManageStore.getItemByProperty('id',this.props.routeParams.id);
          return {
              data:itemData[0],
              title:itemData[0].title
          };
      }
      else
      {
          return { data:{id:this.props.params.id}};
      }
  },
  fetchSeries(sid){
       var _this=this;
       var seriesId = sid? sid:this.props.routeParams.id;
       if(!SeriesManageStore.getDataSource().length)
       {
             ajaxEx.get({url:Const.API.series+'/'+ seriesId},function(d){
                    var  mappingData= SeriesManageStore.mapping([d.data]);
                    _this.setState({data:mappingData[0],title:mappingData[0].title});
             });
       }
       else
       {
           var itemData = SeriesManageStore.getItemByProperty('id',seriesId);
           _this.setState({data:itemData[0],title:itemData[0].title});
       }
  },
  componentDidMount: function () {
       this.fetchSeries();
  },
  componentWillReceiveProps(nextProps){
        if(this.props.params.id==nextProps.params.id) return;
        this.fetchSeries(nextProps.params.id);
  },
  componentWillUnmount: function(){
     //events.unsubscribe('progressChange');
  },
  handleSubmit(e) {
       e.preventDefault();
       var _this= this;
       
       this.props.form.validateFields((errors, values) => {
                if (!!errors) {
                  console.log('Errors in form!!!');
                  e.preventDefault();
                  return;
                }

          ajaxEx.submit({url:Const.API.series+'/'+ _this.state.data.id,formEle:$('#seriesForm')[0],enabledLoadingMsg:true,enabledErrorMsg:true},function(d){
                   SeriesManageStore.clearData();
                   window.history.back();
          });
          return false;
      });
  },
  handleInputChange(e) {
      var data = this.state.data;
      data[e.target.attributes['id'].value]= e.target.value;
      this.setState({
          data: data
      });

      if(e.target.type=='file')
      {
          var fr = new FileReader();
          fr.readAsDataURL(e.target.files[0]);
          fr.onload = function(e) {
             $('#seriesThumbnailImg').attr('src',e.target.result);
          };
      }
  },
  render() {
    var _this = this;
    const { getFieldProps,getFieldError } = this.props.form;
    const titleProps = getFieldProps('title', {
        validate: [{
          rules: [
            { required: true, message: '标题不能为空'}
          ],
          trigger: 'onBlur',
        }],
        initialValue: _this.state.data.title
    });
    return (
    <div>
       <div className="ant-breadcrumb custom">
          <span>
            <span className="ant-breadcrumb-link"><Link to='series'>系列列表</Link></span>
            <span className="ant-breadcrumb-separator">/</span>
          </span>
          <span>
            <span className="ant-breadcrumb-link">{this.state.title}</span>
          </span>
       </div>
       <div className="series-con">
          <Form horizontal id={'seriesForm'} form={this.props.form} method="put">
              <Input type="hidden" name={'series_id'} value={this.state.data.id} name={'series[id]'}/>
              <FormItem
                label="标题：" labelCol={{ span: 3 }} wrapperCol={{ span: 14 }}>
                <Input placeholder="" {...titleProps} value={this.state.data.title} name={'series[title]'} onChange={this.handleInputChange} />
              </FormItem>
              <FormItem
                label="缩略图：" labelCol={{ span: 3 }} wrapperCol={{ span: 14 }}>
                <img id="seriesThumbnailImg" src={this.state.data.thumbnail} />
                 <div><a className="file-upload">
                   选择文件
                   <Input type="file" id="seriesThumbnail" name={'series[thumbnail]'} onChange={this.handleInputChange}/>
                 </a></div>
              </FormItem>
              <FormItem
                label="排序：" labelCol={{ span: 3 }} wrapperCol={{ span: 8 }}>
                <select style={{width:290,border:'none',height:'30'}} name="series[video_sort]"  {...getFieldProps('video_sort')} onChange={this.handleInputChange} value={(this.state.data.video_sort=='降序' || this.state.data.video_sort=='desc')?'desc':'asc'}>
                    <option value="asc">升序</option>
                    <option value="desc">降序</option>
                </select>
              </FormItem>
              <FormItem wrapperCol={{ offset: 3 }}>
                <Button type="primary" className="series-edit-submit" onClick={this.handleSubmit}>保存</Button> 
              </FormItem>
          </Form>
       </div>
     </div>
    );
  }
});

seriesEditView = Form.create()(seriesEditView);

module.exports = seriesEditView;
