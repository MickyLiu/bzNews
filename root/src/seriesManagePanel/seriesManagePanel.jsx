let React = require('react');
let SeriesManageStore = require('./store/seriesManageStore');
let events = require('./../_utils/eventSubscriber');

const SeriesManagePanel = React.createClass({
  componentDidMount: function () {

  },
  render() {
    return (
        <div>
             {this.props.children}
        </div>
    );
  }
});

module.exports = SeriesManagePanel;
