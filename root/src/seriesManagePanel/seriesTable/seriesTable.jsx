import { Table, Icon, Spin, Popconfirm } from 'antd';

let React = require('react');
let events = require('./../../_utils/eventSubscriber');
let ajaxEx = require('./../../_utils/ajaxConfiger');
let Link = require('./../../components/link/link');
let Preloader = require('./../../components/imagePreloader/imagePreloader');
let Const = require('./../../_utils/const');
let SeriesManageStore = require('../store/seriesManageStore');
let PanelTopBar = require('../panelTopBar/panelTopBar');

const columns = [{
    title: 'ID',
    dataIndex: 'id',
    key: 'id'
  },{
    title: '标题',
    dataIndex: 'title',
    key: 'title',
    render(text, record) {
      var showLink = "series/" + record.id + "/show";
      return (
          <Link to={showLink}>{text}</Link>
      );
    }
  }, {
    title: '缩略图',
    key: 'thumbnail',
    dataIndex: 'thumbnail',
    render(text) {
      return (<Preloader className="thumbnail" url={text} />);
    }
  }, {
    title: '视频数目',
    dataIndex: 'videos_count',
    key: 'videos_count',
  },{
    title: '更新时间',
    dataIndex: 'published_at',
    key: 'published_at',
  },{
    title: '播放量',
    dataIndex: 'play_count',
    key: 'play_count',
  },{
    title: '系列视频排序',
    dataIndex: 'video_sort',
    key: 'video_sort',
    render(text,record){
        let videoSort = text=='升序'?'asc':'desc';
        let handleSortChange = (e)=> {
             let sort = e.target.value =='asc'?"升序":"降序";
             ajaxEx.put({url:Const.API.series+'/'+ record.id,form:{"series":{"video_sort":e.target.value,"id":record.id}},enabledErrorMsg:true,enabledSuccessMsg:true},function(d){
                   SeriesManageStore.updateData({id:record.id,"video_sort":sort});
                   events.publish('seriesDataChange',{reloadFromlocal:true});
             });
        }
        return (
             <select onChange={handleSortChange} value={videoSort} style={{width:80,border:"none"}}>
                    <option value="asc">升序</option>
                    <option value="desc">降序</option>
             </select>
        );
    }
  },{
    title: '操作',
    key: 'operation',
    render(text, record) {
      var showLink = "series/" + record.id + "/show";
      return (
        <span>
          <Link to={showLink}>编辑</Link>
          <span className="ant-divider"></span>
          <Popconfirm title="确定要删除这个系列么？" seriesId={record.id} onConfirm={delSeriesConfirm}>
            <a href="#">删除</a>
          </Popconfirm>
        </span>
      );
    }
  }];
  

var delSeriesConfirm =  function(){
      var _this =this;
      ajaxEx.delete({url:Const.API.series+'/'+ _this.props.seriesId,enabledLoadingMsg:true,enabledErrorMsg:true},function(d){
               events.publish('seriesDataChange');
      });
}

const TableView = React.createClass({
  getInitialState() {
     return{
        dataSource :[],
        pageSize:8,
        total:1,
        loading: true,
        currentPage:1
    };
  },
  componentDidMount: function () {
       events.subscribe('seriesDataChange',(function(filter){
            this.getDataSource(this,filter);
       }).bind(this));

       this.setState({loading:true});
       this.getDataSource(this);
  },
  getDataSource(_this,filter){
      var page = 1;
      if(filter && !filter.reloadFromlocal)
      {
         page = filter.page?filter.page:1;

         this.setState({loading: true});
         ajaxEx.get({url:Const.API.projectSeries+"?page="+ page +"&per="+this.state.pageSize},function(d){
             SeriesManageStore.clearData().pushData(SeriesManageStore.mapping(d.data));
             _this.setState({
                dataSource : SeriesManageStore.getDataSource(),
                total: d.meta.total_pages*_this.state.pageSize,
                currentPage:page,
                loading:false
             });
         }).complete(function(){
             _this.setState({loading:false});
         });
      }
      else if(filter && filter.reloadFromlocal)
      {
          _this.setState({
                dataSource : SeriesManageStore.getDataSource()
          });
      }else{
         this.setState({loading: true});
         ajaxEx.get({url:Const.API.projectSeries+"?page="+ page +"&per="+this.state.pageSize},function(d){
             SeriesManageStore.clearData().pushData(SeriesManageStore.mapping(d.data));
             _this.setState({
                dataSource : SeriesManageStore.getDataSource(),
                total: d.meta.total_pages*_this.state.pageSize,
                currentPage:page,
                loading:false
             });
         }).complete(function(){
             _this.setState({loading:false});
         });
      }
  },
  pageChange:function(current){
        this.getDataSource(this,{"page":current});
  },
  componentWillUnmount: function(){
       events.unsubscribe('seriesDataChange');
  },
  render() {
    return (
      <div>
        <PanelTopBar panelTitle= "系列信息"/>
        <Table columns={columns} dataSource={this.state.dataSource} loading={this.state.loading} pagination={{ onChange:this.pageChange, current:this.state.currentPage, pageSize: this.state.pageSize, total: this.state.total, showQuickJumper: true }} />
      </div>
    );
  }
});

module.exports = TableView;
