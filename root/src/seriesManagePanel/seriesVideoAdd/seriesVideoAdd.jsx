import { Table, Icon, Spin,Form,Input,Button,DatePicker,QueueAnim } from 'antd';

let React = require('react');
let events = require('./../../_utils/eventSubscriber');
let ajaxEx = require('./../../_utils/ajaxConfiger');
let Const = require('./../../_utils/const');
let $ = require('jquery');
let VideoManageStore = require('./../../videoManagePanel/store/videoManageStore');
let SeriesManageStore = require('../store/seriesManageStore');
require('./seriesVideoAdd.scss');


const FormItem = Form.Item;
let seriesVideoAddView = React.createClass({
  getInitialState() {
          return {};
  },
  componentDidMount: function () {
  },
  componentWillUnmount: function(){
  },
  handleSubmit(e) {
       e.preventDefault();
       var _this= this;
       this.props.form.validateFields((errors, values) => {
                if (!!errors) {
                  console.log('Errors in form!!!');
                  e.preventDefault();
                  return;
                }

          ajaxEx.submit({url:Const.API.youkuInfoAdd,formEle:$('#videoForm')[0],enabledLoadingMsg:true,enabledErrorMsg:true},function(d){
                     //VideoManageStore.pushData(VideoManageStore.mapping([d.data]));
                     events.publish('videosDataChange');
                     $('#videoForm').resetForm();
                     $('#videoThumbnailImg').attr('src','');
          });
          return false;
      });
  },
  handleInputChange(e) {
      if(e.target.type=='file')
      {
          var fr = new FileReader();
          fr.readAsDataURL(e.target.files[0]);
          fr.onload = function(e) {
             $('#videoThumbnailImg').attr('src',e.target.result);
          };
      }
  },
  handleDateChange(date){
      var data = this.state.data;
      data['published_at']= new Date(date).toISOString();
      this.setState({
          data: data
      });
      $('#published_at').val(data['published_at']);
  },
  render() {
    var _this = this;
    const { getFieldProps,getFieldError } = this.props.form;

    const youkuVidProps = getFieldProps('youku_vid', {
        validate: [{
          rules: [
            { required: true, message: '视频id不能为空'}
          ],
          trigger: 'onBlur',
        }]
    });

    return (
    <div className="series-video-add-con">
          <Form horizontal id={'videoForm'} className="series-video-add-form" form={this.props.form} method="post" action={Const.API.youkuInfoAdd}>
                  <Input type="hidden" name={'series_id'} value={this.props.seriesId}/>
                  <Input placeholder="视频id" name={'youku_vid'} {...youkuVidProps} />
                  <Button type="primary" onClick={this.handleSubmit}>单集导入</Button>
          </Form>
     </div>
    );
  }
});

seriesVideoAddView = Form.create()(seriesVideoAddView);

module.exports = seriesVideoAddView;
