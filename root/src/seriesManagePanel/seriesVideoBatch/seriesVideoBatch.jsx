import { Icon,Form,Input,Button,message } from 'antd';

let React = require('react');
let events = require('./../../_utils/eventSubscriber');
let ajaxEx = require('./../../_utils/ajaxConfiger');
let Const = require('./../../_utils/const');
let $ = require('jquery');

const FormItem = Form.Item;
let seriesVideoBatchView = React.createClass({
  getInitialState() {
          return {};
  },
  componentDidMount: function () {
  },
  componentWillUnmount: function(){
  },
  handleSubmit(e) {
       e.preventDefault();
       var _this= this;
       this.props.form.validateFields((errors, values) => {
                if (!!errors) {
                  console.log('Errors in form!!!');
                  e.preventDefault();
                  return;
                }

          ajaxEx.submit({url:Const.API.videosBatch,formEle:$('#videoBatchForm')[0],enabledLoadingMsg:true,enabledErrorMsg:true},function(d){
                     events.publish('videosDataChange');
                     $('#videoBatchForm').resetForm();
                     message.success('播单添加成功！',1);
          });
          return false;
      });
  },
  render() {
    var _this = this;
    const { getFieldProps,getFieldError } = this.props.form;

    const playlistProps = getFieldProps('playlist', {
        validate: [{
          rules: [
            { required: true, message: '播单ID不能为空'}
          ],
          trigger: 'onBlur',
        }]
    });
    return (
    <div className="series-video-add-con">
          <Form horizontal id={'videoBatchForm'} className="series-video-add-form" form={this.props.form} method="post" action={Const.API.videosBatch}>
                  <Input type="hidden" name={'series_id'} value={this.props.seriesId}/>
                  <Input placeholder="播单id" {...playlistProps} name={'playlist'}  />
                  <Button type="primary" onClick={this.handleSubmit}>批量导入</Button>
          </Form>
     </div>
    );
  }
});

seriesVideoBatchView = Form.create()(seriesVideoBatchView);

module.exports = seriesVideoBatchView;
