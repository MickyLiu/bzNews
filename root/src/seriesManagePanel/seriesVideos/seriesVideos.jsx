import { Table, Icon, Spin, Popconfirm } from 'antd';

let React = require('react');

let events = require('./../../_utils/eventSubscriber');
let Link = require('./../../components/link/link');
let Preloader = require('./../../components/imagePreloader/imagePreloader');
let ajaxEx = require('./../../_utils/ajaxConfiger');
let Const = require('./../../_utils/const');

let VideoManageStore = require('./../../videoManagePanel/store/videoManageStore');

const columns = [{
    title: 'ID',
    dataIndex: 'id',
    key: 'id'
  },{
    title: '标题',
    dataIndex: 'title',
    key: 'title',
  }, {
    title: '缩略图',
    key: 'thumbnail',
    dataIndex: 'thumbnail',
    render(text) {
      return (<Preloader className="thumbnail" url={text} />);
    }
  }, {
    title: '时长',
    dataIndex: 'duration',
    key: 'duration',
  }, {
    title: '发布时间',
    dataIndex: 'published_at',
    key: 'published_at',
  }, {
    title: '播放量',
    dataIndex: 'play_count',
    key: 'play_count',
  }, {
    title: '弹幕数',
    dataIndex: 'danmaku_count',
    key: 'danmaku_count',
  }, {
    title: '评论数',
    dataIndex: 'comments_count',
    key: 'comments_count',
    render(text, record) {
      var commentLink = "videos/" + record.id +"/comments";
      return (
        <Link to={commentLink}>{text}</Link>
      );
    }
  }, {
    title: '集数',
    dataIndex: 'episode',
    key: 'episode',
  }, {
    title: '操作',
    key: 'operation',
    render(text, record) {
      var showLink = "videos/" + record.id;
      var editLink = "videos/" + record.id +"/edit";
      return (
        <span>
          <Link to={editLink}>编辑</Link>
          <span className="ant-divider"></span>
         <Popconfirm title="确定要删除这个视频么？" videoId={record.id} onConfirm={delVideoConfirm}>
            <a href="#">删除</a>
          </Popconfirm>
        </span>
      );
    }
  }];

var delVideoConfirm =  function(){
      //VideoManageStore.delItemByProperty('id',this.props.videoId);
      var _this =this;
      ajaxEx.delete({url:Const.API.videos+'/'+ _this.props.videoId,enabledLoadingMsg:true,enabledErrorMsg:true},function(d){
             events.publish('videosDataChange');
             //window.location.reload();
      });
}

const TableView = React.createClass({
  getInitialState() {
     return{
        dataSource :[],
        pageSize:10,
        total:1,
        loading: true,
        currentPage:1
    };
  },
  componentDidMount: function () {
       events.subscribe('videosDataChange',(function(filter){
             this.getDataSource(this,filter);
       }).bind(this));
       var filter = VideoManageStore.getFilterRule();
       if(filter && filter.mountUseFlag)
       {
          this.getDataSource(this,filter);
          VideoManageStore.setFilterRule(null);
       }
       else
       {
          this.getDataSource(this);
       }
  },
  getDataSource(_this,filter){
      _this.setState({loading:true});
      var page = 1;
      if(filter)
      {
         page = filter.page?filter.page:1;
      }
      ajaxEx.get({url:Const.API.projectVideos + "?series_id=" + _this.props.seriesId +"&page="+ page +"&per="+_this.state.pageSize},function(d){
           VideoManageStore.clearData().pushData(VideoManageStore.mapping(d.data));
           _this.setState({
              dataSource : VideoManageStore.getDataSource(),
              total: d.meta.total_pages*_this.state.pageSize,
              currentPage:page,
              loading:false
           });
       }).complete(function(){
             _this.setState({loading:false});
       });
  },
  pageChange:function(current){
        var _this = this;

        _this.setState({
            loading: true
        });
       VideoManageStore.setFilterRule({"page":current});
       this.getDataSource(this,{"page":current});
  },
  componentWillUnmount: function(){
       VideoManageStore.clearData();
       events.unsubscribe('videosDataChange');
  },
  render() {
    return (
        <Table columns={columns} dataSource={this.state.dataSource} loading={this.state.loading} pagination={{ onChange:this.pageChange, current:this.state.currentPage, pageSize: this.state.pageSize, total: this.state.total, showQuickJumper: true }} />
    );
  }
});

module.exports = TableView;
