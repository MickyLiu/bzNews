let $ = require('jquery');

var SeriesManageStore = {

   clearData:function(){
      this.dataSource=[];
      return this;
   },
	 pushData:function(data) {
      if(!this.dataSource) this.dataSource=[];
	 	  this.dataSource = this.dataSource.concat(data);
      return this;
	 },
   updateData:function(data){
         if(data){
             this.dataSource.forEach(function(o,i){
                  if(o.id==data.id)
                  {
                      o = $.extend(o,data);
                  }
             });
         }
   },
   mapping:function(data){
        var result=[];
        data.forEach(function(o,i){
               $.isNumeric(o.id)?o.attributes.id = o.id.toString():'';
               $.isNumeric(o.id)?o.attributes.key =  o.id.toString():'';
               o.attributes.video_sort = o.attributes.video_sort!=null?(o.attributes.video_sort=='asc'?'升序':'降序'):'';
               o.attributes.published_at = o.attributes.published_at?new Date(o.attributes.published_at).toLocaleString():'';
               result.push(o.attributes);
        });
        return result;
   },
	 getDataSource:function() {
	 	  return !this.dataSource?[]:this.dataSource;
	 },
   getItemByProperty:function(key,value){
        return this.dataSource.filter(function(item){
               return item[key]==value;
        });
   },
   getMappingKeyData:function(){
         return {
         	   "id":"系列ID",
         	   "title":"标题",
         	   "thumbnail":"缩略图",
             "description":"系列描述",
             "videos_count":"视频数目",
             "created_at":"创建时间",
             "updated_at":"更新时间",
             "play_count":"播放量",
             "video_sort":"系列视频排序"
         }
   },
   getItemOnMappingKey:function(item){
   	  var result = {};
   	  var mapping = this.getMappingKeyData();
   	  for(var key in mapping)
   	  {
   	  	  result[mapping[key]] = item[key];
   	  }
        return result;
   }
};

module.exports = SeriesManageStore;
