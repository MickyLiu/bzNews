let Const = require('./../_utils/const');
let events = require('./../_utils/eventSubscriber');

let ajax = {
  getInfo(callback) {
    return auth.ajax(Const.API.team).done(callback);
  },
  getMembers(callback) {
    return auth.ajax(Const.API.customers + "?team_id=" + localStorage.getItem("_teamId_")).done(callback);
  },
  getAbility(callback) {
    return auth.ajax(Const.API.customersRoles).done((data) => {
      localStorage.setItem('ability', JSON.stringify(data.ability));
      let ability = data.ability.filter((v) => v.team_id == localStorage.getItem('_teamId_'));
      if (ability.length > 0) {
        globalVars.set('_currentRole_', ability[0].role);
      } else {
        globalVars.set('_currentRole_', 'member');
      }
      events.publish('renderTopBar');
      if (callback) {
        callback(data);
      }
    })
  },
  transfer(data, callback) {
    return auth.ajax({
      method: 'POST',
      url: Const.API.team + '/transfer',
      data: data,
    }).done(callback)
  },
  delete(data, callback) {
    return auth.ajax({
      method: 'DELETE',
      url: Const.API.team,
      data: data,
    }).done(callback)
  }
}

module.exports = ajax;