let React = require('react');
let ReactDom = require('react-dom');
let Const = require('./../_utils/const');
let events = require('./../_utils/eventSubscriber');

const NameChanger = React.createClass({
  input: null,
  getDefaultProps() {
    return {
      defaultValue: '',
      onChange: () => {},
      changeButtonText: '修改团队名称'
    }
  },
  getInitialState() {
    return {
      value: this.props.defaultValue,
      tempValue: this.props.defaultValue,
      hideInput: true
    }
  },
  // event
  handleShowInput() {
    this.setState({
      tempValue: this.state.value,
      hideInput: false,
      disabled: false,
    }, () => {
      ReactDom.findDOMNode(this.input).focus();
    });
  },
  handleInputChange(e) {
    this.setState({
      tempValue: e.target.value
    });
  },
  handleInputOK() {
    this.setState({
      disabled: true
    })
    auth.ajax({
      method: 'PUT',
      url: Const.API.team,
      data: {
        team: {
          name: this.state.tempValue
        }
      }
    }).done(() => {
      // localStorage
      localStorage.setItem('_teamName_', this.state.tempValue);
      globalVars.set('_teamName_', this.state.tempValue);
      events.publish('renderTopBar');
      this.setState({
        value: this.state.tempValue,
        disabled: false,
        hideInput: true
      });
    });
  },
  handleInputCancel() {
    this.setState({
      hideInput: true
    });
  },
  // render
  render() {
    return (
      <div className="input-label">
      {
        this.state.hideInput ? 
        <div className="label">
          <span className="label-value">{this.state.value}</span>
          <button className="bt showinput" onClick={this.handleShowInput}>{this.props.changeButtonText}</button>
        </div>
        :
        <div className="input">
          <input className={"input-value" + (this.state.disabled ? " disabled" : "")} type="text" value={this.state.tempValue} onChange={this.handleInputChange} ref={(c) => this.input = c } disabled={this.state.disabled}/>
          <button className={"bt ok" + (this.state.disabled ? " disabled" : "")} onClick={this.handleInputOK} disabled={this.state.disabled}>保存</button>
          <button className="bt cancel" onClick={this.handleInputCancel}>取消</button>
        </div>
      }
      </div>
    )
  }
});

module.exports = NameChanger;
