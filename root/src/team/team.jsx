let React = require('react');
let ReactDom = require('react-dom');
let events = require('./../_utils/eventSubscriber');

let NameChanger = require('./nameChanger');
let TeamTransfer = require('./teamTransfer');
let TeamDelete = require('./teamDelete');

let ajax = require('./ajax');

require('./team.scss');

const Team = React.createClass({
  getInitialState() {
   return{
      verified: false,
      allowed: false,
      teamName: globalVars.get('_teamName_'),
      teamBirth: 'XXXX年XX月XX日'
    };
  },
  componentDidMount() {
    // get data
    ajax.getInfo((data) => {
      let birth = data.data.attributes.created_at;
      // verify
      let tid = globalVars.get('_teamId_');
      let ability = JSON.parse(localStorage.getItem('ability'));
      let tability = ability.find((e) => e.team_id == tid);
      if (tability) {
        this.setState({
          verified: true,
          allowed: tability.role == 'super',
          teamBirth: `${birth.slice(0, 4)}年${birth.slice(5, 7)}月${birth.slice(8, 10)}日`
        });
      } else {
        ajax.getAbility(() => {
          tid = globalVars.get('_teamId_');
          ability = JSON.parse(localStorage.getItem('ability'));
          tability = ability.find((e) => e.team_id == tid);
          this.setState({
            verified: true,
            allowed: tability && tability.role == 'super',
            teamBirth: `${birth.slice(0, 4)}年${birth.slice(5, 7)}月${birth.slice(8, 10)}日`
          });
        })
      }
    })
  },

  // render
  notVerify() {
    return (
      <div className="team-page">验证中</div>
    )
  },
  notAllow() {
    return (
      <div className="team-page">你没有权限查看本页面</div>
    )
  },
  render() {
    if (!this.state.verified) {
      return this.notVerify();
    }
    if (this.state.allowed) {
      return (
        <div className="team-page">
          <div className="team-opt name-change">
            <NameChanger defaultValue={this.state.teamName}/>
            <div className="team-birth">团队创建于{this.state.teamBirth}</div>
          </div>
          <div className="team-opt team-transfer">
            <TeamTransfer />
          </div>
          <div className="team-opt team-delete">
            <TeamDelete name={this.state.teamName} />
          </div>
        </div>
      )
    } else {
      return this.notAllow();
    }
  }
});

module.exports = Team;
