import { Select, Modal, Icon } from 'antd';

let React = require('react');
let ReactDom = require('react-dom');

let ajax = require('./ajax');

const TeamDelete = React.createClass({
  getDefaultProps() {
    return {
    }
  },
  getInitialState() {
    return {
      disabled: false,
      confirm: false,
      password: '',
      error: '',
    }
  },
  componentDidMount() {
  },
  
  // event
  handleDelete() {
    this.setState({
      confirm: true
    });
  },
  handleConfirmOk() {
    ajax.delete({
      password: this.state.password
    }, () => {
      ajax.getAbility(() => {
        window.location = "/teams"
      });
    }).fail(() => {
      this.setState({
        error: '密码错误或者服务器问题'
      });
    });
  },
  handleConfirmCancel() {
    this.setState({
      confirm: false
    })
  },
  handlePasswordChange(e) {
    this.setState({
      password: e.target.value
    });
  },
  // render
  render() {
    return (
      <div>
        <div className="team-opt-title-contianer">
          <span className="team-opt-title">删除团队</span>
        </div>
        <div>
          <span>如果你和你的团队成员,从今往后都不再需要访问改团队的信息,可以删除团队账户。</span>
          <button className="bt delete" onClick={this.handleDelete}>删除当前团队</button>
          <Modal
            visible={this.state.confirm}
            onOk={this.handleConfirmOk}
            onCancel={this.handleConfirmCancel}
            width={540}
          >
            <div className="team-delete-modal">
              <div className="title">{`删除团队: ${this.props.name}`}</div>
              <div className="message">
                <div>
                  <span className="emphasize">重要提示：</span>删除团队后，所有内容都会被立即删除,不可恢复。
                </div>
                <div className="confirm-password"><input type="password" value={this.state.password} placeholder="请输入你的登陆密码" onChange={this.handlePasswordChange}/></div>
                <div className="emphasize">{this.state.error}</div>
              </div>
              <div className="sign emphasize"><Icon type="exclamation-circle" /></div>
            </div>
          </Modal>
        </div>
      </div>
    )
  }
});

module.exports = TeamDelete;
