import { Select, Modal } from 'antd';

let React = require('react');
let ReactDom = require('react-dom');

let ajax = require('./ajax');

const TeamTransfer = React.createClass({
  getDefaultProps() {
    return {
    }
  },
  getInitialState() {
    return {
      disabled: false,
      select: null,
      members: [],
      confirm: false,
    }
  },
  componentDidMount() {
    ajax.getMembers((data) => {
      data = data.data;
      data = data.filter((v) => v.id != localStorage.getItem('currentUserId'));
      this.setState({
        members: data
      });
    });
  },
  
  // event
  handleTransfer() {
    this.setState({
      disabled: true
    });
    let d = this.state.members.filter((v) => v.id == this.state.select);
    if (d.length > 0) {
      this.setState({
        confirm: true
      })
    } else {
      this.setState({
        disabled: false
      })
    }
  },
  handleConfirmOk() {
    ajax.transfer({
        "customer_id": this.state.select
      }, () => {
      // localStorage
      ajax.getAbility(() => {
        window.location = "/teams"
      });
    });
  },
  handleConfirmCancel() {
    this.setState({
      confirm: false,
      disabled: false
    })
  },
  handleSelectChange(v) {
    this.setState({
      select: v
    });
  },
  // render
  render() {
    return (
      <div>
        <div className="team-opt-title-contianer">
          <span className="team-opt-title">移交团队账户？</span>
          <Select
            style={{ width: 100, float: 'right' }}
            placeholder="选择成员"
            onChange={this.handleSelectChange}
          >
            {this.state.members.map((m, i) => 
              <Option value={m.id} key={i}>{m.attributes.name}</Option>
            )}
          </Select>
          <Modal visible={this.state.confirm}
            onOk={this.handleConfirmOk} onCancel={this.handleConfirmCancel}>
            <p>移交后，你将不能再访问团队账户页面,但仍有其他管理权限。你确定要移交么？</p>
          </Modal>
        </div>
        <div>
          <span>只有超级管理员才可以管理团队账户,你可以把超级管理员身份移交给其他成员,移交之后你将不能再访问团队账户。</span>
          <button className="bt transfer" onClick={this.handleTransfer}>移交给TA</button>
        </div>
      </div>
    )
  }
});

module.exports = TeamTransfer;
