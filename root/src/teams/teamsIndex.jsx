import { Router, hashHistory, Link } from 'react-router';
import {render} from 'react-dom';

let Const = require('../_utils/const');
let React = require('react');
let routes = require('./../../routes/routes');
let Auth = require('../_utils/auth');
let events = require('../_utils/eventSubscriber');
let ajaxEx = require('../_utils/ajaxConfiger');
let $ = require('jquery');
let TopBar = require('../components/topBar/topBar');
let Footer = require('../components/footer/footer');

let _ = require('lodash');
require('./teamsIndex.scss');
require('./../index/index.scss');
require('./../index/components_common.scss');
import { Spin,Modal,Form,Input } from 'antd';


const FormItem = Form.Item;
window.auth = new Auth({
  page: 'teams',
  target: 'home',
  expect: true
});

const Teams = React.createClass({
  getInitialState() {
   return{
      data:[],
      loading:false,
      listType:0,
      createTeamVisible:false,
      createErrorMsg:'',
      validateStatus:'',
      teamName:'',
      confirmLoading:false
    };
  },
  componentDidMount: function () {
        this.fetchTeams();
  },
  fetchTeams(){
      var _this = this;
      _this.setState({loading:true});
      ajaxEx.get({url:Const.API.urlPrefix + 'teams'},function(d){
           _this.setState({
              data:d.data
           });
      }).complete(function(){
            _this.setState({loading:false});
      });
  },
  showCreateTeam(){
       this.setState({
        createTeamVisible: true,
        createErrorMsg:'',
        validateStatus:'',
        teamName:''
      });
  },
  handleOk(){
    var _this = this;
    if(_this.state.teamName.trim())
    {
        this.setState({
          confirmLoading: true,
          createErrorMsg:'',
          validateStatus:''
        });

        ajaxEx.post({url:Const.API.urlPrefix + 'teams',form:{team:{name:_this.state.teamName.trim()}}},function(d){
               _this.setState({
                createTeamVisible: false,
                confirmLoading: false
              });
              _this.fetchTeams();
        },function(){
              _this.setState({
                confirmLoading: false,
                createErrorMsg:'error',
                validateStatus:'error',
             });
        });
    }
    else{
        _this.setState({
            confirmLoading: false,
            createErrorMsg:'请填写团队名称！',
            validateStatus:'error'
        });
    }
  },
  handleCancel(){
      this.setState({
        createTeamVisible: false
      });
  },
  handleInputChange(e) {
      this.setState({
          teamName: e.target.value
      });
  },
  changeTeam(e){
      let teamId = $(e.currentTarget).attr('name');
      let ability = JSON.parse(localStorage.getItem('ability'));
      for(let i=0; i< ability.length; i++) {
        if(ability[i].team_id == teamId) {
          globalVars.set('_currentRole_',ability[i].role);
          break;
        }
      }

      globalVars.set('_teamId_',teamId);
      globalVars.set('_teamName_',$(e.currentTarget).text());
  },
  render() {
     var text = (<span>test</span>);

    return (
         <div className="team-container">
             <TopBar navFull={false} style={''} full={true} Auth={window.auth}/>
             <Spin spining={this.state.loading}>
                <div style={{minHeight:400}}>
                  <div className="team-list">
                      {this.state.listType?
                        <div>
                        {_.map(this.state.data,(o,index)=>
                            <a className="team-item-grid" href="/" onClick={this.changeTeam} name={o.id}>
                              <img src={o.attributes.image} />
                               <p>{o.attributes.name}</p>
                            </a>
                         )}
                        </div>:
                        <div>
                        {_.map(this.state.data,(o,index)=>
                            <p>
                              <a className="team-item-list" href="/" onClick={this.changeTeam} name={o.id}>
                                 {o.attributes.name}
                              </a>
                            </p>
                         )}
                          <p className="team-add-btn"><a className="team-item-list" onClick={this.showCreateTeam}><span className="team-add-icon">+</span>新的团队</a></p>
                        </div>}
                  </div>
                </div>
             </Spin>
             <Modal title="新建团队"
              visible={this.state.createTeamVisible}
              onOk={this.handleOk}
              confirmLoading={this.state.confirmLoading}
              onCancel={this.handleCancel}
              okText="创建团队">
                  <FormItem
                    label="新团队名称：" labelCol={{ span: 4 }} wrapperCol={{ span: 14 }} help={this.state.createErrorMsg} validateStatus={this.state.validateStatus}>
                    <Input placeholder="" value={this.state.teamName} onChange={this.handleInputChange}/>
                  </FormItem>
                  <div style={{clear:'both'}}></div>
             </Modal>
             <Footer />
          </div>
    );
  }
});

auth.verify(function(data) {
    localStorage.setItem("currentUserId",data.current_customer);
    render(<Teams />, document.getElementById("container"));
});
