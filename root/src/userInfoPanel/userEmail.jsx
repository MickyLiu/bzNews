let React = require('react');
let Const = require('../_utils/const');
let events = require('../_utils/eventSubscriber');
let ajaxEx = require('../_utils/ajaxConfiger');
let $ = require('jquery');
import { Button, Form, Input, message,Modal } from 'antd';
const confirm = Modal.confirm;
const FormItem = Form.Item;

function noop() {
  return false;
}

let UserEmailPanel = React.createClass({
   getInitialState() {
	   return{
            loading: false,
            data:{},
            currentUserId:localStorage.getItem('currentUserId')
	    };
   },
   handleSubmit(e) {
	    e.preventDefault();
	    let _this = this;
	    this.props.form.validateFields((errors, values) => {
	        if (!!errors) {
		          console.log('表单中包含错误！');
		          message.error('表单中包含错误！');
		          return;
	        }
	        _this.setState({
	            loading: true
	        });
	        let data = {
	        	   password: values.password,
		           new_password: values.new_email
	        };
	        
	        let url = Const.API.customers + "/" + _this.state.currentUserId + '/update_email';

	        ajaxEx.post({url:url,form:data,enabledErrorMsg:true},function(d){
	                    message.success('用户信息更改成功！', 3);
	                    _this.props.form.resetFields();
	        }).complete(function(){
          	      _this.setState({loading: false});
		    });
	    });
   },
   render() {
	   	var _this = this;
	    const { getFieldProps, getFieldError, isFieldValidating } = this.props.form;
	    const passwdProps = getFieldProps('password', {
	      rules: [
	        { required: true, whitespace: true, min: 6, message: '请输入你的登陆密码' }
	      ],
	    });
	   	const emailProps = getFieldProps('new_email', {
	      validate: [{
	        rules: [
	          { required: true , message: '请输入邮箱地址' }
	        ],
	        trigger: ['onBlur']
	      }, {
	        rules: [
	          { type: 'email', message: '请输入正确的邮箱地址' },
	        ],
	        trigger: ['onBlur', 'onChange'],
	      }]
	    });
   	    return (
           <div className="edit-user-email">
		        <Form horizontal form={this.props.form} method={'put'}> 
		            <FormItem
			          labelCol={{ span: 2 }}
		              wrapperCol={{ span: 7 }}
			          label="输入密码："
			          hasFeedback>
			          <Input {...passwdProps} type="password" autoComplete="off"
			            onContextMenu={noop} onPaste={noop} onCopy={noop} onCut={noop} />
			        </FormItem>

			        <FormItem
			          labelCol={{ span: 2 }}
		              wrapperCol={{ span: 7 }}
			          label="新邮箱："
			          hasFeedback>
			          <Input type="email" {...emailProps} />
			        </FormItem>

			        <FormItem wrapperCol={{ span: 4, offset: 2 }}>
			          <Button type="primary" style={{backgroundColor:'#2fa9be',borderColor:'#2fa9be'}} onClick={this.handleSubmit} loading={this.state.loading}>保存</Button>
			          <a href="javascript:void(0)" onClick={()=>{this.props.switchEditType(0)}} style={{marginLeft:20}}>取消</a>
			        </FormItem>
		        </Form>
	      </div>   
   	    );
   }
});

UserEmailPanel = Form.create()(UserEmailPanel);

module.exports = UserEmailPanel;
