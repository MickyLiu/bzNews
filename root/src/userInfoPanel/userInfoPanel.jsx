let React = require('react');
let CustomerFormEdit = require('./../components/adminForm/adminForm').CustomerFormEdit;
let Const = require('../_utils/const');
let events = require('../_utils/eventSubscriber');
let ajaxEx = require('../_utils/ajaxConfiger');
let $ = require('jquery');
let UserEmail = require('./userEmail');
let UserPassword = require('./userPassword');
require('./userInfoPanel.scss');
import { Button, Form, Input, message,Modal } from 'antd';
const confirm = Modal.confirm;
const FormItem = Form.Item;

function noop() {
  return false;
}

let UserInfoPanel = React.createClass({
  getInitialState() {
	   return{
            loading: false,
            data:{},
            editType:'0',
            currentUserId:localStorage.getItem('currentUserId')
	    };
  },
  componentDidMount() {
       var _this = this; 
       ajaxEx.get({url:Const.API.customers +'/'+ this.state.currentUserId},function(d){
            _this.setState({data:d.data.attributes});
       });
  },
  handleInputChange(e) {
      var data = this.state.data;
      data[e.target.attributes['id'].value]= e.target.value;
      this.setState({
          data: data
      });

      if(e.target.type=='file')
      {
          var fr = new FileReader();
          fr.readAsDataURL(e.target.files[0]);
          fr.onload = function(e) {
             $('.user-avatar-icon').attr('src',e.target.result);
          };
      }
  },
  switchEditType(type){
        this.setState({editType:type});
  },
  handleSubmit(e) {
    e.preventDefault();
    let _this = this;
    this.props.form.validateFields((errors, values) => {
        if (!!errors) {
          console.log('表单中包含错误！');
          message.error('表单中包含错误！');
          return;
        }
        _this.setState({
            loading: true
        });
        let data = {};
        data.customer = values;

        let url = Const.API.customers + "/" + _this.state.currentUserId;

        ajaxEx.submit({url:url,formEle:$('.edit-user-name form')[0],enabledErrorMsg:true},function(d){
                    _this.setState({
                      loading: false
                    });
                    message.success('用户信息更改成功！', 3);
                    _this.props.form.resetFields();
                    events.publish('UiInfoChange',{userName:d.data.attributes.name})

          },function(){
               _this.setState({
                 loading: false
               });
          });

      });
  },
  confirmLeaveTeam(){
  	    let title = (globalVars.get('_currentRole_')=='super'?<span>团队超管若要退出，需要先移交团队<a href="javascript:void(0)" onClick={()=>{window.location="#/team";window.location.reload()}}>移交团队</a></span>:'确定退出这个团队吗？');
        Modal.confirm({
          title: title,
          content: '',
          okText: '确认',
          cancelText: '取消',
          onOk(){
          	 if(globalVars.get('_currentRole_')!='super')
          	 {
                ajaxEx.delete({url: Const.API.team + "/quit",enabledErrorMsg:true}, function(d) {
                     window.location = "/teams";
                });
             }
          }
        });
  },
  render() {
  	var _this = this;
    const { getFieldProps, getFieldError, isFieldValidating } = this.props.form;
    const nameProps = getFieldProps('name', {
      rules: [
        { required: true }
      ],
      initialValue: _this.state.data.name
    });
     const formItemLayout = {
      labelCol: { span: 3 },
      wrapperCol: { span: 7 },
    };
    return (
      <div className="user-info-panel">
        <h2 className="user-info-panel-title">{(this.state.editType==0 || this.state.editType==1)?'账户设置':'更改密码'}{this.state.editType==0?<a href="javascript:void(0)" style={{float:'right',fontSize:'13px',color:'#FA5454'}} onClick={this.confirmLeaveTeam}>退出团队</a>:null}</h2>
	        {this.state.editType==0?<div className="edit-user-name">
			        <Form horizontal form={this.props.form} method={'put'}>
				         <FormItem
				                label={<img className="user-avatar-icon" src={this.state.data.avatar_url} />} {...formItemLayout} className="user-avatar-item"> 
				                <p><a className="file-upload">
				                   选择新头像
				                   <Input type="file" id={'customer_avatar'} name={'customer[avatar]'} onChange={this.handleInputChange}/>
				                </a></p>
				                <p>你可以选择png/jpg图（100*100）作为头像</p>
				        </FormItem>

				        <FormItem
				          labelCol={{ span: 2 }}
			              wrapperCol={{ span: 20 }}
				          label="邮箱："
				          hasFeedback>
					          <p className="ant-form-text"><Input style={{width:"300px"}} type="email" value={this.state.data.email} disabled /></p>
					          <p className="ant-form-text">
						           <a href="javascript:void(0)" onClick={()=>{this.switchEditType(1)}}>修改邮箱</a>
						      </p>
				        </FormItem>

				        <FormItem
				          labelCol={{ span: 2 }}
			              wrapperCol={{ span: 7 }}
				          label="名字："
				          hasFeedback
				          help={isFieldValidating('name') ? '校验中...' : (getFieldError('name') || []).join(', ')}>
				          <Input {...nameProps} name={'customer[name]'} value={this.state.data.name} onChange={this.handleInputChange}/>
				        </FormItem>

				        <FormItem
				          labelCol={{ span: 2 }}
			              wrapperCol={{ span: 7 }}
				          label="密码：">
				           <a href="javascript:void(0)" onClick={()=>{this.switchEditType(2)}}>修改密码</a>
				        </FormItem>

				        <FormItem wrapperCol={{ span: 4, offset: 2 }}>
				           <Button type="primary" style={{backgroundColor:'#2fa9be',borderColor:'#2fa9be'}} onClick={this.handleSubmit} loading={this.state.loading}>保存</Button>
				        </FormItem>
			        </Form>
		      </div>:null}
		    {this.state.editType==1?<UserEmail switchEditType={this.switchEditType}/>:null}
		    {this.state.editType==2?<UserPassword switchEditType={this.switchEditType}/>:null}
    </div>
    );
  }
});
 
UserInfoPanel = Form.create()(UserInfoPanel);

module.exports = UserInfoPanel;
