let React = require('react');
let Const = require('../_utils/const');
let events = require('../_utils/eventSubscriber');
let ajaxEx = require('../_utils/ajaxConfiger');
let $ = require('jquery');
import { Button, Form, Input, message,Modal } from 'antd';
const confirm = Modal.confirm;
const FormItem = Form.Item;

function noop() {
  return false;
}

let UserPasswordPanel = React.createClass({
   getInitialState() {
	   return{
            loading: false,
            data:{},
            currentUserId:localStorage.getItem('currentUserId')
	    };
   },
   checkPass(rule, value, callback) {
	    const { validateFields } = this.props.form;
	    if (value) {
	      let formatValidate = false;
	      formatValidate = /^(?=.*[A-Z])(?=.*[!@#$&*])(?=.*[0-9])(?=.*[a-z]).{6,}/.test(value);
	      if(!formatValidate) {
	        callback('密码长度最少为6位, 至少包含一个数字, 一个大写字母, 一个小写字母、一个符号:! @ # $ & *');
	      }
	      validateFields(['password_confirmation'], { force: true });
	    }
	    callback();
  },
  checkPass2(rule, value, callback) {
	    const { getFieldValue } = this.props.form;
	    if (value && value !== getFieldValue('new_password')) {
	      callback('两次输入密码不一致！');
	    } else {
	      callback();
	    }
   },
   handleSubmit(e) {
    e.preventDefault();
    let _this = this;
    this.props.form.validateFields((errors, values) => {
        if (!!errors) {
          console.log('表单中包含错误！');
          message.error('表单中包含错误！');
          return;
        }
        _this.setState({
            loading: true
        });
        let data = {
        	   password: values.password,
	           new_password: values.new_password,
	           new_password_confirmation: values.password_confirmation
        };
        
        let url = Const.API.customers + "/" + _this.state.currentUserId + '/update_password';

        ajaxEx.post({url:url,form:data,enabledErrorMsg:true},function(d){
                    message.success('用户信息更改成功！', 3);
                    _this.props.form.resetFields();
        }).complete(function(){
          	      _this.setState({loading: false});
		});
    });
  },

   render() {
	   	var _this = this;
	    const { getFieldProps, getFieldError, isFieldValidating } = this.props.form;
   	    const passwdProps = getFieldProps('password', {
	      rules: [
	        { required: true, whitespace: true, min: 6, message: '请填写6位以上密码' }
	      ],
	    });
	    const newPasswdProps = getFieldProps('new_password', {
	      rules: [
	        { required: true, whitespace: true, min: 6, message: '请填写新密码' },
	        { validator: this.checkPass },
	      ],
	    });
	    const rePasswdProps = getFieldProps('password_confirmation', {
	      rules: [{
	        required: true,
	        whitespace: true,
	        message: '请再次输入密码',
	        min: 6
	      }, {
	        validator: this.checkPass2,
	      }],
	    });
	    return (
	       <div className="edit-user-password">
		        <Form horizontal form={this.props.form} method={'put'}> 

		            <FormItem
			          labelCol={{ span: 2 }}
			          wrapperCol={{ span: 7 }}
			          label="原密码："
			          hasFeedback>
			          <Input {...passwdProps} type="password" name={'customer[password]'} autoComplete="off"
			            onContextMenu={noop} onPaste={noop} onCopy={noop} onCut={noop} />
			        </FormItem>

		            <FormItem
			          labelCol={{ span: 2 }}
			          wrapperCol={{ span: 7 }}
			          label="新密码："
			          hasFeedback>
			          <Input {...newPasswdProps} type="password" name={'customer[new_password]'} autoComplete="off"
			            onContextMenu={noop} onPaste={noop} onCopy={noop} onCut={noop} />
			        </FormItem>

			        <FormItem
			          labelCol={{ span: 2 }}
			          wrapperCol={{ span: 7 }}
			          label="确认密码："
			          hasFeedback>
			          <Input {...rePasswdProps} type="password" name={'customer[password_confirmation]'} autoComplete="off" placeholder="两次输入密码保持一致"
			            onContextMenu={noop} onPaste={noop} onCopy={noop} onCut={noop} />
			        </FormItem>

			        <FormItem wrapperCol={{ span: 4, offset: 2 }}>
			            <Button type="primary" style={{backgroundColor:'#2fa9be',borderColor:'#2fa9be'}} onClick={this.handleSubmit} loading={this.state.loading}>保存</Button>
			            <a href="javascript:void(0)" onClick={()=>{this.props.switchEditType(0)}} style={{marginLeft:20}}>取消</a>
			        </FormItem>
		        </Form>
	      </div>   
	    );
   }
});

UserPasswordPanel = Form.create()(UserPasswordPanel);

module.exports = UserPasswordPanel;
