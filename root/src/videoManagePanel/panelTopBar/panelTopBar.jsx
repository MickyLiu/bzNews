import { Form, Input, Button, Icon, Checkbox } from 'antd';

let React = require('react');
let classNames = require('classnames');
let ajaxEx = require('./../../_utils/ajaxConfiger');
let events = require('./../../_utils/eventSubscriber');
let Const = require('./../../_utils/const');
let VideoManageStore = require('../store/videoManageStore');

require('./panelTopBar.scss');

const FormItem = Form.Item;

let SearchForm = React.createClass({
  getInitialState() {
    return {
      value: '',
      checked: false
    };
  },
  handleInputChange(e) {
    this.setState({
      value: e.target.value,
    });
  },
  handleCheckBoxChange(e) {
    this.setState({
      checked: e.target.checked
    });
  },
  handleSubmit(e) {
      e.preventDefault();
      var _this= this;
      var searchValue = this.state.value.trim();
      if(!this.state.checked)
      {
          if(searchValue)
          {
              VideoManageStore.setFilterRule({'keyWord':searchValue});
              events.publish('videosDataChange',{'keyWord':searchValue});

          }
          else{
              VideoManageStore.setFilterRule(null);
              events.publish('videosDataChange');
          }
      }
      else
      {

          if(searchValue)
          {
             VideoManageStore.setFilterRule({'videoId':searchValue});
             events.publish('videosDataChange',{'videoId':searchValue});
          }
          else
          {
             VideoManageStore.setFilterRule(null);
             events.publish('videosDataChange');
          }
      }
  },
  render() {
    const { getFieldProps } = this.props.form;
    /*let disable = !(!!this.state.value.trim());*/
    let inputPlaceHolder = this.state.checked ? "请输入ID" : "请输入视频名";
    let status = "";
    let helpInfo = "";

    if(this.state.checked && isNaN(Number(this.state.value))) {
      status = "error";
      helpInfo = "请输入正确ID";
      //disable = true;
    }

    return (
      <Form className="search-panel" inline onSubmit={this.handleSubmit}>
       <FormItem hasFeedback validateStatus={status} help={helpInfo}>
         <Input placeholder={inputPlaceHolder}
           onChange={this.handleInputChange}
           value={this.state.value}
          />
       </FormItem>
       <FormItem>
         <label className="ant-checkbox-inline">
           <Checkbox
              onChange={this.handleCheckBoxChange}
             />ID查询
         </label>
       </FormItem>
       <Button type="primary" htmlType="submit">查询</Button>
     </Form>
   );
  }
});

SearchForm = Form.create()(SearchForm);

const PanelTopBar = React.createClass({
  render() {
    return (
      <header className="panel-top-bar">
        <h1 className="panel-title">{this.props.panelTitle}</h1>
        <SearchForm />
      </header>
    );
  }
});

module.exports = PanelTopBar;
