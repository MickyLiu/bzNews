let $ = require('jquery');

var VideoManageStore = {
   clearData:function(){
      this.dataSource=[];
      return this;
   },
	 pushData:function(data) {
      if(!this.dataSource) this.dataSource=[];
	 	  this.dataSource = this.dataSource.concat(data);
      return this;
	 },
   mapping:function(data){
        var result=[];
        data.forEach(function(o,i){
               $.isNumeric(o.id)?o.attributes.id = o.id.toString():'';
               $.isNumeric(o.id)?o.attributes.key =  o.id.toString():'';
               $.isNumeric(o.attributes.duration)?o.attributes.duration =  o.attributes.duration.toString():'';
               $.isNumeric(o.attributes.episode)?o.attributes.episode =  o.attributes.episode.toString():'';
               $.isNumeric(o.attributes.series_id)?o.attributes.series_id =  o.attributes.series_id.toString():'';
               o.attributes.published_at = o.attributes.published_at?new Date(o.attributes.published_at).toLocaleString():'';
               result.push(o.attributes);
        });
        return result;
   },
	 getDataSource:function() {
	 	  return !this.dataSource?[]:this.dataSource;
	 },
   getFilterRule:function(){
      return this.filter;
   },
   setFilterRule:function(rule,extend){
         if(extend)
         {
            this.filter = $.extend(this.filter,rule);
         }
         else
         {
             this.filter = rule;
         }
   },
   getItemByProperty:function(key,value){
        return this.dataSource.filter(function(item){
               return item[key]==value;
        });
   },
   getMappingKeyData:function(){
         return {
         	   "id":"视频ID",
         	   "title":"标题",
         	   "series_id":"系列ID",
         	   "thumbnail":"缩略图",
             "episode":"集数",
         	   "source_url":"视频地址",
             "youku_vid":"视频id",
         	   "duration":"时长",
         	   "published_at":"发布时间",
         	   "play_count":"播放量",
         	   "danmaku_count":"弹幕数",
         	   "comments_count":"评论数"
         }
   },
   getItemOnMappingKey:function(item){
   	  var result = {};
   	  var mapping = this.getMappingKeyData();
   	  for(var key in mapping)
   	  {
   	  	  result[mapping[key]] = item[key];
   	  }
        return result;
   },
   delItemByProperty:function(key,value){
        var _this=this;
        this.dataSource.forEach(function(o,i){
              if(o[key]==value)
              {
                  _this.dataSource.shift(i);
                  return;
              }
        });
   }
};

module.exports = VideoManageStore;
