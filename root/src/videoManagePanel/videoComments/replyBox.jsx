import { Icon,Spin,Form,Input,Button,Pagination,Select,Modal,Popover } from 'antd';
let _ = require('lodash');
let React = require('react');
let Link = require('./../../components/link/link');
let PopOut = require('./../../components/popOut/popOut');
let events = require('./../../_utils/eventSubscriber');
let ajaxEx = require('./../../_utils/ajaxConfiger');
let Const = require('./../../_utils/const');
let $ = require('jquery');


const FormItem = Form.Item;
const Option = Select.Option;
const ReplyBox = React.createClass({
  getInitialState() {
      return {
         loading:false,
         visible:false,
         data:{content:''}
      };
  },
  componentDidMount: function () {
      events.subscribe('CancelReplyBox',(function(){
              this.setState({visible:false});
      }).bind(this));
  },
  componentWillUnmount: function(){

  },
  handleSubmit(e) {
       e.preventDefault();
       var _this= this;

       var postData = {
        "comment": {
          "upvotable_type": 'Video',
          "content": _this.state.data.content,
          "upvotable_id":_this.props.videoId,
          "reply_user_id": _this.props.data.attributes.user_id,
          "user_id":_this.state.data.user_id
        },
        "parent_id":_this.props.rootCommentId
      };

      if(_this.props.data.attributes.parent_id === undefined) {
        postData.comment.reply_user_id = 0;
      }

       if(!postData.comment.user_id)
       {
            Modal.error({
                    title: '请选择一个水军！',
                    content: ''
            });
            return false;
       }
       else if(!postData.comment.content){
            Modal.error({
                    title: '请填写回复内容！',
                    content: ''
            });
            return false;
       }
       _this.setState({loading:true});
       ajaxEx.post({url:Const.API.projectPrefix +'/water_armies/comments',form:postData,enabledErrorMsg:true},function(d){
               _this.setState({data:{content:''},visible:false});
               events.publish('commentCommentDataChange_' + _this.props.rootCommentId.toString());
       }).complete(function(){
              _this.setState({loading:false});
       });
  },
  handleInputChange(e) {
      var data = this.state.data;
      var name = e.target.attributes['name'].value.match(/\[\S+\]/)[0].replace(/\[|\]/g,'');
      data[name]= e.target.value;
      this.setState({
          data: data
      });
  },
  handleSelectChange(uid) {
      var data = this.state.data;
      data.user_id = uid;
      this.setState({
          data: data
      });
  },
  toggleBox(e){
    e.stopPropagation();
    this.setState({visible:!this.state.visible});
  },
  handleCancel(){
      this.setState({visible:false});
  },
  render() {
    const content = (
          <Form horizontal form={this.props.form} method="post" style={{width:180,marginTop:20}}>
                          <FormItem>
                               <Select showSearch
                                style={{ width: 180 }}
                                placeholder="请选择马甲"
                                optionFilterProp="children"
                                notFoundContent="无法找到"
                                searchPlaceholder="输入关键词"
                                name={'comment[user_id]'}
                                onChange={this.handleSelectChange} value={this.state.data.user_id}>
                                    {_.map(this.props.armies,(o,index)=>
                                        <Option value={o.id} key={o.id}>{o.attributes.name}</Option>
                                    )}
                              </Select>
                          </FormItem>
                          <FormItem>
                             <Input type="textarea"  name={'comment[content]'} placeholder={'请填写回复内容'} value={this.state.data.content} onChange={this.handleInputChange}/>
                          </FormItem>
                          <FormItem>
                                <Button type="primary" onClick={this.handleSubmit} loading={this.state.loading} style={{marginRight:20}}>发送</Button>
                                <Button type="primary" onClick={this.handleCancel} >取消</Button>
                          </FormItem>
                      </Form>
      );
    return (
           <Popover placement="bottom" title={''} overlay={content} trigger="click" visible={this.state.visible}>
              <a href="javascript:void(0);" onClick={this.toggleBox} style={{marginLeft:5}}>
                 回复
              </a>
          </Popover>
    );
  }
});

module.exports = ReplyBox;
