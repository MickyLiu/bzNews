import { Collapse,Pagination,Spin,Popconfirm,Icon } from 'antd';
let _ = require('lodash');
let React = require('react');
let events = require('./../../_utils/eventSubscriber');
let ajaxEx = require('./../../_utils/ajaxConfiger');
let Const = require('./../../_utils/const');
let $ = require('jquery');
let VideoManageStore = require('../store/videoManageStore');
let Link = require('./../../components/link/link');
let ReplyBox = require('./replyBox');
let ReplyView = require('./replyView');
require('./videoComments.scss');
const Panel = Collapse.Panel;

let CommentItem = React.createClass({
  getInitialState() {
      return {};
  },
  componentDidMount: function () {
  },
  componentWillUnmount: function(){
  },
  deleteItem(){
       var _this =this;
       var commentId=this.props.data.id;
       ajaxEx.delete({url:Const.API.urlPrefix +'comments/'+ commentId,enabledErrorMsg:true},function(d){
               if(_this.props.level==1){
                     events.publish('videoCommentDataChange');
               }
               else if(_this.props.level==2)
               {
                     events.publish('commentCommentDataChange_' + _this.props.rootCommentId.toString());
               }
       });
  },
  render() {
    var _this = this;
    var replyStr = ()=>{
       if(this.props.data.attributes.reply_user_name && this.props.data.attributes.reply_user_id !== 0)
       {
         return <span><span className="reply-str">回复:</span><a>{this.props.data.attributes.reply_user_name}</a></span>
       }
       else
       {
          return null;
       }
    };
    var replyPics = ()=>{ return (this.props.level==1 && this.props.data.attributes.image_url)?this.props.data.attributes.image_url.split('，'):[]};
    return (
          <div className="comment_con">
              <div className="user_avatar">
                  <p><img src={this.props.data.attributes.user_avatar_url} /></p>
              </div>
              <div className="comment_content">
                 <p><a>{this.props.data.attributes.user_name}</a>{replyStr()}</p>
                 <p>{new Date(this.props.data.attributes.created_at).toLocaleString()}</p>
                 <p>{this.props.data.attributes.content}</p>
                 <p>{_.map(replyPics(),(o,index)=><img src={o}/>)}</p>
                 <p>
                   <Icon type="like" /><span className="vote-count">{this.props.data.attributes.vote_count}</span>
                   <Popconfirm title="确定要删除这条评论么？" onClick={(e)=>{ e.stopPropagation()}} onConfirm={this.deleteItem}>
                      <a href="javascript:void()">删除</a>
                   </Popconfirm>
                   <ReplyBox armies={this.props.armies} rootCommentId={this.props.rootCommentId} data={this.props.data} level={this.props.level} videoId={this.props.videoId}/>
                  </p>
              </div>
          </div>
    );
  }
});

let CommentItems = React.createClass({
  getInitialState() {
      return {
               data:[],
               loading:true,
               pageSize:8,
               total:24,
               currentPage:1
             };
  },
  componentDidMount: function () {
     var _this = this;

     if(_this.props.level==0)
     {
         events.subscribe('videoCommentDataChange',(function(){
              this.getCommentsData(this.state.currentPage);
         }).bind(this));
     }
     else if(_this.props.level==1)
     {
         events.subscribe('commentCommentDataChange_' + this.props.data.id.toString(),(function(){
              this.getCommentsData(this.state.currentPage);
         }).bind(this));
     }

     if(_this.props.level<2)
     {
        this.getCommentsData(1);
     }
  },
  componentWillUnmount: function(){
     if(this.props.level==0)
     {
         events.unsubscribe('videoCommentDataChange')
     }
     else if(this.props.level==1){
         events.unsubscribe('commentCommentDataChange_'+ this.props.data.id.toString());
     }
  },
  changeComment(key){
     console.log(this.state.data[key]);
     //this.getChildrens(key);
  },
  componentWillReceiveProps(nextProps){
        /*if(this.props.videoId==nextProps.videoId) return;
        this.props=nextProps;
        this.getCommentsData(1);
        console.log('props change');*/
  },
  showTotal(total) {
     return `共 ${this.state.total} 条`;
  },
  getCommentsData(page){
        var _this =this,url='',pid='',vid= this.props.videoId,per=this.state.pageSize;
        this.setState({
            loading: true
        });

        if(_this.props.level==1)
        {
           pid = this.props.data.id;
           url = Const.API.comments +'/'+ pid +'/children?comment_id='+ pid +'&page='+page + '&per=' + per;
        }
        else if(_this.props.level==0){
           url = Const.API.videos +'/'+ vid +'/comments?video_id='+ vid +'&page='+page + '&per=' + per;
        }
        else
        {
           url='';
        }
        if(_this.props.level<2)
        {

              ajaxEx.get({url:url},function(d){
                       _this.setState({
                          data:d.data,
                          total: d.meta.total_counts,
                          currentPage:page
                       });
              }).complete(function(){
                       _this.setState({loading:false});
              });
        }
        else
        {
             _this.setState({
                      data:[],
                      loading:false
              });
        }
  },
  pageChange:function(current){
        this.getCommentsData(current);
  },
  render() {
    var _this = this;

    var comment = (o)=> { return  <CommentItem data={o} level={this.props.level+1} armies={this.props.armies} videoId={this.props.videoId} rootCommentId={this.props.level==0?o.id:this.props.rootCommentId}/> };

    return (
        <div>
           {this.props.level<2?<div>
              {this.state.data.length>0?
                <div>
                    {this.props.level==0?<h3 className="comments-title">评论列表  <span>共{this.state.total}条</span></h3>:null}
                    <Spin spining={this.state.loading}>
                         <Collapse onChange={this.changeComment}>
                           {_.map(this.state.data,(o,index)=>
                              <Panel header={comment(o)} key={index}>
                                       <CommentItems level={this.props.level+1} data={o} rootCommentId={this.props.level==0?o.id:this.props.rootCommentId} videoId={this.props.videoId} armies={this.props.armies}/>
                              </Panel>
                           )}
                         </Collapse>
                    </Spin>
                  <Pagination size="small" className="comments-pagination" onChange={this.pageChange} current={this.state.currentPage} pageSize={this.state.pageSize} total={this.state.total} showTotal={this.props.level!=0?this.showTotal:null} />
                </div>
                :<h3 className="comments-title">评论列表  <span>共0条</span></h3>}
             </div>:null}
         </div>
    );
  }
});


let videoCommentsView = React.createClass({
  getInitialState() {
      if(VideoManageStore.getDataSource().length)
      {
          var itemData = VideoManageStore.getItemByProperty('id',this.props.routeParams.id);
          return {
              data:itemData[0],
              title:itemData[0].title,
              armies:[]
          };
      }
      else
      {
          return { data:{},armies:[]};
      }
  },
  fetchVideo(vid){
       var _this=this;
       var videoId = vid? vid:this.props.routeParams.id;
       if(!VideoManageStore.getDataSource().length)
       {
             ajaxEx.get({url:Const.API.videos+'/'+ videoId},function(d){
                    var  mappingData= VideoManageStore.mapping([d.data]);
                    _this.setState({data:mappingData[0],title:mappingData[0].title});
             });
       }
       else
       {
           var itemData = VideoManageStore.getItemByProperty('id',videoId);
           _this.setState({data:itemData[0],title:itemData[0].title});
       }
  },
  componentDidMount: function () {
       var _this = this;
       this.fetchVideo();
       ajaxEx.get({url:Const.API.water_armies},function(d){
                _this.setState({armies:d.data});
       });
  },
  componentWillReceiveProps(nextProps){
       /* if(this.props.params.id==nextProps.params.id) return;
        this.fetchVideo(nextProps.params.id);*/
  },
  componentWillUnmount: function(){
       events.unsubscribe('CancelReplyBox');
  },
  cancelPopOutBox(){
     events.publish('CancelReplyBox');
  },
  render() {
    var _this = this;

    return (
        <div onClick={this.cancelPopOutBox}>
           <div className="ant-breadcrumb custom">
              <span>
                <span className="ant-breadcrumb-link"><Link to='videos'>视频列表</Link></span>
                <span className="ant-breadcrumb-separator">/</span>
              </span>
              <span>
                <span className="ant-breadcrumb-link">{this.state.title}</span>
              </span>
           </div>
           <div className="video-con comments-list-con">
             <CommentItems level={0} videoId={this.props.params.id} armies={this.state.armies}/>

             <ReplyView videoId={this.props.routeParams.id} armies={this.state.armies} allowImg={false}/>
           </div>
        </div>
    );
  }
});

module.exports = videoCommentsView;
