import { Table, Icon, Spin,Form,Input,InputNumber,Button,Upload } from 'antd';

let React = require('react');
let events = require('./../../_utils/eventSubscriber');
let ajaxEx = require('./../../_utils/ajaxConfiger');
let VideoManageStore = require('../store/videoManageStore');
let Link = require('./../../components/link/link');
let Preloader = require('./../../components/imagePreloader/imagePreloader');
let Const = require('./../../_utils/const');
require('./videoDetail.scss');


const FormItem = Form.Item;
const videoDetailView = React.createClass({
  getInitialState() {
     if(VideoManageStore.getDataSource().length)
     {
        var itemData = VideoManageStore.getItemByProperty('id',this.props.routeParams.id);
        return this.getShowData(itemData[0]);
     }
     else
     {
        return  {
           data:{title:''},
           dataSource:[],
           columns:[]
        };
     }
  },
  getShowData(dataIn){
      var columns = [{
        title: '',
        dataIndex: 'd1'
      }, {
        title: '',
        dataIndex: 'd2',
        render(text,record) {
          if('缩略图'==record.d1)
          {
             return (<Preloader className="thumbnail" url={text} />);
          }
          else
          {
             return text;
          }
        }
      }];

      var dataSource =  [];
      var mapping = VideoManageStore.getItemOnMappingKey(dataIn);
      for(var i in mapping)
      {
           dataSource.push({
            d1: i,
            d2: mapping[i]
          });
      }
      return {
          data:dataIn,
          dataSource:dataSource,
          columns:columns
      };
  },
  fetchVideo(vid){
       var _this=this;
       var videoId = vid? vid:this.props.routeParams.id;
       if(!VideoManageStore.getDataSource().length)
       {
             ajaxEx.get({url:Const.API.videos+'/'+ videoId},function(d){
                   var  mappingData= VideoManageStore.mapping([d.data]);
                  _this.setState(_this.getShowData(mappingData[0]));
             });
       }
       else
       {
           var itemData = VideoManageStore.getItemByProperty('id',videoId);
           _this.setState(_this.getShowData(itemData[0]));
       }
  },
  componentDidMount() {
        this.fetchVideo();
  },
  componentWillReceiveProps(nextProps){
        if(this.props.params.id==nextProps.params.id) return;
        this.fetchVideo(nextProps.params.id);
  },
  componentWillUnmount: function(){
     //events.unsubscribe('progressChange');
  },
  render() {
    return (
    <div>
       <div className="ant-breadcrumb custom">
          <span>
            <span className="ant-breadcrumb-link"><Link to='videos'>视频列表</Link></span>
            <span className="ant-breadcrumb-separator">/</span>
          </span>
          <span>
            <span className="ant-breadcrumb-link">{this.state.data.title}</span>
          </span>
       </div>
       <div className="video-con">
          <Table columns={this.state.columns}  dataSource={this.state.dataSource} size="middle" pagination={false} showHeader={false}/>
       </div>
     </div>
    );
  }
});

module.exports = videoDetailView;
