import { Table, Icon, Spin,Form,Input,InputNumber,Button,DatePicker } from 'antd';

let React = require('react');
let events = require('./../../_utils/eventSubscriber');
let ajaxEx = require('./../../_utils/ajaxConfiger');
let Const = require('./../../_utils/const');
let Link = require('./../../components/link/link');
let $ = require('jquery') ;
let VideoManageStore = require('../store/videoManageStore');
require('./videoEdit.scss');


const FormItem = Form.Item;
let videoEditView = React.createClass({
  getInitialState() {
      if(VideoManageStore.getDataSource().length)
      {
          var itemData = VideoManageStore.getItemByProperty('id',this.props.routeParams.id);
          return {
              data:itemData[0],
              title:itemData[0].title
          };
      }
      else
      {
          return { data:{}};
      }
  },
  fetchVideo(vid){
       var _this=this;
       var videoId = vid? vid:this.props.routeParams.id;
       if(!VideoManageStore.getDataSource().length)
       {
             ajaxEx.get({url:Const.API.videos+'/'+ videoId},function(d){
                    var  mappingData= VideoManageStore.mapping([d.data]);
                    _this.setState({data:mappingData[0],title:mappingData[0].title});
             });
       }
       else
       {
           var itemData = VideoManageStore.getItemByProperty('id',videoId);
           _this.setState({data:itemData[0],title:itemData[0].title});
       }
  },
  componentDidMount: function () {
       this.fetchVideo();
  },
  componentWillReceiveProps(nextProps){
        if(this.props.params.id==nextProps.params.id) return;
        this.fetchVideo(nextProps.params.id);
  },
  componentWillUnmount: function(){
     //events.unsubscribe('progressChange');
  },
  handleSubmit(e) {
       e.preventDefault();
       var _this= this;
       this.props.form.validateFields((errors, values) => {
                if (!!errors) {
                  console.log('Errors in form!!!');
                  e.preventDefault();
                  return;
                }

          ajaxEx.submit({url:Const.API.videos+'/'+ _this.state.data.id,formEle:$('#videoForm')[0],enabledLoadingMsg:true,enabledErrorMsg:true},function(d){
                   VideoManageStore.clearData();
                   VideoManageStore.setFilterRule({mountUseFlag:true},true);
                   window.history.back();
          });
          return false;
      });
      // console.log('收到表单值：', this.props.form.getFieldsValue());
  },
  handleInputChange(e) {
      var data = this.state.data;
      data[e.target.attributes['id'].value]= e.target.value;
      this.setState({
          data: data
      });

      if(e.target.type=='file')
      {
          var fr = new FileReader();
          fr.readAsDataURL(e.target.files[0]);
          fr.onload = function(e) {
             $('#videoThumbnailImg').attr('src',e.target.result);
          };
      }
  },
  render() {
    var _this = this;
    const { getFieldProps,getFieldError } = this.props.form;

    const youkuVidProps = getFieldProps('youku_vid', {
        validate: [{
          rules: [
            { required: true, message: '视频id不能为空'}
          ],
          trigger: 'onBlur',
        }],
        initialValue: _this.state.data.source_url
    });

    const titleProps = getFieldProps('title', {
        validate: [{
          rules: [
            { required: true, message: '标题不能为空'}
          ],
          trigger: 'onBlur',
        }],
        initialValue: _this.state.data.title
    });
    const episodeProps = getFieldProps('episode', {
        validate: [{
          rules: [
            { required: true, message: '集数不能为空'}
          ],
          trigger: 'onBlur',
        }],
        initialValue: _this.state.data.episode
    });

    return (
    <div>
       <div className="ant-breadcrumb custom">
          <span>
            <span className="ant-breadcrumb-link"><Link to='videos'>视频列表</Link></span>
            <span className="ant-breadcrumb-separator">/</span>
          </span>
          <span>
            <span className="ant-breadcrumb-link">{this.state.title}</span>
          </span>
       </div>
       <div className="video-con">
          <Form horizontal id={'videoForm'} form={this.props.form} method="put" action={Const.API.videos+'/'+ this.state.data.id}>
              <FormItem
                label="标题：" labelCol={{ span: 3 }} wrapperCol={{ span: 14 }}>
                <Input placeholder="" {...titleProps} value={this.state.data.title} name={'video[title]'} onChange={this.handleInputChange} />
              </FormItem>
              <FormItem
                label="缩略图：" labelCol={{ span: 3 }} wrapperCol={{ span: 14 }}>
                <img id="videoThumbnailImg" src={this.state.data.thumbnail} />
                <div><a className="file-upload">
                   选择文件
                   <Input type="file" id="videoThumbnail" name={'video[thumbnail]'} onChange={this.handleInputChange}/>
                </a></div>
              </FormItem>
              <FormItem
                label="集数：" labelCol={{ span: 3 }} wrapperCol={{ span: 14 }}>
                <Input placeholder="" {...episodeProps}  name={'video[episode]'} value={this.state.data.episode} onChange={this.handleInputChange}/>
              </FormItem>
               <FormItem
                label="视频id：" labelCol={{ span: 4 }} wrapperCol={{ span: 14 }}>
                <Input placeholder="" name={'video[youku_vid]'} {...youkuVidProps} value={this.state.data.youku_vid} onChange={this.handleInputChange} />
              </FormItem>
              <FormItem wrapperCol={{ offset: 3 }}>
                <Button type="primary" onClick={this.handleSubmit}>保存</Button>
              </FormItem>
          </Form>
       </div>
     </div>
    );
  }
});

videoEditView = Form.create()(videoEditView);

module.exports = videoEditView;
