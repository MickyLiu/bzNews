import { Table, Icon, Spin, Popconfirm, notification } from 'antd';

let React = require('react');
let events = require('./../../_utils/eventSubscriber');
let ajaxEx = require('./../../_utils/ajaxConfiger');
let Const = require('./../../_utils/const');
let locationHelper = require('./../../_utils/locationHelper');
let VideoManageStore = require('../store/videoManageStore');
let PanelTopBar = require('../panelTopBar/panelTopBar');
let Link = require('./../../components/link/link');
let Preloader = require('./../../components/imagePreloader/imagePreloader');

const columns = [{
    title: 'ID',
    dataIndex: 'id',
    key: 'id'
  },{
    title: '标题',
    dataIndex: 'title',
    key: 'title',
  }, {
    title: '缩略图',
    key: 'thumbnail',
    dataIndex: 'thumbnail',
    render(text) {
      return (<Preloader className="thumbnail" url={text} />);
    }
  }, {
    title: '时长',
    dataIndex: 'duration',
    key: 'duration',
  }, {
    title: '发布时间',
    dataIndex: 'published_at',
    key: 'published_at',
  }, {
    title: '播放量',
    dataIndex: 'play_count',
    key: 'play_count',
  }, {
    title: '弹幕数',
    dataIndex: 'danmaku_count',
    key: 'danmaku_count',
  }, {
    title: '评论数',
    dataIndex: 'comments_count',
    key: 'comments_count',
    render(text, record) {
      var commentLink = "videos/" + record.id +"/comments";
      return (
        <Link to={commentLink}>{text}</Link>
      );
    }
  }, {
    title: '集数',
    dataIndex: 'episode',
    key: 'episode',
  }, {
    title: '操作',
    key: 'operation',
    render(text, record) {
      var editLink = "videos/" + record.id +"/edit";
      return (
        <span>
          <Link to={editLink}>编辑</Link>
          <span className="ant-divider"></span>
         <Popconfirm title="确定要删除这个视频么？" videoId={record.id} onConfirm={delVideoConfirm}>
            <a href="#">删除</a>
          </Popconfirm>
        </span>
      );
    }
  }];

var delVideoConfirm =  function(){
      var _this =this;
      ajaxEx.delete({url:Const.API.videos+'/'+ _this.props.videoId,enabledLoadingMsg:true,enabledErrorMsg:true},function(d){
              var filter= VideoManageStore.getFilterRule()

              if(filter)
              {
                    events.publish('videosDataChange',filter);
              }
              else
              {
                   events.publish('videosDataChange');
              }
      });
}

const TableView = React.createClass({
  getInitialState() {
     return{
        dataSource :[],
        pageSize:8,
        total:1,
        loading: true,
        currentPage:1
    };
  },
  componentDidMount: function () {
       events.subscribe('videosDataChange',(function(filter){
             this.getDataSource(this,filter);
       }).bind(this));

       var filter = VideoManageStore.getFilterRule();
       if(filter && filter.mountUseFlag)
       {
          this.getDataSource(this,filter);
          VideoManageStore.setFilterRule(null);
       }
       else
       {
          this.getDataSource(this);
       }
  },
  getDataSource(_this,filter){
      var url = '';
      var page = 1;
      if(filter)
      {
         page = filter.page?filter.page:1;
         if(filter.keyWord)
         {
             url = Const.API.videoSearch +'?key_word='+ filter.keyWord +"&page="+ page +"&per=" +this.state.pageSize;
         }
         else if(filter.videoId)
         {
             url = Const.API.videos+'/'+ filter.videoId;
         }
         else
         {
              url = Const.API.projectVideos+"?page="+ page +"&per="+this.state.pageSize;
         }
      }
      else
      {
          url = Const.API.projectVideos+"?page="+ page +"&per="+this.state.pageSize;
      }
      _this.setState({loading:true});
      ajaxEx.get({url:url}).done(function(d){
          if(filter && filter.videoId) d.data = [(d.meta={"total_pages":1},d.data)];
          VideoManageStore.clearData().pushData(VideoManageStore.mapping(d.data));
          _this.setState({
            dataSource : VideoManageStore.getDataSource(),
            total: d.meta.total_pages * _this.state.pageSize,
            currentPage:page
          });
      }).fail(function (jqXHR) {
        let code = jqXHR.status;
        if (code == 404) {
          // 没找到视频
          VideoManageStore.clearData();
          _this.setState({
            dataSource : VideoManageStore.getDataSource(),
            total: 0,
            currentPage: page
          });
        } else {
          // 出现错误
          notification['error']({
            description: '发往服务器的请求在处理时出现问题，请稍后重试。',
          });
        }
      }).always(function(){
               _this.setState({loading:false});
      });
  },
  pageChange:function(current){
        this.setState({
            loading: true
        });
        var filter= VideoManageStore.getFilterRule()
         
        if(filter)
        {
             filter.page = current;
             this.getDataSource(this,filter);
        }
        else
        {
            this.getDataSource(this,{"page":current});
        }
        VideoManageStore.setFilterRule({"page":current});
  },
  componentWillUnmount: function(){
       events.unsubscribe('videosDataChange');
  },
  render() {
    return (
      <div>
        <PanelTopBar panelTitle= "视频信息"/>
        <Table columns={columns} dataSource={this.state.dataSource} loading={this.state.loading} pagination={{ onChange:this.pageChange, current:this.state.currentPage, pageSize: this.state.pageSize, total: this.state.total, showQuickJumper: true }} />
      </div>
    );
  }
});

module.exports = TableView;
