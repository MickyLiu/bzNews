/*
* 结构：
*
* name: '随便取一个',
* entry: '入口脚本',
* plugins: {
*   template: '注入的html模板',
*   filename: '生成的html的文件名'
* },
* rewrite: '开发环境下URL映射（正式环境下写在网关内）'
*
* 注意：所有目录在'/root/src'开始
*/

var routes = [{
  name: 'index',
  entry: './index/start',
  plugins: {
    template: './../templates/index.html',
    filename: 'index.html'
  },
  rewrite: /\/index/
},
// {
//   name: 'login',
//   entry: './login/login',
//   plugins: {
//     template: './../templates/login.html',
//     filename: 'login.html'
//   },
//   rewrite: /\/login/,
//  },
 {
  name: 'userLogin',
  entry: './../extra/userLogin/start',
  plugins: {
    template: './../templates/login.html',
    filename: 'userlogin.html'
  },
  rewrite: /\/userlogin/,
}, {
  name: 'extra',
  entry: './../extra/start',
  plugins: {
    template: './../templates/extra.html',
    filename: 'extra.html'
  },
  rewrite: /\/extra/,
},{
  name: 'teams',
  entry: './teams/teamsIndex',
  plugins: {
    template: './../templates/teams.html',
    filename: 'teams.html'
  },
  rewrite: /\/teams/,
},{
  name: 'home',
  entry: './home/home',
  plugins: {
    template: './../templates/home.html',
    filename: 'home.html'
  },
  rewrite: /\/home/,
},{
  name: 'invite',
  entry: './invite/invite',
  plugins: {
    template: './../templates/invite.html',
    filename: 'invite.html'
  },
  rewrite: /\/invite/,
},{
  name: 'searchAuthor',
  entry: './searchAuthor/searchAuthor',
  plugins: {
    template: './../templates/searchAuthor.html',
    filename: 'searchAuthor.html'
  },
  rewrite: /\/searchAuthor/,
}]

module.exports = routes;
